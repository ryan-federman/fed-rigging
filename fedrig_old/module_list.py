import maya.cmds as cmds

import fedrig.layout.layout_list as lyt_list
import import_modules as mods


class ModuleList:
    '''
    Dictionary of layouts in the scene, used to initialize layouts in a new
    scene
    '''
    module_objs = dict()
    instances = []

    def __init__(self):
        self.module_objs = dict()
        self.initialize_objects()
        self.__class__.instances = []
        self.__class__.instances.append(self)

    def initialize_objects(self):
        '''
        Creates layout objects for a given layout object's dag
        Args:
            object_name (str): string name of layout object
        '''
        self.module_objs = dict()

        # Get all layouts in scene
        scene_layouts = lyt_list.LayoutList()

        # initialize all modules as individual objects
        modules = cmds.ls('*_MOD')

        # If modules exist in the scene load them
        if modules:
            new_mods = dict()
            mod_create = dict()
            for mod in modules:
                mod_elements = mod.split('_')
                side = mod_elements[0]
                name = ''
                for i, element in enumerate(mod_elements[1:-2]):
                    if i == 0:
                        name = element
                    else:
                        name += '_' + element
                new_mods[side + '_' + name] = mod

            # Check for previous instances of module list, build on previously created modules
            if self.instances:
                old_objs = []
                for obj in self.instances[0].module_objs:
                    old_objs += [obj]
                # If there are new modules to create add them to a list of modules to create
                for new, module in new_mods.items():
                    if new not in old_objs:
                        mod_create[new] = module

                # If a module no longer exists remove it from the modules dict
                for old in old_objs:
                    if old not in new_mods:
                        self.instances[0].module_objs.pop(old)

                self.module_objs = self.instances[0].module_objs
            else:
                mod_create = new_mods

            for mod_name, mod in mod_create.items():
                side, name = mod_name.split('_')
                # Create new module
                module_type = cmds.attributeQuery('moduleType', node=mod, listEnum=True)[0].split(':')[0]
                new_module = mods.create_module(module_type, side=side, name=name)

                # Initialize layout objects for in and out connections of module
                in_conn_module = cmds.attributeQuery('inConnModule', node=mod, listEnum=True)[0]
                out_conn_module = cmds.attributeQuery('outConnModule', node=mod, listEnum=True)[0]
                new_module.in_conn_module = scene_layouts.get_layout(in_conn_module)
                new_module.out_conn_module = scene_layouts.get_layout(out_conn_module)

                # Initialize normal attributes
                new_module.in_conns_build = cmds.attributeQuery('inConnModule', node=mod, listEnum=True)[0].split(':')
                new_module.out_conn_build = cmds.attributeQuery('outConnModule', node=mod, listEnum=True)[0]
                new_module.module_grp = mod
                new_module.module_nodes = self.remove_string(['None'], cmds.attributeQuery('moduleNodes', node=mod, listEnum=True)[0].split(':'))
                new_module.custom_nodes = self.remove_string(['None'],cmds.attributeQuery('customNodes', node=mod, listEnum=True)[0].split(':'))
                new_module.parts = self.remove_string(['None'], cmds.attributeQuery('parts', node=mod, listEnum=True)[0].split(':'))
                new_module.controls = self.remove_string(['None'], cmds.attributeQuery('controls', node=mod, listEnum=True)[0].split(':'))
                new_module.bones = self.remove_string(['None'], cmds.attributeQuery('bones', node=mod, listEnum=True)[0].split(':'))

                # Initialize layout objects and add to the class object
                new_module.layouts = dict()
                layouts = cmds.attributeQuery('layouts', node=mod, listEnum=True)[0].split(':')
                for each in layouts:
                    key, lyt_name = each.split('-')
                    lyt = scene_layouts.get_layout(lyt_name)
                    new_module.layouts[key] = lyt

                # Add objects to this class's dictionary of layouts
                self.module_objs['{}_{}'.format(side, name)] = new_module

            # After creating modules add attributes filled by module objects
            for mod in modules:
                mod_elements = mod.split('_')
                side = mod_elements[0]
                name = ''
                for i, element in enumerate(mod_elements[1:-2]):
                    if i == 0:
                        name = element
                    else:
                        name += '_' + element
                module = self.module_objs['{}_{}'.format(side, name)]

                # Add parent module to object
                parent_module = cmds.attributeQuery('parentModule', node=mod, listEnum=True)[0]
                if parent_module != 'None':
                    module.parent_module = self.module_objs[parent_module]

                # Add child modules to object
                child_modules = cmds.attributeQuery('childModules', node=mod, listEnum=True)[0].split(':')
                for mod in child_modules:
                    if mod != 'None':
                        module.child_modules.append(self.module_objs[mod])

    def get_module(self, module_name):
        '''
        Gives the layout object for it's string name
        Args:
            object_name (str): string name of layout object to return
        Return:
            Layout Object: instance of layout object class with all loaded attrs
        '''
        module = None
        for mod_name, mod in self.module_objs.items():
            if mod_name == module_name:
                module = self.module_objs[mod_name]

        return module

    def add_module(self, module):
        '''
        Pass in a layout object to add to this layout list
        Args:
            layout (BaseLayoutObject): layout object to add to list
        '''

        module_name = '{}_{}'.format(module.side, module.name)
        self.module_objs[module_name] = module

    @staticmethod
    def remove_string(strings, list):
        '''
        If string is in list remove from list, otherwise return the list
        Args:
            strings: strings to check whether in list
            list: list to remove strings from
        Return:
             list: list without the given string
        '''

        for string in strings:
            if string in list:
                list.remove(string)

        return list
