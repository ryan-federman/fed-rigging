import maya.cmds as cmds

import fedrig.layout.layout_objects as lyt
import fedrig.layout.layout_list as lyt_list
import fedrig.base_module as bm
import fedrig.module_attributes as mod_attr
import create.nodes as node
import create.nurbs as nrb
import create.dag as dag

import custom_utils.control_shapes as cs
import custom_utils.maths as maths
import custom_utils.shape as shape
import custom_utils.constraint as con
import custom_utils.attribute as attr

reload(bm)


class NeckModule(bm.BaseModule):
    module_type = 'Neck'

    def __init__(self, name='neck', side='C', initialize=False):
        # Re initialize scene layouts
        self.scene_layouts = lyt_list.LayoutList()

        super(NeckModule, self).__init__(name=name, side=side, initialize=initialize)

    def module_layout_objects(self):
        # Create layout objects and base positions
        self.layouts['base'] = base_lyt = lyt.BaseLayoutObject(side=self.side, name=self.name + '_base')
        self.layouts['mid'] = mid_lyt = lyt.BaseLayoutObject(side=self.side, name=self.name + '_mid')
        self.layouts['head'] = head_lyt = lyt.BaseLayoutObject(side=self.side, name=self.name + '_head')

        # Declare in and out connectors for module
        self.in_conn_module = base_lyt
        self.out_conn_module = head_lyt

        base_lyt.set_main_child(mid_lyt)
        mid_lyt.set_main_child(head_lyt)

        # Set default attributes for layout objects
        base_lyt.dag.connect(aimType_in=2,
                             upType_in=0)
        mid_lyt.dag.connect(aimType_in=2,
                            up_in=2,
                            upType_in=0)
        head_lyt.dag.connect(aimType_in=1,
                             aim_in=1,
                             up_in=2)

        base_lyt.set_position((0, 56.369, -0.014))
        mid_lyt.set_position((0, 58.036, 0.537))
        head_lyt.set_position((0, 59.425, 1.144))

        # Apply transforms to main dag
        for key, obj in self.layouts.items():
            obj.change_space('World')
        for key, obj in self.layouts.items():
            obj.apply_transforms()
        for key, obj in self.layouts.items():
            obj.change_space('Parent')

        head_lyt.dag.connect(up_in=2)

    def module_attributes(self):
        '''
        Attributes specific to a module that influence a build

        Write dictionary of functions for how to return a value from
        '''
        self.attributes['numBones'] = mod_attr.Attribute('numBones', 'Num Bones', self)
        self.attributes['numBones'].slider(min=4, max=30, default=4)

    def module_build(self):
        # get layout objects
        base_lyt = self.layouts['base']
        mid_lyt = self.layouts['mid']
        head_lyt = self.layouts['head']

        # create controls
        base_ctrl, base_ctrl_zero, base_ofs_ctrl, base_ofs_ctrl_zero = self.create_control('{}_{}_01'.format(self.side, self.name), 'circle')
        mid_ctrl, mid_ctrl_zero, mid_ofs_ctrl, mid_ofs_ctrl_zero = self.create_control('{}_{}_02'.format(self.side, self.name), 'circle')
        head_ctrl, head_ctrl_zero, head_ofs_ctrl, head_ofs_ctrl_zero = self.create_control('{}_{}_03'.format(self.side, self.name), 'circle')
        self.controls += [base_ctrl_zero]

        cmds.parent(mid_ctrl_zero, base_ofs_ctrl)
        cmds.parent(head_ctrl_zero, mid_ofs_ctrl)

        # position controls
        base_mtx = base_lyt.get_output_matrix()
        mid_mtx = mid_lyt.get_output_matrix()
        head_mtx = head_lyt.get_output_matrix()

        cmds.xform(base_ctrl_zero, matrix=base_mtx, ws=True)
        cmds.xform(mid_ctrl_zero, matrix=mid_mtx, ws=True)
        cmds.xform(head_ctrl_zero, matrix=head_mtx, ws=True)

        # create curves and bones
        crv = nrb.curve_from_dags('{}_{}_CRV'.format(self.side, self.name), [base_ofs_ctrl, mid_ofs_ctrl, head_ofs_ctrl])
        up_crv = cmds.duplicate(crv, name='{}_{}_up_CRV'.format(self.side, self.name))[0]
        self.parts += [crv, up_crv]

        # move cvs of up curve
        cvs = cmds.ls(up_crv + '.cv[*]', fl=True)
        base_x, base_y, base_z, base_pos = maths.matrix_to_vectors(base_mtx)
        mid_x, mid_y, mid_z, mid_pos = maths.matrix_to_vectors(mid_mtx)
        head_x, head_y, head_z, head_pos = maths.matrix_to_vectors(head_mtx)

        cv_vecs = []
        cv0_pos = base_y + base_pos
        cv1_pos = mid_y + mid_pos
        cv2_pos = head_y + head_pos
        cv_vecs += [cv0_pos, cv1_pos, cv2_pos]

        crv_jnts = []
        ctrls = [base_ofs_ctrl, mid_ofs_ctrl, head_ofs_ctrl]

        for i, cv in enumerate(cvs):
            if i < 3:
                cmds.select(clear=True)
                jnt = node.Node(cv + 'JNT', 'joint', suffix=False, list_add=self.module_nodes)
                crv_jnts += [jnt.node]
                self.parts += [jnt.node]

                cmds.xform(cv, t=cv_vecs[i], ws=True)
                cmds.xform(jnt.node, t=cv_vecs[i], ws=True)

                # constrain joint to a given control
                mcon = con.matrix_constraint(ctrls[i], jnt.node, maintain_offset=True)
                cmds.setAttr(mcon + '.posVector', 0, 1, 0)
                self.custom_nodes += [mcon]
            else:
                # constrain joint to a given control
                cmds.xform(cv, t=cv_vecs[-1], ws=True)

        skc = cmds.skinCluster(crv_jnts, up_crv)[0]
        self.module_nodes += [skc]

        # create bones for main spine
        bone_zeros = dag.dags_on_curve(up_crv, crv, '{}_{}'.format(self.side, self.name), 'bone', num_dags=self.attributes['numBones'].value())
        self.bones += bone_zeros

        # create base bones
        base_bone_zero, base_bone = self.create_bone('{}_{}_base'.format(self.side, self.name))
        head_bone_zero, head_bone = self.create_bone('{}_{}_upper'.format(self.side, self.name))
        for bone in [base_bone, head_bone]:
            cmds.setAttr(bone + '.radius', 1.2)

        base_mcon = con.matrix_constraint(base_ofs_ctrl, base_bone_zero, maintain_offset=False)
        head_mcon = con.matrix_constraint(head_ctrl, head_bone_zero, maintain_offset=False)
        self.custom_nodes += [base_mcon, head_mcon]

        # Declare in and out connectors for build
        self.in_conns_build = [base_ctrl_zero]
        self.out_conn_build = head_ctrl

    # FUNCTIONS SPECIFIC TO MODULE #
    def create_control(self, name, ctrl_type, ofs_type='dotted_circle', create_ofs=True, **kwargs):
        '''
        Creates a control and a sub control
        Args:
            name(str): name of control
            ctrl_type(str): type of control being created
            ofs_type(str): type of control for sub control
            kwargs: keyword arguments for specific shape types
        Return:
            [list]: list in order, control, offset control, control zero, offset control zero
        '''
        ctrls = []

        ctrl = cs.create_nurbscurve(name + '_CTRL', ctrl_type, **kwargs)
        ctrl_zero = node.Node(ctrl + '_ZERO', 'transform', list_add=self.module_nodes, suffix=False).node
        ctrls += [ctrl, ctrl_zero]
        self.module_nodes += [ctrl]

        shape.modify_shape(ctrl, rotate=(90, 0, 90))
        attr.lock_attributes(ctrl, ['v', 'sx', 'sy', 'sz'])

        cmds.parent(ctrl, ctrl_zero)

        if create_ofs:
            ofs_ctrl = cs.create_nurbscurve(name + '_OFS_CTRL', ofs_type)
            ofs_ctrl_zero = node.Node(ofs_ctrl + '_ZERO', 'transform', list_add=self.module_nodes, suffix=False).node
            ofs_ctrl_shapes = cmds.listRelatives(ofs_ctrl, s=True)
            self.module_nodes += [ofs_ctrl]
            ctrls += [ofs_ctrl, ofs_ctrl_zero]

            shape.modify_shape(ofs_ctrl, rotate=(90, 0, 90), scale=(.8, .8, .8))
            shape.change_color(ofs_ctrl, 'light brown', change_shapes=True)

            cmds.parent(ofs_ctrl, ofs_ctrl_zero)
            cmds.parent(ofs_ctrl_zero, ctrl)

            attr.add_title(ctrl, 'VISIBILITY')
            attr.add_bool(ctrl, 'offset')
            for each in ofs_ctrl_shapes:
                cmds.connectAttr(ctrl + '.offset', each + '.v')
            attr.lock_attributes(ofs_ctrl, ['v', 'sx', 'sy', 'sz'])

        return ctrls

    def create_bone(self, name):
        '''
        Creates a bone and a zero transform
        Args:
            name (str): name of bone
        Return:
            list[(str)]: zero transform, bone
        '''
        cmds.select(clear=True)
        bone = node.Node(name, 'joint', list_add=self.module_nodes).node
        zero = node.Node(bone + '_ZERO', 'transform', suffix=False).node
        cmds.parent(bone, zero)

        self.bones += [zero]

        return [zero, bone]
