import maya.cmds as cmds

import fedrig.layout.layout_objects as lyt
import fedrig.layout.layout_list as lyt_list
import fedrig.base_module as bm
import fedrig.module_attributes as mod_attr
import create.nodes as node

import custom_utils.control_shapes as cs
import custom_utils.maths as maths
import custom_utils.shape as shape
import custom_utils.constraint as con
import custom_utils.attribute as attr

reload(bm)
reload(mod_attr)
reload(maths)


class ArmModule(bm.BaseModule):
    module_type = 'Arm'

    def __init__(self, name='arm', side='L', initialize=False):
        # Re initialize scene layouts
        self.scene_layouts = lyt_list.LayoutList()

        super(ArmModule, self).__init__(name=name, side=side, initialize=initialize)

    def module_layout_objects(self):
        # Create layout objects and base positions
        self.layouts['clav'] = clav_lyt = lyt.BaseLayoutObject(side=self.side, name=self.name + '_clav')
        self.layouts['shoulder'] = shoulder_lyt = lyt.BaseLayoutObject(side=self.side, name=self.name + '_shoulder')
        self.layouts['elbow'] = elbow_lyt = lyt.BaseLayoutObject(side=self.side, name=self.name + '_elbow')
        self.layouts['hand'] = hand_lyt = lyt.BaseLayoutObject(side=self.side, name=self.name + '_hand')

        # Declare in and out connectors for module
        self.in_conn_module = clav_lyt
        self.out_conn_module = hand_lyt

        clav_lyt.set_main_child(shoulder_lyt)
        shoulder_lyt.set_main_child(elbow_lyt)
        elbow_lyt.set_main_child(hand_lyt)

        # Set default attributes for layout objects
        clav_lyt.dag.connect(up_in=2,
                             aimType_in=2,
                             upType_in=0)
        shoulder_lyt.dag.connect(up_in=3,
                                 aimType_in=2,
                                 upType_in=2,
                                 planeType_in=1)
        elbow_lyt.dag.connect(up_in=3,
                              aimType_in=2,
                              upType_in=2)
        hand_lyt.dag.connect(aim_in=1,
                             up_in=3,
                             aimType_in=1,
                             upType_in=2)

        clav_lyt.set_position((2.147, 54.062, 1.046))
        shoulder_lyt.set_position((7.261, 52.445, -1.221))
        elbow_lyt.set_position((13.215, 45.922, 0.663))
        hand_lyt.set_position((15.513, 40.844, 8.835))

        # If the side is right apply the right transformations and attributes
        if self.side == 'R':
            clav_lyt.set_position((-2.147, 54.062, 1.046))
            shoulder_lyt.set_position((-7.261, 52.445, -1.221))
            elbow_lyt.set_position((-13.215, 45.922, 0.663))
            hand_lyt.set_position((-15.513, 40.844, 8.835))

    def module_attributes(self):
        '''
        Attributes specific to a module that influence a build

        Write dictionary of functions for how to return a value from
        '''
        self.attributes['twistJoints'] = mod_attr.Attribute('twistJoints', 'Twist Joints', self)
        self.attributes['twistJoints'].slider(max=6, default=2)
        self.attributes['poleDistance'] = mod_attr.Attribute('poleDistance', 'Pole Vector Distance', self)
        self.attributes['poleDistance'].slider(max=100, default=10)

    def module_build(self):
        # get layout objects
        clav = self.layouts['clav']
        shoulder = self.layouts['shoulder']
        elbow = self.layouts['elbow']
        hand = self.layouts['hand']

        # create ik joints for chain
        shoulder_ik_jnt = node.Node('{}_{}_shoulder_IK_JNT'.format(self.side, self.name), 'joint', list_add=self.module_nodes, suffix=False)
        elbow_ik_jnt = node.Node('{}_{}_elbow_IK_JNT'.format(self.side, self.name), 'joint', list_add=self.module_nodes, suffix=False)
        hand_ik_jnt = node.Node('{}_{}_hand_IK_JNT'.format(self.side, self.name), 'joint', list_add=self.module_nodes, suffix=False)
        self.parts += [shoulder_ik_jnt.node]
        cmds.select(clear=True)

        # create fk joints for chain
        shoulder_fk_jnt = node.Node('{}_{}_shoulder_FK_JNT'.format(self.side, self.name), 'joint', list_add=self.module_nodes, suffix=False)
        elbow_fk_jnt = node.Node('{}_{}_elbow_FK_JNT'.format(self.side, self.name), 'joint', list_add=self.module_nodes, suffix=False)
        hand_fk_jnt = node.Node('{}_{}_hand_FK_JNT'.format(self.side, self.name), 'joint', list_add=self.module_nodes, suffix=False)
        self.parts += [shoulder_fk_jnt.node]
        cmds.select(clear=True)

        # create main bones
        clav_bone = node.Node('{}_{}_clav'.format(self.side, self.name), 'joint', list_add=self.module_nodes)
        shoulder_bone = node.Node('{}_{}_shoulder'.format(self.side, self.name), 'joint', list_add=self.module_nodes, radius=1.2)
        elbow_bone = node.Node('{}_{}_elbow'.format(self.side, self.name), 'joint', list_add=self.module_nodes, radius=1.2)
        hand_bone = node.Node('{}_{}_hand'.format(self.side, self.name), 'joint', list_add=self.module_nodes, radius=1.2)
        shoulder_top_twist_bone = node.Node('{}_{}_shoulder_twist_01'.format(self.side, self.name), 'joint', list_add=self.module_nodes)
        elbow_top_twist_bone = node.Node('{}_{}_elbow_twist_01'.format(self.side, self.name), 'joint', list_add=self.module_nodes)
        hand_twist_bone = node.Node('{}_{}_hand_twist'.format(self.side, self.name), 'joint', list_add=self.module_nodes)

        shoulder_top_twist_bone_ofs = node.Node(shoulder_top_twist_bone.node + '_OFS', 'transform', suffix=False, list_add=self.module_nodes)
        elbow_top_twist_bone_ofs = node.Node(elbow_top_twist_bone.node + '_OFS', 'transform', suffix=False, list_add=self.module_nodes)
        hand_twist_bone_ofs = node.Node(hand_twist_bone.node + '_OFS', 'transform', suffix=False, list_add=self.module_nodes)
        clav_bone_ofs = node.Node(clav_bone.node + '_OFS', 'transform', suffix=False, list_add=self.module_nodes)
        shoulder_bone_ofs = node.Node(shoulder_bone.node + '_OFS', 'transform', suffix=False, list_add=self.module_nodes)
        elbow_bone_ofs = node.Node(elbow_bone.node + '_OFS', 'transform', suffix=False, list_add=self.module_nodes)
        hand_bone_ofs = node.Node(hand_bone.node + '_OFS', 'transform', suffix=False, list_add=self.module_nodes)

        shoulder_top_twist_bone_zero = node.Node(shoulder_top_twist_bone.node + '_ZERO', 'transform', suffix=False, list_add=self.module_nodes)
        elbow_top_twist_bone_zero = node.Node(elbow_top_twist_bone.node + '_ZERO', 'transform', suffix=False, list_add=self.module_nodes)
        hand_twist_bone_zero = node.Node(hand_twist_bone.node + '_ZERO', 'transform', suffix=False, list_add=self.module_nodes)
        clav_bone_zero = node.Node(clav_bone.node + '_ZERO', 'transform', suffix=False, list_add=self.module_nodes)
        shoulder_bone_zero = node.Node(shoulder_bone.node + '_ZERO', 'transform', suffix=False, list_add=self.module_nodes)
        elbow_bone_zero = node.Node(elbow_bone.node + '_ZERO', 'transform', suffix=False, list_add=self.module_nodes)
        hand_bone_zero = node.Node(hand_bone.node + '_ZERO', 'transform', suffix=False, list_add=self.module_nodes)
        self.bones += [clav_bone_zero.node]

        cmds.select(clear=True)

        # parent bones correctly under offsets and zeros
        cmds.parent(clav_bone.node, clav_bone_ofs.node)
        cmds.parent(clav_bone_ofs.node, clav_bone_zero.node)

        cmds.parent(shoulder_bone.node, shoulder_bone_ofs.node)
        cmds.parent(shoulder_bone_ofs.node, shoulder_bone_zero.node)
        cmds.parent(shoulder_bone_zero.node, clav_bone.node)

        cmds.parent(shoulder_top_twist_bone.node, shoulder_top_twist_bone_ofs.node)
        cmds.parent(shoulder_top_twist_bone_ofs.node, shoulder_top_twist_bone_zero.node)
        cmds.parent(shoulder_top_twist_bone_zero.node, clav_bone.node)

        cmds.parent(elbow_top_twist_bone.node, elbow_top_twist_bone_ofs.node)
        cmds.parent(elbow_top_twist_bone_ofs.node, elbow_top_twist_bone_zero.node)
        cmds.parent(elbow_top_twist_bone_zero.node, shoulder_top_twist_bone.node)

        cmds.parent(elbow_bone.node, elbow_bone_ofs.node)
        cmds.parent(elbow_bone_ofs.node, elbow_bone_zero.node)
        cmds.parent(elbow_bone_zero.node, shoulder_top_twist_bone.node)

        cmds.parent(hand_twist_bone.node, hand_twist_bone_ofs.node)
        cmds.parent(hand_twist_bone_ofs.node, hand_twist_bone_zero.node)
        cmds.parent(hand_twist_bone_zero.node, elbow_bone.node)

        cmds.parent(hand_bone.node, hand_bone_ofs.node)
        cmds.parent(hand_bone_ofs.node, hand_bone_zero.node)
        cmds.parent(hand_bone_zero.node, elbow_bone.node)

        # put bones into correct positions
        clav_mtx = clav.get_output_matrix()
        shoulder_mtx = shoulder.get_output_matrix()
        elbow_mtx = elbow.get_output_matrix()
        hand_mtx = hand.get_output_matrix()

        clav_norm_mtx = clav.get_output_matrix(normalize=True)
        shoulder_norm_mtx = shoulder.get_output_matrix(normalize=True)
        elbow_norm_mtx = elbow.get_output_matrix(normalize=True)
        hand_norm_mtx = hand.get_output_matrix(normalize=True)

        cmds.xform(shoulder_ik_jnt.node, matrix=shoulder_norm_mtx, ws=True)
        cmds.xform(elbow_ik_jnt.node, matrix=elbow_norm_mtx, ws=True)
        cmds.xform(hand_ik_jnt.node, matrix=hand_norm_mtx, ws=True)
        cmds.xform(shoulder_fk_jnt.node, matrix=shoulder_norm_mtx, ws=True)
        cmds.xform(elbow_fk_jnt.node, matrix=elbow_norm_mtx, ws=True)
        cmds.xform(hand_fk_jnt.node, matrix=hand_norm_mtx, ws=True)
        cmds.xform(hand_ik_jnt.node, matrix=hand_norm_mtx, ws=True)

        # Constrain bones to joints
        shoulder_ik_con = con.matrix_constraint(shoulder_ik_jnt.node, shoulder_bone.node, maintain_offset=False, connect='')
        elbow_ik_con = con.matrix_constraint(elbow_ik_jnt.node, elbow_bone_zero.node, maintain_offset=False, connect='')
        shoulder_fk_con = con.matrix_constraint(shoulder_fk_jnt.node, shoulder_bone.node, maintain_offset=False, connect='')
        elbow_fk_con = con.matrix_constraint(elbow_fk_jnt.node, elbow_bone_zero.node, maintain_offset=False, connect='')
        hand_ik_con = con.matrix_constraint(hand_ik_jnt.node, hand_bone_ofs.node, maintain_offset=False, connect='')
        hand_fk_con = con.matrix_constraint(hand_fk_jnt.node, hand_bone_ofs.node, maintain_offset=False, connect='')
        shoulder_pb = node.Node('{}_{}_shoulder'.format(self.side, self.name), 'pairBlend', list_add=self.module_nodes)
        elbow_pb = node.Node('{}_{}_elbow'.format(self.side, self.name), 'pairBlend', list_add=self.module_nodes)
        wrist_pb = node.Node('{}_{}_wrist'.format(self.side, self.name), 'pairBlend', list_add=self.module_nodes)

        shoulder_pb.connect(inRotate1=shoulder_fk_con + '.outRotate',
                            inRotate2=shoulder_ik_con + '.outRotate',
                            inTranslate1=shoulder_fk_con + '.outTranslate',
                            inTranslate2=shoulder_ik_con + '.outTranslate',
                            outRotate=shoulder_bone.node + '.rotate',
                            outTranslate=shoulder_bone.node + '.translate')

        elbow_pb.connect(inRotate1=elbow_fk_con + '.outRotate',
                         inRotate2=elbow_ik_con + '.outRotate',
                         inTranslate1=elbow_fk_con + '.outTranslate',
                         inTranslate2=elbow_ik_con + '.outTranslate',
                         outRotate=elbow_bone_zero.node + '.rotate',
                         outTranslate=elbow_bone_zero.node + '.translate')
        elbow_pb.connect(outTranslate=elbow_top_twist_bone_zero.node + '.translate')

        wrist_pb.connect(inRotate1=hand_fk_con + '.outRotate',
                         inRotate2=hand_ik_con + '.outRotate',
                         inTranslate1=hand_fk_con + '.outTranslate',
                         inTranslate2=hand_ik_con + '.outTranslate',
                         outRotate=hand_bone_ofs.node + '.rotate',
                         outTranslate=hand_bone_ofs.node + '.translate')

        cmds.xform(hand_bone_zero.node, matrix=hand_norm_mtx, ws=True)
        self.custom_nodes += [shoulder_ik_con, elbow_ik_con, hand_ik_con, shoulder_fk_con, elbow_fk_con, hand_fk_con]

        # Create ikhandle
        elbow_ik_jnt.connect(preferredAngleY=-90)
        ikh, effector = cmds.ikHandle(name='{}_{}_IKH'.format(self.side, self.name),
                                      startJoint=shoulder_ik_jnt.node,
                                      endEffector=hand_ik_jnt.node)
        self.module_nodes += [ikh, effector]
        self.parts += [ikh]

        # Get Pole Vector position
        shoulder_pos = maths.matrix_to_vectors(shoulder_mtx)[3]
        elbow_pos = maths.matrix_to_vectors(elbow_mtx)[3]
        wrist_pos = maths.matrix_to_vectors(hand_mtx)[3]

        pv = maths.get_pole_vector(shoulder_pos, elbow_pos, wrist_pos, length=self.attributes['poleDistance'].value())

        # Create controls
        clav_ctrl = cs.create_nurbscurve('{}_{}_clav_CTRL'.format(self.side, self.name), 'crescent')
        shoulder_fk_ctrl = cs.create_nurbscurve('{}_{}_shoulder_FK_CTRL'.format(self.side, self.name), 'circle')
        elbow_fk_ctrl = cs.create_nurbscurve('{}_{}_elbow_FK_CTRL'.format(self.side, self.name), 'circle')
        hand_fk_ctrl = cs.create_nurbscurve('{}_{}_hand_FK_CTRL'.format(self.side, self.name), 'circle')
        hand_ik_ctrl = cs.create_nurbscurve('{}_{}_hand_IK_CTRL'.format(self.side, self.name), 'long_hexagon')
        pv_ctrl = cs.create_nurbscurve('{}_{}_pv_CTRL'.format(self.side, self.name), 'diamond')
        self.module_nodes += [clav_ctrl, shoulder_fk_ctrl, elbow_fk_ctrl, hand_fk_ctrl, hand_ik_ctrl, pv_ctrl]

        clav_ctrl_zero = node.Node(clav_ctrl + '_ZERO', 'transform', suffix=False, list_add=self.module_nodes)
        shoulder_fk_ctrl_zero = node.Node(shoulder_fk_ctrl + '_ZERO', 'transform', suffix=False, list_add=self.module_nodes)
        hand_fk_ctrl_zero = node.Node(hand_fk_ctrl + '_ZERO', 'transform', suffix=False, list_add=self.module_nodes)
        elbow_fk_ctrl_zero = node.Node(elbow_fk_ctrl + '_ZERO', 'transform', suffix=False, list_add=self.module_nodes)
        hand_ik_ctrl_zero = node.Node(hand_ik_ctrl + '_ZERO', 'transform', suffix=False, list_add=self.module_nodes)
        pv_ctrl_zero = node.Node(pv_ctrl + '_ZERO', 'transform', suffix=False, list_add=self.module_nodes)
        self.controls += [clav_ctrl_zero.node, hand_ik_ctrl_zero.node, pv_ctrl_zero.node]

        # Modify shapes
        shape.modify_shape(hand_ik_ctrl, translate=(0, 1, 0), rotate=(0, 90, 0))
        shape.modify_shape(clav_ctrl, rotate=(0, 90, 0))
        shape.modify_shape(shoulder_fk_ctrl, rotate=(0, 90, 0))
        shape.modify_shape(elbow_fk_ctrl, rotate=(0, 90, 0))
        shape.modify_shape(hand_fk_ctrl, rotate=(0, 90, 0))

        # Parent under zeroes
        cmds.parent(clav_ctrl, clav_ctrl_zero.node)
        cmds.parent(shoulder_fk_ctrl, shoulder_fk_ctrl_zero.node)
        cmds.parent(elbow_fk_ctrl, elbow_fk_ctrl_zero.node)
        cmds.parent(hand_fk_ctrl, hand_fk_ctrl_zero.node)
        cmds.parent(hand_ik_ctrl, hand_ik_ctrl_zero.node)
        cmds.parent(pv_ctrl, pv_ctrl_zero.node)

        # FK hierarchy
        cmds.parent(shoulder_fk_ctrl_zero.node, clav_ctrl)
        cmds.parent(elbow_fk_ctrl_zero.node, shoulder_fk_ctrl)
        cmds.parent(hand_fk_ctrl_zero.node, elbow_fk_ctrl)

        # Position controls
        cmds.xform(clav_ctrl_zero.node, matrix=clav_mtx)
        cmds.xform(shoulder_fk_ctrl_zero.node, matrix=shoulder_mtx, ws=True)
        cmds.xform(elbow_fk_ctrl_zero.node, matrix=elbow_mtx, ws=True)
        cmds.xform(hand_fk_ctrl_zero.node, matrix=hand_mtx, ws=True)
        cmds.xform(hand_ik_ctrl_zero.node, matrix=hand_mtx)
        cmds.xform(pv_ctrl_zero.node, t=(pv[0], pv[1], pv[2]), ws=True)

        # Add necessary attributes onto controls
        attr.add_float(hand_fk_ctrl, 'FKIK')
        attr.add_proxy(hand_ik_ctrl, hand_fk_ctrl + '.FKIK', 'FKIK')
        attr.add_title(hand_ik_ctrl, 'SPACE')
        attr.add_title(pv_ctrl, 'SPACE')
        attr.add_enum(hand_ik_ctrl, 'spaces', ['world', 'clavicle'])
        attr.add_enum(pv_ctrl, 'spaces', ['world', 'clavicle'])

        # Add spaces to hand ik and pv controls
        hand_pb = node.Node(hand_ik_ctrl + '_SPC', 'pairBlend')
        pv_pb = node.Node(pv_ctrl + '_SPC', 'pairBlend')
        hand_clav_space_con = con.matrix_constraint(clav_ctrl, hand_ik_ctrl_zero.node, connect='')
        pv_clav_space_con = con.matrix_constraint(clav_ctrl, pv_ctrl_zero.node, connect='')
        hand_world_space_con = con.matrix_constraint('C_global_OFS_CTRL', hand_ik_ctrl_zero.node, connect='')
        pv_world_space_con = con.matrix_constraint('C_global_OFS_CTRL', pv_ctrl_zero.node, connect='')
        self.module_nodes += [hand_clav_space_con, pv_clav_space_con, hand_world_space_con, pv_world_space_con]
        self.custom_nodes += [hand_clav_space_con, pv_clav_space_con, hand_world_space_con, pv_world_space_con]

        hand_pb.connect(inTranslate1=hand_world_space_con + '.outTranslate',
                        inRotate1=hand_world_space_con + '.outRotate',
                        inTranslate2=hand_clav_space_con + '.outTranslate',
                        inRotate2=hand_clav_space_con + '.outRotate',
                        weight=hand_ik_ctrl + '.spaces',
                        outTranslate=hand_ik_ctrl_zero.node + '.translate',
                        outRotate=hand_ik_ctrl_zero.node + '.rotate')
        pv_pb.connect(inTranslate1=pv_world_space_con + '.outTranslate',
                      inRotate1=pv_world_space_con + '.outRotate',
                      inTranslate2=pv_clav_space_con + '.outTranslate',
                      inRotate2=pv_clav_space_con + '.outRotate',
                      weight=pv_ctrl + '.spaces',
                      outTranslate=pv_ctrl_zero.node + '.translate',
                      outRotate=pv_ctrl_zero.node + '.rotate')

        # Create connections for controls
        pv_con = cmds.poleVectorConstraint(pv_ctrl, ikh)[0]
        clav_con = con.matrix_constraint(clav_ctrl, clav_bone_zero.node, maintain_offset=False, connect='rt')
        wrist_ikh_con = con.matrix_constraint(hand_ik_ctrl, ikh, connect='t')
        wrist_ik_jnt_con = con.matrix_constraint(hand_ik_ctrl, hand_ik_jnt.node, connect='r', maintain_offset=False)
        shoulder_ik_con = con.matrix_constraint(clav_ctrl, shoulder_ik_jnt.node, connect='rt')
        shoulder_fk_con = con.matrix_constraint(shoulder_fk_ctrl, shoulder_fk_jnt.node, maintain_offset=False, connect='rt')
        elbow_fk_con = con.matrix_constraint(elbow_fk_ctrl, elbow_fk_jnt.node, maintain_offset=False, connect='rt')
        hand_fk_con = con.matrix_constraint(hand_fk_ctrl, hand_fk_jnt.node, maintain_offset=False, connect='rt')
        fkik_rev = node.Node('{}_{}_fkik'.format(self.side, self.name), 'reverse')

        shoulder_pb.connect(weight=hand_ik_ctrl + '.FKIK')
        elbow_pb.connect(weight=hand_ik_ctrl + '.FKIK')
        wrist_pb.connect(weight=hand_ik_ctrl + '.FKIK')

        # Move twist bones to the correct positions
        cmds.xform(shoulder_bone_zero.node, matrix=shoulder_norm_mtx, ws=True)
        cmds.xform(shoulder_top_twist_bone_zero.node, matrix=shoulder_norm_mtx, ws=True)
        cmds.xform(elbow_top_twist_bone_zero.node, matrix=elbow_norm_mtx, ws=True)
        cmds.setAttr(elbow_top_twist_bone_zero.node + '.r', 0, 0, 0)
        cmds.xform(hand_twist_bone_zero.node, matrix=hand_norm_mtx, ws=True)

        # Rotation connections for twist joints
        shoulder_top_twist_bone.connect(rotateY_in=shoulder_bone.node + '.ry',
                                             rotateZ_in=shoulder_bone.node + '.rz')
        elbow_top_twist_bone_zero.connect(rotateX_in=shoulder_bone.node + '.rx')
        hand_twist_bone_zero.connect(rotateX_in=hand_bone_ofs.node + '.rx')

        # Add stretch to ik
        attr.add_float(hand_ik_ctrl, 'stretch')
        dist = node.Node('{}_{}_ik_stretch'.format(self.name, self.side), 'distanceBetween', list_add=self.module_nodes)
        mult = node.Node('{}_{}_ik_stretch'.format(self.name, self.side), 'multiplyDivide', list_add=self.module_nodes)
        cond = node.Node('{}_{}_ik_stretch'.format(self.name, self.side), 'condition', list_add=self.module_nodes)
        blend = node.Node('{}_{}_ik_stretch'.format(self.name, self.side), 'blendColors', list_add=self.module_nodes)
        full_distance = cmds.getAttr(hand_fk_jnt.node + '.tx') + cmds.getAttr(elbow_fk_jnt.node + '.tx')

        dist.connect(inMatrix1=shoulder_fk_ctrl + '.worldMatrix[0]',
                     inMatrix2=hand_ik_ctrl + '.worldMatrix[0]')
        mult.connect(input1X=dist.node + '.distance',
                     input2X=full_distance,
                     operation=2)
        cond.connect(firstTerm=dist.node + '.distance',
                     secondTerm=full_distance,
                     operation=3,
                     colorIfTrueR=mult.node + '.outputX')
        blend.connect(blender=hand_ik_ctrl + '.stretch',
                      color1R=cond.node + '.outColorR',
                      color2R=1)
        shoulder_ik_jnt.connect(scaleX_in=blend.node + '.outputR')
        elbow_ik_jnt.connect(scaleX_in=blend.node + '.outputR')

        # Create extra twist bones
        twist_num = self.attributes['twistJoints'].value()
        num = 2

        for i in range(twist_num):
            # Create bones
            shoulder_twist_bone = node.Node('{}_{}_shoulder_twist_0{}'.format(self.side, self.name, str(i + num)), 'joint', list_add=self.module_nodes)
            shoulder_twist_bone_ofs = node.Node('{}_{}_shoulder_twist_0{}_OFS'.format(self.side, self.name, str(i + num)), 'transform', suffix=False, list_add=self.module_nodes)
            shoulder_twist_bone_zero = node.Node('{}_{}_shoulder_twist_0{}_ZERO'.format(self.side, self.name, str(i + num)), 'transform', suffix=False, list_add=self.module_nodes)

            elbow_twist_bone = node.Node('{}_{}_elbow_twist_0{}'.format(self.side, self.name, str(i + num)), 'joint', list_add=self.module_nodes)
            elbow_twist_bone_ofs = node.Node('{}_{}_elbow_twist_0{}_OFS'.format(self.side, self.name, str(i + num)), 'transform', suffix=False, list_add=self.module_nodes)
            elbow_twist_bone_zero = node.Node('{}_{}_elbow_twist_0{}_ZERO'.format(self.side, self.name, str(i + num)), 'transform', suffix=False, list_add=self.module_nodes)

            shoulder_twist_bmtx = node.Node('{}_{}_shoulder_twist_0{}'.format(self.side, self.name, str(i + num)), 'matrixBlend', list_add=self.module_nodes)
            elbow_twist_bmtx = node.Node('{}_{}_elbow_twist_0{}'.format(self.side, self.name, str(i + num)), 'matrixBlend', list_add=self.module_nodes)
            shoulder_mm = node.Node('{}_{}_shoulder_twist_0{}'.format(self.side, self.name, str(i + num)), 'multMatrix', list_add=self.module_nodes)
            elbow_mm = node.Node('{}_{}_elbow_twist_0{}'.format(self.side, self.name, str(i + num)), 'multMatrix', list_add=self.module_nodes)
            shoulder_dm = node.Node('{}_{}_shoulder_twist_0{}'.format(self.side, self.name, str(i + num)), 'decomposeMatrix', list_add=self.module_nodes)
            elbow_dm = node.Node('{}_{}_elbow_twist_0{}'.format(self.side, self.name, str(i + num)), 'decomposeMatrix', list_add=self.module_nodes)
            ratio = float(1 + i)/float(twist_num + 1)

            cmds.parent(shoulder_twist_bone.node, shoulder_twist_bone_ofs.node)
            cmds.parent(shoulder_twist_bone_ofs.node, shoulder_twist_bone_zero.node)
            cmds.parent(shoulder_twist_bone_zero.node, shoulder_top_twist_bone.node)

            cmds.parent(elbow_twist_bone.node, elbow_twist_bone_ofs.node)
            cmds.parent(elbow_twist_bone_ofs.node, elbow_twist_bone_zero.node)
            cmds.parent(elbow_twist_bone_zero.node, elbow_bone.node)

            cmds.select(clear=True)

            shoulder_twist_bone_zero.connect(rotateX_in=0,
                                             rotateY_in=0,
                                             rotateZ_in=0)
            elbow_twist_bone_zero.connect(rotateX_in=0,
                                          rotateY_in=0,
                                          rotateZ_in=0)

            # Position bones
            shoulder_twist_bmtx.connect(input1=shoulder_bone.node + '.worldMatrix[0]',
                                        input2=elbow_bone.node + '.worldMatrix[0]',
                                        blend=ratio)
            elbow_twist_bmtx.connect(input1=elbow_bone.node + '.worldMatrix[0]',
                                     input2=hand_bone.node + '.worldMatrix[0]',
                                     blend=ratio)
            shoulder_mm.connect(matrixIn_0=shoulder_twist_bmtx.node + '.output',
                                matrixIn_1=shoulder_twist_bone_zero.node + '.parentInverseMatrix[0]')
            elbow_mm.connect(matrixIn_0=elbow_twist_bmtx.node + '.output',
                             matrixIn_1=elbow_twist_bone_zero.node + '.parentInverseMatrix[0]')
            shoulder_dm.connect(inputMatrix=shoulder_mm.node + '.matrixSum',
                                outputTranslate=shoulder_twist_bone_zero.node + '.t')
            elbow_dm.connect(inputMatrix=elbow_mm.node + '.matrixSum',
                             outputTranslate=elbow_twist_bone_zero.node + '.t')

            # Connect rotation
            top_mult = node.Node(shoulder_twist_bone.node + '_ROT', 'multiplyDivide', list_add=self.module_nodes)
            bot_mult = node.Node(elbow_twist_bone.node + '_ROT', 'multiplyDivide', list_add=self.module_nodes)
            twist_mult = (float(i) + 1.0)/float(twist_num + 1)

            top_mult.connect(input1X=shoulder_bone.node + '.rx',
                             input2X=twist_mult,
                             outputX=shoulder_twist_bone_zero.node + '.rx')
            bot_mult.connect(input1X=hand_bone_ofs.node + '.rx',
                             input2X=twist_mult,
                             outputX=elbow_twist_bone_zero.node + '.rx')

        # Connect visibilities to fkik switch
        fkik_rev.connect(inputX=hand_ik_ctrl + '.FKIK',
                         outputX=shoulder_fk_ctrl + '.v')
        cmds.connectAttr(hand_ik_ctrl + '.FKIK', hand_ik_ctrl + '.v')
        cmds.connectAttr(hand_ik_ctrl + '.FKIK', pv_ctrl + '.v')

        attr.lock_attributes(hand_ik_ctrl, ['v', 'sx', 'sy', 'sz'])
        attr.lock_attributes(pv_ctrl, ['v', 'sx', 'sy', 'sz', 'rx', 'ry', 'rz'])
        attr.lock_attributes(hand_fk_ctrl, ['v', 'sx', 'sy', 'sz', 'tx', 'ty', 'tz'])
        attr.lock_attributes(elbow_fk_ctrl, ['v', 'sx', 'sy', 'sz', 'tx', 'ty', 'tz', 'rx', 'rz'])
        attr.lock_attributes(shoulder_fk_ctrl, ['v', 'sx', 'sy', 'sz', 'tx', 'ty', 'tz'])
        attr.lock_attributes(clav_ctrl, ['v', 'sx', 'sy', 'sz'])

        self.custom_nodes += [wrist_ikh_con, wrist_ik_jnt_con, shoulder_ik_con, clav_con, shoulder_fk_con, elbow_fk_con, hand_fk_con]
        self.module_nodes += [pv_con]

        # Declare in and out connectors for build
        self.in_conns_build = [clav_ctrl_zero.node, hand_ik_ctrl_zero.node, pv_ctrl_zero.node]
        self.out_conn_build = hand_bone.node
