import maya.cmds as cmds

import fedrig.layout.layout_objects as lyt
import fedrig.layout.layout_list as lyt_list
import fedrig.base_module as bm
import fedrig.module_attributes as mod_attr
import create.nodes as node

import custom_utils.control_shapes as cs
import custom_utils.maths as maths
import custom_utils.shape as shape
import custom_utils.constraint as con
import custom_utils.attribute as attr

reload(bm)


class LegModule(bm.BaseModule):
    module_type = 'Leg'

    def __init__(self, name='leg', side='L', initialize=False):
        # Re initialize scene layouts
        self.scene_layouts = lyt_list.LayoutList()

        super(LegModule, self).__init__(name=name, side=side, initialize=initialize)

    def module_layout_objects(self):
        # Create layout objects and base positions
        self.layouts['hip'] = clav_lyt = lyt.BaseLayoutObject(side=self.side, name=self.name + '_hip')
        self.layouts['thigh'] = shoulder_lyt = lyt.BaseLayoutObject(side=self.side, name=self.name + '_thigh')
        self.layouts['knee'] = elbow_lyt = lyt.BaseLayoutObject(side=self.side, name=self.name + '_knee')
        self.layouts['foot'] = wrist_lyt = lyt.BaseLayoutObject(side=self.side, name=self.name + '_foot')

        # Declare in and out connectors for module
        self.in_conn_module = clav_lyt
        self.out_conn_module = wrist_lyt

        clav_lyt.set_main_child(shoulder_lyt)
        shoulder_lyt.set_main_child(elbow_lyt)
        elbow_lyt.set_main_child(wrist_lyt)

        # Set default attributes for layout objects
        clav_lyt.dag.connect(up_in=2,
                             aimType_in=2,
                             upType_in=0)
        shoulder_lyt.dag.connect(up_in=3,
                                 aimType_in=2,
                                 upType_in=2,
                                 planeType_in=1)
        elbow_lyt.dag.connect(up_in=3,
                              aimType_in=2,
                              upType_in=2)
        wrist_lyt.dag.connect(aim_in=1,
                              up_in=3,
                              aimType_in=1,
                              upType_in=2)

        clav_lyt.set_position((0, 0, 0))
        shoulder_lyt.set_position((1, 0, 0))
        elbow_lyt.set_position((4, 0, -2))
        wrist_lyt.set_position((7, 0, 0))

        # If the side is right apply the right transformations and attributes
        if self.side == 'R':
            clav_lyt.set_position((0, 0, 0))
            shoulder_lyt.set_position((-1, 0, 0))
            elbow_lyt.set_position((-4, 0, -2))
            wrist_lyt.set_position((-7, 0, 0))

    def module_attributes(self):
        '''
        Attributes specific to a module that influence a build

        Write dictionary of functions for how to return a value from
        '''
        self.attributes['twistJoints'] = mod_attr.Attribute('twistJoints', 'Twist Joints', self)
        self.attributes['twistJoints'].slider()

    def module_build(self):
        # get layout objects
        clav = self.layouts['hip']
        shoulder = self.layouts['thigh']
        elbow = self.layouts['knee']
        wrist = self.layouts['foot']

        # create ik joints for chain
        shoulder_ik_jnt = node.Node('{}_{}_thigh_IK_JNT'.format(self.side, self.name), 'joint', list_add=self.module_nodes, suffix=False)
        elbow_ik_jnt = node.Node('{}_{}_knee_IK_JNT'.format(self.side, self.name), 'joint', list_add=self.module_nodes, suffix=False)
        wrist_ik_jnt = node.Node('{}_{}_foot_IK_JNT'.format(self.side, self.name), 'joint', list_add=self.module_nodes, suffix=False)
        self.parts += [shoulder_ik_jnt.node]
        cmds.select(clear=True)

        # create fk joints for chain
        shoulder_fk_jnt = node.Node('{}_{}_thigh_FK_JNT'.format(self.side, self.name), 'joint', list_add=self.module_nodes, suffix=False)
        elbow_fk_jnt = node.Node('{}_{}_knee_FK_JNT'.format(self.side, self.name), 'joint', list_add=self.module_nodes, suffix=False)
        wrist_fk_jnt = node.Node('{}_{}_foot_FK_JNT'.format(self.side, self.name), 'joint', list_add=self.module_nodes, suffix=False)
        self.parts += [shoulder_fk_jnt.node]
        cmds.select(clear=True)

        # create main bones
        clav_bone = node.Node('{}_{}_hip'.format(self.side, self.name), 'joint', list_add=self.module_nodes)
        shoulder_bone = node.Node('{}_{}_thigh'.format(self.side, self.name), 'joint', list_add=self.module_nodes)
        elbow_bone = node.Node('{}_{}_knee'.format(self.side, self.name), 'joint', list_add=self.module_nodes)
        wrist_bone = node.Node('{}_{}_foot'.format(self.side, self.name), 'joint', list_add=self.module_nodes)

        clav_bone_ofs = node.Node(clav_bone.node + '_OFS', 'transform', suffix=False, list_add=self.module_nodes)
        shoulder_bone_ofs = node.Node(shoulder_bone.node + '_OFS', 'transform', suffix=False, list_add=self.module_nodes)
        elbow_bone_ofs = node.Node(elbow_bone.node + '_OFS', 'transform', suffix=False, list_add=self.module_nodes)
        wrist_bone_ofs = node.Node(wrist_bone.node + '_OFS', 'transform', suffix=False, list_add=self.module_nodes)

        clav_bone_zero = node.Node(clav_bone.node + '_ZERO', 'transform', suffix=False, list_add=self.module_nodes)
        shoulder_bone_zero = node.Node(shoulder_bone.node + '_ZERO', 'transform', suffix=False, list_add=self.module_nodes)
        elbow_bone_zero = node.Node(elbow_bone.node + '_ZERO', 'transform', suffix=False, list_add=self.module_nodes)
        wrist_bone_zero = node.Node(wrist_bone.node + '_ZERO', 'transform', suffix=False, list_add=self.module_nodes)
        self.bones += [clav_bone_zero.node, shoulder_bone_zero.node, elbow_bone_zero.node, wrist_bone_zero.node]

        cmds.select(clear=True)

        # parent bones correctly under offsets and zeros
        cmds.parent(clav_bone.node, clav_bone_ofs.node)
        cmds.parent(clav_bone_ofs.node, clav_bone_zero.node)

        cmds.parent(shoulder_bone.node, shoulder_bone_ofs.node)
        cmds.parent(shoulder_bone_ofs.node, shoulder_bone_zero.node)
        cmds.parent(shoulder_bone_zero.node, clav_bone.node)

        cmds.parent(elbow_bone.node, elbow_bone_ofs.node)
        cmds.parent(elbow_bone_ofs.node, elbow_bone_zero.node)
        cmds.parent(elbow_bone_zero.node, shoulder_bone.node)

        cmds.parent(wrist_bone.node, wrist_bone_ofs.node)
        cmds.parent(wrist_bone_ofs.node, wrist_bone_zero.node)
        cmds.parent(wrist_bone_zero.node, elbow_bone.node)

        # put bones into correct positions
        clav_mtx = clav.get_output_matrix()
        shoulder_mtx = shoulder.get_output_matrix()
        elbow_mtx = elbow.get_output_matrix()
        wrist_mtx = wrist.get_output_matrix()

        cmds.xform(shoulder_ik_jnt.node, matrix=shoulder_mtx, ws=True)
        cmds.xform(elbow_ik_jnt.node, matrix=elbow_mtx, ws=True)
        cmds.xform(wrist_ik_jnt.node, matrix=wrist_mtx, ws=True)
        cmds.xform(shoulder_fk_jnt.node, matrix=shoulder_mtx, ws=True)
        cmds.xform(elbow_fk_jnt.node, matrix=elbow_mtx, ws=True)
        cmds.xform(wrist_fk_jnt.node, matrix=wrist_mtx, ws=True)

        # Constrain bones to joints
        shoulder_ik_con = con.matrix_constraint(shoulder_ik_jnt.node, shoulder_bone_zero.node, maintain_offset=False, connect='')
        elbow_ik_con = con.matrix_constraint(elbow_ik_jnt.node, elbow_bone_zero.node, maintain_offset=False, connect='')
        wrist_ik_con = con.matrix_constraint(wrist_ik_jnt.node, wrist_bone_zero.node, maintain_offset=False, connect='')
        shoulder_fk_con = con.matrix_constraint(shoulder_fk_jnt.node, shoulder_bone_zero.node, maintain_offset=False, connect='')
        elbow_fk_con = con.matrix_constraint(elbow_fk_jnt.node, elbow_bone_zero.node, maintain_offset=False, connect='')
        wrist_fk_con = con.matrix_constraint(wrist_fk_jnt.node, wrist_bone_zero.node, maintain_offset=False, connect='')
        shoulder_pb = node.Node('{}_{}_thigh'.format(self.side, self.name), 'pairBlend', list_add=self.module_nodes)
        elbow_pb = node.Node('{}_{}_knee'.format(self.side, self.name), 'pairBlend', list_add=self.module_nodes)
        wrist_pb = node.Node('{}_{}_foot'.format(self.side, self.name), 'pairBlend', list_add=self.module_nodes)

        shoulder_pb.connect(inRotate1=shoulder_fk_con + '.outRotate',
                            inRotate2=shoulder_ik_con + '.outRotate',
                            inTranslate1=shoulder_fk_con + '.outTranslate',
                            inTranslate2=shoulder_ik_con + '.outTranslate',
                            outRotate=shoulder_bone_zero.node + '.rotate',
                            outTranslate=shoulder_bone_zero.node + '.translate')

        elbow_pb.connect(inRotate1=elbow_fk_con + '.outRotate',
                         inRotate2=elbow_ik_con + '.outRotate',
                         inTranslate1=elbow_fk_con + '.outTranslate',
                         inTranslate2=elbow_ik_con + '.outTranslate',
                         outRotate=elbow_bone_zero.node + '.rotate',
                         outTranslate=elbow_bone_zero.node + '.translate')

        wrist_pb.connect(inRotate1=wrist_fk_con + '.outRotate',
                         inRotate2=wrist_ik_con + '.outRotate',
                         inTranslate1=wrist_fk_con + '.outTranslate',
                         inTranslate2=wrist_ik_con + '.outTranslate',
                         outRotate=wrist_bone_zero.node + '.rotate',
                         outTranslate=wrist_bone_zero.node + '.translate')

        self.custom_nodes += [shoulder_ik_con, elbow_ik_con, wrist_ik_con, shoulder_fk_con, elbow_fk_con, wrist_fk_con]

        # Create ikhandle
        elbow_ik_jnt.connect(preferredAngleY=-90)
        ikh, effector = cmds.ikHandle(name='{}_{}_IKH'.format(self.side, self.name),
                                      startJoint=shoulder_ik_jnt.node,
                                      endEffector=wrist_ik_jnt.node)
        self.module_nodes += [ikh, effector]
        self.parts += [ikh]

        # Get Pole Vector position
        shoulder_pos = maths.matrix_to_vectors(shoulder_mtx)[3]
        elbow_pos = maths.matrix_to_vectors(elbow_mtx)[3]
        wrist_pos = maths.matrix_to_vectors(wrist_mtx)[3]

        pv = maths.get_pole_vector(shoulder_pos, elbow_pos, wrist_pos, length=5)

        # Create controls
        clav_ctrl = cs.create_nurbscurve('{}_{}_hip_CTRL'.format(self.side, self.name), 'circle')
        shoulder_fk_ctrl = cs.create_nurbscurve('{}_{}_thigh_FK_CTRL'.format(self.side, self.name), 'circle')
        elbow_fk_ctrl = cs.create_nurbscurve('{}_{}_knee_FK_CTRL'.format(self.side, self.name), 'circle')
        hand_fk_ctrl = cs.create_nurbscurve('{}_{}_foot_FK_CTRL'.format(self.side, self.name), 'circle')
        hand_ik_ctrl = cs.create_nurbscurve('{}_{}_foot_IK_CTRL'.format(self.side, self.name), 'long_hexagon')
        pv_ctrl = cs.create_nurbscurve('{}_{}_pv_CTRL'.format(self.side, self.name), 'diamond')
        self.module_nodes += [clav_ctrl, shoulder_fk_ctrl, elbow_fk_ctrl, hand_fk_ctrl, hand_ik_ctrl, pv_ctrl]

        clav_ctrl_zero = node.Node(clav_ctrl + '_ZERO', 'transform', suffix=False, list_add=self.module_nodes)
        shoulder_fk_ctrl_zero = node.Node(shoulder_fk_ctrl + '_ZERO', 'transform', suffix=False, list_add=self.module_nodes)
        hand_fk_ctrl_zero = node.Node(hand_fk_ctrl + '_ZERO', 'transform', suffix=False, list_add=self.module_nodes)
        elbow_fk_ctrl_zero = node.Node(elbow_fk_ctrl + '_ZERO', 'transform', suffix=False, list_add=self.module_nodes)
        hand_ik_ctrl_zero = node.Node(hand_ik_ctrl + '_ZERO', 'transform', suffix=False, list_add=self.module_nodes)
        pv_ctrl_zero = node.Node(pv_ctrl + '_ZERO', 'transform', suffix=False, list_add=self.module_nodes)
        self.controls += [clav_ctrl_zero.node, hand_ik_ctrl_zero.node, pv_ctrl_zero.node]

        # Modify shapes
        shape.modify_shape(hand_ik_ctrl, translate=(0, 1, 0), rotate=(0, 90, 0))
        shape.modify_shape(hand_ik_ctrl, rotate=(0, 0, 90))
        shape.modify_shape(hand_ik_ctrl, rotate=(-90, 0, 0))
        shape.modify_shape(clav_ctrl, rotate=(0, 90, 0))
        shape.modify_shape(shoulder_fk_ctrl, rotate=(0, 90, 0))
        shape.modify_shape(elbow_fk_ctrl, rotate=(0, 90, 0))
        shape.modify_shape(hand_fk_ctrl, rotate=(0, 90, 0))

        # Parent under zeroes
        cmds.parent(clav_ctrl, clav_ctrl_zero.node)
        cmds.parent(shoulder_fk_ctrl, shoulder_fk_ctrl_zero.node)
        cmds.parent(elbow_fk_ctrl, elbow_fk_ctrl_zero.node)
        cmds.parent(hand_fk_ctrl, hand_fk_ctrl_zero.node)
        cmds.parent(hand_ik_ctrl, hand_ik_ctrl_zero.node)
        cmds.parent(pv_ctrl, pv_ctrl_zero.node)

        # FK hierarchy
        cmds.parent(shoulder_fk_ctrl_zero.node, clav_ctrl)
        cmds.parent(elbow_fk_ctrl_zero.node, shoulder_fk_ctrl)
        cmds.parent(hand_fk_ctrl_zero.node, elbow_fk_ctrl)

        # Position controls
        cmds.xform(clav_ctrl_zero.node, matrix=clav_mtx)
        cmds.xform(shoulder_fk_ctrl_zero.node, matrix=shoulder_mtx, ws=True)
        cmds.xform(elbow_fk_ctrl_zero.node, matrix=elbow_mtx, ws=True)
        cmds.xform(hand_fk_ctrl_zero.node, matrix=wrist_mtx, ws=True)
        cmds.xform(hand_ik_ctrl_zero.node, matrix=wrist_mtx)
        cmds.xform(pv_ctrl_zero.node, t=(pv[0], pv[1], pv[2]), ws=True)

        # Add necessary attributes onto controls
        attr.add_float(hand_fk_ctrl, 'FKIK')
        attr.add_proxy(hand_ik_ctrl, hand_fk_ctrl + '.FKIK', 'FKIK')

        # Create connections for controls
        pv_con = cmds.poleVectorConstraint(pv_ctrl, ikh)[0]
        clav_con = con.matrix_constraint(clav_ctrl, clav_bone_zero.node, maintain_offset=False)
        wrist_ikh_con = con.matrix_constraint(hand_ik_ctrl, ikh, connect='t')
        wrist_ik_jnt_con = con.matrix_constraint(hand_ik_ctrl, wrist_ik_jnt.node, connect='r')
        shoulder_ik_con = con.matrix_constraint(clav_ctrl, shoulder_ik_jnt.node)
        shoulder_fk_con = con.matrix_constraint(shoulder_fk_ctrl, shoulder_fk_jnt.node, maintain_offset=False)
        elbow_fk_con = con.matrix_constraint(elbow_fk_ctrl, elbow_fk_jnt.node, maintain_offset=False)
        wrist_fk_con = con.matrix_constraint(hand_fk_ctrl, wrist_fk_jnt.node, maintain_offset=False)
        fkik_rev = node.Node('{}_{}_fkik'.format(self.side, self.name), 'reverse')

        shoulder_pb.connect(weight=hand_ik_ctrl + '.FKIK')
        elbow_pb.connect(weight=hand_ik_ctrl + '.FKIK')
        wrist_pb.connect(weight=hand_ik_ctrl + '.FKIK')

        # Connect visibilities to fkik switch
        fkik_rev.connect(inputX=hand_ik_ctrl + '.FKIK',
                         outputX=shoulder_fk_ctrl + '.v')
        cmds.connectAttr(hand_ik_ctrl + '.FKIK', hand_ik_ctrl + '.v')
        cmds.connectAttr(hand_ik_ctrl + '.FKIK', pv_ctrl + '.v')

        attr.lock_attributes(hand_ik_ctrl, ['v', 'sx', 'sy', 'sz'])
        attr.lock_attributes(pv_ctrl, ['v', 'sx', 'sy', 'sz', 'rx', 'ry', 'rz'])
        attr.lock_attributes(hand_fk_ctrl, ['v', 'sx', 'sy', 'sz', 'tx', 'ty', 'tz'])
        attr.lock_attributes(elbow_fk_ctrl, ['v', 'sx', 'sy', 'sz', 'tx', 'ty', 'tz', 'rx', 'rz'])
        attr.lock_attributes(shoulder_fk_ctrl, ['v', 'sx', 'sy', 'sz', 'tx', 'ty', 'tz'])
        attr.lock_attributes(clav_ctrl, ['v', 'sx', 'sy', 'sz'])

        self.custom_nodes += [wrist_ikh_con, wrist_ik_jnt_con, shoulder_ik_con, clav_con, shoulder_fk_con, elbow_fk_con, wrist_fk_con]
        self.module_nodes += [pv_con]

        # Declare in and out connectors for build
        self.in_conns_build = [clav_ctrl_zero.node, hand_ik_ctrl_zero.node, pv_ctrl_zero.node]
        self.out_conn_build = hand_ik_ctrl
