import maya.cmds as cmds

import fedrig.layout.layout_objects as lyt
import fedrig.layout.layout_list as lyt_list
import fedrig.base_module as bm
import fedrig.module_attributes as mod_attr
import create.nodes as node
import create.nurbs as nrb
import create.dag as dag

import custom_utils.control_shapes as cs
import custom_utils.maths as maths
import custom_utils.shape as shape
import custom_utils.constraint as con
import custom_utils.attribute as attr

reload(bm)


class SpineModule(bm.BaseModule):
    module_type = 'Spine'

    def __init__(self, name='spine', side='C', initialize=False):
        # Re initialize scene layouts
        self.scene_layouts = lyt_list.LayoutList()

        super(SpineModule, self).__init__(name=name, side=side, initialize=initialize)

    def module_layout_objects(self):
        # Create layout objects and base positions
        self.layouts['hip'] = hip_lyt = lyt.BaseLayoutObject(side=self.side, name=self.name + '_hip')
        self.layouts['spine_base'] = spine_base_lyt = lyt.BaseLayoutObject(side=self.side, name=self.name + '_spine_base')
        self.layouts['spine_01'] = spine_01_lyt = lyt.BaseLayoutObject(side=self.side, name=self.name + '_spine_01')
        self.layouts['spine_02'] = spine_02_lyt = lyt.BaseLayoutObject(side=self.side, name=self.name + '_spine_02')
        self.layouts['chest'] = chest_lyt = lyt.BaseLayoutObject(side=self.side, name=self.name + '_chest')

        # Declare in and out connectors for module
        self.in_conn_module = spine_base_lyt
        self.out_conn_module = chest_lyt

        spine_base_lyt.set_main_child(hip_lyt)
        spine_base_lyt.set_main_child(spine_01_lyt)
        spine_01_lyt.set_main_child(spine_02_lyt)
        spine_02_lyt.set_main_child(chest_lyt)

        # Set default attributes for layout objects
        spine_base_lyt.dag.connect(aimType_in=2,
                                   up_in=2,
                                   upType_in=0)
        hip_lyt.dag.connect(aim_in=1,
                            aimType_in=1,
                            upType_in=0)
        spine_01_lyt.dag.connect(aimType_in=2,
                                 up_in=3)
        spine_02_lyt.dag.connect(aimType_in=2,
                                 up_in=3)
        chest_lyt.dag.connect(aimType_in=1,
                              aim_in=1,
                              up_in=3)

        spine_base_lyt.set_position((0, 37.939, 0.981))
        hip_lyt.set_position((0, 39.381, 1.097))
        spine_01_lyt.set_position((0, 42.801, 1.097))
        spine_02_lyt.set_position((0, 46.269, 0.787))
        chest_lyt.set_position((0, 50.143, 0.213))

        # Apply transforms to main dag
        for key, obj in self.layouts.items():
            obj.change_space('World')
        for key, obj in self.layouts.items():
            obj.apply_transforms()
        for key, obj in self.layouts.items():
            obj.change_space('Parent')

        spine_01_lyt.dag.connect(up_in=2)
        spine_02_lyt.dag.connect(up_in=2)
        chest_lyt.dag.connect(up_in=2)

    def module_attributes(self):
        '''
        Attributes specific to a module that influence a build

        Write dictionary of functions for how to return a value from
        '''
        self.attributes['numBones'] = mod_attr.Attribute('numBones', 'Num Bones', self)
        self.attributes['numBones'].slider(min=4, max=30, default=10)

    def module_build(self):
        # get layout objects
        hip_lyt = self.layouts['hip']
        spine_base_lyt = self.layouts['spine_base']
        spine_01_lyt = self.layouts['spine_01']
        spine_02_lyt = self.layouts['spine_02']
        chest_lyt = self.layouts['chest']

        # create controls
        root_ctrl, root_ctrl_zero = self.create_control('{}_root'.format(self.side), 'square', create_ofs=False)
        base_ctrl, base_ctrl_zero, base_ofs_ctrl, base_ofs_ctrl_zero = self.create_control('{}_spine_base'.format(self.side), 'octagon')
        spine_01_ctrl, spine_01_ctrl_zero, spine_01_ofs_ctrl, spine_01_ofs_ctrl_zero = self.create_control('{}_spine_01'.format(self.side), 'octagon')
        spine_02_ctrl, spine_02_ctrl_zero, spine_02_ofs_ctrl, spine_02_ofs_ctrl_zero = self.create_control('{}_spine_02'.format(self.side), 'octagon')
        hip_ctrl, hip_ctrl_zero = self.create_control('{}_hip'.format(self.side), 'circle', create_ofs=False)
        chest_ctrl, chest_ctrl_zero, chest_ofs_ctrl, chest_ofs_ctrl_zero = self.create_control('{}_chest'.format(self.side), 'octagon')
        self.controls += [root_ctrl_zero]

        shape.modify_shape(root_ctrl, rotate=(45, 0, 0), scale=(1.4, 1.4, 1.4))

        cmds.parent(hip_ctrl_zero, root_ctrl)
        cmds.parent(base_ctrl_zero, root_ctrl)
        cmds.parent(spine_01_ctrl_zero, base_ofs_ctrl)
        cmds.parent(spine_02_ctrl_zero, spine_01_ofs_ctrl)
        cmds.parent(chest_ctrl_zero, spine_02_ofs_ctrl)

        # position controls
        hip_mtx = hip_lyt.get_output_matrix()
        spine_base_mtx = spine_base_lyt.get_output_matrix()
        spine_01_mtx = spine_01_lyt.get_output_matrix()
        spine_02_mtx = spine_02_lyt.get_output_matrix()
        chest_mtx = chest_lyt.get_output_matrix()

        cmds.xform(root_ctrl_zero, matrix=spine_base_mtx, ws=True)
        cmds.xform(base_ctrl_zero, matrix=spine_base_mtx, ws=True)
        cmds.xform(hip_ctrl_zero, matrix=hip_mtx, ws=True)
        cmds.xform(spine_01_ctrl_zero, matrix=spine_01_mtx, ws=True)
        cmds.xform(spine_02_ctrl_zero, matrix=spine_02_mtx, ws=True)
        cmds.xform(chest_ctrl_zero, matrix=chest_mtx, ws=True)

        # create curves and bones
        crv = nrb.curve_from_dags('{}_{}_CRV'.format(self.side, self.name), [base_ofs_ctrl, spine_01_ofs_ctrl, spine_02_ofs_ctrl, chest_ofs_ctrl])
        up_crv = cmds.duplicate(crv, name='{}_{}_up_CRV'.format(self.side, self.name))[0]
        self.parts += [crv, up_crv]

        # move cvs of up curve
        cvs = cmds.ls(up_crv + '.cv[*]', fl=True)
        base_x, base_y, base_z, base_pos = maths.matrix_to_vectors(spine_base_mtx)
        spine_01_x, spine_01_y, spine_01_z, spine_01_pos = maths.matrix_to_vectors(spine_01_mtx)
        spine_02_x, spine_02_y, spine_02_z, spine_02_pos = maths.matrix_to_vectors(spine_02_mtx)
        chest_x, chest_y, chest_z, chest_pos = maths.matrix_to_vectors(chest_mtx)

        cv_vecs = []
        cv0_pos = base_y + base_pos
        cv1_pos = spine_01_y + spine_01_pos
        cv2_pos = spine_02_y + spine_02_pos
        cv3_pos = chest_y + chest_pos
        cv_vecs += [cv0_pos, cv1_pos, cv2_pos, cv3_pos]

        crv_jnts = []
        ctrls = [base_ofs_ctrl, spine_01_ofs_ctrl, spine_02_ofs_ctrl, chest_ofs_ctrl]

        for i, cv in enumerate(cvs):
            cmds.select(clear=True)
            jnt = node.Node(cv + 'JNT', 'joint', suffix=False, list_add=self.module_nodes)
            crv_jnts += [jnt.node]
            self.parts += [jnt.node]

            cmds.xform(cv, t=cv_vecs[i], ws=True)
            cmds.xform(jnt.node, t=cv_vecs[i], ws=True)

            # constrain joint to a given control
            mcon = con.matrix_constraint(ctrls[i], jnt.node, maintain_offset=True)
            cmds.setAttr(mcon + '.posVector', 0, 1, 0)
            self.custom_nodes += [mcon]

        skc = cmds.skinCluster(crv_jnts, up_crv)[0]
        self.module_nodes += [skc]

        # create bones for main spine
        bone_zeros = dag.dags_on_curve(up_crv, crv, '{}_{}'.format(self.side, self.name), 'bone', num_dags=self.attributes['numBones'].value())
        self.bones += bone_zeros

        # create base bones
        spine_base_bone_zero, spine_base_bone = self.create_bone('{}_spine_base'.format(self.side))
        hip_bone_zero, hip_bone = self.create_bone('{}_hip'.format(self.side))
        chest_bone_zero, chest_bone = self.create_bone('{}_chest'.format(self.side))
        for bone in [spine_base_bone, hip_bone, chest_bone]:
            cmds.setAttr(bone + '.radius', 1.2)

        base_mcon = con.matrix_constraint(base_ofs_ctrl, spine_base_bone_zero, maintain_offset=False)
        hip_mcon = con.matrix_constraint(hip_ctrl, hip_bone_zero, maintain_offset=False)
        chest_mcon = con.matrix_constraint(chest_ofs_ctrl, chest_bone_zero, maintain_offset=False)
        self.custom_nodes += [base_mcon, hip_mcon, chest_mcon]

        # Declare in and out connectors for build
        self.in_conns_build = [base_ctrl_zero]
        self.out_conn_build = chest_ofs_ctrl

    # FUNCTIONS SPECIFIC TO MODULE #
    def create_control(self, name, ctrl_type, ofs_type='dotted_circle', create_ofs=True, **kwargs):
        '''
        Creates a control and a sub control
        Args:
            name(str): name of control
            ctrl_type(str): type of control being created
            ofs_type(str): type of control for sub control
            kwargs: keyword arguments for specific shape types
        Return:
            [list]: list in order, control, offset control, control zero, offset control zero
        '''
        ctrls = []

        ctrl = cs.create_nurbscurve(name + '_CTRL', ctrl_type, **kwargs)
        ctrl_zero = node.Node(ctrl + '_ZERO', 'transform', list_add=self.module_nodes, suffix=False).node
        ctrls += [ctrl, ctrl_zero]
        self.module_nodes += [ctrl]

        shape.modify_shape(ctrl, rotate=(90, 0, 90))
        attr.lock_attributes(ctrl, ['v', 'sx', 'sy', 'sz'])

        cmds.parent(ctrl, ctrl_zero)

        if create_ofs:
            ofs_ctrl = cs.create_nurbscurve(name + '_OFS_CTRL', ofs_type)
            ofs_ctrl_zero = node.Node(ofs_ctrl + '_ZERO', 'transform', list_add=self.module_nodes, suffix=False).node
            ofs_ctrl_shapes = cmds.listRelatives(ofs_ctrl, s=True)
            self.module_nodes += [ofs_ctrl]
            ctrls += [ofs_ctrl, ofs_ctrl_zero]

            shape.modify_shape(ofs_ctrl, rotate=(90, 0, 90), scale=(.8, .8, .8))
            shape.change_color(ofs_ctrl, 'light brown', change_shapes=True)

            cmds.parent(ofs_ctrl, ofs_ctrl_zero)
            cmds.parent(ofs_ctrl_zero, ctrl)

            attr.add_bool(ctrl, 'offset')
            for each in ofs_ctrl_shapes:
                cmds.connectAttr(ctrl + '.offset', each + '.v')
            attr.lock_attributes(ofs_ctrl, ['v', 'sx', 'sy', 'sz'])

        return ctrls

    def create_bone(self, name):
        '''
        Creates a bone and a zero transform
        Args:
            name (str): name of bone
        Return:
            list[(str)]: zero transform, bone
        '''
        cmds.select(clear=True)
        bone = node.Node(name, 'joint', list_add=self.module_nodes).node
        zero = node.Node(bone + '_ZERO', 'transform', suffix=False).node
        cmds.parent(bone, zero)

        self.bones += [zero]

        return [zero, bone]
