import maya.cmds as cmds
import maya.api.OpenMaya as om

import fedrig.layout.layout_objects as lyt
import fedrig.layout.layout_list as lyt_list
import fedrig.base_module as bm
import fedrig.module_attributes as mod_attr
import create.nodes as node

import custom_utils.control_shapes as cs
import custom_utils.maths as maths
import custom_utils.shape as shape
import custom_utils.constraint as con
import custom_utils.attribute as attr

reload(bm)
reload(mod_attr)
reload(maths)


class HandModule(bm.BaseModule):
    module_type = 'Hand'
    fingers = 4

    def __init__(self, name='hand', side='L', initialize=False):
        # Re initialize scene layouts
        self.fingers = 4
        self.scene_layouts = lyt_list.LayoutList()

        super(HandModule, self).__init__(name=name, side=side, initialize=initialize)

    def module_layout_objects(self):
        # Create layout objects and base positions
        self.layouts['hand'] = hand_lyt = lyt.BaseLayoutObject(side=self.side, name=self.name + '_hand')
        self.layouts['thumb_meta'] = thumb_meta_lyt = lyt.BaseLayoutObject(side=self.side, name=self.name + '_thumb_meta')
        self.layouts['thumb_01'] = thumb_01_lyt = lyt.BaseLayoutObject(side=self.side, name=self.name + '_thumb_01')
        self.layouts['thumb_02'] = thumb_02_lyt = lyt.BaseLayoutObject(side=self.side, name=self.name + '_thumb_02')
        self.layouts['thumb_tip'] = thumb_tip_lyt = lyt.BaseLayoutObject(side=self.side, name=self.name + '_thumb_tip')

        # Declare in and out connectors for module
        self.in_conn_module = hand_lyt
        self.out_conn_module = hand_lyt

        hand_lyt.set_main_child(thumb_meta_lyt)
        thumb_meta_lyt.set_main_child(thumb_01_lyt)
        thumb_01_lyt.set_main_child(thumb_02_lyt)
        thumb_02_lyt.set_main_child(thumb_tip_lyt)

        # Set default attributes for layout objects
        thumb_meta_lyt.dag.connect(up_in=2,
                                   aimType_in=2,
                                   upType_in=0)
        thumb_01_lyt.dag.connect(up_in=2,
                                 aimType_in=2,
                                 upType_in=0,
                                 planeType_in=0)
        thumb_02_lyt.dag.connect(up_in=2,
                                 aimType_in=2,
                                 upType_in=0)
        thumb_tip_lyt.dag.connect(aim_in=1,
                                  up_in=2,
                                  aimType_in=1,
                                  upType_in=0)

        if self.side == 'L':
            hand_lyt.set_position((15.566, 40.365, 9.327))
            thumb_meta_lyt.set_position((14.9, 39.91, 10.072))
            thumb_01_lyt.set_position((13.945, 39.159, 10.762))
            thumb_02_lyt.set_position((13.415, 38.248, 11.254))
            thumb_tip_lyt.set_position((12.774, 37.159, 11.909))

        # If the side is right apply the right transformations and attributes
        else:
            hand_lyt.set_position((-15.566, 40.365, 9.327))
            thumb_meta_lyt.set_position((-14.9, 39.91, 10.072))
            thumb_01_lyt.set_position((-13.945, 39.159, 10.762))
            thumb_02_lyt.set_position((-13.415, 38.248, 11.254))
            thumb_tip_lyt.set_position((-12.774, 37.159, 11.909))

        letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
        # Create fingers
        for i in range(self.fingers):
            finger_meta = self.layouts['finger{}_meta'.format(letters[i])] = lyt.BaseLayoutObject(side=self.side, name=self.name + '_finger{}_meta'.format(letters[i]))
            finger_01 = self.layouts['finger{}_01'.format(letters[i])] = lyt.BaseLayoutObject(side=self.side, name=self.name + '_finger{}_01'.format(letters[i]))
            finger_02 = self.layouts['finger{}_02'.format(letters[i])] = lyt.BaseLayoutObject(side=self.side, name=self.name + '_finger{}_02'.format(letters[i]))
            finger_03 = self.layouts['finger{}_03'.format(letters[i])] = lyt.BaseLayoutObject(side=self.side, name=self.name + '_finger{}_03'.format(letters[i]))
            finger_tip = self.layouts['finger{}_tip'.format(letters[i])] = lyt.BaseLayoutObject(side=self.side, name=self.name + '_finger{}_tip'.format(letters[i]))

            finger_meta.set_parent(hand_lyt)
            finger_meta.set_main_child(finger_01)
            finger_01.set_main_child(finger_02)
            finger_02.set_main_child(finger_03)
            finger_03.set_main_child(finger_tip)

            # Set default attributes for layout objects
            finger_meta.dag.connect(up_in=2,
                                    aimType_in=2,
                                    upType_in=0)
            finger_01.dag.connect(up_in=2,
                                  aimType_in=2,
                                  upType_in=0,
                                  planeType_in=0)
            finger_02.dag.connect(up_in=2,
                                  aimType_in=2,
                                  upType_in=0)
            finger_03.dag.connect(up_in=2,
                                  aimType_in=2,
                                  upType_in=0)
            finger_tip.dag.connect(aim_in=1,
                                   up_in=2,
                                   aimType_in=1,
                                   upType_in=0)

            if self.side == 'L':
                meta_vec = om.MVector(15.408730259733714, 39.949391600038645, 10.29074004902225)
                one_vec = om.MVector(15.433047298204247, 38.37507676685243, 12.443823172021482)
                two_vec = om.MVector(15.327275142937555, 37.12096786295208, 13.039797201846318)
                three_vec = om.MVector(15.127880506607879, 36.28173336794637, 13.172515588121067)
                tip_vec = om.MVector(14.88724228516718, 35.37971502188869, 13.242941767386991)

                cmds.setAttr(finger_meta.dag.node + '.rz', -11.304)
                finger_meta.set_position(meta_vec)
                finger_01.set_position(one_vec)
                finger_02.set_position(two_vec)
                finger_03.set_position(three_vec)
                finger_tip.set_position(tip_vec)
                if i != 0:
                    spacing_vec = maths.matrix_to_vectors(finger_meta.get_output_matrix())[2] * -i
                    finger_meta.set_position(meta_vec + spacing_vec)
            elif self.side == 'R':
                meta_vec = om.MVector(-15.408730259733714, 39.949391600038645, 10.29074004902225)
                one_vec = om.MVector(-15.433047298204247, 38.37507676685243, 12.443823172021482)
                two_vec = om.MVector(-15.327275142937555, 37.12096786295208, 13.039797201846318)
                three_vec = om.MVector(-15.127880506607879, 36.28173336794637, 13.172515588121067)
                tip_vec = om.MVector(-14.88724228516718, 35.37971502188869, 13.242941767386991)

                cmds.setAttr(finger_meta.dag.node + '.rz', -11.304)
                finger_meta.set_position(meta_vec)
                finger_01.set_position(one_vec)
                finger_02.set_position(two_vec)
                finger_03.set_position(three_vec)
                finger_tip.set_position(tip_vec)
                if i != 0:
                    spacing_vec = maths.matrix_to_vectors(finger_meta.get_output_matrix())[2] * -i
                    finger_meta.set_position(meta_vec + spacing_vec)

    def module_attributes(self):
        '''
        Attributes specific to a module that influence a build

        Write dictionary of functions for how to return a value from
        '''
        self.attributes['fingers'] = mod_attr.Attribute('fingers', 'Fingers', self)
        self.attributes['fingers'].slider(min=1, max=7, default=4)

    def module_build(self):

        # get layout objects
        hand_lyt = self.layouts['hand']
        thumb_meta_lyt = self.layouts['thumb_meta']
        thumb_01_lyt = self.layouts['thumb_01']
        thumb_02_lyt = self.layouts['thumb_02']
        thumb_tip = self.layouts['thumb_tip']

        # create bones
        hand_bone = node.Node('{}_{}_fingerBase'.format(self.side, self.name), 'joint', list_add=self.module_nodes)
        thumb_meta_bone = node.Node('{}_thumb_meta'.format(self.side), 'joint', list_add=self.module_nodes)
        thumb_01_bone = node.Node('{}_thumb_01'.format(self.side), 'joint', list_add=self.module_nodes)
        thumb_02_bone = node.Node('{}_thumb_02'.format(self.side), 'joint', list_add=self.module_nodes)
        thumb_tip_bone = node.Node('{}_thumb_tip'.format(self.side), 'joint', list_add=self.module_nodes)

        hand_bone_zero = node.Node(hand_bone.node + '_ZERO', 'transform', list_add=self.module_nodes, suffix=False)
        thumb_meta_bone_zero = node.Node(thumb_meta_bone.node + '_ZERO', 'transform', list_add=self.module_nodes, suffix=False)
        thumb_01_bone_zero = node.Node(thumb_01_bone.node + '_ZERO', 'transform', list_add=self.module_nodes, suffix=False)
        thumb_02_bone_zero = node.Node(thumb_02_bone.node + '_ZERO', 'transform', list_add=self.module_nodes, suffix=False)
        thumb_tip_bone_zero = node.Node(thumb_tip_bone.node + '_ZERO', 'transform', list_add=self.module_nodes, suffix=False)
        self.bones += [hand_bone_zero.node]

        cmds.parent(hand_bone.node, hand_bone_zero.node)
        cmds.parent(thumb_meta_bone.node, thumb_meta_bone_zero.node)
        cmds.parent(thumb_01_bone.node, thumb_01_bone_zero.node)
        cmds.parent(thumb_02_bone.node, thumb_02_bone_zero.node)
        cmds.parent(thumb_tip_bone.node, thumb_tip_bone_zero.node)

        cmds.parent(thumb_meta_bone_zero.node, hand_bone.node)
        cmds.parent(thumb_01_bone_zero.node, thumb_meta_bone.node)
        cmds.parent(thumb_02_bone_zero.node, thumb_01_bone.node)
        cmds.parent(thumb_tip_bone_zero.node, thumb_02_bone.node)

        # create controls
        thumb_meta_ctrl = cs.create_nurbscurve('{}_thumb_meta_CTRL'.format(self.side), 'crescent')
        thumb_01_ctrl = cs.create_nurbscurve('{}_thumb_01_CTRL'.format(self.side), 'circle')
        thumb_02_ctrl = cs.create_nurbscurve('{}_thumb_02_CTRL'.format(self.side), 'circle')

        thumb_meta_ctrl_zero = node.Node(thumb_meta_ctrl + '_ZERO', 'transform', list_add=self.module_nodes, suffix=False)
        thumb_01_ctrl_zero = node.Node(thumb_01_ctrl + '_ZERO', 'transform', list_add=self.module_nodes, suffix=False)
        thumb_02_ctrl_zero = node.Node(thumb_02_ctrl + '_ZERO', 'transform', list_add=self.module_nodes, suffix=False)
        self.module_nodes += [thumb_meta_ctrl, thumb_01_ctrl, thumb_02_ctrl]
        self.controls += [thumb_meta_ctrl_zero.node]

        shape.modify_shape(thumb_meta_ctrl, rotate=(0, 90, 0))
        shape.modify_shape(thumb_meta_ctrl, scale=(0.3, 0.3, 0.3))
        shape.modify_shape(thumb_meta_ctrl, translate=(0, 1.2, 0))
        shape.modify_shape(thumb_01_ctrl, rotate=(0, 90, 0))
        shape.modify_shape(thumb_01_ctrl, scale=(0.2, 0.2, 0.2))
        shape.modify_shape(thumb_01_ctrl, translate=(0, .9, 0))
        shape.modify_shape(thumb_02_ctrl, rotate=(0, 90, 0))
        shape.modify_shape(thumb_02_ctrl, scale=(0.2, 0.2, 0.2))
        shape.modify_shape(thumb_02_ctrl, translate=(0, .9, 0))

        attr.lock_attributes(thumb_meta_ctrl, ['sx', 'sy', 'sz', 'v'])
        attr.lock_attributes(thumb_01_ctrl, ['tx', 'ty', 'tz', 'sx', 'sy', 'sz', 'v'])
        attr.lock_attributes(thumb_02_ctrl, ['tx', 'ty', 'tz', 'rx', 'ry', 'sx', 'sy', 'sz', 'v'])

        cmds.parent(thumb_meta_ctrl, thumb_meta_ctrl_zero.node)
        cmds.parent(thumb_01_ctrl, thumb_01_ctrl_zero.node)
        cmds.parent(thumb_02_ctrl, thumb_02_ctrl_zero.node)

        cmds.parent(thumb_01_ctrl_zero.node, thumb_meta_ctrl)
        cmds.parent(thumb_02_ctrl_zero.node, thumb_01_ctrl)

        # position controls
        hand_mtx = hand_lyt.get_output_matrix()
        thumb_meta_mtx = thumb_meta_lyt.get_output_matrix()
        thumb_01_mtx = thumb_01_lyt.get_output_matrix()
        thumb_02_mtx = thumb_02_lyt.get_output_matrix()
        thumb_tip_mtx = thumb_tip.get_output_matrix()

        cmds.xform(hand_bone_zero.node, matrix=hand_mtx, ws=True)
        cmds.xform(thumb_meta_ctrl_zero.node, matrix=thumb_meta_mtx, ws=True)
        cmds.xform(thumb_01_ctrl_zero.node, matrix=thumb_01_mtx, ws=True)
        cmds.xform(thumb_02_ctrl_zero.node, matrix=thumb_02_mtx, ws=True)

        # connect controls
        thumb_meta_ctrl_con = con.matrix_constraint(hand_bone.node, thumb_meta_ctrl_zero.node, connect='rt')
        thumb_meta_bone_con = con.matrix_constraint(thumb_meta_ctrl, thumb_meta_bone_zero.node, maintain_offset=False)
        thumb_01_bone_con = con.matrix_constraint(thumb_01_ctrl, thumb_01_bone_zero.node, maintain_offset=False)
        thumb_02_bone_con = con.matrix_constraint(thumb_02_ctrl, thumb_02_bone_zero.node, maintain_offset=False)
        cmds.xform(thumb_tip_bone_zero.node, matrix=thumb_tip_mtx, ws=True)
        self.custom_nodes += [thumb_meta_ctrl_con, thumb_meta_bone_con, thumb_01_bone_con, thumb_02_bone_con]

        # create finger setups
        letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
        for i in range(self.fingers):
            # get layouts
            finger_meta = self.layouts['finger{}_meta'.format(letters[i])]
            finger_01 = self.layouts['finger{}_01'.format(letters[i])]
            finger_02 = self.layouts['finger{}_02'.format(letters[i])]
            finger_03 = self.layouts['finger{}_03'.format(letters[i])]
            finger_tip = self.layouts['finger{}_tip'.format(letters[i])]

            # create bones
            finger_meta_bone = node.Node('{}_finger{}_meta'.format(self.side, letters[i]), 'joint', list_add=self.module_nodes)
            finger_01_bone = node.Node('{}_finger{}_01'.format(self.side, letters[i]), 'joint', list_add=self.module_nodes)
            finger_02_bone = node.Node('{}_finger{}_02'.format(self.side, letters[i]), 'joint', list_add=self.module_nodes)
            finger_03_bone = node.Node('{}_finger{}_03'.format(self.side, letters[i]), 'joint', list_add=self.module_nodes)
            finger_tip_bone = node.Node('{}_finger{}_tip'.format(self.side, letters[i]), 'joint', list_add=self.module_nodes)

            finger_meta_bone_zero = node.Node(finger_meta_bone.node + '_ZERO', 'transform', list_add=self.module_nodes, suffix=False)
            finger_01_bone_zero = node.Node(finger_01_bone.node + '_ZERO', 'transform', list_add=self.module_nodes, suffix=False)
            finger_02_bone_zero = node.Node(finger_02_bone.node + '_ZERO', 'transform', list_add=self.module_nodes, suffix=False)
            finger_03_bone_zero = node.Node(finger_03_bone.node + '_ZERO', 'transform', list_add=self.module_nodes, suffix=False)
            finger_tip_bone_zero = node.Node(finger_tip_bone.node + '_ZERO', 'transform', list_add=self.module_nodes, suffix=False)

            cmds.parent(finger_meta_bone.node, finger_meta_bone_zero.node)
            cmds.parent(finger_01_bone.node, finger_01_bone_zero.node)
            cmds.parent(finger_02_bone.node, finger_02_bone_zero.node)
            cmds.parent(finger_03_bone.node, finger_03_bone_zero.node)
            cmds.parent(finger_tip_bone.node, finger_tip_bone_zero.node)

            cmds.parent(finger_meta_bone_zero.node, hand_bone.node)
            cmds.parent(finger_01_bone_zero.node, finger_meta_bone.node)
            cmds.parent(finger_02_bone_zero.node, finger_01_bone.node)
            cmds.parent(finger_03_bone_zero.node, finger_02_bone.node)
            cmds.parent(finger_tip_bone_zero.node, finger_03_bone.node)

            # create controls
            finger_meta_ctrl = cs.create_nurbscurve('{}_finger{}_meta_CTRL'.format(self.side, letters[i]), 'crescent')
            finger_01_ctrl = cs.create_nurbscurve('{}_finger{}_01_CTRL'.format(self.side, letters[i]), 'circle')
            finger_02_ctrl = cs.create_nurbscurve('{}_finger{}_02_CTRL'.format(self.side, letters[i]), 'circle')
            finger_03_ctrl = cs.create_nurbscurve('{}_finger{}_03_CTRL'.format(self.side, letters[i]), 'circle')

            finger_meta_ctrl_ofs = node.Node(finger_meta_ctrl + '_OFS', 'transform', list_add=self.module_nodes, suffix=False)
            finger_meta_ctrl_zero = node.Node(finger_meta_ctrl + '_ZERO', 'transform', list_add=self.module_nodes, suffix=False)
            finger_01_ctrl_zero = node.Node(finger_01_ctrl + '_ZERO', 'transform', list_add=self.module_nodes, suffix=False)
            finger_02_ctrl_zero = node.Node(finger_02_ctrl + '_ZERO', 'transform', list_add=self.module_nodes, suffix=False)
            finger_03_ctrl_zero = node.Node(finger_03_ctrl + '_ZERO', 'transform', list_add=self.module_nodes, suffix=False)
            self.module_nodes += [finger_meta_ctrl, finger_01_ctrl, finger_02_ctrl, finger_03_ctrl]
            self.controls += [finger_meta_ctrl_zero.node]

            shape.modify_shape(finger_meta_ctrl, rotate=(0, 90, 0))
            shape.modify_shape(finger_meta_ctrl, scale=(0.3, 0.3, 0.3))
            shape.modify_shape(finger_meta_ctrl, translate=(0, 1.2, 0))
            shape.modify_shape(finger_01_ctrl, rotate=(0, 90, 0))
            shape.modify_shape(finger_01_ctrl, scale=(0.2, 0.2, 0.2))
            shape.modify_shape(finger_01_ctrl, translate=(0, .9, 0))
            shape.modify_shape(finger_02_ctrl, rotate=(0, 90, 0))
            shape.modify_shape(finger_02_ctrl, scale=(0.2, 0.2, 0.2))
            shape.modify_shape(finger_02_ctrl, translate=(0, .9, 0))
            shape.modify_shape(finger_03_ctrl, rotate=(0, 90, 0))
            shape.modify_shape(finger_03_ctrl, scale=(0.2, 0.2, 0.2))
            shape.modify_shape(finger_03_ctrl, translate=(0, .9, 0))

            attr.lock_attributes(finger_meta_ctrl, ['sx', 'sy', 'sz', 'v'])
            attr.lock_attributes(finger_01_ctrl, ['tx', 'ty', 'tz', 'sx', 'sy', 'sz', 'v'])
            attr.lock_attributes(finger_02_ctrl, ['tx', 'ty', 'tz', 'rx', 'ry', 'sx', 'sy', 'sz', 'v'])
            attr.lock_attributes(finger_03_ctrl, ['tx', 'ty', 'tz', 'rx', 'ry', 'sx', 'sy', 'sz', 'v'])

            cmds.parent(finger_meta_ctrl_ofs.node, finger_meta_ctrl_zero.node)
            cmds.parent(finger_meta_ctrl, finger_meta_ctrl_ofs.node)
            cmds.parent(finger_01_ctrl, finger_01_ctrl_zero.node)
            cmds.parent(finger_02_ctrl, finger_02_ctrl_zero.node)
            cmds.parent(finger_03_ctrl, finger_03_ctrl_zero.node)

            cmds.parent(finger_01_ctrl_zero.node, finger_meta_ctrl)
            cmds.parent(finger_02_ctrl_zero.node, finger_01_ctrl)
            cmds.parent(finger_03_ctrl_zero.node, finger_02_ctrl)

            # position controls
            finger_meta_mtx = finger_meta.get_output_matrix()
            finger_01_mtx = finger_01.get_output_matrix()
            finger_02_mtx = finger_02.get_output_matrix()
            finger_03_mtx = finger_03.get_output_matrix()
            finger_tip_mtx = finger_tip.get_output_matrix()

            cmds.xform(finger_meta_ctrl_zero.node, matrix=finger_meta_mtx, ws=True)
            cmds.xform(finger_01_ctrl_zero.node, matrix=finger_01_mtx, ws=True)
            cmds.xform(finger_02_ctrl_zero.node, matrix=finger_02_mtx, ws=True)
            cmds.xform(finger_03_ctrl_zero.node, matrix=finger_03_mtx, ws=True)

            # connect controls
            finger_meta_ctrl_con = con.matrix_constraint(hand_bone.node, finger_meta_ctrl_zero.node, connect='rt')
            finger_meta_bone_con = con.matrix_constraint(finger_meta_ctrl, finger_meta_bone_zero.node, maintain_offset=False)
            finger_01_bone_con = con.matrix_constraint(finger_01_ctrl, finger_01_bone_zero.node, maintain_offset=False)
            finger_02_bone_con = con.matrix_constraint(finger_02_ctrl, finger_02_bone_zero.node, maintain_offset=False)
            finger_03_bone_con = con.matrix_constraint(finger_03_ctrl, finger_03_bone_zero.node, maintain_offset=False)
            cmds.xform(finger_tip_bone_zero.node, matrix=finger_tip_mtx, ws=True)
            self.custom_nodes += [finger_meta_ctrl_con, finger_meta_bone_con, finger_01_bone_con, finger_02_bone_con, finger_03_bone_con]

            # add meta attribute
            attr.add_float(finger_meta_ctrl, 'autoRotate', default=0.1)
            mult = node.Node(finger_meta_ctrl + 'autoRotate', 'multiplyDivide', list_add=self.module_nodes)
            mult.connect(input1X=finger_meta_ctrl + '.autoRotate',
                         input1Y=finger_meta_ctrl + '.autoRotate',
                         input1Z=finger_meta_ctrl + '.autoRotate',
                         input2=finger_01_ctrl + '.rotate',
                         output=finger_meta_ctrl_ofs.node + '.rotate')

        # Declare in and out connectors for build
        self.in_conns_build = [hand_bone_zero.node]
        self.out_conn_build = hand_bone.node
