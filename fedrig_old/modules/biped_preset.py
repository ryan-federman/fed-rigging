import arm_module as arm
import hand_module as hand
import neck_module as neck
import spine_module as spine


def create_biped(**kwargs):
    l_arm = arm.ArmModule()
    r_arm = arm.ArmModule(side='R')
    c_spine = spine.SpineModule()
    c_neck = neck.NeckModule()

    l_arm.parent(c_spine)
    r_arm.parent(c_spine)
    c_neck.parent(c_spine)
