# from Qt import QtCompat, QtWidgets, QtCore, QtGui
from PySide2 import QtWidgets, QtCore, QtGui
from shiboken2 import wrapInstance

from maya import OpenMayaUI as omui
import maya.cmds as cmds
import maya.api.OpenMaya as om

import fedrig.module_list as mod_list
import fedrig.layout.layout_list as lyt_list
import fedrig.import_modules as mods
import custom_utils.control_shapes as cs


class FedRig(QtWidgets.QWidget):
    modules = []
    module_list = mod_list.ModuleList()

    def __init__(self):
        self.attribute_widgets = []

        self.load_modules()

        # if the ui exists then delete it
        old_window = omui.MQtUtil_findWindow('FedRig')
        if old_window:
            cmds.deleteUI('FedRig')

        # create a new dialog and give it the main maya window as its parent
        # store it as the parent for our current UI to be put inside
        parent = QtWidgets.QDialog(parent=getMayaMainWindow())

        # set its name so that we can find and delete it later
        parent.setObjectName('FedRig')
        parent.setWindowTitle('Fed Rig')

        super(FedRig, self).__init__(parent=parent)

        QtWidgets.QVBoxLayout(parent)

        self.buildUI()

        self.parent().layout().addWidget(self)

        parent.show()

    def buildUI(self):

        # create main layouts
        main_layout = QtWidgets.QVBoxLayout(self)
        create_button = QtWidgets.QPushButton('CREATE MODULE')
        create_button.setStyleSheet("background-color: black")
        create_button.setFixedHeight(20)

        attributes_button = QtWidgets.QPushButton('MODULE FUNCTIONS')
        attributes_button.setStyleSheet("background-color: black")
        attributes_button.setFixedHeight(20)

        create_grp = QtWidgets.QGroupBox()
        top_grp = QtWidgets.QGroupBox()
        self.attribute_grp = QtWidgets.QGroupBox()

        self.create_layout = create_layout = QtWidgets.QGridLayout(create_grp)
        self.module_layout = top_layout = QtWidgets.QGridLayout(top_grp)
        self.attribute_layout = QtWidgets.QGridLayout(self.attribute_grp)

        module_buttons_layout = QtWidgets.QGridLayout()

        main_layout.addWidget(create_button)
        main_layout.addWidget(create_grp)
        main_layout.addWidget(attributes_button)
        main_layout.addWidget(top_grp)
        main_layout.addWidget(self.attribute_grp)
        main_layout.addLayout(module_buttons_layout)

        # objects for creating module
        module_text = QtWidgets.QLabel()
        module_text.setText('Module Type:')
        self.create_module_cb = QtWidgets.QComboBox()
        for mod, names in mods.module_names.items():
            self.create_module_cb.addItem(mod)
        self.create_module_cb.currentIndexChanged.connect(self.set_module_type)

        name_text = QtWidgets.QLabel()
        name_text.setText('Name:')
        self.side_cb = QtWidgets.QComboBox()
        self.side_cb.addItem('C')
        self.side_cb.addItem('L')
        self.side_cb.addItem('R')
        self.name_line = QtWidgets.QLineEdit()

        create_button = QtWidgets.QPushButton('Create Module')
        create_button.clicked.connect(self.create_module)

        # text box for target module
        source_module_text = QtWidgets.QLabel()
        source_module_text.setText('Set Module:')
        self.module_cb = QtWidgets.QComboBox()
        self.module_cb.addItem('None')
        for mod in self.modules:
            self.module_cb.addItem(mod)
        self.module_cb.currentIndexChanged.connect(self.box_set_module)
        get_module_button = QtWidgets.QPushButton('Set Module')
        get_module_button.clicked.connect(self.set_module)

        current_module_text = QtWidgets.QLabel()
        current_module_text.setText('Current Module:')
        self.module_text = QtWidgets.QLabel()
        self.module_text.setText('None')

        current_parent_text = QtWidgets.QLabel()
        current_parent_text.setText('Current Parent:')
        self.parent_module_text = QtWidgets.QLabel()
        self.parent_module_text.setText('None')

        set_parent_text = QtWidgets.QLabel()
        set_parent_text.setText('Set Parent:')
        self.set_parent_cb = QtWidgets.QComboBox()
        self.set_parent_cb.addItem('None')
        for mod in self.modules:
            self.set_parent_cb.addItem(mod)
        self.set_parent_cb.currentIndexChanged.connect(self.box_set_parent)
        set_parent_button = QtWidgets.QPushButton()
        set_parent_button.setText('Set Parent')
        set_parent_button.clicked.connect(self.set_parent)

        mirror_text = QtWidgets.QLabel()
        mirror_text.setText('Mirror Module:')
        mirror_box = QtWidgets.QComboBox()
        mirror_box.addItem('All')
        mirror_box.addItem('Position')
        mirror_box.addItem('Attributes')
        mirror_button = QtWidgets.QPushButton()
        mirror_button.setText('Mirror')
        mirror_button.clicked.connect(self.mirror)

        lyt_space_text = QtWidgets.QLabel()
        lyt_space_text.setText('Change Layout Space:')
        self.lyt_space_cb = QtWidgets.QComboBox()
        self.lyt_space_cb.addItem('Parent')
        self.lyt_space_cb.addItem('World')
        lyt_space_button = QtWidgets.QPushButton()
        lyt_space_button.setText('Change Space')
        lyt_space_button.clicked.connect(self.change_layout_space)

        module_space_text = QtWidgets.QLabel()
        module_space_text.setText('Change Module Space:')
        self.module_space_cb = QtWidgets.QComboBox()
        self.module_space_cb.addItem('Parent')
        self.module_space_cb.addItem('World')
        module_space_button = QtWidgets.QPushButton()
        module_space_button.setText('Change Space')
        module_space_button.clicked.connect(self.change_module_space)

        delete_button = QtWidgets.QPushButton()
        delete_button.setText('Delete')
        delete_button.clicked.connect(self.delete)

        update_button = QtWidgets.QPushButton()
        update_button.setText('Update')
        update_button.clicked.connect(self.update)

        build_module_button = QtWidgets.QPushButton()
        build_module_button.setText('Build Module')
        build_module_button.clicked.connect(self.build)

        deconstruct_module_button = QtWidgets.QPushButton()
        deconstruct_module_button.setText('Deconstruct Module')
        deconstruct_module_button.clicked.connect(self.deconstruct)

        build_all_button = QtWidgets.QPushButton()
        build_all_button.setText('Build All')
        build_all_button.clicked.connect(self.build_all)

        deconstruct_all_button = QtWidgets.QPushButton()
        deconstruct_all_button.setText('Deconstruct All')
        deconstruct_all_button.clicked.connect(self.deconstruct_all)

        # Add widgets to layouts
        create_layout.addWidget(module_text, 0, 0, 1, 1)
        create_layout.addWidget(self.create_module_cb, 0, 1, 1, 2)
        create_layout.addWidget(name_text, 1, 0, 1, 1)
        create_layout.addWidget(self.side_cb, 1, 1, 1, 1)
        create_layout.addWidget(self.name_line, 1, 2, 1, 1)
        create_layout.addWidget(create_button, 2, 0, 1, 3)

        module_buttons_layout.addWidget(build_module_button, 0, 0, 1, 1)
        module_buttons_layout.addWidget(deconstruct_module_button, 0, 1, 1, 1)
        module_buttons_layout.addWidget(build_all_button, 1, 0, 1, 1)
        module_buttons_layout.addWidget(deconstruct_all_button, 1, 1, 1, 1)

        top_layout.addWidget(current_module_text, 0, 0, 1, 1)
        top_layout.addWidget(self.module_text, 0, 1, 1, 1)

        top_layout.addWidget(current_parent_text, 1, 0, 1, 1)
        top_layout.addWidget(self.parent_module_text, 1, 1, 1, 1)

        top_layout.addWidget(source_module_text, 2, 0, 1, 1)
        top_layout.addWidget(self.module_cb, 2, 1, 1, 1)
        top_layout.addWidget(get_module_button, 2, 2, 1, 1)

        top_layout.addWidget(set_parent_text, 3, 0, 1, 1)
        top_layout.addWidget(self.set_parent_cb, 3, 1, 1, 1)
        top_layout.addWidget(set_parent_button, 3, 2, 1, 1)

        top_layout.addWidget(mirror_text, 4, 0, 1, 1)
        top_layout.addWidget(mirror_box, 4, 1, 1, 1)
        top_layout.addWidget(mirror_button, 4, 2, 1, 1)

        top_layout.addWidget(lyt_space_text, 5, 0, 1, 1)
        top_layout.addWidget(self.lyt_space_cb, 5, 1, 1, 1)
        top_layout.addWidget(lyt_space_button, 5, 2, 1, 1)

        top_layout.addWidget(module_space_text, 6, 0, 1, 1)
        top_layout.addWidget(self.module_space_cb, 6, 1, 1, 1)
        top_layout.addWidget(module_space_button, 6, 2, 1, 1)

        top_layout.addWidget(delete_button, 7, 0, 1, 3)
        top_layout.addWidget(update_button, 8, 0, 1, 3)

        top_grp.setFixedHeight(top_grp.sizeHint().height())

        self.set_module_type()

    def build(self):
        # Get Module
        mod_name = self.module_cb.currentText()
        module = self.module_list.get_module(mod_name)

        module.build()
        self.reset_attribute_widgets(mod_name)

    def build_all(self):
        for name, mod in self.module_list.module_objs.items():
            mod.build()
        mod_name = self.module_cb.currentText()
        self.reset_attribute_widgets(mod_name)

        # mirror all controls that exist over the left side
        for each in cmds.ls('L_*CTRL'):
            r_ctrl = 'R_' + each.split('L_')[1]
            if cmds.ls(r_ctrl):
                cs.mirror_curve([each])

    def deconstruct(self):
        # Get Module
        mod_name = self.module_cb.currentText()
        self.module_text.setText(mod_name)
        module = self.module_list.get_module(mod_name)

        module.deconstruct()
        self.reset_attribute_widgets(mod_name)

    def deconstruct_all(self):
        for name, mod in self.module_list.module_objs.items():
            mod.deconstruct()
        mod_name = self.module_cb.currentText()
        self.reset_attribute_widgets(mod_name)

    def mirror(self):
        mod_name = self.module_cb.currentText()
        module = self.module_list.get_module(mod_name)
        module.mirror_module()

    def change_layout_space(self):
        '''
        Change the selected layouts space
        '''

        selection = cmds.ls(sl=True)
        cmds.select(clear=True)
        if selection:
            for each in selection:
                lyt = each
                if 'LAYOUT' in lyt:
                    layouts = lyt_list.LayoutList()
                    layout = layouts.get_layout(lyt)

                    space = self.lyt_space_cb.currentText()
                    layout.change_space(space)

    def change_module_space(self):
        '''
        Change the selected modules space
        '''
        space = self.module_space_cb.currentText()
        mod_name = self.module_cb.currentText()
        module = self.module_list.get_module(mod_name)
        module.change_space(space)

    def refresh(self):
        '''
        Refreshes the GUI with an updated module list
        '''
        # Clear
        self.module_cb.clear()
        self.set_parent_cb.clear()
        self.module_text.setText('None')
        self.parent_module_text.setText('None')

        self.load_modules()

        self.module_cb.addItem('None')
        for mod in self.modules:
            self.module_cb.addItem(mod)

        self.set_parent_cb.addItem('None')
        for mod in self.modules:
            self.set_parent_cb.addItem(mod)

    def update(self):
        '''
        Refreshes scene with new module code
        '''
        reload(mods)
        self.module_list.__class__.instances = None
        self.load_modules()

    def delete(self):
        '''
        Deletes the current module
        '''
        mod_name = self.module_text.text()
        module = self.module_list.get_module(mod_name)
        module.delete()
        self.refresh()

    def box_set_parent(self):
        '''
        Sets the parent of the current module when combo box is clicked
        '''
        mod_name = self.module_cb.currentText()
        parent_name = self.set_parent_cb.currentText()

        if mod_name:
            if mod_name == parent_name:
                if mod_name != 'None':
                    raise RuntimeError('Cannot parent module under itself')

            mod = self.module_list.get_module(mod_name)
            parent = self.module_list.get_module(parent_name)

            if mod:
                if parent:
                    mod.parent(parent)
                    self.parent_module_text.setText(parent_name)
                else:
                    if mod.parent_module:
                        mod.unparent()
                    self.parent_module_text.setText('None')

    def set_parent(self):
        selection = cmds.ls(sl=True)
        mod_name = self.module_cb.currentText()
        mod = self.module_list.get_module(mod_name)
        if selection:
            parent_name = cmds.attributeQuery('module', node=selection[0], listEnum=True)[0]
            if mod_name == parent_name:
                raise RuntimeError('Cannot parent module under itself')

            parent = self.module_list.get_module(parent_name)
            if mod:
                mod.parent(parent)
                self.parent_module_text.setText(parent_name)
                index = self.set_parent_cb.findText(parent_name)
                self.set_parent_cb.setCurrentIndex(index)
        else:
            mod.unparent()
            self.parent_module_text.setText('None')
            self.set_parent_cb.setCurrentIndex(0)

    def box_set_module(self):
        '''
        Sets current module when combo box is clicked
        '''

        # Delete old widgets
        for widget in self.attribute_widgets:
            self.attribute_layout.removeWidget(widget)
            self.delete_widget(widget)

        self.attribute_widgets = []

        mod_name = self.module_cb.currentText()
        self.module_text.setText(mod_name)

        # Add new widgets to module
        module = self.module_list.get_module(mod_name)
        row = 0
        if module:
            module.initialize_attributes()

            for key, attr in module.attributes.items():
                for column, widget in enumerate(attr.widgets):
                    self.attribute_layout.addWidget(widget, row, int(column), 1, 1)
                    self.attribute_widgets.append(widget)
                row += 1

            # If there is a parent change the widgets related to the parent
            if module.parent_module:
                parent = module.parent_module.side + '_' + module.parent_module.name
                self.parent_module_text.setText(parent)
                index = self.set_parent_cb.findText(parent)
                self.set_parent_cb.setCurrentIndex(index)
            else:
                self.parent_module_text.setText('None')
                self.set_parent_cb.setCurrentIndex(0)
        else:
            self.module_text.setText('None')
            self.parent_module_text.setText('None')
            self.module_cb.setCurrentIndex(0)
            self.set_parent_cb.setCurrentIndex(0)

    def set_module(self):
        '''
        Sets current module based on selection
        '''
        # Delete old widgets
        for widget in self.attribute_widgets:
            self.attribute_layout.removeWidget(widget)
            self.delete_widget(widget)

        self.attribute_widgets = []

        selection = cmds.ls(sl=True)
        mod_name = None
        if selection:
            try:
                mod_name = cmds.attributeQuery('module', node=selection[0], listEnum=True)[0]
            except RuntimeError:
                pass
        if mod_name:
            module = self.module_list.get_module(mod_name)
            row = 0
            if module:
                # Set module text and module combo box
                self.module_text.setText(mod_name)
                index = self.module_cb.findText(mod_name)
                self.module_cb.setCurrentIndex(index)

                module.initialize_attributes()
                for key, attr in module.attributes.items():
                    for column, widget in enumerate(attr.widgets):
                        self.attribute_layout.addWidget(widget, row, int(column), 1, 1)
                        self.attribute_widgets.append(widget)
                    row += 1

                # If there is a parent change the widgets related to the parent
                if module.parent_module:
                    parent = module.parent_module.side + '_' + module.parent_module.name
                    self.parent_module_text.setText(parent)
                    index = self.set_parent_cb.findText(parent)
                    self.set_parent_cb.setCurrentIndex(index)
                else:
                    self.parent_module_text.setText('None')
        else:
            self.module_text.setText('None')
            self.parent_module_text.setText('None')
            self.module_cb.setCurrentIndex(0)
            self.set_parent_cb.setCurrentIndex(0)

    def load_modules(self):
        '''
        Loads all modules in the current scene
        '''
        self.module_list = mod_list.ModuleList()
        self.modules = []
        for name, mod in self.module_list.module_objs.items():
            self.modules.append(name)

    def set_module_type(self):
        '''
        Sets default name and side for the given module type before creating
        '''
        mod = self.create_module_cb.currentText()
        side, name = mods.module_names[mod]

        # Set default side and name
        index = self.side_cb.findText(side)
        self.side_cb.setCurrentIndex(index)
        self.name_line.setText(name)

    def create_module(self):
        '''
        Creates a module from the given selection
        '''
        mod_type = self.create_module_cb.currentText()
        side = self.side_cb.currentText()
        name = self.name_line.text()

        mods.create_module(mod_type, side=side, name=name, initialize=False)

        self.refresh()

    def toggle_group(self, widget, max_height):
        height = widget.height()
        if height < 1:
            # widget.setFixedHeight(widget.sizeHint().height())
            widget.setFixedHeight(max_height)
        else:
            widget.setFixedHeight(0)

    def reset_attribute_widgets(self, mod_name):
        '''
        Resets the attribute widgets in the current layout
        :param mod_name:
        :return:
        '''
        # Delete old widgets
        for widget in self.attribute_widgets:
            self.attribute_layout.removeWidget(widget)
            self.delete_widget(widget)

        self.attribute_widgets = []

        module = self.module_list.get_module(mod_name)
        row = 0
        if module:
            # Set module text and module combo box
            self.module_text.setText(mod_name)
            index = self.module_cb.findText(mod_name)
            self.module_cb.setCurrentIndex(index)

            module.initialize_attributes()
            for key, attr in module.attributes.items():
                for column, widget in enumerate(attr.widgets):
                    self.attribute_layout.addWidget(widget, row, int(column), 1, 1)
                    self.attribute_widgets.append(widget)
                row += 1

            # If there is a parent change the widgets related to the parent
            if module.parent_module:
                parent = module.parent_module.side + '_' + module.parent_module.name
                self.parent_module_text.setText(parent)
                index = self.set_parent_cb.findText(parent)
                self.set_parent_cb.setCurrentIndex(index)
            else:
                self.parent_module_text.setText('None')

    def delete_widget(self, widget):
        cmds.undoInfo(openChunk=True)
        widget.setParent(None)
        widget.setVisible(False)
        widget.deleteLater()
        cmds.undoInfo(closeChunk=True)


def getMayaMainWindow():
    """
    Since Maya is Qt, we can parent our UIs to it.
    This means that we don't have to manage our UI and can leave it to Maya.
    Returns:
        QtWidgets.QMainWindow: The Maya MainWindow
    """
    # Get a reference to Maya's MainWindow
    win = omui.MQtUtil_mainWindow()

    # Wrap the window reference into Qt
    ptr = wrapInstance(long(win), QtWidgets.QMainWindow)

    return ptr
