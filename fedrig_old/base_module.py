import copy

import maya.cmds as cmds

import fedrig.layout.layout_list as lyt_list
import create.nodes as node
import custom_utils.attribute as attr
import custom_utils.shape as shape

reload(lyt_list)


class BaseModule(object):
    layouts = dict()
    attributes = dict()
    side = 'C'
    name = 'default'
    module_type = None

    parent_module = None
    child_modules = []

    in_conn_module = None
    out_conn_module = None

    in_conns_build = []
    out_conn_build = None

    module_grp = None
    module_nodes = []
    parts = []
    controls = []
    bones = []
    custom_nodes = []

    build_state = 'layout'
    scene_layouts = lyt_list.LayoutList()

    def __init__(self, name='default', side='C', initialize=False):
        self.side = side
        self.name = name
        self.module_grp = None
        self.module_nodes = []
        self.parts = []
        self.controls = []
        self.bones = []
        self.custom_nodes = []
        self.child_modules = []
        self.attributes = dict()
        self.scene_layouts = lyt_list.LayoutList()

        # If module is not being initialized create layout objects from scratch
        if not initialize:
            self.layout_objects()
            for name, lyt in self.layouts.items():
                attr.add_enum(lyt.dag.node, 'module', [self.side + '_' + self.name], visible=False)
        else:
            self.initialize()

        self.initialize_attributes()

    def module_layout_objects(self):
        pass

    def module_build(self):
        pass

    def module_attributes(self):
        '''
        Attributes specific to a module that influence a build

        Write dictionary of functions for how to return a value from
        '''
        pass

    # Not to be overridden
    def layout_objects(self):
        '''
        Wraps the local layout object creation script of the module
        '''
        self.layouts = dict()
        self.module_layout_objects()
        self.create_module()

        # Add all layout objects of this module to the scene's layout list
        if lyt_list.LayoutList.instances:
            layout_list = lyt_list.LayoutList.instances[0]
            for key, lyt in self.layouts.items():
                layout_list.add_layout(lyt)
        else:
            lyt_list.LayoutList()

    def initialize(self):
        '''
        Initializes layout objects of a module
        '''
        for key, lyt in self.layouts.items():
            self.layouts[key] = self.scene_layouts.get_layout(lyt)

    def initialize_attributes(self):
        '''
        Gets previously stored attributes and applies it to the attribute object
        '''
        self.module_attributes()
        if self.attributes:
            for key in self.attributes:
                if cmds.ls('{}.{}'.format(self.module_grp, key)):
                    value = cmds.getAttr('{}.{}'.format(self.module_grp, key))
                    self.attributes[key].default_value = value
                    self.attributes[key].set_module_value()

    def build(self):
        '''
        Wraps the local build script of the module
        '''
        # Create the module attribute objects to pull values from
        self.initialize_attributes()

        cmds.undoInfo(openChunk=True)
        cmds.select(clear=True)
        if self.build_state == 'layout':
            self.build_state = 'build'
            self.module_build()
            self.build_end()
        cmds.undoInfo(closeChunk=True)

    def deconstruct(self):
        '''
        Reset rig back to layout mode
        '''
        # Deletes all created nodes
        if self.build_state == 'layout':
            pass
        else:
            if self.module_nodes:
                nodes = cmds.ls(self.module_nodes)
                if nodes:
                    cmds.delete(nodes)
            else:
                nodes = cmds.attributeQuery('moduleNodes', node=self.module_grp, listEnum=True)
                nodes = nodes.split(':')
                nodes = cmds.ls(nodes)
                if nodes:
                    cmds.delete(nodes)

            if self.parts:
                nodes = cmds.ls(self.parts)
                if nodes:
                    cmds.delete(nodes)

            if self.bones:
                nodes = cmds.ls(self.bones)
                if nodes:
                    cmds.delete(nodes)

            if self.custom_nodes:
                nodes = cmds.ls(self.custom_nodes)
                if nodes:
                    cmds.delete(nodes)

            self.controls = []
            self.bones = []
            self.parts = []
            self.custom_nodes = []
            self.module_nodes = []

            self.in_conns_build = []
            self.out_conn_build = None

            # Resets class attributes
            self.reset_module()

            # Show layouts
            for key, lyt in self.layouts.items():
                cmds.setAttr(lyt.dag.node + '_ZERO.v', 1)
                if lyt.line_crv:
                    cmds.setAttr(lyt.line_crv + '.v', 1)

            self.build_state = 'layout'

    def create_module(self, custom_attrs=None):
        '''
        Creates transform node in outliner that defines the attributes of this module
        '''
        self.module_grp = node.Node('{}_{}_{}_MOD'.format(self.side, self.name, self.module_type), 'transform', suffix=False).node
        cmds.parent(self.module_grp, 'modules_rig_GRP')

        # Repurpose module objects as strings
        if self.parent_module:
            parent_module = ['{}_{}'.format(self.parent_module.side, self.parent_module.name)]
        else:
            parent_module = ['None']
        if self.child_modules:
            child_modules = []
            for mod in self.child_modules:
                child_modules.append('{}_{}'.format(mod.side, mod.name))
        else:
            child_modules = ['None']

        # Repurpose regular variables
        if self.module_nodes:
            module_nodes = self.module_nodes
        else:
            module_nodes = ['None']
        if self.in_conn_module:
            in_conn_module = [self.in_conn_module.dag.node]
        else:
            in_conn_module = ['None']
        if self.out_conn_module:
            out_conn_module = [self.out_conn_module.dag.node]
        else:
            out_conn_module = ['None']
        if self.in_conns_build:
            in_conns_build = self.in_conns_build
        else:
            in_conns_build = ['None']
        if self.out_conn_build:
            out_conn_build = [self.out_conn_build]
        else:
            out_conn_build = ['None']
        if self.controls:
            controls = self.controls
        else:
            controls = ['None']
        if self.parts:
            parts = self.parts
        else:
            parts = ['None']
        if self.bones:
            bones = self.bones
        else:
            bones = ['None']
        if self.custom_nodes:
            custom_nodes = self.custom_nodes
        else:
            custom_nodes = ['None']
        if self.module_type:
            module_type = [self.module_type]
        else:
            raise RuntimeError('No module type specified')

        attr.add_enum(self.module_grp, 'moduleNodes', module_nodes)
        attr.add_enum(self.module_grp, 'inConnModule', in_conn_module)
        attr.add_enum(self.module_grp, 'outConnModule', out_conn_module)
        attr.add_enum(self.module_grp, 'inConnsBuild', in_conns_build)
        attr.add_enum(self.module_grp, 'outConnBuild', out_conn_build)
        attr.add_enum(self.module_grp, 'parentModule', parent_module)
        attr.add_enum(self.module_grp, 'childModules', child_modules)

        attr.add_enum(self.module_grp, 'controls', controls)
        attr.add_enum(self.module_grp, 'parts', parts)
        attr.add_enum(self.module_grp, 'bones', bones)
        attr.add_enum(self.module_grp, 'customNodes', custom_nodes)
        attr.add_enum(self.module_grp, 'moduleType', module_type)
        attr.add_enum(self.module_grp, 'buildState', [self.build_state])

        # Extract dag nodes from layout objects
        layout_list = []
        for key, lyt in self.layouts.items():
            layout_list.append('{}-{}'.format(key, lyt.dag.node))

        attr.add_enum(self.module_grp, 'layouts', layout_list)

        if custom_attrs:
            for key, value in custom_attrs.items():
                # Add attribute with correct value to module group
                if type(value) == float:
                    attr.add_float(self.module_grp, key, min=value, max=value, default=value)
                elif type(value) == int:
                    attr.add_int(self.module_grp, key, min=value, max=value, default=value)
                elif type(value) == bool:
                    attr.add_bool(self.module_grp, key, default=value)
                elif type(value) == basestring:
                    attr.add_enum(self.module_grp, key, [value])

        self.initialize_attributes()

    def reset_module(self):
        '''
        Resets class attributes that are related to the build of the module
        '''
        custom_attrs = dict()
        # Add custom attributes to module group
        if self.attributes:
            for key in self.attributes:
                if cmds.ls('{}.{}'.format(self.module_grp, key)):
                    value = cmds.getAttr('{}.{}'.format(self.module_grp, key))
                    custom_attrs[key] = value
        cmds.delete(self.module_grp)

        self.create_module(custom_attrs=custom_attrs)

    def mirror_module(self):
        '''
        Mirrors module on the opposite side
        '''
        for key, lyt in self.layouts.items():
            lyt.mirror_opposite_layout()

    def parent(self, parent):
        '''
        Parents module under another module
        Args:
            parent(module object): module to parent the current module under
        '''
        if parent != self.parent_module:
            self.parent_module = parent
            self.parent_module.child_modules.append(self)
            parent_lyt = parent.out_conn_module
            parent_lyt.set_main_child(self.in_conn_module)

            self.parent_module.reset_module()
            self.reset_module()

    def change_space(self, space):
        '''
        Change space on the module
        '''
        self.in_conn_module.change_space(space)

    def delete(self):
        '''
        Deletes module
        '''

        self.deconstruct()
        for key, lyt in self.layouts.items():
            lyt.delete()
        cmds.delete(self.module_grp)

    def unparent(self):
        '''
        Re-parents current module under world
        '''
        self.parent_module.child_modules.remove(self)
        self.parent_module.reset_module()
        self.parent_module = None
        self.in_conn_module.unparent()

        self.reset_module()

    def build_end(self):
        '''
        Run at the end of each build script
        '''
        if self.module_grp:
            self.reset_module()
        else:
            self.create_module()

        # Parent all nodes under their appropriate groups
        for each in self.parts:
            cmds.parent(each, 'parts_rig_GRP')
        for each in self.controls:
            cmds.parent(each, 'C_global_OFS_CTRL')
        for each in self.bones:
            cmds.parent(each, 'bone_rig_GRP')
        if self.parent_module:
            if self.parent_module.build_state == 'build':
                for each in self.in_conns_build:
                    cmds.parent(each, self.parent_module.out_conn_build)
        if self.child_modules:
            for child in self.child_modules:
                if child.build_state == 'build':
                    for each in child.in_conns_build:
                        cmds.parent(each, self.out_conn_build)

        # Hide layouts
        for key, lyt in self.layouts.items():
            cmds.setAttr(lyt.dag.node + '_ZERO.v', 0)
            if lyt.line_crv:
                cmds.setAttr(lyt.line_crv + '.v', 0)

        # Color controls
        if self.side == 'L':
            color = 'light blue'
        elif self.side == 'R':
            color = 'light red'
        elif self.side == 'C':
            color = 'yellow'
        for each in self.controls:
            shape.change_color(each, color)

        # Add module attribute to all nodes for easy selection
        for each in self.module_nodes:
            attr.add_enum(each, 'module', [self.side + '_' + self.name], visible=False)

        cmds.select(clear=True)

    def finish(self):
        '''
        For when a rig no longer needs to be modified, cleans up module
        '''
        pass
