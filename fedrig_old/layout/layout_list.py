import maya.cmds as cmds

import layout_objects as lyt
import create.nodes as node

reload(lyt)


class LayoutList:
    '''
    Dictionary of layouts in the scene, used to initialize layouts in a new
    scene
    '''
    layout_objs = dict()
    instances = []

    def __init__(self):
        self.layout_objs = dict()
        self.initialize_objects()
        self.__class__.instances = []
        self.__class__.instances.append(self)

    def initialize_objects(self):
        '''
        Creates layout objects for a given layout object's dag
        Args:
            object_name (str): string name of layout object
        '''
        self.layout_objs = dict()

        # initialize all layouts as individual objects
        layouts_nodes = cmds.ls('*_LAYOUT')
        layout_names = []
        new_layouts = []
        if self.__class__.instances:
            for name, obj in self.__class__.instances[0].layout_objs.items():
                self.layout_objs[name] = obj

        for layout in layouts_nodes:
            if cmds.ls(layout + '.layoutName'):
                name = cmds.attributeQuery('layoutName', node=layout, listEnum=True)[0].split(':')[0]
                side = cmds.attributeQuery('layoutSide', node=layout, listEnum=True)[0].split(':')[0]
                layout_names.append('{}_{}'.format(side, name))

                # Add layouts that don't exist to layout objs list
                if '{}_{}'.format(side, name) not in self.layout_objs:
                    new_lyt = lyt.BaseLayoutObject(side=side, name=name, initialize=True)
                    new_layouts.append('{}_{}'.format(side, name))
                    # Initialize the rest of the attributes
                    new_lyt.dag = node.Node('{}_{}_LAYOUT'.format(side, name), 'joint', initialize=True)
                    new_lyt.zero = node.Node('{}_{}_LAYOUT_ZERO'.format(side, name), 'transform', initialize=True)
                    new_lyt.rot_object = node.Node('{}_{}_ROT_LAYOUT'.format(side, name), 'transform', initialize=True)
                    new_lyt.rot_nodes = cmds.attributeQuery('rotationNodes', node=layout, listEnum=True)[0].split(':')
                    new_lyt.default_position = None
                    new_lyt.default_rotation = None

                    # Add objects to this class's dictionary of layouts
                    self.layout_objs['{}_{}'.format(side, name)] = new_lyt

        # Delete objs that no longer exist from previous layout list
        for name, obj in self.layout_objs.items():
            if name not in layout_names:
                self.layout_objs.pop(name)

        # Initialize attributes that are not always on a layout object
        for layout in new_layouts:
            if cmds.ls(layout + '.layoutName'):
                new_lyt = self.layout_objs[layout.split('_LAYOUT')[0]]

                transforms = cmds.ls(transforms=True)
                top_grp = None
                for each in transforms:
                    top_attr = cmds.ls(each + '.topRigGrp')
                    if top_attr:
                        top_grp = each
                if top_grp:
                    new_lyt.top_grp = top_grp

                if cmds.ls(layout + '.parentObject'):
                    parent = cmds.attributeQuery('parentObject', node=layout, listEnum=True)[0].split(':')[0]
                    parent = parent.split('_LAYOUT')[0]
                    new_lyt.parent_object = self.layout_objs[parent]

                if cmds.ls(layout + '.childObject'):
                    child = cmds.attributeQuery('childObject', node=layout, listEnum=True)[0].split(':')[0]
                    child = child.split('_LAYOUT')[0]
                    new_lyt.child_object = self.layout_objs[child]

                if cmds.ls(layout + '.children'):
                    children = cmds.attributeQuery('children', node=layout, listEnum=True)[0].split(':')
                    children_objs = []
                    for each in children:
                        each = each.split('_LAYOUT')[0]
                        children_objs += [self.layout_objs[each]]
                    new_lyt.children = children_objs

                if cmds.ls(layout + '.lineCrv'):
                    new_lyt.line_crv = cmds.attributeQuery('lineCrv', node=layout, listEnum=True)[0].split(':')[0]

                if cmds.ls(layout + '.spaceNode'):
                    new_lyt.space_blend = cmds.attributeQuery('spaceNode', node=layout, listEnum=True)[0].split(':')[0]

    def get_layout(self, layout_name):
        '''
        Gives the layout object for it's string name
        Args:
            object_name (str): string name of layout object to return
        Return:
            Layout Object: instance of layout object class with all loaded attrs
        '''
        if '_LAYOUT' in layout_name:
            new_lyt = layout_name.split('_LAYOUT')[0]
        else:
            new_lyt = layout_name

        return self.layout_objs[new_lyt]

    def add_layout(self, layout):
        '''
        Pass in a layout object to add to this layout list
        Args:
            layout (BaseLayoutObject): layout object to add to list
        '''

        layout_name = '{}_{}'.format(layout.side, layout.name)
        self.layout_objs[layout_name] = layout
