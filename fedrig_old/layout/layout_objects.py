import maya.cmds as cmds
import maya.api.OpenMaya as om
import maya.mel as mel

import custom_utils.attribute as attr
import custom_utils.control_shapes as cs
import custom_utils.shape as shape
import custom_utils.maths as maths
import custom_utils.generic as gen
import custom_utils.constraint as con
import create.nodes as node

axis_keys = ['+x', '-x', '+y', '-y', '+z', '-z']
aim_keys = ['Local', 'Parent', 'Child']
up_keys = ['Local', 'Scene', 'Plane']


class BaseLayoutObject:
    side = 'C'
    name = 'default'
    dag = None
    zero = None
    rot_object = None
    module = None
    parent_object = None
    child_object = None
    children = []
    mirror_side = None
    line_crv = None
    rot_nodes = []
    default_position = None
    default_rotation = None
    default_aim = '+x'
    default_up = '+y'
    default_aim_type = 'Local'
    default_up_type = 'Local'
    default_plane_type = None
    top_grp = None
    space_blend = None
    custom_nodes = []

    position = None

    def __init__(self, side='C', name='default', create_group=True, initialize=False):
        '''
        Args:
            side: side of object
            name: base name of object
            create_group: whether to create a group structure for layout objects to
                          live in
        '''
        # initialize all object attributes
        self.name = None
        self.dag = None
        self.zero = None
        self.rot_object = None
        self.module = None
        self.parent_object = None
        self.child_object = None
        self.children = []
        self.side = 'C'
        self.line_crv = None
        self.rot_nodes = []
        self.default_position = None
        self.default_rotation = None
        self.top_grp = None
        self.space_blend = None
        self.custom_nodes = []

        self.name = name
        self.side = side

        if self.side == 'L':
            self.mirror_side = 'R'
        elif self.side == 'R':
            self.mirror_side = 'L'

        # Initialize top group of rig if it exists
        transforms = cmds.ls(transforms=True)
        if transforms:
            for each in transforms:
                grp_attr = cmds.ls(each + '.topRigGrp')
                if grp_attr:
                    self.top_grp = each

        # Indicates whether this object is being used to create a layout or initialize an existing one
        if not initialize:
            self.create_main_object()
            self.create_rotation_object()

            # Create group structure if called on then setup the dags for this layout object
            layout_grp = None
            if create_group:
                transforms = cmds.ls(transforms=True)
                for each in transforms:
                    grp_attr = cmds.ls(each + '.layoutGrp')
                    if grp_attr:
                        layout_grp = each
                if not layout_grp:
                    self.top_grp = self.create_group_structure()
                    layout_grp = 'layouts_rig_GRP'

                cmds.parent(self.zero.node, layout_grp)

        # make all joints at base scale of 1 for accurate layout objects
        mel.eval('jointDisplayScale 1;')

    def create_main_object(self):
        colors = {'C': 'yellow',
                  'L': 'light blue',
                  'R': 'light red'}
        color = colors[self.side]
        self.zero = node.Node('{}_{}_LAYOUT_ZERO'.format(self.side, self.name), 'transform', suffix=False)
        self.dag = node.Node('{}_{}_LAYOUT'.format(self.side, self.name), 'joint', drawLabel=1, type=18, suffix=False)
        cmds.setAttr(self.dag.node + '.otherType', self.name, type='string')

        attr.add_world_transforms(self.dag.node)
        attr.add_parent_world_position(self.dag.node)
        shape.change_color(self.dag.node, color)

        attr.lock_attributes(self.dag.node, ['sx', 'sy', 'sz', 'v'])

    def create_rotation_object(self):
        self.rot_object = cs.create_nurbscurve('{}_{}_ROT_LAYOUT'.format(self.side, self.name), 'transformation_axis')
        self.rot_object = node.Node(self.rot_object, 'transform', initialize=True)
        cmds.parent(self.rot_object.node, self.dag.node)

        aim_options = ['Local', 'Parent']
        if self.child_object:
            aim_options.append('Child')
        plane_options = self.plane_check()
        up_options = ['Local', 'Scene']
        if plane_options:
            up_options.append('Plane')

        # add attributes to dag node that affect rotation matrix
        self.main_object_attributes(aim_options, up_options)

        # connect rotation object attributes
        self.rot_object.connect(scaleX_in=self.dag.node + '.rotRadius',
                                scaleY_in=self.dag.node + '.rotRadius',
                                scaleZ_in=self.dag.node + '.rotRadius',
                                visibility=self.dag.node + '.rotDisplay')
        attr.hide_attributes(self.rot_object.node, ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v'])

        # set default values for attributes
        self.dag.connect(up_in=2)

        # matrix node for rotation object
        rotation_matrix = node.Node('{}_rotObject'.format(self.dag.node), 'fourByFourMatrix')
        rot_mtx_mult = node.Node(rotation_matrix.node, 'multMatrix')
        rot_dm = node.Node(rotation_matrix.node, 'decomposeMatrix')
        self.rot_nodes += [rotation_matrix.node, rot_mtx_mult.node, rot_dm.node]

        rot_mtx_mult.connect(matrixIn_0=rotation_matrix.node + '.output',
                             matrixIn_1=self.rot_object.node + '.parentInverseMatrix[0]')
        rot_dm.connect(inputMatrix=rot_mtx_mult.node + '.matrixSum',
                       outputTranslate=self.rot_object.node + '.translate',
                       outputRotate=self.rot_object.node + '.rotate')

        ########################################################################
        # create aim vectors
        aim_choice = node.Node('{}_aim'.format(self.dag.node), 'choice',
                               selector=self.dag.node + '.aimType')
        self.rot_nodes += [aim_choice.node]

        # create local aim vector
        local_aim_vecp = node.Node('{}_localAim'.format(self.dag.node), 'vectorProduct')
        local_aim_pma = node.Node('{}_localAim'.format(self.dag.node), 'plusMinusAverage')
        self.rot_nodes += [local_aim_vecp.node, local_aim_pma.node]

        local_aim_vecp.connect(operation=4, input1X=1, matrix=self.dag.node + '.worldMatrix[0]')
        local_aim_pma.connect(operation=2,
                              output3D=aim_choice.node + '.input[0]',
                              input3D_0=local_aim_vecp.node + '.output',
                              input3D_1=self.dag.node + '.worldPosition')

        # create parent aim vector
        par_aim_pma = node.Node('{}_parentAim'.format(self.dag.node), 'plusMinusAverage',
                                operation=2,
                                output3D=aim_choice.node + '.input[1]',
                                input3D_1=self.dag.node + '.worldPosition')
        self.rot_nodes += [par_aim_pma.node]

        if self.parent_object:
            par_aim_pma.connect(input3D_0=self.parent_object.dag.node + '.worldPosition')
        else:
            par_aim_pma.connect(input3D_0=self.dag.node + '.parentWorldPosition')

        # create child aim vector
        if self.child_object:
            child_aim_pma = node.Node(self.dag.node + '_childAim', 'plusMinusAverage',
                                      operation=2,
                                      output3D=aim_choice.node + '.input[2]',
                                      input3D_0=self.child_object.dag.node + '.worldPosition',
                                      input3D_1=self.dag.node + '.worldPosition')
            self.rot_nodes += [child_aim_pma.node]

        # create up vectors
        up_choice = node.Node(self.dag.node + '_up', 'choice', selector=self.dag.node + '.upType')
        self.rot_nodes += [up_choice.node]

        # create local up vector
        local_up_vecp = node.Node(self.dag.node + '_localUp', 'vectorProduct')
        local_up_pma = node.Node(self.dag.node + 'localUp', 'plusMinusAverage')
        self.rot_nodes += [local_up_vecp.node, local_up_pma.node]

        local_up_vecp.connect(operation=4,
                              input1Y=1,
                              matrix=self.dag.node + '.worldMatrix[0]')

        local_up_pma.connect(operation=2,
                             input3D_0=local_up_vecp.node + '.output',
                             input3D_1=self.dag.node + '.worldPosition',
                             output3D=up_choice.node + '.input[0]')

        # create scene up vector
        scene_up_vecp = node.Node(self.dag.node + '_sceneUp', 'vectorProduct', suffix=True,
                                  operation=0,
                                  input1Y=1,
                                  output=up_choice.node + '.input[1]')
        self.rot_nodes += [scene_up_vecp.node]

        # create plane up vectors
        if plane_options:
            attr.add_enum(self.dag.node, 'planeType', plane_options, keyable=False)
            plane_choice = node.Node(self.dag.node + '_plane', 'choice',
                                     selector=self.dag.node + '.planeType',
                                     output=up_choice.node + '.input[2]')
            self.rot_nodes += [plane_choice.node]

            plane_index = 0
            if 'Neighbours' in plane_options:
                neighbour_normal = self.create_plane_normal(self.parent_object, self, self.child_object, 'neighbour')
                neighbour_normal.connect(output='{}.input[{}]'.format(plane_choice.node, str(plane_index)))
                plane_index += 1
            if 'Children' in plane_options:
                children_normal = self.create_plane_normal(self, self.child_object, self.child_object.child_object, 'children')
                children_normal.connect(output='{}.input[{}]'.format(plane_choice.node, str(plane_index)))
                plane_index += 1
            if 'Parents' in plane_options:
                parents_normal = self.create_plane_normal(self.parent_object.parent_object, self.parent_object, self, 'parents')
                parents_normal.connect(output='{}.input[{}]'.format(plane_choice.node, str(plane_index)))

        # conditions for if the set option calls for a negative vector
        aim_neg_cond = node.Node(self.dag.node + '_aim_neg', 'condition')
        up_neg_cond = node.Node(self.dag.node + '_up_neg', 'condition')
        aim_neg_mult = node.Node(self.dag.node + 'aim_neg', 'multiplyDivide')
        up_neg_mult = node.Node(self.dag.node + 'up_neg', 'multiplyDivide')
        self.rot_nodes += [aim_neg_cond.node, up_neg_cond.node, aim_neg_mult.node, up_neg_mult.node]

        aim_neg_mult.connect(operation=1,
                             input1=aim_choice.node + '.output',
                             input2=(-1, -1, -1))

        up_neg_mult.connect(operation=1,
                            input1=up_choice.node + '.output',
                            input2=(-1, -1, -1))

        aim_neg_cond.connect(operation=0,
                             colorIfTrue=aim_choice.node + '.output',
                             colorIfFalse=aim_neg_mult.node + '.output')
        cmds.expression(s='{}.firstTerm = {}.aim % 2;'.format(aim_neg_cond.node, self.dag.node))

        up_neg_cond.connect(operation=0,
                            colorIfTrue=up_choice.node + '.output',
                            colorIfFalse=up_neg_mult.node + '.output')
        cmds.expression(s='{}.firstTerm = {}.up % 2;'.format(up_neg_cond.node, self.dag.node))

        # create cross product of aim and up vectors
        side_vec_cp = node.Node(self.dag.node + '_sideVec', 'vectorProduct',
                                operation=2,
                                normalizeOutput=1,
                                input1=up_neg_cond.node + '.outColor',
                                input2=aim_neg_cond.node + '.outColor')

        # create cross product of aim and side vectors for final up vector
        up_vec_cp = node.Node(self.dag.node + '_upVec', 'vectorProduct',
                              operation=2,
                              normalizeOutput=1,
                              input1=aim_neg_cond.node + '.outColor',
                              input2=side_vec_cp.node + '.output')
        self.rot_nodes += [side_vec_cp.node, up_vec_cp.node]

        #######################################################################
        # assign created aim, up, and side to the appropriate vectors

        # create condition nodes for x vector
        x_aim_cond = node.Node(self.dag.node + '_xvec_aim', 'condition')
        x_up_cond = node.Node(self.dag.node + '_xvec_up', 'condition')
        x_vec_normalize = node.Node(self.dag.node + '_xvec_normalize', 'vectorProduct')
        self.rot_nodes += [x_aim_cond.node, x_up_cond.node, x_vec_normalize.node]

        x_aim_cond.connect(operation=5,
                           firstTerm=self.dag.node + '.aim',
                           secondTerm=1,
                           colorIfTrue=aim_neg_cond.node + '.outColor',
                           colorIfFalse=side_vec_cp.node + '.output')

        x_up_cond.connect(operation=5,
                          firstTerm=self.dag.node + '.up',
                          secondTerm=1,
                          colorIfTrue=up_vec_cp.node + '.output',
                          colorIfFalse=x_aim_cond.node + '.outColor')

        x_vec_normalize.connect(operation=0,
                                normalizeOutput=1,
                                input1=x_up_cond.node + '.outColor')

        # create nodes for y vector
        y_aim1_cond = node.Node(self.dag.node + '_yvec_aim_01', 'condition')
        y_aim2_cond = node.Node(self.dag.node + '_yvec_aim_02', 'condition')
        y_up1_cond = node.Node(self.dag.node + '_yvec_up_01', 'condition')
        y_up2_cond = node.Node(self.dag.node + '_yvec_up_02', 'condition')
        y_vec_normalize = node.Node(self.dag.node + '_yvec_normalize', 'vectorProduct')
        self.rot_nodes += [y_aim1_cond.node, y_aim2_cond.node, y_up1_cond.node, y_up2_cond.node, y_vec_normalize.node]

        y_aim1_cond.connect(operation=0,
                            firstTerm=self.dag.node + '.aim',
                            secondTerm=2,
                            colorIfTrue=aim_neg_cond.node + '.outColor',
                            colorIfFalse=side_vec_cp.node + '.output')

        y_aim2_cond.connect(operation=0,
                            firstTerm=self.dag.node + '.aim',
                            secondTerm=3,
                            colorIfTrue=aim_neg_cond.node + '.outColor',
                            colorIfFalse=y_aim1_cond.node + '.outColor')

        y_up1_cond.connect(operation=0,
                           firstTerm=self.dag.node + '.up',
                           secondTerm=2,
                           colorIfTrue=up_vec_cp.node + '.output',
                           colorIfFalse=y_aim2_cond.node + '.outColor')

        y_up2_cond.connect(operation=0,
                           firstTerm=self.dag.node + '.up',
                           secondTerm=3,
                           colorIfTrue=up_vec_cp.node + '.output',
                           colorIfFalse=y_up1_cond.node + '.outColor')

        y_vec_normalize.connect(operation=0,
                                normalizeOutput=1,
                                input1=y_up2_cond.node + '.outColor')

        # create nodes for z vector
        z_aim_cond = node.Node(self.dag.node + '_zvec_aim', 'condition')
        z_up_cond = node.Node(self.dag.node + '_zvec_up', 'condition')
        z_vec_normalize = node.Node(self.dag.node + '_zvec_normalize', 'vectorProduct')
        self.rot_nodes += [z_aim_cond.node, z_up_cond.node, z_vec_normalize.node]

        z_aim_cond.connect(operation=3,
                           firstTerm=self.dag.node + '.aim',
                           secondTerm=4,
                           colorIfTrue=aim_neg_cond.node + '.outColor',
                           colorIfFalse=side_vec_cp.node + '.output')

        z_up_cond.connect(operation=3,
                          firstTerm=self.dag.node + '.up',
                          secondTerm=4,
                          colorIfTrue=up_vec_cp.node + '.output',
                          colorIfFalse=z_aim_cond.node + '.outColor')

        z_vec_normalize.connect(operation=0,
                                normalizeOutput=1,
                                input1=z_up_cond.node + '.outColor')

        # connect nodes up to matrix
        rotation_matrix.connect(in00=x_vec_normalize.node + '.outputX',
                                in01=x_vec_normalize.node + '.outputY',
                                in02=x_vec_normalize.node + '.outputZ',
                                in10=y_vec_normalize.node + '.outputX',
                                in11=y_vec_normalize.node + '.outputY',
                                in12=y_vec_normalize.node + '.outputZ',
                                in20=z_vec_normalize.node + '.outputX',
                                in21=z_vec_normalize.node + '.outputY',
                                in22=z_vec_normalize.node + '.outputZ',
                                in30=self.dag.node + '.worldPositionX',
                                in31=self.dag.node + '.worldPositionY',
                                in32=self.dag.node + '.worldPositionZ')

        # create attribute to store node names of rotation object
        attr.add_enum(self.dag.node, 'rotationNodes', self.rot_nodes, visible=False)

        # add all custom nodes to custom node asset
        if not cmds.ls('layouts_custom_nodes'):
            cmds.container(name='layouts_custom_nodes')

        cmds.container('layouts_custom_nodes', edit=True, addNode=self.custom_nodes)

    def reset_rotation_object(self):
        # Get previously held attributes to reapply on this object
        aim = cmds.getAttr(self.dag.node + '.aim')
        up = cmds.getAttr(self.dag.node + '.up')
        aim_type = cmds.getAttr(self.dag.node + '.aimType')
        up_type = cmds.getAttr(self.dag.node + '.upType')
        plane_type = 0
        if cmds.ls(self.dag.node + '.planeType'):
            plane_type = cmds.getAttr(self.dag.node + '.planeType')

        # delete rotation object's nodes
        self.rot_nodes = cmds.attributeQuery('rotationNodes', node=self.dag.node, listEnum=True)[0].split(':')
        for each in self.rot_nodes:
            try:
                cmds.delete(each)
            except ValueError:
                pass

        # delete rotation object
        cmds.delete(self.rot_object.node)

        # delete all created custom attributes on main object
        cmds.setAttr(self.dag.node + '.DISPLAY', lock=False)
        cmds.deleteAttr(self.dag.node + '.DISPLAY')
        cmds.deleteAttr(self.dag.node + '.rotRadius')
        cmds.deleteAttr(self.dag.node + '.rotDisplay')
        cmds.deleteAttr(self.dag.node + '.nameDisplay')
        cmds.deleteAttr(self.dag.node + '.aim')
        cmds.deleteAttr(self.dag.node + '.up')
        cmds.deleteAttr(self.dag.node + '.aimType')
        cmds.deleteAttr(self.dag.node + '.upType')
        cmds.setAttr(self.dag.node + '.ROTATION', lock=False)
        cmds.deleteAttr(self.dag.node + '.ROTATION')
        cmds.deleteAttr(self.dag.node + '.layoutSide')
        cmds.deleteAttr(self.dag.node + '.layoutName')
        cmds.deleteAttr(self.dag.node + '.rotationNodes')

        plane_attr = cmds.ls(self.dag.node + '.planeType')
        if plane_attr:
            cmds.deleteAttr(self.dag.node + '.planeType')
        if cmds.ls(self.dag.node + '.parentObject'):
            cmds.deleteAttr(self.dag.node + '.parentObject')
        if cmds.ls(self.dag.node + '.childObject'):
            cmds.deleteAttr(self.dag.node + '.childObject')
        if cmds.ls(self.dag.node + '.children'):
            cmds.deleteAttr(self.dag.node + '.children')
        if cmds.ls(self.dag.node + '.lineCrv'):
            cmds.deleteAttr(self.dag.node + '.lineCrv')
        if cmds.ls(self.dag.node + '.spaceNode'):
            cmds.deleteAttr(self.dag.node + '.spaceNode')
        if cmds.ls(self.dag.node + '.customNodes'):
            cmds.deleteAttr(self.dag.node + '.customNodes')

        # reset rotation attributes of modules
        self.rot_nodes = []
        self.rot_object = None

        self.create_rotation_object()

        # Reset attributes to old attribute values
        cmds.setAttr(self.dag.node + '.aim', aim)
        cmds.setAttr(self.dag.node + '.up', up)
        try:
            cmds.setAttr(self.dag.node + '.aimType', aim_type)
        except RuntimeError:
            pass
        try:
            cmds.setAttr(self.dag.node + '.upType', up_type)
        except RuntimeError:
            pass
        if cmds.ls(self.dag.node + '.planeType'):
            try:
                cmds.setAttr(self.dag.node + '.planeType', plane_type)
            except RuntimeError:
                pass

    def set_parent(self, layout_object):
        '''
        Parents given layout under a given layout object
        Args:
            layout_object : layout object to be parented under
        '''
        for each in self.custom_nodes:
            if cmds.ls(each):
                cmds.delete(each)
        self.custom_nodes = []
        self.parent_object = None
        self.parent_object = layout_object
        self.parent_object.children.append(self)
        parent_dag = self.parent_object.dag.node

        constraint = con.matrix_constraint(parent_dag, self.zero.node, connect='')
        self.custom_nodes += [constraint]
        dcpm = node.Node(self.dag.node, 'decomposeMatrix')
        space_blend = node.Node('{}_{}_LYT'.format(self.side, self.name), 'matrixBlend')
        self.custom_nodes += [space_blend.node]
        space_blend.connect(input1=constraint + '.outMatrix',
                            output=dcpm.node + '.inputMatrix')
        dcpm.connect(outputTranslate=self.zero.node + '.translate',
                     outputRotate=self.zero.node + '.rotate',
                     outputScale=self.zero.node + '.scale')

        self.space_blend = space_blend.node
        self.reset_rotation_object()

    def unparent(self):
        '''
        Parents layout to world
        '''
        if self.parent_object.child_object == self:
            cmds.delete(self.parent_object.line_crv)
            self.parent_object.line_crv = None
            self.parent_object.child_object = None
            self.parent_object.children.remove(self)
        elif self in self.parent_object.children:
            self.parent_object.children.remove(self)
        self.parent_object.reset_rotation_object()
        self.parent_object = None

        if cmds.ls(self.space_blend):
            cmds.delete(self.space_blend)
        self.space_blend = None

        self.reset_rotation_object()

    def set_main_child(self, layout_object):
        '''
        Parents given layout under the current layout and sets it as the main child
        Args:
            layout_object : layout object to parent to current layout
        '''

        self.child_object = None
        self.child_object = layout_object

        layout_object.set_parent(self)
        if self.parent_object:
            self.parent_object.reset_rotation_object()
        if layout_object.child_object:
            layout_object.child_object.reset_rotation_object()

        layout_object.reset_rotation_object()

        if self.line_crv:
            cmds.delete(self.line_crv)
        self.child_line()

        self.reset_rotation_object()

    def add_children(self, layout_object):
        '''
        Parents given layout under the current layout
        Args:
            layout_object : layout object to parent to current layout
        '''
        if layout_object not in self.children:
            self.children.append(layout_object)

    def get_output_matrix(self, world=True, normalize=False):
        '''
        Returns matrix from the given layout objects rot object
        Args:
            world (bool): if the matrix is given back in world space
            normalize (bool): if the rotation vectors should be normalized to produce a 1, 1, 1 scale
        Return:
             om.MMatrix: matrix of layout object
        '''
        mtx = om.MMatrix(cmds.xform(self.rot_object.node, matrix=True, ws=world, q=True))
        if normalize:
            x, y, z, pos = maths.matrix_to_vectors(mtx)
            x = x.normal()
            y = y.normal()
            z = z.normal()
            mtx = maths.create_matrix(x, y, z, pos)

        return mtx

    def plane_check(self):
        '''
        Check to see what relatives this layout has in order to build the normals
        of planes
        '''
        plane_types = []
        if self.parent_object and self.child_object:
            plane_types.append('Neighbours')

        if self.child_object:
            if self.child_object.child_object:
                plane_types.append('Children')

        if self.parent_object:
            if self.parent_object.parent_object:
                plane_types.append('Parents')

        return plane_types

    def create_plane_normal(self, root, mid, end, plane_type):
        vec_sub_1 = node.Node('{}_{}_vec1'.format(self.dag.node, plane_type), 'plusMinusAverage')
        vec_sub_2 = node.Node('{}_{}_vec2'.format(self.dag.node, plane_type), 'plusMinusAverage')
        cross_prod = node.Node('{}_{}_cp'.format(self.dag.node, plane_type), 'vectorProduct')
        self.rot_nodes += [vec_sub_1.node, vec_sub_2.node, cross_prod.node]

        vec_sub_1.connect(operation=2,
                          input3D_0=mid.dag.node + '.worldPosition',
                          input3D_1=root.dag.node + '.worldPosition')
        vec_sub_2.connect(operation=2,
                          input3D_0=end.dag.node + '.worldPosition',
                          input3D_1=mid.dag.node + '.worldPosition')
        cross_prod.connect(operation=2,
                           input1=vec_sub_1.node + '.output3D',
                           input2=vec_sub_2.node + '.output3D')

        return cross_prod

    def child_line(self):
        self.line_crv = cs.create_nurbscurve(self.dag.node + '_child_line_CRV', 'line')
        self.dag.connect(worldPosition_out=self.line_crv + '.cv[0]')
        self.child_object.dag.connect(worldPosition_out=self.line_crv + '.cv[1]')

        cmds.setAttr(self.line_crv + '.overrideEnabled', 1)
        cmds.setAttr(self.line_crv + '.overrideDisplayType', 1)

        if self.top_grp:
            transforms = cmds.ls(transforms=True)
            for each in transforms:
                grp_attr = cmds.ls(each + '.layoutDNTGrp')
                if grp_attr:
                    layout_dnt_grp = each
            cmds.parent(self.line_crv, layout_dnt_grp)

    def main_object_attributes(self, aim_options, up_options):
        # Create attributes to re-initialize layout object
        if self.parent_object:
            attr.add_enum(self.dag.node, 'parentObject', [self.parent_object.dag.node], visible=False)
        if self.child_object:
            attr.add_enum(self.dag.node, 'childObject', [self.child_object.dag.node], visible=False)
        if self.children:
            children = []
            for each in self.children:
                children.append(each.dag.node)
            attr.add_enum(self.dag.node, 'children', children, visible=False)
        if self.line_crv:
            attr.add_enum(self.dag.node, 'lineCrv', [self.line_crv], visible=False)
        attr.add_enum(self.dag.node, 'layoutSide', [self.side], visible=False)
        attr.add_enum(self.dag.node, 'layoutName', [self.name], visible=False)
        if self.custom_nodes:
            attr.add_enum(self.dag.node, 'customNodes', self.custom_nodes, visible=False)

        if self.space_blend:
            attr.add_enum(self.dag.node, 'spaceNode', [self.space_blend], visible=False)

        # Create normal/visible attributes for layout object
        attr.add_title(self.dag.node, 'DISPLAY')
        attr.add_float(self.dag.node, 'rotRadius', max=100.0, keyable=False)
        attr.add_bool(self.dag.node, 'rotDisplay')
        attr.add_bool(self.dag.node, 'nameDisplay')

        self.dag.connect(nameDisplay_out=self.dag.node + '.drawLabel')

        attr.add_title(self.dag.node, 'ROTATION')

        attr.add_enum(self.dag.node, 'aim', ['+x', '-x', '+y', '-y', '+z', '-z'], keyable=False)
        attr.add_enum(self.dag.node, 'up', ['+x', '-x', '+y', '-y', '+z', '-z'], keyable=False)
        attr.add_enum(self.dag.node, 'aimType', aim_options, keyable=False)
        attr.add_enum(self.dag.node, 'upType', up_options, keyable=False)

        # Set attributes to default values of object
        cmds.setAttr(self.dag.node + '.aim', axis_keys.index(self.default_aim))
        cmds.setAttr(self.dag.node + '.up', axis_keys.index(self.default_up))
        cmds.setAttr(self.dag.node + '.aimType', aim_keys.index(self.default_aim_type))
        cmds.setAttr(self.dag.node + '.upType', up_keys.index(self.default_up_type))

    def set_position(self, position):
        '''
        Sets position of main object to a world coordinate
        Args:
            position tuple(): world position to move object to
        '''
        cmds.xform(self.dag.node, t=(position[0], position[1], position[2]), ws=True)

    def mirror_self(self):
        '''
        Mirrors current layout over the x axis
        '''

        self.mirror_dag(self.dag.node, self.dag.node)

    def mirror_opposite_layout(self, custom_mirror=[]):
        '''
        Mirrors the layout object that has the same name with the opposite side

        Mirrors both transformational attributes and copies attribute values to
        opposite layout.
        '''
        if not custom_mirror:
            if self.mirror_side:
                opposite_dag = self.mirror_side + '_' + gen.remove_prefix(self.dag.node)
                attr.copy_attributes(self.dag.node, opposite_dag)
                self.mirror_dag(self.dag.node, opposite_dag)

        if custom_mirror:
            opposite_dag = custom_mirror[1] + self.dag.node.split(custom_mirror[0])[1]
            attr.copy_attributes(self.dag.node, opposite_dag)
            self.mirror_dag(self.dag.node, opposite_dag)

    def change_space(self, space):
        '''
        Changes space of current object
        Args:
            space (str): string "world" or "parent"
        '''
        pos = cmds.xform(self.dag.node, t=True, ws=True, q=True)
        rot = cmds.xform(self.dag.node, ro=True, ws=True, q=True)
        if space == 'World':
            if self.space_blend:
                cmds.setAttr(self.space_blend + '.blend', 1)
        elif space == 'Parent':
            if self.space_blend:
                cmds.setAttr(self.space_blend + '.blend', 0)

        cmds.xform(self.dag.node, t=(pos[0], pos[1], pos[2]), ws=True)
        cmds.xform(self.dag.node, ro=(rot[0], rot[1], rot[2]), ws=True)

    def apply_transforms(self):
        '''
        Applies transformations from rotation object to layout object
        '''

        pos = cmds.xform(self.rot_object.node, t=True, ws=True, q=True)
        rot = cmds.xform(self.rot_object.node, ro=True, ws=True, q=True)

        cmds.xform(self.dag.node, t=(pos[0], pos[1], pos[2]), ws=True)
        cmds.xform(self.dag.node, ro=(rot[0], rot[1], rot[2]), ws=True)

    def bind_objects(self, objects):
        '''
        List of objects that should be snapped to this layout object
        Args:
            objects list[(str)]: objects that will be snapped to layout object
                                 when called
        '''

        bound_attr = cmds.ls(self.dag.node + '.boundObjects')
        if bound_attr:
            prev_objects = cmds.attributeQuery('boundObjects', node=self.dag.node, listEnum=True)[0].split(':')
            objects += prev_objects
            cmds.setAttr(self.dag.node + '.boundObjects', lock=False)
            cmds.deleteAttr(self.dag.node + '.boundObjects')

        attr.add_enum(self.dag.node, 'boundObjects', objects, visible=False, keyable=False)

    def delete(self):
        '''
        Deletes layout object
        '''
        if self.parent_object:
            self.unparent()
        for child in self.children:
            child.unparent()
        cmds.delete(self.zero.node)
        cmds.delete(self.line_crv)

    @staticmethod
    def create_group_structure():
        transforms = cmds.ls(transforms=True)
        top_grp = None
        for each in transforms:
            top_attr = cmds.ls(each + '.topRigGrp')
            if top_attr:
                top_grp = each
        if not top_grp:
            top_grp = node.Node('untitled_rig', 'transform').node
            control_grp = node.Node('control_rig', 'transform').node
            parts_grp = node.Node('parts_rig', 'transform').node
            geo_grp = node.Node('geo_rig', 'transform').node
            bone_grp = node.Node('bone_rig', 'transform').node
            layouts_grp = node.Node('layouts_rig', 'transform').node
            layouts_DNT_grp = node.Node('layouts_rig_DNT', 'transform').node
            module_grp = node.Node('modules_rig', 'transform').node

            cmds.parent(control_grp, top_grp)
            cmds.parent(parts_grp, top_grp)
            cmds.parent(geo_grp, top_grp)
            cmds.parent(bone_grp, top_grp)
            cmds.parent(layouts_grp, top_grp)
            cmds.parent(layouts_DNT_grp, layouts_grp)
            cmds.parent(module_grp, layouts_DNT_grp)

            attr.add_enum(top_grp, 'topRigGrp', ['Top Group'], visible=False, keyable=False)
            attr.add_enum(layouts_grp, 'layoutGrp', ['Layout Group'], visible=False, keyable=False)
            attr.add_enum(layouts_DNT_grp, 'layoutDNTGrp', ['Layout DNT Group'], visible=False, keyable=False)

            cmds.setAttr(parts_grp + '.visibility', 0)

            # Create global control
            ctrl = cs.create_nurbscurve('C_global_CTRL', 'global')
            attr.add_world_transforms(ctrl)
            attr.add_float(ctrl, 'globalScale', max=50)

            cmds.connectAttr(ctrl + '.globalScale', ctrl + '.sx')
            cmds.connectAttr(ctrl + '.globalScale', ctrl + '.sy')
            cmds.connectAttr(ctrl + '.globalScale', ctrl + '.sz')

            attr.lock_attributes(ctrl, ['sx', 'sy', 'sz', 'v'])
            shape.change_color(ctrl, 'yellow', change_shapes=True)

            # Create global offset control
            ofs_ctrl = cs.create_nurbscurve('C_global_OFS_CTRL', 'square')
            attr.add_world_transforms(ofs_ctrl)
            attr.add_float(ofs_ctrl, 'globalScale', max=50)

            shape.modify_shape(ofs_ctrl, rotate=(90, 0, 0), scale=(4, 4, 4))

            cmds.connectAttr(ofs_ctrl + '.globalScale', ofs_ctrl + '.sx')
            cmds.connectAttr(ofs_ctrl + '.globalScale', ofs_ctrl + '.sy')
            cmds.connectAttr(ofs_ctrl + '.globalScale', ofs_ctrl + '.sz')

            attr.lock_attributes(ofs_ctrl, ['sx', 'sy', 'sz', 'v'])
            shape.change_color(ofs_ctrl, 'light brown', change_shapes=True)

            cmds.parent(ofs_ctrl, ctrl)
            cmds.parent(ctrl, 'control_rig_GRP')

        return top_grp

    @staticmethod
    def mirror_dag(dag, new_dag):
        '''
        Mirrors a dag over the x axis
        '''

        mtx = cmds.xform(dag, matrix=True, ws=True, q=True)

        new = maths.mirror_matrix(mtx)

        cmds.xform(new_dag, matrix=new, ws=True)
