import maya.cmds as cmds
import maya.api.OpenMaya as om
import copy as copy

from custom_utils import maths


def face_to_vertex(faces):
    vertices = []
    for face in faces:
        vtxs = cmds.ls(cmds.polyListComponentConversion(face, tv=True), fl=True)
        for vtx in vtxs:
            vertices.append(vtx)

    return vertices


def move_points_to_geo(points, geo):
    """ Moves selected points to a piece of geometry with matching point order
    Args:
        points [str]: list of selected points to move
        geo (str): geometry to move the points to
    """

    for point in points:
        vtx_num = point.split('.')[-1]
        vtx = geo + '.' + vtx_num

        pos = cmds.xform(vtx, t=True, ws=True, q=True)
        cmds.xform(point, t=pos, ws=True)


def closest_point_on_surface(surface, point, mvec=False):
    ''' find closest point on surface and it's UV values

    Args:
        surface (str): nurbs surface
        point tuple(float, float, float): position to reference
        mvec bool: if True returns point as an mvector
    Return:
         tuple(float, float, float), (int), (int): position, u param, v param
    '''
    sel = om.MSelectionList()
    sel.add(surface)

    plane = om.MFnNurbsSurface()
    plane.setObject(sel.getDagPath(0))

    point = om.MPoint(om.MVector(point))

    pos, parU, parV = plane.closestPoint(point)
    if mvec:
        pos = om.MVector(pos[0], pos[1], pos[2])

    return pos, parU, parV


def closest_point_on_mesh(geo, point, add_normal=False):
    '''
    Gets closest point and face on mesh
    Args:
        geo (str): name of geo
        point om.MVector(): point to reference for getting closest point
        add_normal (bool): adds normal to output if True
    Return:
        om.MVector: closest point on mesh [closest_point, face, normal]

    '''
    obj = om.MSelectionList()
    obj.add(geo)
    mesh = om.MFnMesh(obj.getComponent(0)[0])

    pos = om.MPoint(point[0], point[1], point[2])
    point, normal, face = mesh.getClosestPointAndNormal(pos, space=4)
    close_point = om.MVector(point[0], point[1], point[2])
    face = '{}.f[{}]'.format(geo, face)
    output = [close_point]
    if add_normal:
        output.append(normal)
    output.append(face)

    return output

def dag_to_mesh(geo, dag):
    '''
    Puts the given to the closest point on the given mesh
    Args:
        geo (str): name of geo
        dag (str): name of dag to project to geo
    '''

    pos = cmds.xform(dag, t=True, ws=True, q=True)
    closest_point = closest_point_on_mesh(geo, pos)[0]
    cmds.xform(dag, t=closest_point, ws=True)

def closest_vtx_on_mesh(geo, point):
    obj = om.MSelectionList()
    obj.add(geo)
    mesh = om.MFnMesh(obj.getComponent(0)[0])

    pos = om.MPoint(point[0], point[1], point[2])
    point, face = mesh.getClosestPoint(pos, space=4)
    face = '{}.f[{}]'.format(geo, face)
    vertices = cmds.polyListComponentConversion(face, tv=True)
    vertices = cmds.ls(vertices, fl=True)
    start_pos = om.MVector(point[0], point[1], point[2])

    # find which vertex has the same position as the given point
    shortest_distance = None
    closest_vtx = None
    for i, vtx in enumerate(vertices):
        pos = cmds.xform(vtx, t=True, ws=True, q=True)
        end_pos = om.MVector(pos[0], pos[1], pos[2])
        distance = (start_pos - end_pos).length()
        if not closest_vtx:
            shortest_distance = distance
            closest_vtx = vtx
        elif distance < shortest_distance:
            shortest_distance = distance
            closest_vtx = vtx
        if distance == 0:
            break
    return closest_vtx


def find_closest_mesh(reference, meshes):
    '''
    Finds the closest mesh to the given dag or point
    Args:
        reference(str): dag node to reference for position
        meshes list[(str)]: list of meshes to pull from
    Return:
        str: closest mesh to the given dag
    '''
    closest_mesh = list()
    if isinstance(reference, basestring):
        pos = cmds.xform(reference, t=True, ws=True, q=True)
        pos = om.MVector(pos[0], pos[1], pos[2])
    else:
        pos = reference
    for mesh in meshes:
        point_on_mesh, normal, face = closest_point_on_mesh(mesh, pos, add_normal=True)
        vec_between = point_on_mesh - pos
        distance = vec_between.length()

        if closest_mesh:
            if distance < closest_mesh[1]:
                closest_mesh = [mesh, distance]
        else:
            closest_mesh = [mesh, distance]

    return closest_mesh[0]


def find_closest_surface(reference, surfaces):
    '''
    Finds the closest surface to the given dag or point
    Args:
        reference(str): dag node to reference for position
        meshes list[(str)]: list of surfaces to pull from
    Return:
        str: closest mesh to the given dag
    '''
    closest_mesh = list()
    if isinstance(reference, basestring):
        pos = cmds.xform(reference, t=True, ws=True, q=True)
        pos = om.MVector(pos[0], pos[1], pos[2])
    else:
        pos = reference
    for surface in surfaces:
        point_on_mesh, parU, parV = closest_point_on_surface(surface, pos, mvec=True)
        vec_between = point_on_mesh - pos
        distance = vec_between.length()

        if closest_mesh:
            if distance < closest_mesh[1]:
                closest_mesh = [surface, distance]
        else:
            closest_mesh = [surface, distance]

    return closest_mesh[0]


def matrix_on_curve(up_vec, curve, param=0.001, point=None, aim='x', up='y'):
    '''
    Gives a matrix on a curve given the distance down a curve
    Args:
        up (om.MVector): up vector to reference from
    '''

    sel = om.MSelectionList()
    sel.add(curve)

    crv = om.MFnNurbsCurve()
    crv.setObject(sel.getDagPath(0))

    if point:
        pos, par = closest_point_on_curve(curve, point)
        tangent = crv.tangent(par)
    else:
        pos = crv.getPointAtParam(param)
        tangent = crv.tangent(param)

    mtx = maths.fixed_matrix(pos, aim_vector=tangent, up_vector=up_vec, aim=aim, up=up)

    return mtx


def closest_point_on_curve(curve, point):
    ''' find closest point on curve and it's parameter

    Args:
        surface (str): nurbs surface
        point tuple(float, float, float): position to reference
    Return:
         tuple(float, float, float), (int), (int): position, u param, v param
    '''
    sel = om.MSelectionList()
    sel.add(curve)

    crv = om.MFnNurbsCurve()
    crv.setObject(sel.getDagPath(0))

    point = om.MPoint(om.MVector(point))

    pos, par = crv.closestPoint(point)

    return [pos, par]


def average_position(objects, dags=True):
    if dags:
        added_vecs = om.MVector()
        for each in objects:
            pos = cmds.xform(each, t=True, ws=True, q=True)
            pos = om.MVector(pos[0], pos[1], pos[2])
            added_vecs += pos
    else:
        added_vecs = om.MVector()
        for each in objects:
            added_vecs += each

    position = added_vecs / float(len(objects))

    return position


def closest_average_geo(dags, geos, level=3, vec_size=0.3):
    '''
    Finds closest mesh based on the entirety of a chain
    @param dags:
    @param geos:
    @param level:
    @param vec_size:
    @return:
    '''

    # Create default dictionary of geos to see how many times a geo was indicated to be the closest
    closest_geos = dict()
    for geo in geos:
        closest_geos[geo] = 0

    # Find how many joints each geo was closest to
    mapping = joints_to_geos(dags, geos, level=level, vec_size=vec_size)
    for geo, joints in mapping.items():
        closest_geos[geo] = len(joints)

    # Find which geo in the dictionary has the highest amount of instances
    closest_geo = [None, 0]
    for geo, i in closest_geos.items():
        if i > closest_geo[1]:
            closest_geo = [geo, i]

    return closest_geo[0]


def joints_to_geos(joints, geos, level=0, vec_size=0.3):
    '''
    Creates a mapping of joints that are the closest to their respective meshes
    @param joints:
    @param geos:
    @param level:
    @param vec_size:
    @return:
    '''

    # Create dictionary of geos to assign influnces to
    inf_mapping = dict()

    # Create geo dictionary to indicate how often each geo is the closest to a given point
    closest_geos_default = dict()
    for geo in geos:
        closest_geos_default[geo] = 0
    for geo in geos:
        inf_mapping[geo] = list()

    # Find closest mesh to each joint
    for jnt in joints:
        # print 'Finding closest mesh to {}'.format(jnt)
        pos = cmds.xform(jnt, t=True, ws=True, q=True)
        pos = om.MVector(pos[0], pos[1], pos[2])
        closest_geo = None
        if level == 0:
            closest_geo = [find_closest_mesh(pos, geos)]
        else:
            closest_geos = copy.deepcopy(closest_geos_default)
            mtx = cmds.xform(jnt, matrix=True, ws=True, q=True)
            x, y, z, pos = maths.matrix_to_vectors(mtx)

            # Multiply each vec by the influence
            x = x * vec_size
            y = y * vec_size
            z = z * vec_size

            x_pos_vec = x + pos
            x_neg_vec = (x * -1) + pos
            y_pos_vec = y + pos
            y_neg_vec = (y * -1) + pos
            z_pos_vec = z + pos
            z_neg_vec = (z * -1) + pos

            geo1 = find_closest_mesh(pos, geos)
            closest_geos[geo1] += 1
            if level > 0:
                geo2 = find_closest_mesh(x_pos_vec, geos)
                geo3 = find_closest_mesh(x_neg_vec, geos)
                closest_geos[geo2] += 1
                closest_geos[geo3] += 1
            if level > 1:
                geo4 = find_closest_mesh(y_pos_vec, geos)
                geo5 = find_closest_mesh(y_neg_vec, geos)
                closest_geos[geo4] += 1
                closest_geos[geo5] += 1
            if level > 2:
                geo6 = find_closest_mesh(z_pos_vec, geos)
                geo7 = find_closest_mesh(z_neg_vec, geos)
                closest_geos[geo6] += 1
                closest_geos[geo7] += 1

            # Find which geo in the dictionary has the highest amount of instances
            closest_geo = [None, 0]
            for geo, i in closest_geos.items():
                if i > closest_geo[1]:
                    closest_geo = [geo, i]

        # Add joint to the geometry's list it's closest to in the dictionary
        inf_mapping[closest_geo[0]].append(jnt)

    return inf_mapping


def get_MVector(object):
    pos = cmds.xform(object, t=True, ws=True, q=True)
    vec = om.MVector(pos[0], pos[1], pos[2])

    return vec
