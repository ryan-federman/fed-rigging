import maya.cmds as cmds


def add_suffix(name, suffix):
    new = '{}_{}'.format(name, suffix)
    return new


def add_prefix(name, prefix):
    new = '{}_{}'.format(prefix, name)
    return new


def remove_prefix(name):
    '''
    Removes the prefix from a given string
    Args:
        name (str): string to remove prefix from
    Return:
        (str): name without prefix
    '''
    no_prefix = name.split('_')
    no_prefix.remove(no_prefix[0])
    new_name = ''
    for i, each in enumerate(no_prefix):
        if i == 0:
            new_name += each
        else:
            new_name += '_' + each
    return new_name


def remove_suffix(name):
    new_name = ''
    split_name = name.split('_')
    split_name.pop()
    for i, each in enumerate(split_name):
        if i == 0:
            new_name += each
        else:
            new_name += '_' + each
    return new_name


def create_int_string(num, leading=1):
    '''
    Makes an into into a string with leading zeros
    Args:
        num (int): integer to convert
        leading (int): amount of zeros to add onto integer
    Return:
        (str): converted integer
    '''

    number = str(num)
    if num < 10 and leading > 0:
        number = str(0) + number
    if num < 100 and leading > 1:
        number = str(0) + number
    if num < 1000 and leading > 2:
        number = str(0) + number
    if num < 10000 and leading > 3:
        number = str(0) + number

    return number


def rename_list(objs, basename, start_index=0):
    '''
    Renames list of objects with a given string as a basename where # represents where the index goes
    :param objs:
    :param basename:
    :param suffix:
    :return:
    '''

    leading = basename.count('#') - 1
    prefix, suffix = basename.split('#')

    # Get uuids for stable renaming
    bases = cmds.ls(objs, uuid=True)

    for i, uuid in enumerate(bases):
        obj = cmds.ls(uuid)
        cmds.rename(obj, 'temp{}'.format(i))

    for i, uuid in enumerate(bases):
        num = create_int_string(i + start_index, leading=leading)
        new_name = '{}{}{}'.format(prefix, num, suffix)
        obj = cmds.ls(uuid)
        cmds.rename(obj, new_name)


def rename_hierarchies(objs, basename, suffix='JNT'):
    '''
    Renames a list of objects and their descendents, ex Lf_Test#_#_JNT
    Args:
        objs list[(str)]: list of objects
        basename: basename
    '''
    bases = cmds.ls(objs, uuid=True)
    all = []
    for base in bases:
        obj = cmds.ls(base)
        jnts = cmds.listRelatives(obj, ad=True, f=True)
        for jnt in jnts:
            uuid = cmds.ls(jnt, uuid=True)[0]
            all.append(uuid)
        all.append(base)
    for i, uuid in enumerate(all):
        obj = cmds.ls(uuid)
        cmds.rename(obj, 'temp{}'.format(i))
    for i, base in enumerate(bases):
        obj = cmds.ls(base)
        children = cmds.listRelatives(obj, ad=True, f=True)
        children.reverse()
        uuids = [base]
        for jnt in children:
            uuid = cmds.ls(jnt, uuid=True)[0]
            uuids.append(uuid)
        for x, uuid in enumerate(uuids):
            obj = cmds.ls(uuid)
            cmds.rename(obj, '{}{}_{}_{}'.format(basename, i, x, suffix))

class ProgressWindow:
    def __init__(self, num_steps=100, title='Progress Window'):
        self.amount = 0
        self.step_amount = 1
        self.title = title

        if num_steps != 100:
            div = float(100)/float(num_steps)
            self.step_amount = float(1) * div

        cmds.progressWindow(endProgress=1)

        cmds.progressWindow(title=title,
                            progress=self.amount,
                            isInterruptable=True)

    def step(self, step_amount=None):
        if step_amount:
            self.amount += step_amount
        else:
            self.amount += self.step_amount

        cmds.progressWindow(edit=True, progress=self.amount, status=str('{}% {}'.format(str(self.amount), self.title)))

        if self.amount >= 100:
            cmds.progressWindow(endProgress=1)

    def end(self):
        cmds.progressWindow(endProgress=1)

    def start(self):
        cmds.progressWindow(title=self.title,
                            progress=self.amount,
                            isInterruptable=True)
