import maya.cmds as cmds


def find_skin(dag):
    skincluster = None

    history = cmds.listHistory(dag)
    for node in history:
        if cmds.nodeType(node) == 'skinCluster':
            skincluster = node

    return skincluster


def find_influences(skc):
    joints = cmds.skinCluster(skc, wi=True, q=True)

    return joints


def copy_skincluster(target, objects):
    skc = find_skin(target)
    if not skc:
        cmds.error(target + ' has no skincluster')
    joints = find_influences(skc)
    for obj in objects:
        old_skin = find_skin(obj)
        if old_skin:
            cmds.skinCluster(obj, ub=True, e=True)
        new_skc = cmds.skinCluster(joints, obj)[0]
        cmds.copySkinWeights(ss=skc, ds=new_skc, noMirror=True, surfaceAssociation='closestPoint', influenceAssociation='closestJoint')
