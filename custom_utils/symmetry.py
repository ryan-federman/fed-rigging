import copy

import maya.cmds as cmds
import maya.api.OpenMaya as om

import custom_utils.component as comp
import custom_utils.attribute as attr


def create_symmetry(geo, axis='x', center_influence=.0001, side_influence=.0001):
    '''
    Creates attributes on the dag node of a mesh that map symmetrical vertices
    to one another
    Args:
        geo (str): name of geo to find symmetry
        axis (str): axis to do symmetry over
    '''

    # check to see if attributes exist
    if cmds.ls('{}.centerSymMap'.format(geo)):
        cmds.deleteAttr('{}.centerSymMap'.format(geo))
    if cmds.ls('{}.sideSymMap'.format(geo)):
        cmds.deleteAttr('{}.sideSymMap'.format(geo))

    if axis == 'x':
        sym_value = 0
    elif axis == 'y':
        sym_value = 1
    elif axis == 'z':
        sym_value = 2

    geo_position = cmds.xform(geo, t=True, ws=True, q=True)
    geo_position = om.MVector(geo_position[0], geo_position[1], geo_position[2])
    vertices = cmds.ls('{}.vtx[*]'.format(geo), fl=True)
    left_vtx_positions = {}
    side_vtx_mapping = []
    center_vtx_mapping = []

    # with com.Progress(max_value=len(vertices), status='Finding side') as prog_bar:
    for vtx in vertices:
        pos = cmds.xform(vtx, t=True, ws=True, q=True)
        pos = om.MVector(pos[0], pos[1], pos[2])
        local_pos = pos - geo_position
        if round(local_pos[sym_value], 3) >= side_influence:
            left_vtx_positions[vtx] = [local_pos[0], local_pos[1], local_pos[2]]
        elif round(local_pos[sym_value], 5) <= center_influence:
            center_vtx_mapping.append(vtx)

            # prog_bar.step()

    # Create progress window
    num_verts = len(left_vtx_positions)
    # with com.Progress(max_value=num_verts, status='Creating symmetry') as prog_bar:
    for vtx, pos in left_vtx_positions.items():
        mirrored_value = pos[sym_value] * -1
        mirrored_pos = copy.deepcopy(pos)
        mirrored_pos.remove(mirrored_pos[sym_value])
        mirrored_pos.insert(sym_value, mirrored_value)

        mirrored_vtx = comp.closest_vtx_on_mesh(geo, mirrored_pos)
        enum_value = '{}-{}'.format(vtx, mirrored_vtx)
        side_vtx_mapping.append(enum_value)

            # prog_bar.step()

    attr.add_enum(geo, 'centerSymMap', center_vtx_mapping, visible=False)
    attr.add_enum(geo, 'sideSymMap', side_vtx_mapping, visible=False)


def mirror_mesh(geo, align_center=True, side='left', axis='x'):
    '''
    Mirror the given mesh based on the created symmetry
    Args:
         geo (str): mesh to mirror
         side (str): side to keep the same in the mirror
    '''

    if axis == 'x':
        sym_value = 0
    elif axis == 'y':
        sym_value = 1
    elif axis == 'z':
        sym_value = 2

    geo_pos = cmds.xform(geo, t=True, ws=True, q=True)
    geo_pos = om.MVector(geo_pos[0], geo_pos[1], geo_pos[2])
    side_vertices = cmds.attributeQuery('sideSymMap', node=geo, listEnum=True)[0].split(':')
    center_vertices = cmds.attributeQuery('centerSymMap', node=geo, listEnum=True)[0].split(':')
    left_verts = []
    right_verts = []
    for verts in side_vertices:
        if '=' not in verts:
            left, right = verts.split('-')
            left_verts.append(left)
            right_verts.append(right)

    # function for mirroring the given verts
    def mirror(sources, targets, sym_value):

        # with com.Progress(max_value=len(sources)) as prog_bar:
        for i, each in enumerate(sources):
            pos = cmds.xform(each, t=True, ws=True, q=True)
            pos = om.MVector(pos[0], pos[1], pos[2])
            pos = pos - geo_pos
            old_pos = [pos[0], pos[1], pos[2]]
            mirrored_value = old_pos[sym_value] * -1
            mirrored_pos = copy.deepcopy(old_pos)
            mirrored_pos.remove(mirrored_pos[sym_value])
            mirrored_pos.insert(sym_value, mirrored_value)

            mirrored_pos_vec = om.MVector(mirrored_pos[0], mirrored_pos[1], mirrored_pos[2])
            mirrored_pos = mirrored_pos_vec + geo_pos

            print mirrored_pos
            cmds.xform(targets[i], t=mirrored_pos, ws=True)
            # cmds.move(mirrored_pos[0], mirrored_pos[1], mirrored_pos[2], [targets[i]], ws=True)

                # prog_bar.step()

    if side == 'left':
        mirror(left_verts, right_verts, sym_value)
    else:
        mirror(right_verts, left_verts, sym_value)

    # align center verts with center of mesh
    if align_center:
        local_geo_center = cmds.xform(geo, t=True, ws=True, q=True)[sym_value]
        for vtx in center_vertices:
            pos = cmds.xform(vtx, t=True, ws=True, q=True)
            pos.remove(pos[sym_value])
            pos.insert(sym_value, local_geo_center)

            cmds.move(pos[0], pos[1], pos[2], [vtx], ws=True)


# -0.847
