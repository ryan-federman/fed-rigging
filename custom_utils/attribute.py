import maya.cmds as cmds


def break_attr_conn(attr):
    """Breaks connections to the given attribute

    Args:
        attr(str): Attribute to delete connections to
    """

    source = cmds.listConnections(attr, d=False, s=True, plugs=True)
    if source:
        cmds.disconnectAttr(source[0], attr)


def switch_attr_conn(source, target, break_source=True):
    '''
    Switches the connection from one attribute to another
    Args:
         source(str): source attribute to take connection from
         target(str): target attribute to put connection onto
         break_source(bool): whether to break the connection from the source after the operation
    '''

    con = cmds.listConnections(source, d=False, s=True, plugs=True)[0]
    cmds.connectAttr(con, target)

    if break_source:
        break_attr_conn(source)


def connect_visibility(attribute, dags, force=False):
    '''Connects a given attribute to dag nodes

    Args:
        attribute (str): attribute to control visibility
        dags list[(str)]: dags whose visibility is being controlled
    '''

    for each in dags:
        connected = cmds.listConnections(each + '.v', d=False, s=True, plugs=True)
        if connected:
            if force:
                break_attr_conn(each + '.v')
        cmds.connectAttr(attribute, each + '.v')


def multiple_visibility(attr, name, *args):
    '''Attaches an attributes indices to the visibility of objects

    Args:
         attr (str): name of attribute to drive visibility
         name (str): base name of condition node
         *args [(str)]: objects whose visibility is being driven
    '''

    objects = ['none']
    for each in args:
        objects.append(each)

    for i, dags in enumerate(objects):
        if i > 0:
            condition = cmds.createNode('condition', name='{}_{}_VIS_COND'.format(name, str(i)))

            cmds.connectAttr(attr, condition + '.firstTerm')
            cmds.setAttr(condition + '.secondTerm', i)
            cmds.setAttr(condition + '.colorIfTrueR', 1)
            cmds.setAttr(condition + '.colorIfFalseR', 0)

            for dag in dags:
                cmds.connectAttr(condition + '.outColorR', dag + '.visibility')


def add_proxy(node, proxy_attr):
    ''' Creates proxy attribute to another attribute

    Args:
        node (str): node in which to put the new attribute on
        proxy_attr (str): attribute to be proxied
        name (str): name of new attribute
    Returns:
        str: proxy attribute
    '''
    name = proxy_attr.split('.')[1]
    attr = cmds.addAttr(node, proxy=proxy_attr, shortName=name)
    return attr


def unlock_attributes(dag, attrs, channelbox=True, displayable=True):
    """Wrapper for unlocking attributes

    Args:
        dag (str): dag to unlock attributes on
        attrs list[(str)]: attributes to unlock
        channelbox (bool): whether to hide the attributes being unlocked from the channelbox
    """

    for attr in attrs:
        if displayable:
            displayable = False
        else:
            displayable = True
        attribute = '{}.{}'.format(dag, attr)
        cmds.setAttr(attribute, keyable=channelbox, channelBox=displayable, lock=False)


def lock_attributes(dag, attrs, channelbox=False, displayable=True):
    """Wrapper for locking attributes

    Args:
        dag (str): dag to lock attributes on
        attrs list[(str)]: attributes to lock
        channelbox (bool): whether to hide the attributes being locked from the channelbox
    """

    for attr in attrs:
        if displayable:
            displayable = False
        else:
            displayable = True
        attribute = '{}.{}'.format(dag, attr)
        cmds.setAttr(attribute, keyable=channelbox, channelBox=displayable, lock=True)


def hide_attributes(dag, attrs):
    """Wrapper for locking attributes

    Args:
        dag (str): dag to hide attributes on
        attrs list[(str)]: attributes to hide
    """
    for attr in attrs:
        attribute = '{}.{}'.format(dag, attr)
        cmds.setAttr(attribute, keyable=False, channelBox=False)


def add_enum(node, name, enum_list, visible=True, keyable=True, default=0):
    '''Wrapper for making an enum attribute

    Args:
        node (str): node to add attribute to
        name (str): name of attribute
        enum_list list[(str)]: list of enums
        visible (bool): whether the attribute will be visible in the channelbox
        keyable (bool): whether the attribute will be keyable
    Return:
         (str): attribute name
    '''

    enum_names = ''
    for i, arg in enumerate(enum_list):
        if arg != enum_list[-1]:
            enum_names += arg + ':'
        else:
            enum_names += arg

    cmds.addAttr(node,
                 shortName=name,
                 attributeType='enum',
                 enumName=enum_names,
                 keyable=visible)
    cmds.setAttr('{}.{}'.format(node, name), keyable=visible)
    if not keyable:
        cmds.setAttr('{}.{}'.format(node, name), channelBox=True)

    # Set default value
    cmds.setAttr('{}.{}'.format(node, name), default)

    return '{}.{}'.format(node, name)


def add_bool(node, name, default=1, visible=True, keyable=True):
    '''Wrapper for making bool attribute

    Args:
        node (str): node to add attribute to
        name (str): name of attribute
        visible (bool): whether the attribute will be visible in the channelbox
        keyable (bool): whether the attribute will be keyable
    Return:
         (str): attribute name
    '''

    cmds.addAttr(node,
                 shortName=name,
                 attributeType='bool',
                 defaultValue=default)

    cmds.setAttr('{}.{}'.format(node, name), keyable=keyable, channelBox=visible)

    return '{}.{}'.format(node, name)


def add_float(node, name, min=0.0, max=1.0, default=0, visible=True, keyable=True):
    '''Wrapper for making a float attribute

    Args:
        node (str): node to add attribute to
        name (str): name of attribute
        min (float): min number
        max (float): max number
        default (float): default number
        visible (bool): whether the attribute will be visible in the channelbox
        keyable (bool): whether the attribute will be keyable
    Return:
         (str): attribute name
    '''

    cmds.addAttr(node,
                 shortName=name,
                 attributeType='float',
                 min=min,
                 max=max,
                 defaultValue=default,
                 keyable=visible)

    cmds.setAttr('{}.{}'.format(node, name), default)

    if not keyable:
        cmds.setAttr('{}.{}'.format(node, name), channelBox=visible)

    return '{}.{}'.format(node, name)


def add_int(node, name, min=0.0, max=1.0, default=1.0, visible=True, keyable=True):
    '''Wrapper for making an int attribute

    Args:
        node (str): node to add attribute to
        name (str): name of attribute
        min (float): min number
        max (float): max number
        default (float): default number
        visible (bool): whether the attribute will be visible in the channelbox
        keyable (bool): whether the attribute will be keyable
    Return:
         (str): attribute name
    '''

    cmds.addAttr(node,
                 shortName=name,
                 attributeType='long',
                 min=min,
                 max=max,
                 defaultValue=default,
                 keyable=visible)

    if not keyable:
        cmds.setAttr('{}.{}'.format(node, name), channelBox=visible)

    return '{}.{}'.format(node, name)


def add_title(node, name):
    '''Wrapper for making an enum attribute

    Args:
        node (str): node to add attribute to
        name (str): name of attribute
        enum_list list[(str)]: list of enums
        visible (bool): whether the attribute will be visible in the channelbox
        keyable (bool): whether the attribute will be keyable
    Return:
         (str): attribute name
    '''

    cmds.addAttr(node,
                 shortName=name,
                 niceName=' ',
                 attributeType='enum',
                 enumName=name)
    cmds.setAttr('{}.{}'.format(node, name), keyable=False, channelBox=True, lock=True)

    return '{}.{}'.format(node, name)

def add_vector(node, name, visible=True, keyable=True):
    '''Wrapper for making an vector attribute

    Args:
        node (str): node to add attribute to
        name (str): name of attribute
        enum_list list[(str)]: list of enums
        visible (bool): whether the attribute will be visible in the channelbox
        keyable (bool): whether the attribute will be keyable
    Return:
         (str): attribute name
    '''
    cmds.addAttr(node, ln=name, at='double3')
    cmds.addAttr(node, ln=name + 'X', at='double', p=name)
    cmds.addAttr(node, ln=name + 'Y', at='double', p=name)
    cmds.addAttr(node, ln=name + 'Z', at='double', p=name)
    attrs = [name, name + 'X', name + 'Y', name + 'Z']

    if keyable:
        unlock_attributes(node, attrs, channelbox=visible, displayable=visible)
        for attr in attrs:
            cmds.setAttr('{}.{}'.format(node, attr), keyable=True)

    return '{}.{}'.format(node, name)

def add_world_transforms(node, translation=True, rotation=True, scale=True):
    '''
    Adds a vector attribute representing the world position onto the given dag
    Args:
        node (str): dag node that the attribute will be added onto
    Return:
        str: name of attribute
    '''
    dm = cmds.createNode('decomposeMatrix', name=node + '_worldTransforms_DM')
    cmds.connectAttr(node + '.worldMatrix[0]', dm + '.inputMatrix')
    attributes = []
    if translation:
        attr = cmds.ls(node + '.worldPosition')
        if not attr:
            cmds.addAttr(node, ln='worldPosition', at='double3')
            cmds.addAttr(node, ln='worldPositionX', at='double', p='worldPosition')
            cmds.addAttr(node, ln='worldPositionY', at='double', p='worldPosition')
            cmds.addAttr(node, ln='worldPositionZ', at='double', p='worldPosition')
            cmds.connectAttr(dm + '.outputTranslate', node + '.worldPosition')
        attributes += [node + '.worldPosition']
    if rotation:
        attr = cmds.ls(node + '.worldRotation')
        if not attr:
            cmds.addAttr(node, ln='worldRotation', at='double3')
            cmds.addAttr(node, ln='worldRotationX', at='double', p='worldRotation')
            cmds.addAttr(node, ln='worldRotationY', at='double', p='worldRotation')
            cmds.addAttr(node, ln='worldRotationZ', at='double', p='worldRotation')
            cmds.connectAttr(dm + '.outputRotate', node + '.worldRotation')
        attributes += [node + '.worldRotation']
    if scale:
        attr = cmds.ls(node + '.worldScale')
        if not attr:
            cmds.addAttr(node, ln='worldScale', at='double3')
            cmds.addAttr(node, ln='worldScaleX', at='double', p='worldScale')
            cmds.addAttr(node, ln='worldScaleY', at='double', p='worldScale')
            cmds.addAttr(node, ln='worldScaleZ', at='double', p='worldScale')
            cmds.connectAttr(dm + '.outputScale', node + '.worldScale')
        attributes += [node + '.worldScale']
    if not translation:
        if not rotation:
            if not scale:
                cmds.delete(dm)

    return attributes


def add_parent_world_position(node):
    '''
    Adds a vector attribute representing the world position onto the given dag's parent
    Args:
        node (str): dag node that the attribute will be added onto
    Return:
        str: name of attribute
    '''
    attr = cmds.ls(node + '.parentWorldPosition')
    if not attr:
        dm = cmds.createNode('decomposeMatrix', name=node + '_parentWorldPosition_DM')

        cmds.addAttr(node, ln='parentWorldPosition', at='double3')
        cmds.addAttr(node, ln='parentWorldPositionX', at='double', p='parentWorldPosition')
        cmds.addAttr(node, ln='parentWorldPositionY', at='double', p='parentWorldPosition')
        cmds.addAttr(node, ln='parentWorldPositionZ', at='double', p='parentWorldPosition')

        cmds.connectAttr(node + '.parentMatrix[0]', dm + '.inputMatrix')
        cmds.connectAttr(dm + '.outputTranslate', node + '.parentWorldPosition')

    return node + '.parentWorldPosition'


def copy_attributes(source, target):

    attrs = cmds.listAttr(source, v=True, u=True, k=True)
    for attr in attrs:
        try:
            value = cmds.getAttr('{}.{}'.format(source, attr))
            cmds.setAttr('{}.{}'.format(target, attr), value)
        except:
            pass
