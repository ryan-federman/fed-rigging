import sys

import maya.cmds as cmds

import shape as shape

# refer to shape list at end of file


def create_nurbscurve(name, shape_type, **kwargs):
    '''
    Creates nurbscurve of the given shape
    Args:
        name (str): name of the created dag
        shape_type (str): shape of the created curve
        color1 (str): first color of the first curve created under a dag
        color2 (str): second color of the first curve created under a dag
        color3 (str): third color of the first curve created under a dag

    Return:
        (str): transform of the created curve
    '''
    transform = shapes[shape_type](name, **kwargs)

    return transform


def replace_shape(dag, curve_type):
    '''Replaces a nurbscurve under a control

    Args:
        dag (str): dag node in which the new shape node will be put under
        new_shape (str): type of nurbscurve being created
    Returns:
        (str): new shape node
    '''

    new_shape = create_nurbscurve('temp_CRV', curve_type)
    new_shape = cmds.listRelatives(new_shape, s=True)[0]
    old_shape = cmds.listRelatives(dag, s=True)

    cmds.parent(new_shape, dag, r=True, s=True)
    cmds.delete(old_shape)

    return new_shape


def mirror_curve(curves):
    '''
    mirrors the opposite curve to the given one over the x axis
    '''
    cmds.undoInfo(openChunk=True)
    for crv in curves:
        print crv
        mirror_crv = 'R_' + crv.split('L_')[1]
        if cmds.ls(mirror_crv):
            l_cvs = cmds.ls('{}.cv[*]'.format(crv), fl=True)
            r_cvs = cmds.ls('{}.cv[*]'.format(mirror_crv), fl=True)

            for i, cv in enumerate(l_cvs):
                mirror_cv = r_cvs[i]
                pos = cmds.xform(cv, t=True, ws=True, q=True)
                cmds.xform(mirror_cv, t=(pos[0] * -1, pos[1], pos[2]), ws=True)
    cmds.undoInfo(closeChunk=True)


def diamond_shape(name):
    ctrl1 = cmds.circle(name=name + '_01')
    cmds.setAttr(ctrl1[1] + '.degree', 1)
    cmds.setAttr(ctrl1[1] + '.sections', 4)
    ctrl2 = cmds.duplicate(ctrl1[0], name=name + '_02')[0]
    cmds.setAttr(ctrl2 + '.rx', 90)
    ctrl3 = cmds.duplicate(ctrl2, name=name + '_03')[0]
    cmds.setAttr(ctrl3 + '.rz', 90)

    cmds.makeIdentity(ctrl1, apply=True)
    cmds.makeIdentity(ctrl2, apply=True)
    cmds.makeIdentity(ctrl3, apply=True)

    transform = cmds.createNode('transform', name=name)

    shape1 = cmds.listRelatives(ctrl1, s=True)[0]
    shape2 = cmds.listRelatives(ctrl2, s=True)[0]
    shape3 = cmds.listRelatives(ctrl3, s=True)[0]

    cmds.parent(shape1, transform, r=True, shape=True)
    cmds.parent(shape2, transform, r=True, shape=True)
    cmds.parent(shape3, transform, r=True, shape=True)

    cmds.delete(ctrl1)
    cmds.delete(ctrl2)
    cmds.delete(ctrl3)

    shape.modify_shape(transform, scale=(0.5, 0.5, 0.5))

    return transform


def sphere_shape(name, color1='yellow', color2='yellow', color3='yellow'):
    transform = cmds.createNode('transform', name=name)

    ctrl1 = cmds.circle(name=transform + '_01')
    ctrl2 = cmds.duplicate(ctrl1[0], name=transform + '_02')[0]
    cmds.setAttr(ctrl2 + '.rx', 90)
    ctrl3 = cmds.duplicate(ctrl2, name=transform + '_03')[0]
    cmds.setAttr(ctrl3 + '.rz', 90)

    cmds.makeIdentity(ctrl1, apply=True)
    cmds.makeIdentity(ctrl2, apply=True)
    cmds.makeIdentity(ctrl3, apply=True)

    shape1 = cmds.listRelatives(ctrl1, s=True)[0]
    shape2 = cmds.listRelatives(ctrl2, s=True)[0]
    shape3 = cmds.listRelatives(ctrl3, s=True)[0]
    shape.change_color(shape1, color1)
    shape.change_color(shape2, color2)
    shape.change_color(shape3, color3)

    cmds.parent(shape1, transform, r=True, shape=True)
    cmds.parent(shape2, transform, r=True, shape=True)
    cmds.parent(shape3, transform, r=True, shape=True)

    cmds.delete(ctrl1)
    cmds.delete(ctrl2)
    cmds.delete(ctrl3)

    return transform


def gear_shape(name, color1='green', color2='yellow'):
    points1 = [[2.5975156903303487e-17, 0.42420650462465426, 2.1436068835532312e-16], [-0.07366268648252944, 0.4177618546325686, 2.1110406783335593e-16], [-0.11836423974539217, 0.3252030759713798, 1.7198039010544555e-16], [-0.1730369424947683, 0.2997087759873136, 1.584979848618037e-16], [-0.27267468512116333, 0.32496103560264145, 1.642098141377543e-16], [-0.3249610356026414, 0.2726746851211634, 1.3778839447867933e-16], [-0.299708775987314, 0.17303694249476867, 9.150885422597558e-17], [-0.3252030759713791, 0.11836423974539222, 6.259574287587165e-17], [-0.4177618546325685, 0.07366268648252954, 3.722334289633067e-17], [-0.4242065046246541, 8.484563548205961e-17, 4.287432801619066e-32], [-0.3408162450527514, -0.06009509946654795, -3.178069155345885e-17], [-0.3252030759713791, -0.1183642397453921, -6.259574287587157e-17], [-0.36737360945555136, -0.21210325231232693, -1.0718034417766146e-16], [-0.3249610356026413, -0.27267468512116316, -1.3778839447867923e-16], [-0.2224520053073589, -0.26510797650483175, -1.401996985519866e-16], [-0.1730369424947683, -0.2997087759873136, -1.5849798486180368e-16], [-0.14508716951140518, -0.3986237220851706, -2.014331570340848e-16], [-0.07366268648252952, -0.41776185463256826, -2.1110406783335576e-16], [-8.842927295999715e-17, -0.3460738849895373, -1.8301770845195113e-16], [0.06009509946654782, -0.3408162450527518, -1.8023725822200955e-16], [0.14508716951140496, -0.3986237220851705, -2.0143315703408475e-16], [0.21210325231232682, -0.36737360945555114, -1.8564180168842874e-16], [0.2224520053073589, -0.26510797650483175, -1.401996985519866e-16], [0.2651079765048311, -0.22245200530735912, -1.1764151534613767e-16], [0.36737360945555103, -0.2121032523123269, -1.0718034417766146e-16], [0.3986237220851704, -0.14508716951140507, -7.331567335467656e-17], [0.3408162450527507, -0.060095099466548105, -3.178069155345892e-17], [0.34607388498953656, -6.921831319315417e-17, -3.6605411771834556e-32], [0.417761854632568, 0.07366268648252927, 3.722334289633054e-17], [0.3986237220851703, 0.14508716951140488, 7.331567335467646e-17], [0.29970877598731316, 0.17303694249476834, 9.150885422597541e-17], [0.2651079765048311, 0.22245200530735837, 1.1764151534613732e-16], [0.27267468512116294, 0.32496103560264084, 1.64209814137754e-16], [0.21210325231232682, 0.3673736094555508, 1.8564180168842857e-16], [0.11836423974539194, 0.32520307597137893, 1.719803901054451e-16], [0.06009509946654807, 0.34081624505275115, 1.802372582220092e-16], [9.661973119781079e-17, 0.4242065046246534, 2.143606883553227e-16]]
    points2 = [[1.4562177982594717e-17, 0.237818414137586, 1.2017476960475023e-16], [-0.05914303457760096, 0.23034691133998098, 1.1639925823167393e-16], [-0.11456989477982536, 0.20840186494620458, 1.0530995337737488e-16], [-0.16279790713443482, 0.17336216292941287, 8.760363684945914e-17], [-0.20079672825496708, 0.1274294786343304, 6.439286163465267e-17], [-0.22617875246053065, 0.07348993154381332, 3.713604610296171e-17], [-0.23734913382173475, 0.01493274177733642, 7.545836217797801e-18], [-0.23360599619586606, -0.04456272707379315, -2.2518506308574686e-17], [-0.21518453468625504, -0.10125815589263042, -5.116792826630638e-17], [-0.18324223746894694, -0.15159116237528625, -7.660228110858946e-17], [-0.1397861565536568, -0.1923991386126063, -9.722343090533681e-17], [-0.08754679732457832, -0.22111796937636147, -1.117356749755349e-16], [-0.029806550844998492, -0.23594314490919382, -1.1922715565195295e-16], [0.029806550844998492, -0.23594314490919382, -1.1922715565195295e-16], [0.0875467973245783, -0.22111796937636147, -1.117356749755349e-16], [0.1397861565536568, -0.19239913861260624, -9.72234309053368e-17], [0.18324223746894694, -0.15159116237528616, -7.660228110858942e-17], [0.21518453468625498, -0.10125815589263036, -5.116792826630635e-17], [0.23360599619586594, -0.0445627270737931, -2.2518506308574658e-17], [0.23734913382173461, 0.014932741777336453, 7.545836217797818e-18], [0.2261787524605305, 0.07348993154381332, 3.713604610296171e-17], [0.20079672825496694, 0.1274294786343304, 6.439286163465267e-17], [0.16279790713443465, 0.17336216292941284, 8.760363684945913e-17], [0.11456989477982525, 0.20840186494620455, 1.0530995337737487e-16], [0.05914303457760086, 0.23034691133998086, 1.1639925823167388e-16], [-7.784883968679139e-17, 0.23781841413758587, 1.2017476960475018e-16]]
    curve1 = cmds.curve(point=points1,
                        degree=1)
    curve2 = cmds.curve(point=points2,
                        degree=1)

    shape1 = cmds.listRelatives(curve1, s=True)[0]
    shape2 = cmds.listRelatives(curve2, s=True)[0]

    shape.change_color(shape1, color1)
    shape.change_color(shape2, color2)
    cmds.parent(shape2, curve1, relative=True, shape=True)
    cmds.delete(curve2)
    ctrl = cmds.rename(curve1, name)

    cmds.setAttr(ctrl + '.tx', lock=True, keyable=False, channelBox=False)
    cmds.setAttr(ctrl + '.ty', lock=True, keyable=False, channelBox=False)
    cmds.setAttr(ctrl + '.tz', lock=True, keyable=False, channelBox=False)
    cmds.setAttr(ctrl + '.rx', lock=True, keyable=False, channelBox=False)
    cmds.setAttr(ctrl + '.ry', lock=True, keyable=False, channelBox=False)
    cmds.setAttr(ctrl + '.rz', lock=True, keyable=False, channelBox=False)
    cmds.setAttr(ctrl + '.sx', lock=True, keyable=False, channelBox=False)
    cmds.setAttr(ctrl + '.sy', lock=True, keyable=False, channelBox=False)
    cmds.setAttr(ctrl + '.sz', lock=True, keyable=False, channelBox=False)
    cmds.setAttr(ctrl + '.v', lock=True, keyable=False, channelBox=False)

    return ctrl


def circle_shape(name):
    transform, node = cmds.circle(name=name)
    cmds.delete(node)

    return transform


def square_shape(name):
    transform, node = cmds.circle(name=name, degree=1, sections=4)
    cmds.delete(node)
    return transform


def octagon_shape(name):
    transform, node = cmds.circle(name=name, degree=1, sections=8)
    cmds.delete(node)
    return transform


def triangle_shape(name):
    transform, node = cmds.circle(name=name, degree=1, sections=3)
    cmds.delete(node)
    return transform


def cube_shape(name):
    transforms = {'t1': (0, 1, 0),
                  'r1': (90, 0, 0),
                  't2': (0, -1, 0),
                  'r2': (90, 0, 0),
                  't3': (0, 0, 1),
                  'r3': (0, 0, 0),
                  't4': (0, 0, -1),
                  'r4': (0, 0, 0)}
    main_transform = cmds.createNode('transform', name=name)
    squares = []
    for i in range(1, 5):
        translate = transforms['t' + str(i)]
        rotate = transforms['r' + str(i)]

        square = square_shape(name)
        shape.modify_shape(square, scale=(1.4142, 1.4142, 1.4142))
        shape.modify_shape(square, rotate=(0, 0, 45))
        shape.modify_shape(square, rotate=rotate)
        shape.modify_shape(square, translate=translate)

        curve_shape = cmds.listRelatives(square, s=True)[0]
        cmds.parent(curve_shape, main_transform, r=True, s=True)
        squares.append(square)
    cmds.delete(squares)
    shape.modify_shape(main_transform, scale=(0.5, 0.5, 0.5))

    return main_transform


def pyramid_shape(name):
    main_transform = cmds.createNode('transform', name=name)
    curve1 = cmds.curve(p=[(1, 0, 1), (0, 1.5, 0), (-1, 0, -1)], degree=1)
    curve2 = cmds.curve(p=[(-1, 0, 1), (0, 1.5, 0), (1, 0, -1)], degree=1)
    curve3 = cmds.curve(p=[(1, 0, 1), (-1, 0, 1), (-1, 0, -1), (1, 0, -1), (1, 0, 1)], degree=1)
    curve1_shape = cmds.listRelatives(curve1, s=True)[0]
    curve2_shape = cmds.listRelatives(curve2, s=True)[0]
    curve3_shape = cmds.listRelatives(curve3, s=True)[0]

    cmds.parent(curve1_shape, main_transform, r=True, s=True)
    cmds.parent(curve2_shape, main_transform, r=True, s=True)
    cmds.parent(curve3_shape, main_transform, r=True, s=True)

    cmds.delete([curve1, curve2, curve3])

    return main_transform


def line_shape(name, length=1.0):
    transform = cmds.curve(name=name, p=[(0, 0, 0), (0, length, 0)], degree=1)

    return transform


def arrow_flat_shape(name):
    line_transform = line_shape(name, length=2)
    triangle_transform = triangle_shape(name)
    shape.modify_shape(triangle_transform, scale=(.5, .5, .5))
    shape.modify_shape(triangle_transform, translate=(0, 2.25, 0))
    tri_shape = cmds.listRelatives(triangle_transform, s=True)[0]
    cmds.parent(tri_shape, line_transform, r=True, s=True)

    return line_transform


def arrow_3d_shape(name):
    pyr_transform = pyramid_shape(name)
    line_transform = line_shape(name, length=2)
    shape.modify_shape(pyr_transform, scale=(.3, .5, .3))
    shape.modify_shape(pyr_transform, translate=(0, 2, 0))
    line = cmds.listRelatives(line_transform, s=True)[0]
    cmds.parent(line, pyr_transform, r=True, s=True)

    cmds.delete(line_transform)

    return pyr_transform


def transformation_axis_shape(name, color1='red', color2='green', color3='blue'):
    main_transform = cmds.createNode('transform', name=name)
    x_axis = arrow_3d_shape(name + '_x')
    y_axis = arrow_3d_shape(name + '_y')
    z_axis = arrow_3d_shape(name + '_z')

    shape.modify_shape(x_axis, rotate=(0, 0, -90))
    shape.modify_shape(z_axis, rotate=(90, 0, 0))

    x_shapes = cmds.listRelatives(x_axis, s=True)
    y_shapes = cmds.listRelatives(y_axis, s=True)
    z_shapes = cmds.listRelatives(z_axis, s=True)

    shape.change_color(x_axis, color1, change_shapes=True)
    shape.change_color(y_axis, color2, change_shapes=True)
    shape.change_color(z_axis, color3, change_shapes=True)

    cmds.parent(x_shapes, main_transform, r=True, s=True)
    cmds.parent(y_shapes, main_transform, r=True, s=True)
    cmds.parent(z_shapes, main_transform, r=True, s=True)

    cmds.delete([x_axis, y_axis, z_axis])

    shape.modify_shape(main_transform, scale=(0.18, 0.18, 0.18))

    return main_transform


def long_hexagon_shape(name):
    positions = [[-0.5531369714957218, 4.647786040080469e-17, -0.9974591164649268], [-0.8949943842653815, -4.647786349942732e-17, -0.2924800290495903], [-0.3667070776026866, -1.7752964658616013e-17, 0.9974602224520734], [0.36670707760268656, -1.7752964658616007e-17, 0.9974602224520734], [0.8949943842653815, -4.6477863499427315e-17, -0.29248002904959036], [0.5531369714957218, -2.4533993216454873e-17, -0.9974602224520734], [-0.5531369714957218, 4.6477863499427315e-17, -0.9974591164649268]]
    curve, node = cmds.circle(name=name, degree=1, s=6)
    for i, pos in enumerate(positions):
        cmds.xform('{}.cv[{}]'.format(curve, str(i)), t=(pos[0], pos[1], pos[2]), ws=True)
    shape.modify_shape(curve, scale=(0.5, 0.5, 0.5))

    return curve


def crescent_shape(name):
    positions = [[0.7836116248912245, 0.7836116248912246, 3.9597584073715234e-16], [6.785732323110912e-17, 1.1081941875543877, 5.599944043425693e-16], [-0.7836116248912245, 0.7836116248912244, 3.959758407371522e-16], [-1.1081941875543881, 5.74489823752483e-17, 2.903020880872022e-32], [-0.7836116248912245, 0.4609104824753293, 1.567029985641954e-16], [-1.1100856969603225e-16, 0.6491536556677218, 2.2042481078564902e-16], [0.7836116248912245, 0.4609104824753294, 1.5670299856419544e-16], [1.1081941875543881, -1.511240500779959e-16, -7.636623919858859e-32]]
    curve, node = cmds.circle(name=name, degree=3, s=8)
    for i, pos in enumerate(positions):
        cmds.xform('{}.cv[{}]'.format(curve, str(i)), t=(pos[0], pos[1], pos[2]), ws=True)

    return curve


def dotted_circle_shape(name):
    transform = cmds.createNode('transform', name=name)
    curve, node = cmds.circle(name=name, degree=3, sweep=20)
    first_shape = cmds.listRelatives(curve, s=True)[0]
    cmds.parent(first_shape, transform, r=True, s=True)
    cmds.delete(curve)
    rot = 30
    for i in range(12):
        crv = cmds.duplicate(first_shape, name=name + 'dot' + str(i))[0]
        cmds.setAttr(crv + '.rz', rot)
        cmds.makeIdentity([crv], apply=True)
        new_shape = cmds.listRelatives(crv, s=True)[0]
        cmds.parent(new_shape, transform, r=True, s=True)
        cmds.delete(crv)
        rot += 30
    cmds.delete(node)

    return transform


def global_shape(name):
    positions = [[3.6689236135146186e-16, 3.6689236135146186e-16, -5.991806970089769], [-1.3549857527947027, 2.5535216613351093e-16, -4.170217344483276], [-2.5773360593649928, 2.1721552653591544e-16, -3.547398755088395], [-3.5473987550883948, 1.5781631777141957e-16, -2.577336059364993], [-4.170217344483275, 8.296894825251505e-17, -1.354985752794704], [-5.991806970089768, 8.356552986775701e-32, -1.3647286699469357e-15], [-4.170217344483274, -8.296894825251492e-17, 1.3549857527947018], [-3.5473987550883948, -1.5781631777141944e-16, 2.577336059364991], [-2.577336059364993, -2.1721552653591534e-16, 3.5473987550883934], [-1.3549857527947045, -2.5535216613351074e-16, 4.170217344483273], [-3.027789184272726e-15, -3.668923613514617e-16, 5.991806970089766], [1.3549857527947, -2.553521661335108e-16, 4.170217344483273], [2.5773360593649897, -2.1721552653591539e-16, 3.5473987550883943], [3.547398755088392, -1.5781631777141957e-16, 2.577336059364993], [4.170217344483272, -8.296894825251509e-17, 1.3549857527947047], [5.991806970089766, -2.2613185136377715e-31, 3.693013390003041e-15], [4.170217344483273, 8.296894825251477e-17, -1.3549857527946993], [3.5473987550883943, 1.5781631777141934e-16, -2.577336059364989], [2.577336059364993, 2.1721552653591521e-16, -3.547398755088391], [1.354985752794705, 2.5535216613351064e-16, -4.170217344483271], [4.35823759573336e-15, 3.6689236135146166e-16, -5.991806970089765]]
    curve, node = cmds.circle(name=name, degree=1, s=20)
    for i, pos in enumerate(positions):
        cmds.xform('{}.cv[{}]'.format(curve, str(i)), t=(pos[0], pos[1], pos[2]), ws=True)
    cmds.delete(node)

    return curve


shapes = {'diamond': diamond_shape,
          'sphere': sphere_shape,
          'gear': gear_shape,
          'circle': circle_shape,
          'square': square_shape,
          'octagon': octagon_shape,
          'triangle': triangle_shape,
          'cube': cube_shape,
          'pyramid': pyramid_shape,
          'line': line_shape,
          'arrow_flat': arrow_flat_shape,
          'arrow_3d': arrow_3d_shape,
          'transformation_axis': transformation_axis_shape,
          'long_hexagon': long_hexagon_shape,
          'crescent': crescent_shape,
          'dotted_circle': dotted_circle_shape,
          'global': global_shape}
