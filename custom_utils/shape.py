import maya.cmds as cmds
import maya.mel as mel


def modify_shape(dag, translate=(0, 0, 0), rotate=(0, 0, 0), scale=(1, 1, 1), rotationOrder='xyz'):
    '''Modify a shape node without affecting transforms

    Args:
        dag (str): dag node of shape node
        translate
    '''
    rotate_dict = {}
    rotate_dict['x'] = rotate[0]
    rotate_dict['y'] = rotate[1]
    rotate_dict['z'] = rotate[2]

    shapes = cmds.listRelatives(dag, s=True)
    cvs = []
    for each in shapes:
        cv_list = cmds.ls('{}.cv[*]'.format(each))
        for cv in cv_list:
            cvs.append(cv)
    cmds.select(cvs)

    cmds.move(*translate, relative=True)
    for each in rotationOrder:
        if each == 'x':
            cmds.rotate(rotate[0], 0, 0, cvs, ocp=True)
        elif each == 'y':
            cmds.rotate(0, rotate[1], 0, cvs, ocp=True)
        elif each == 'z':
            cmds.rotate(0, 0, rotate[2], cvs, ocp=True)
    cmds.scale(*scale)

    cmds.select(clear=True)


def get_shapes(dag):
    '''
    Returns shapes of a dag node
    Args:
        dag(str): dag node to get shapes from
    Return:
        shapes[list]: list of shapes
    '''
    shapes = cmds.listRelatives(dag, s=True)

    return shapes


def change_color(dag, color, change_shapes=False):
    colors = {'yellow': 17,
              'red': 13,
              'blue': 6,
              'brown': 24,
              'green': 14,
              'light red': 20,
              'light blue': 18,
              'light brown': 21,
              'light green': 23}

    if change_shapes:
        dag_shapes = cmds.listRelatives(dag, s=True)
        for shape in dag_shapes:
            cmds.setAttr(shape + '.overrideEnabled', 1)
            cmds.setAttr(shape + '.overrideColor', colors[color])
    else:
        cmds.setAttr(dag + '.overrideEnabled', 1)
        cmds.setAttr(dag + '.overrideColor', colors[color])


def rebuild_blendshapes(bs_node, geo, new_bs=None, bs_connect=True, exclude=[]):
    '''Rebuilds all the blendshape connections onto a new blendshape node on the
    given object
    Args:
        bs_node (str): blendshape to copy
        geo (str): geo to put connections onto
        new_bs (str): if given will put new connections onto this blendshape
        bs_connect (bool): if true will make connections to old bs node, if False
                           will connect to old connections
        exclude list[(str)]: list of targets to exclude
    '''
    conns = {}

    # Get all blendshape targets
    targets = cmds.ls(bs_node + '.w[*]')

    # Get all connections for the targets
    for target in targets:
        weight = target.split('.')[1]

        conn = cmds.listConnections(target, plugs=True)
        if conn:
            conns[weight] = conn[0]
        else:
            conns[weight] = None

    # Create blendshape node if not given
    if not new_bs:
        new_bs = cmds.blendShape(geo, name=geo + '_BS')[0]

    geo_shape = cmds.listRelatives(geo, s=True)[0]

    # Duplicate geos
    for i, target in enumerate(targets):
        base_name = target.split('.')[1]
        if base_name not in exclude:
            # If attribute is connected then disconnect so you can toggle it
            if conns[base_name]:
                cmds.disconnectAttr(conns[base_name], target)
            cmds.setAttr(target, 1)
            bs_geo = cmds.duplicate(geo, name='{}_{}_BS'.format(geo, base_name))[0]
            bs_geo_shape = cmds.listRelatives(bs_geo, s=True)[0]
            mel.eval('blendShape -e -tc on -t {} {} {} 1 -w {} 0  {};'.format(geo_shape, i, bs_geo_shape, i, new_bs))
            if bs_connect:
                cmds.connectAttr(target, new_bs + '.' + bs_geo_shape)
            elif conns[base_name]:
                cmds.connectAttr(conns[base_name], new_bs + '.' + bs_geo_shape)

            # rename attribute to it's base name
            mel.eval('blendShapeRenameTargetAlias {} {} {};'.format(new_bs, i, base_name))

            # Reconnect connected attribute
            if conns[base_name]:
                cmds.connectAttr(conns[base_name], target)
            else:
                # Set attribute back to 0
                cmds.setAttr(target, 0)
