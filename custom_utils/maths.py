import maya.cmds as cmds
import maya.api.OpenMaya as om


def get_normal_of_plane(root, mid, end):
    '''Creates normalized normal vector of a plane
    Args:
        root om.MVector(): Root Vector
        mid om.MVector(): Mid Vector
        end om.MVector(): End Vector
    Return:
        om.MVector(): normal of plane
    '''
    # get vectors of plane
    upper_vec = (mid - root).normal()
    line_vec = (end - mid).normal()

    normal_vector = (upper_vec ^ line_vec).normal()

    return normal_vector


def get_pole_vector(root, mid, end, length=1.0, flip=False):
    '''Create a pole vector based on 3 positions on a plane
    Args:
        root: Root vector
        mid: Mid vector
        end: End vector
        length: Length of pole vector from mid of chain
        flip: whether the direction of the pole vector needs to be flipped
    Return:
         om.MVector(): pole vector position
    '''
    if flip:
        direction = 1
    else:
        direction = -1
    norm = get_normal_of_plane(root, mid, end).normal() * direction
    cross = (end - root).normal()
    pv = (norm ^ cross).normal() * length
    pv += mid

    return pv


def create_matrix(x_vec=(1, 0, 0), y_vec=(0, 1, 0), z_vec=(0, 0, 1), pos=(0, 0, 0), orient=True):
    '''
    Takes 4 vectors as inputs to create an MMatrix
    Returns:
        MMatrix: matrix from given vectors
    '''
    matrix = om.MMatrix((x_vec[0], x_vec[1], x_vec[2], 0,
                        y_vec[0], y_vec[1], y_vec[2], 0,
                        z_vec[0], z_vec[1], z_vec[2], 0,
                        pos[0], pos[1], pos[2], 1))

    return matrix


def plane_matrix(vector1, vector2, vector3):
    '''
    Get the transformation of the center of a plane
    Args:
        vector1 om.MVector(): first point of plane
        vector2 om.MVector(): second point of plane
        vector3 om.MVector(): third point of plane
    Return:
        om.MMatrix(): matrix of plane
    '''
    norm_vec = get_normal_of_plane(vector1, vector2, vector3)

    up_vec = (vector1 - vector2).normal()

    side_vec = norm_vec ^ up_vec

    pos_x = 0
    pos_y = 0
    pos_z = 0
    for each in [vector1, vector2, vector3]:
        pos = each
        pos_x += pos[0]
        pos_y += pos[1]
        pos_z += pos[2]
    pos_x = pos_x/3.0
    pos_y = pos_y/3.0
    pos_z = pos_z/3.0
    pos = [pos_x, pos_y, pos_z]
    mtx = create_matrix(x_vec=up_vec, y_vec=norm_vec, z_vec=side_vec, pos=pos)

    return mtx


def point_to_plane(pointA, pointB, pointC, project_point):
        A = om.MVector(pointA)
        B = om.MVector(pointB)
        C = om.MVector(pointC)
        D = om.MVector(project_point)

        v1 = B - A
        v2 = B - C
        n = (v1 ^ v2).normal()

        d = ((n.x * B.x) + (n.y * B.y) + (n.z * B.z)) * -1.0

        distance = (n.x * D.x) + (n.y * D.y) + (n.z * D.z) + d
        scaled_normal = n * distance

        final_point = D - scaled_normal

        return final_point


def axis_in_matrix(matrix, axis='+x', add_pos=True):
    ''' Gets the position of a vector within a given matrix
    Args:
        matrix (om.MMatrix()): matrix to get vector from
        axis (str): +x, -x, +y, -y, +z, -z
    Returns:
        om.MVector(): point within matrix
    '''
    point = None

    x = om.MVector(matrix[0], matrix[1], matrix[2])
    y = om.MVector(matrix[4], matrix[5], matrix[6])
    z = om.MVector(matrix[8], matrix[9], matrix[10])
    pos = om.MVector(matrix[12], matrix[13], matrix[14])

    if axis == '+x':
        point = x
    elif axis == '-x':
        point = -x
    elif axis == '+y':
        point = y
    elif axis == '-y':
        point = -y
    elif axis == '+z':
        point = z
    elif axis == '-z':
        point = -z
    elif axis == 'pos':
        point = pos

    if point != pos:
        if add_pos:
            point += pos

    return point


def matrix_to_vectors(matrix):
    '''Deconstructs a given matrix into 4 vectors
    Args:
        matrix: matrix to be deconstructed
    Return:
        four vectors of the transformation matrix
    '''
    vec1 = om.MVector(matrix[0], matrix[1], matrix[2])
    vec2 = om.MVector(matrix[4], matrix[5], matrix[6])
    vec3 = om.MVector(matrix[8], matrix[9], matrix[10])
    vec4 = om.MVector(matrix[12], matrix[13], matrix[14])

    return [vec1, vec2, vec3, vec4]


def mirror_matrix(matrix, axis='x', x_reverse=False, y_reverse=False, z_reverse=True, position=True, rotation=True):
    '''
    Args:
        matrix: matrix to mirror
        axis: axis to mirror over
        aim_reverse: if True reverses the aim vector
        position: if position should be mirrored
        rotation: if rotation should be mirrored
    Return:
        MMatrix: mirrored matrix
    '''
    x, y, z, pos = matrix_to_vectors(matrix)
    if x_reverse:
        x_reverse = -1
    else:
        x_reverse = 1
    if y_reverse:
        y_reverse = -1
    else:
        y_reverse = 1
    if z_reverse:
        z_reverse = -1
    else:
        z_reverse = 1

    if axis == 'x':
        new_x = om.MVector(x[0]*-1, x[1], x[2]) * x_reverse
        new_y = om.MVector(y[0]*-1, y[1], y[2]) * y_reverse
        new_z = (new_x ^ new_y) * z_reverse
        new_pos = (pos[0] * -1, pos[1], pos[2])
    elif axis == 'y':
        new_x = om.MVector(x[0], x[1]*-1, x[2]) * x_reverse
        new_y = om.MVector(y[0], y[1]*-1, y[2]) * y_reverse
        new_z = new_x ^ new_y * z_reverse
        new_pos = (pos[0], pos[1]*-1, pos[2])
    elif axis == 'z':
        new_x = om.MVector(x[0], x[1], x[2]*-1) * x_reverse
        new_z = om.MVector(z[0], z[1], z[2]*-1) * y_reverse
        new_y = new_z ^ new_x * z_reverse
        new_pos = (pos[0], pos[1], pos[2]*-1)

    if not rotation:
        new_x = x
        new_y = y
        new_z = z
    else:
        new_x *= -1
        new_y *= -1
        new_z *= -1
    if not position:
        new_pos = pos

    new_matrix = om.MMatrix((new_x[0], new_x[1], new_x[2], 0,
                             new_y[0], new_y[1], new_y[2], 0,
                             new_z[0], new_z[1], new_z[2], 0,
                             new_pos[0], new_pos[1], new_pos[2], 1))

    return new_matrix


def fixed_matrix(pos, aim_vector, up_vector, aim='x', up='y'):
    '''
    Fixes transformation values of matrix so all axiss are pointing in the right direction
    Args:
        pos (tuple): position of transformation matrix
        aim_vector (tuple): where to align the given aim axis
        up_vector (tuple): where to align the given up axis
        aim (str): axis to use for aim
        up (str): axis to use for up
    Return:
        om.MMatrix(): transformation matrix with accurate axiss
    '''
    # Create empty null to check transforms
    null = cmds.createNode('transform')

    aim_vector = om.MVector(aim_vector[0], aim_vector[1], aim_vector[2]).normal()
    up_vector = om.MVector(up_vector[0], up_vector[1], up_vector[2]).normal()

    # Account for negative values
    if '-' in aim:
        aim_vector = aim_vector * -1
    if '-' in up:
        up_vector = up_vector * -1

    # Create side vector
    side_vec = aim_vector ^ up_vector

    # Realign up vector
    up_vec = aim_vector ^ side_vec
    up_dot = up_vector * up_vec
    if up_dot < 0:
        up_vec *= -1

    # Assign all vectors
    vecs = {}
    for vec in ['x', 'y', 'z']:
        if vec in aim:
            vecs[vec] = aim_vector
        elif vec in up:
            vecs[vec] = up_vec
        else:
            vecs[vec] = side_vec

    dag_mtx = create_matrix(x_vec=(vecs['x']).normal(), y_vec=(vecs['y']).normal(),
                            z_vec=(vecs['z']).normal(), pos=pos)

    # Check to see if side vector is pointing the right way
    cmds.xform(null, matrix=dag_mtx)
    scales = cmds.getAttr(null + '.s')[0]

    for scale in scales:
        if scale < 0:
            side_vec *= -1
            dag_mtx = create_matrix(x_vec=(vecs['x']).normal(), y_vec=(vecs['y']).normal(),
                                    z_vec=(vecs['z']).normal(), pos=pos)
            cmds.xform(null, matrix=dag_mtx)

    cmds.delete(null)

    return dag_mtx


def average_vectors(vectors):
    added_vecs = om.MVector()
    for each in vectors:
        added_vecs += each

    vector = added_vecs/float(len(vectors))

    return vector


def remap_values(OldMin, OldMax, NewMin, NewMax, input):
    OldRange = (OldMax - OldMin)
    NewRange = (NewMax - NewMin)
    NewValue = (((input - OldMin) * NewRange) / OldRange) + NewMin

    return NewValue

def get_MVector(object):
    pos = cmds.xform(object, t=True, ws=True, q=True)
    vec = om.MVector(pos[0], pos[1], pos[2])

    return vec


def linspace():
    pass
