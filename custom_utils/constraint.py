import maya.cmds as cmds
import maya.api.OpenMaya as om

import create.nodes as node


def attach_to_surface(surface, dag):
    """Takes a dag node and attaches it to the nearest point on a surface,
       replacement for a follicle
    Args:
         surface (str): name of the transform of the nurbs surface that the dag
                        node will be attached to
         dag (str): name of the dag node which will be attached
    Returns:
        str: Name of transform that follows the surface
    """

    # nodes to attach dag to surface
    POSI = cmds.createNode('pointOnSurfaceInfo', name=dag + '_POSI')
    matrix_node = cmds.createNode('fourByFourMatrix', name=dag + '_4X4')
    foll = cmds.createNode('transform', name=dag + '_custom_foll')
    foll_MSC = cmds.createNode('millSimpleConstraint', name=foll + '_MSC')
    MM = cmds.createNode('multMatrix', name=dag + '_MM')

    # find closest point on surface and it's UV values
    sel = om.MSelectionList()
    sel.add(surface)

    plane = om.MFnNurbsSurface()
    plane.setObject(sel.getDagPath(0))

    point = om.MPoint(om.MVector(cmds.xform(dag, t=True, ws=True, q=True)))

    pos, parU, parV = plane.closestPoint(point)

    cmds.xform(dag, t=(pos[0], pos[1], pos[2]), ws=True)

    # attach dag to surface
    cmds.connectAttr(surface + '.local', POSI + '.inputSurface')
    cmds.setAttr(POSI + '.parameterU', parU)
    cmds.setAttr(POSI + '.parameterV', parV)

    # create matrix from POSI node
    cmds.connectAttr(POSI + '.normalizedTangentVX', matrix_node + '.in00')
    cmds.connectAttr(POSI + '.normalizedTangentVY', matrix_node + '.in01')
    cmds.connectAttr(POSI + '.normalizedTangentVZ', matrix_node + '.in02')
    cmds.connectAttr(POSI + '.normalizedNormalX', matrix_node + '.in10')
    cmds.connectAttr(POSI + '.normalizedNormalY', matrix_node + '.in11')
    cmds.connectAttr(POSI + '.normalizedNormalZ', matrix_node + '.in12')
    cmds.connectAttr(POSI + '.normalizedTangentUX', matrix_node + '.in20')
    cmds.connectAttr(POSI + '.normalizedTangentUY', matrix_node + '.in21')
    cmds.connectAttr(POSI + '.normalizedTangentUZ', matrix_node + '.in22')
    cmds.connectAttr(POSI + '.positionX', matrix_node + '.in30')
    cmds.connectAttr(POSI + '.positionY', matrix_node + '.in31')
    cmds.connectAttr(POSI + '.positionZ', matrix_node + '.in32')

    cmds.connectAttr(matrix_node + '.output', MM + '.matrixIn[0]')
    cmds.connectAttr(surface + '.worldMatrix[0]', MM + '.matrixIn[1]')

    cmds.connectAttr(MM + '.matrixSum', foll_MSC + '.inMatrix')
    cmds.connectAttr(foll + '.parentInverseMatrix[0]', foll_MSC + '.parentInverseMatrix')

    cmds.connectAttr(foll_MSC + '.outTranslate', foll + '.translate')
    cmds.connectAttr(foll_MSC + '.outRotate', foll + '.rotate')

    matrix_constraint(foll, dag)

    return foll


def matrix_constraint(source, target, maintain_offset=True, connect='srt'):
    """
    Args:
        object (str): object that will be constraining an object
        target (str): object being constrained
        maintain_offset (bool): maintains the offset between the objects
        connect (str): connects scale, rotation, translation
    Returns:
        str: name of node that outputs translation and rotation values
    """
    constraint = node.Node(target, 'matrixConstraint')

    if maintain_offset:
        # get matrix of target within the space of the source object
        proxy = cmds.createNode('transform', name=target + '_temp_PROXY')
        proxy_pos = cmds.xform(target, t=True, ws=True, q=True)
        proxy_rot = cmds.xform(target, ro=True, ws=True, q=True)
        cmds.xform(proxy, t=proxy_pos, ws=True)
        cmds.xform(proxy, ro=proxy_rot, ws=True)
        cmds.parent(proxy, source)

        target_mtx = cmds.xform(proxy, matrix=True, ws=False, q=True)
        target_pos = cmds.getAttr(proxy + '.t')[0]

        cmds.delete(proxy)

        # get vectors of matrix
        target_x = om.MVector((target_mtx[0], target_mtx[1], target_mtx[2]))
        target_y = om.MVector((target_mtx[4], target_mtx[5], target_mtx[6]))
        target_z = om.MVector((target_mtx[8], target_mtx[9], target_mtx[10]))
        pos_vec = om.MVector((target_pos[0], target_pos[1], target_pos[2]))

        x_vec = pos_vec + target_x
        y_vec = pos_vec + target_y
        z_vec = pos_vec + target_z
    else:
        pos_vec = (0, 0, 0)
        x_vec = (1, 0, 0)
        y_vec = (0, 1, 0)
        z_vec = (0, 0, 1)

    constraint.connect(sourceMatrix=source + '.worldMatrix[0]',
                       parentInverseMatrix=target + '.parentInverseMatrix[0]',
                       xVector=(x_vec[0], x_vec[1], x_vec[2]),
                       yVector=(y_vec[0], y_vec[1], y_vec[2]),
                       zVector=(z_vec[0], z_vec[1], z_vec[2]),
                       posVector=(pos_vec[0], pos_vec[1], pos_vec[2]))

    if 's' in connect:
        constraint.connect(outScale=target + '.s')
    if 'r' in connect:
        constraint.connect(outRotate=target + '.r')
    if 't' in connect:
        constraint.connect(outTranslate=target + '.t')

    return constraint.node
