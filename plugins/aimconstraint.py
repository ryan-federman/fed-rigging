import maya.api.OpenMaya as om

def maya_useNewAPI():
    pass

class AimNode(om.MPxNode):
        node_name = 'ryanAim'
        node_id = om.MTypeId(0x83000)

        # class variables
        target_mtx_in = None
        aim_mtx_in = None
        up_mtx_in = None
        output = None

        def __init__(self):
            om.MPxNode.__init__(self)

        def compute(self, plug, data):
            if plug == AimNode.output:
                # Get Matrices
                tgt_mtx = data.inputValue(AimNode.target_mtx_in).asMatrix()
                aim_mtx = data.inputValue(AimNode.aim_mtx_in).asMatrix()
                up_mtx = data.inputValue(AimNode.up_mtx_in).asMatrix()

                # Calculate 3 vectors for rotation
                tgt_pos = om.MVector(tgt_mtx.getElement(3, 0), tgt_mtx.getElement(3, 1), tgt_mtx.getElement(3, 2))
                aim_pos = om.MVector(aim_mtx.getElement(3, 0), aim_mtx.getElement(3, 1), aim_mtx.getElement(3, 2))
                up_pos = om.MVector(up_mtx.getElement(3, 0), up_mtx.getElement(3, 1), up_mtx.getElement(3, 2))

                aim_vec = (aim_pos - tgt_pos).normal()
                up_vec = (up_pos - tgt_pos).normal() * -1
                side_vec = aim_vec ^ up_vec

                # Revaluate up vec
                up_vec = aim_vec ^ side_vec

                # Create matrix
                mmtx = vectors_to_matrix(aim_vec, up_vec, side_vec)

                outputHandle = data.outputValue(AimNode.output)
                outputHandle.setMMatrix(mmtx)

                data.setClean(plug)

        # creator
        @staticmethod
        def creator():
                return AimNode()

        # initializer
        @staticmethod
        def initialize():
                nAttr = om.MFnMatrixAttribute()

                # target mtx
                AimNode.target_mtx_in = nAttr.create("targetObject", "tgtObj", om.MFnMatrixAttribute.kDouble)
                nAttr.storable = True

                # aim mtx
                AimNode.aim_mtx_in = nAttr.create("aimObject", "aimObj", om.MFnMatrixAttribute.kDouble)
                nAttr.storable = True

                # up mtx
                AimNode.up_mtx_in = nAttr.create("upObject", "upObj", om.MFnMatrixAttribute.kDouble)
                nAttr.storable = True


                # output mtx
                AimNode.output = nAttr.create("output", "out", om.MFnMatrixAttribute.kDouble)
                nAttr.storable = True
                nAttr.writable = True

                # add attributes
                AimNode.addAttribute(AimNode.target_mtx_in)
                AimNode.addAttribute(AimNode.up_mtx_in)
                AimNode.addAttribute(AimNode.aim_mtx_in)
                AimNode.addAttribute(AimNode.output)
                AimNode.attributeAffects(AimNode.target_mtx_in, AimNode.output)
                AimNode.attributeAffects(AimNode.up_mtx_in, AimNode.output)
                AimNode.attributeAffects(AimNode.aim_mtx_in, AimNode.output)

# initialize the script plug-in
def initializePlugin(obj):
    fn_plugin = om.MFnPlugin(obj, 'ryan federman', '1.0')
    fn_plugin.registerNode(AimNode.node_name, AimNode.node_id, AimNode.creator, AimNode.initialize)


# uninitialize the script plug-in
def uninitializePlugin(obj):
    fn_plugin = om.MFnPlugin(obj)
    fn_plugin.deregisterNode(AimNode.node_id)


def vectors_to_matrix(x_vec=(1, 0, 0), y_vec=(0, 1, 0), z_vec=(0, 0, 1), pos=(0, 0, 0)):
    '''
    Takes 4 vectors as inputs to create an MMatrix
    Returns:
        MMatrix: matrix from given vectors
    '''
    matrix = om.MMatrix((x_vec[0], x_vec[1], x_vec[2], 0,
                        y_vec[0], y_vec[1], y_vec[2], 0,
                        z_vec[0], z_vec[1], z_vec[2], 0,
                        pos[0], pos[1], pos[2], 1))
    return matrix
