# import numpy as np
import copy as copy

import maya.cmds as cmds
import maya.api.OpenMaya as om
import maya.mel as mel

import create.nurbs as nrb
import fedrig.layout.layout_objects as lyt
import custom_utils.component as comp
import custom_utils.maths as maths
import custom_utils.skin as skin
import custom_utils.generic as gen
import tools.transform as trans

import sys

path = 'M:/home/harryh/git/rigginglab/rigginglab/skinning_utilities'
if path not in sys.path:
   sys.path.append(path)

import skinFromVirtualNrb

reload(gen)

import sys
p = 'M:/home/RyanF/git/fed-rigging'
if p not in sys.path:
    sys.path.append(p)
# path = 'M:/home/harryh/git/rigginglab/rigginglab/skinning_utilities'
# if path not in sys.path:
#    sys.path.append(path)
#
# import skinFromVirtualNrb
#
# reload(gen)

# import weight_utils_v31 as wgt


# def wing_locs(name, create=True):
#     start = []
#     end = []
#     if create:
#         # Create starter locs
#         for i in range(4):
#             start_loc = cmds.spaceLocator(name='Lf_{}_{}_LOC'.format(name, i))[0]
#             end_loc = cmds.spaceLocator(name='Lf_{}End_{}_LOC'.format(name, i))[0]
#             start.append(start_loc)
#             end.append(end_loc)
#
#         up_loc0 = cmds.spaceLocator(name='Lf_{}_upLoc0_LOC'.format(name))[0]
#         up_loc1 = cmds.spaceLocator(name='Lf_{}_upLoc1_LOC'.format(name))[0]
#
#     else:
#         for i in range(4):
#             start_loc = 'Lf_{}_{}_LOC'.format(name, i)
#             end_loc = 'Lf_{}End_{}_LOC'.format(name, i)
#             start.append(start_loc)
#             end.append(end_loc)
#
#         up_loc0 = 'Lf_{}_upLoc0_LOC'.format(name)
#         up_loc1 = 'Lf_{}_upLoc1_LOC'.format(name)
#
#     return start, end, up_loc0, up_loc1
#
#
# name = 'Wing0'
# start, end, up_loc0, up_loc1 = wing_locs(name, create=False)
#
# # Create joints
# main_jnts = []
# start_jnts = []
# end_jnts = []
# vertical = ['In', 'MidIn', 'MidOut', 'Out']
# vert_dict = dict()
# cmds.select(clear=True)
# for i in range(4):
#     if i < 3:
#         jnt = cmds.joint(name='Lf_{}_{}_JNT'.format(name, i))
#     else:
#         jnt = cmds.joint(name='Lf_{}Hand_0_JNT'.format(name))
#     main_jnts.append(jnt)
# cmds.select(clear=True)
#
# for i in range(10):
#     jnt = cmds.joint(name='Lf_{}Start_{}_JNT'.format(name, i))
#     start_jnts.append(jnt)
# cmds.select(clear=True)
#
# for i in range(10):
#     jnt = cmds.joint(name='Lf_{}End_{}_JNT'.format(name, i))
#     end_jnts.append(jnt)
# cmds.select(clear=True)
#
# for each in vertical:
#     vert_dict[each] = []
#     for i in range(4):
#         jnt = cmds.joint(name='Lf_{}{}_{}_JNT'.format(name, each, i))
#         vert_dict[each].append(jnt)
#     cmds.select(clear=True)
#
# parent_jnt = cmds.joint(name='Lf_{}Par_0_JNT'.format(name))
#
# # Reposition joints
# for i, each in enumerate(main_jnts):
#     pos = cmds.xform(start[i], t=True, ws=True, q=True)
#     cmds.xform(each, t=pos, ws=True)
#
# for i in range(3):
#     jnts = start_jnts[i * 3: (i * 3) + 4]
#     trans.dags_on_vector(jnts, [start[i], start[i + 1]])
#
# for i in range(3):
#     jnts = end_jnts[i * 3: (i * 3) + 4]
#     trans.dags_on_vector(jnts, [end[i], end[i + 1]])
#
# for i, each in enumerate(vertical):
#     trans.dags_on_vector(vert_dict[each], [start[i], end[i]])
#
# pos = cmds.xform(start[0], t=True, ws=True, q=True)
# cmds.xform(parent_jnt, t=pos, ws=True)
#
# # Orient joints
# trans.rot_to_plane(main_jnts[0:3], up='-y')
# trans.aim_at_next(main_jnts[2:4], up_vector=[up_loc0, up_loc1], rotate_last=True)
#
# for each in vertical:
#     jnts = vert_dict[each]
#     trans.aim_at_next(jnts, up_vector=[up_loc0, up_loc1], rotate_last=True)
#
# trans.aim_at_next(start_jnts, up_vector=[up_loc0, up_loc1], rotate_last=True)
# trans.aim_at_next(end_jnts, up_vector=[up_loc0, up_loc1], rotate_last=True)
#
# # Orient feathers
# sections = ['Bot', 'Mid', 'Top']
# for sect in sections:
#     bases = cmds.ls('Lf_{}_{}Feth*_0_JNT'.format(name, sect))
#     for base in bases:
#         new_base = base.split('_0_JNT')[0]
#         jnts = cmds.ls('{}_*_JNT'.format(new_base))
#         trans.aim_at_next(jnts, up_vector=[up_loc0, up_loc1], rotate_last=True)


# # Tools for placing wing joints
# sel = cmds.ls(sl=True)
# loc1 = cmds.spaceLocator()[0]
# loc2 = cmds.spaceLocator()[0]
# pos1 = cmds.xform(sel[0], t=True, ws=True, q=True)
# pos2 = cmds.xform(sel[3], t=True, ws=True, q=True)
# cmds.xform(loc1, t=pos1, ws=True)
# cmds.xform(loc2, t=pos2, ws=True)
# trans.dags_on_vector(sel, [loc1, loc2])
# cmds.delete([loc1, loc2])
#
# sel = cmds.ls(sl=True)
# refs = cmds.ls(sl=True)
# trans.dags_on_vector(sel, refs)
#
# trans.aim_at_next(cmds.ls(sl=True), up='y', aim='x', up_vector='y', rotate_last=True)
#
# for each in cmds.ls(sl=True):
#     base = each.split('0_LOC')[0]
#     loc = cmds.spaceLocator(name=base + '_1_LOC')[0]
#     cmds.parent(loc, each)
#     cmds.setAttr(loc + '.t', 0, -5, 0)
#     cmds.setAttr(loc + '.r', 0, 0, 0)
#
# trans.dags_to_mesh([cmds.ls(sl=True)[0]], cmds.ls(sl=True)[1])
#
# sel = cmds.ls(sl=True)
# geo = sel[-1]
# sel.remove(geo)
# trans.dags_to_mesh(sel, geo)
#
# for each in cmds.ls(sl=True):
#     base = each.split('_0_LOC')[0]
#     child = cmds.listRelatives(each, children=True)[1]
#     print child
#     cmds.rename(child, base + '_1_LOC')
#
# for i in range(34):
#     locs = ['Lf_PrimaryFeth{}_0_LOC'.format(i), 'Lf_PrimaryFeth{}_1_LOC'.format(i)]
#     base = 'Lf_PrimaryFeth'
#     cmds.select(clear=True)
#     jnts = []
#     for x in range(4):
#         joint = cmds.joint(name='{}{}_{}_JNT'.format(base, i, x))
#         jnts.append(joint)
#
#     trans.dags_on_vector(jnts, locs)

# def find_closest_mesh(dag, meshes):
#     closest_mesh = list()
#     pos = cmds.xform(dag, t=True, ws=True, q=True)
#     pos = om.MVector(pos[0], pos[1], pos[2])
#     for mesh in meshes:
#         point_on_mesh, normal, face = comp.closest_point_on_mesh(mesh, pos, add_normal=True)
#         vec_between = point_on_mesh - pos
#         distance = vec_between.length()
#
#         if closest_mesh:
#             if distance < closest_mesh[1]:
#                 closest_mesh = [mesh, distance]
#         else:
#             closest_mesh = [mesh, distance]
#
#     return closest_mesh[0]

# def skin_closest_hair(base, geos, level=3, vec_size=0.3, remove=None, rename=True, inside=True):
#     '''
#     Skins joint chains to geometries based on which is the closest to that joint chain
#     Args:
#         base (str): 0 joint of each strand with an asterisk Ex: Ct_Hair*_0_JNT
#         geos list[(str)]: list of geometries to skin
#         level int: indicates number of vectors to reference when finding closest geos, highest is 3
#         vec_size float: size of vectors coming from objects
#         remove (str): removes a string from the base list
#         inside (bool): indicates whether the function will make sure joints skinned are within the given geometry
#     '''
#
#     renamed_geos = dict()
#     base_jnts = cmds.ls(base)
#
#     if remove:
#         base_jnts.remove(remove)
#
#     # if len(base_jnts) != len(geos):
#     #     cmds.error('Length of joints does not equal length of geos')
#
#     if inside:
#         for jnt in base_jnts:
#             skin_check = 0
#             jnt_base = jnt.split('_0_')[0]
#             query_name = jnt.split('_0_')[0] + '_*_JNT'
#             strand_jnts = cmds.ls(query_name)
#             geo_list = copy.deepcopy(geos)
#             while len(geo_list) > 0:
#                 closest_geo = closest_average_geo(strand_jnts, geo_list, level=level, vec_size=vec_size)
#
#                 # Check to see if joints are inside mesh
#                 inside_check = 0
#                 for jnt in strand_jnts:
#                     pos = cmds.xform(jnt, t=True, ws=True, q=True)
#                     pos = om.MVector(pos[0], pos[1], pos[2])
#                     point_on_mesh, normal, face = comp.closest_point_on_mesh(closest_geo, pos, add_normal=True)
#                     vec_between = point_on_mesh - pos
#                     dot = vec_between * normal
#                     if dot > 0:
#                         inside_check += 1
#
#                 if inside_check == len(strand_jnts):
#                     cmds.skinCluster([closest_geo] + strand_jnts)
#                     geos.remove(closest_geo)
#                     if rename:
#                         renamed_geos[closest_geo] = jnt_base + '_GEO'
#
#                     skin_check = 1
#                     break
#                 else:
#                     geo_list.remove(closest_geo)
#
#             if skin_check == 0:
#                 print '{} chain was not skinned'.format(jnt)
#
#     else:
#         for jnt in base_jnts:
#             jnt_base = jnt.split('_0_')[0]
#             query_name = jnt.split('_0_')[0] + '_*_JNT'
#             strand_jnts = cmds.ls(query_name)
#             closest_geo = closest_average_geo(strand_jnts, geos, level=level, vec_size=vec_size)
#
#             if rename:
#                 renamed_geos[closest_geo] = jnt_base + '_GEO'
#
#             cmds.skinCluster([closest_geo] + strand_jnts)
#             geos.remove(closest_geo)
#
#     # Rename geos with their named equivalents
#     if rename:
#         for geo, new_name in renamed_geos.items():
#             cmds.rename(geo, new_name)
##### Create chain down verts tool
import maya.api.OpenMaya as om

if not cmds.ls('ChainStart_LOC'):
    start_loc = cmds.spaceLocator(name='ChainStart_LOC')[0]

chain_base_name = 'Ct_TailWaveA'

vertices = cmds.ls(sl=True, fl=True)


def chain_on_mesh(vertices, base_name, start_dag, aim='x', up='y'):
    '''
    Aligns a chain along a set of vertices on a mesh
    Args:
        vertices list[str]: list of vertices to lie chain on
        base_name (str): base name of chain
        start_dag (str): name of dag to take initial start position of
        aim (str): aim vector of chain
        up (str): up vector to align to normal
    '''

    vert_vecs = dict()

    # Get start position of chain
    start_pos = cmds.xform(start_dag, t=True, ws=True, q=True)
    start_pos = om.MVector(start_pos[0], start_pos[1], start_pos[2])

    # Record all positions of verts
    for vtx in vertices:
        pos = cmds.xform(vtx, t=True, ws=True, q=True)
        vert_vecs[vtx] = om.MVector(pos[0], pos[1], pos[2])

    # Find closest vertex to starting locator
    closest_vtx = ''
    closest_distance = None
    for vtx, pos in vert_vecs.items():
        distance = (start_pos - pos).length()
        if not closest_distance:
            closest_distance = distance
            closest_vtx = vtx
        else:
            if distance < closest_distance:
                closest_distance = distance
                closest_vtx = vtx

    cmds.select(clear=True)

    # Create first joint at vtx
    current_jnt = cmds.joint(name='{}_0_CHAIN'.format(base_name))
    cmds.xform(current_jnt, t=vert_vecs[closest_vtx], ws=True)

    current_pos = vert_vecs[closest_vtx]
    current_vtx = closest_vtx
    del vert_vecs[closest_vtx]
    closest_vtx = None
    closest_distance = None
    closest_pos = None
    # Go through remainder of verts and place the next joint based on the next closest vtx to the current one
    for i in range(len(vert_vecs)):
        for vtx, pos in vert_vecs.items():
            distance = (current_pos - pos).length()
            if not closest_vtx:
                closest_vtx = vtx
                closest_distance = distance
                closest_pos = pos
            else:
                if distance < closest_distance:
                    closest_vtx = vtx
                    closest_distance = distance
                    closest_pos = pos

        # Create joint and parent it to previous joint
        next_jnt = cmds.joint(name='{}_{}_CHAIN'.format(chain_base_name, i + 1))
        cmds.xform(next_jnt, t=closest_pos, ws=True)

        # Reset attributes for next loop
        del vert_vecs[closest_vtx]
        current_jnt = next_jnt
        current_vtx = closest_vtx
        current_pos = closest_pos
        closest_vtx = None
        closest_distance = None
        closest_pos = None

    # Align all joints based on the up vector of the geometry
    dag = trans.DagRelationships()
    chains = cmds.ls(sl=True)
    mesh = vertices[0].split('.vtx')[0]
    for chain in chains:
        base = chain.split('0')[0]
        chain = cmds.ls(base + '*_JNT')
        dag.list_dags(chain)
        dag.unparent()

        for i, each in enumerate(chain):
            mtx = cmds.xform(each, matrix=True, q=True)
            trans.dags_to_mesh([each], mesh, rotate=True, position=False, aim='z', up='y', up_vector=(0, 1, 0))

            # Get norm vec and compare it to the previous aim vec
            if i == 0:
                mtx = cmds.xform(each, matrix=True, q=True)
                vec = maths.axis_in_matrix(mtx, axis='+z', add_pos=False)
                old_up = vec
            else:
                mtx = cmds.xform(each, matrix=True, q=True)
                vec = maths.axis_in_matrix(mtx, axis='+z', add_pos=False)
                dot = vec * old_up
                if dot < 0:
                    trans.dags_to_mesh([each], mesh, rotate=True, position=False, aim='-z', up='y',
                                       up_vector=(0, 1, 0))
                    print dot
                    vec *= -1

                old_up = vec
        trans.aim_at_next(chain, up=up, aim=aim, up_vector='z', rotate_last=True)
        dag.parent()

############################ Create noise node on curve
def closest_point_on_curve(curve, point):
    ''' find closest point on curve and it's parameter

    Args:
        surface (str): nurbs surface
        point tuple(float, float, float): position to reference
    Return:
         tuple(float, float, float), (int), (int): position, u param, v param
    '''
    sel = om.MSelectionList()
    sel.add(curve)

    crv = om.MFnNurbsCurve()
    crv.setObject(sel.getDagPath(0))

    point = om.MPoint(om.MVector(point))

    pos, par = crv.closestPoint(point)

    return [pos, par]


# Create blendshape and noise curve
noise_crv = cmds.duplicate('Ct_WormRibbon_pathCrv_CRV', name='Ct_WormRibbon_noise_pathCrv_CRV')
bs_crv = cmds.duplicate('Ct_WormRibbon_pathCrv_CRV', name='Ct_WormRibbon_BS_noise_pathCrv_CRV')
noise_cvs = cmds.ls('Ct_WormRibbon_noise_pathCrv_CRV.cv[*]', fl=True)

# Create noise
deformer = cmds.textureDeformer(noise_crv,
                                name="{}_texureDeformer_TXD".format(crv),
                                direction="Handle",
                                pointSpace="World",
                                strength=1.0,  # IMPORTANT KEEP AT 1.0 ... if needs to scale use texture magnitude
                                offset=-0.5,
                                )[0]

noise = cmds.createNode("noise", n="{}_Noise_TXD".format(crv))
place2d = cmds.createNode("place2dTexture", n="{}_place2dTexture_TXD".format(crv))
cmds.connectAttr(noise + ".outColor", deformer + ".texture")
cmds.connectAttr(place2d + ".outUV", noise + ".uvCoord")
cmds.connectAttr(place2d + ".outUvFilterSize", noise + ".uvFilterSize")

cmds.setAttr(deformer + ".strength", 1.0)
cmds.setAttr(noise + ".threshold", 0.0)
cmds.setAttr(noise + ".amplitude", 1.0)
cmds.setAttr(noise + ".ratio", 0.0)
cmds.setAttr(noise + ".frequencyRatio", 2.0)
cmds.setAttr(noise + ".depthMax", 3.0)
cmds.setAttr(noise + ".frequency", 4.444)
cmds.setAttr(noise + ".noiseType", 2)  # Wave

cmds.setAttr(noise + ".numWaves", 5)  # Wave

# Create attributes on head control
attr.add_enum('Ct_WormRibbon_0_CTL', 'Jiggety', ['-'], keyable=False)
attr.add_float('Ct_WormRibbon_0_CTL', 'NoiseSize', default=60, min=-1000, max=1000)
attr.add_float('Ct_WormRibbon_0_CTL', 'NoiseFrequency', default=1, min=-1000, max=1000)
attr.add_float('Ct_WormRibbon_0_CTL', 'Time', default=0, min=-10000, max=10000)

cmds.connectAttr('Ct_WormRibbon_0_CTL.NoiseSize', noise + '.amplitude')
cmds.connectAttr('Ct_WormRibbon_0_CTL.NoiseFrequency', noise + '.frequency')
cmds.connectAttr('Ct_WormRibbon_0_CTL.Time', noise + '.time')

max_par = len(noise_cvs) - 2
for i, cv in enumerate(noise_cvs):
    # Get cv position
    noise_pos = cmds.xform('{}.cv[{}]'.format(noise_crv, i), t=True, ws=True, q=True)
    new_pos, par = closest_point_on_curve(noise_crv, noise_pos)
    mult_value = float(par) / float(max_par)

    # Hook up noise to poci nodes
    poci = cmds.createNode('pointOnCurveInfo', name='{}_cv_bs_POCI'.format(cv))
    cmds.connectAttr(noise_crv + '.local', poci + '.inputCurve')
    cmds.setAttr(poci + '.parameter', par)

    # Blend values from 0 to 1 based on position on the curve, add onto pre-existing translation values
    mdl = cmds.createNode('multDoubleLinear', name='{}_cv_bs_MDL'.format(cv))
    add = cmds.createNode('plusMinusAverage', name='{}_cv_bs_ADD'.format(cv))
    sub = cmds.createNode('plusMinusAverage', name='{}_cv_bs_SUB'.format(cv))
    pos = cmds.xform('{}.cv[{}]'.format(bs_crv, i), t=True, ws=True, q=True)

    cmds.setAttr(sub + '.operation', 2)
    cmds.setAttr(sub + '.input3D[0]', pos[0], pos[1], pos[2])
    cmds.connectAttr(poci + '.positionY', sub + '.input3D[1].input3Dy')

    cmds.connectAttr(sub + '.output3Dy', mdl + '.input1')
    cmds.setAttr(mdl + '.input2', mult_value)

    cmds.setAttr(add + '.input3D[0]', pos[0], pos[1], pos[2])
    cmds.connectAttr(mdl + '.output', add + '.input3D[1].input3Dy')

    cmds.connectAttr(add + '.output3D', bs_crv + '.controlPoints[{}]'.format(i))


###############################################
dag = trans.DagRelationships()
chains = cmds.ls(sl=True)

for chain in chains:
    base = chain.split('0')[0]
    chain = cmds.ls(base + '*_JNT')
    dag.list_dags(chain)
    dag.unparent()

    for i, each in enumerate(chain):
        mtx = cmds.xform(each, matrix=True, q=True)
        trans.dags_to_mesh([each], 'tailOutSubD', rotate=True, position=False, aim='z', up='y', up_vector=(0, 1, 0))

        # Get norm vec and compare it to the previous aim vec
        if i == 0:
            mtx = cmds.xform(each, matrix=True, q=True)
            vec = maths.axis_in_matrix(mtx, axis='+z', add_pos=False)
            old_up = vec
        else:
            mtx = cmds.xform(each, matrix=True, q=True)
            vec = maths.axis_in_matrix(mtx, axis='+z', add_pos=False)
            dot = vec * old_up
            if dot < 0:
                trans.dags_to_mesh([each], 'tailOutSubD', rotate=True, position=False, aim='-z', up='y',
                                   up_vector=(0, 1, 0))
                print dot
                vec *= -1

            old_up = vec
    trans.aim_at_next(chain, up='z', aim='x', up_vector='z', rotate_last=True)
    dag.parent()



###################################

chains = cmds.ls(sl=True)

for chain in chains:
    ctrl = '{}_*_CTL'.format("Ct_TailWave")
    base = chain.split('0')[0]
    chain_jnts = cmds.ls(base + '*_JNT')
    jnts = cmds.ls(chain_jnts)

    ### CURVE ###

    nbOfCtrls = len(jnts)
    for i, jnt in enumerate(jnts):
        positions = [(i, 0, 0) for i in range(0, nbOfCtrls)]

    curve_degree = 2  # cubic
    crvBuild = cmds.curve(d=curve_degree, p=positions, n='test_CRV')
    crvBuildShape = cmds.listRelatives(crvBuild, s=1)[0]

    rebuildCrvNode = cmds.createNode('rebuildCurve')
    cmds.setAttr(rebuildCrvNode + '.rebuildType', 0)  # uniform
    cmds.setAttr(rebuildCrvNode + '.degree', curve_degree)  # degree quadratic
    cmds.setAttr(rebuildCrvNode + '.endKnots', 0)  # non multiple end knots
    cmds.setAttr(rebuildCrvNode + '.keepRange', 0)  # 0-1
    cmds.setAttr(rebuildCrvNode + '.keepControlPoints', 1)
    cmds.connectAttr(crvBuildShape + '.local', rebuildCrvNode + '.inputCurve', f=1)
    crv = cmds.curve(d=curve_degree, p=positions, n='scaleResample_CRV')
    crvShape = cmds.listRelatives(crv, s=1)[0]
    cmds.setAttr(crv + '.v', 0)
    # cmds.parent(crv,'Placer_world_GRP')
    cmds.setAttr(crv + '.it', 0)
    cmds.connectAttr(rebuildCrvNode + '.outputCurve', crvShape + '.create', f=1)
    cmds.disconnectAttr(rebuildCrvNode + '.outputCurve', crvShape + '.create')
    cmds.delete(rebuildCrvNode, crvBuild)

    ### CONNECT CURVE ON INFO > ROTATION CHANNEL

    inc = 1.0 / float(nbOfCtrls - 1)
    value = 0.0
    for i, jnt in enumerate(jnts):
        pci = cmds.createNode("pointOnCurveInfo", n="sample" + str(i) + "_PCI")
        cmds.connectAttr(crvShape + '.local', pci + '.inputCurve', f=1)
        cmds.setAttr(pci + '.parameter', value)
        cmds.setAttr(pci + '.turnOnPercentage', True)

        value += inc

        mult = cmds.createNode('multDoubleLinear', n='ROTATION_' + str(i) + '_multDoubeLinear')
        cmds.connectAttr(pci + '.result.position.positionY', mult + ".input1", f=1)
        cmds.connectAttr(ctrl + '.multiplyRotation', mult + ".input2", f=1)

        # cmds.connectAttr(crvShape + '.controlPoints[{}].yValue'.format(str(i)),zro+".ry", f=1)
        cmds.connectAttr(mult + '.output', jnt + ".ry", f=1)

    ### TEXTURE DEFORMER

    try:
        cmds.addAttr(ctrl, ln='separator', nn='------------', at='enum', en='------------', k=True)
        cmds.setAttr(ctrl + '.separator', cb=True)
        cmds.addAttr(ctrl, ln='envelope', at='float', min=0, max=1, dv=0, k=True)
        cmds.addAttr(ctrl, ln='time', at='float', dv=0, k=True)
        cmds.addAttr(ctrl, ln='frequency', at='float', dv=0.35, k=True)
    except:
        pass

    # adding textureDeformer

    deformer = cmds.textureDeformer(crv,
                                    name="{}_texureDeformer_TXD".format(crv),
                                    direction="Handle",
                                    pointSpace="World",
                                    strength=1.0,  # IMPORTANT KEEP AT 1.0 ... if needs to scale use texture magnitude
                                    offset=-0.5,
                                    )[0]

    noise = cmds.createNode("noise", n="{}_Noise_TXD".format(crv))
    place2d = cmds.createNode("place2dTexture", n="{}_place2dTexture_TXD".format(crv))
    cmds.connectAttr(noise + ".outColor", deformer + ".texture")
    cmds.connectAttr(place2d + ".outUV", noise + ".uvCoord")
    cmds.connectAttr(place2d + ".outUvFilterSize", noise + ".uvFilterSize")

    cmds.setAttr(deformer + ".strength", 1.0)
    cmds.setAttr(noise + ".threshold", 0.0)
    cmds.setAttr(noise + ".amplitude", 2.0)
    cmds.setAttr(noise + ".ratio", 0.0)
    cmds.setAttr(noise + ".frequencyRatio", 2.0)
    cmds.setAttr(noise + ".depthMax", 3.0)
    cmds.setAttr(noise + ".frequency", 500.0)
    cmds.setAttr(noise + ".noiseType", 2)  # Wave

    cmds.setAttr(noise + ".numWaves", 5)  # Wave

    cmds.connectAttr(ctrl + ".time", noise + ".time")
    cmds.connectAttr(ctrl + ".frequency", noise + ".frequency")

################################ RESET JOINT ORIENT
trans_dict = {}
for each in cmds.ls(sl=True):
    pos = cmds.xform(each, t=True, ws=True, q=True)
    rot = cmds.xform(each, ro=True, ws=True, q=True)
    trans_dict[each] = [pos, rot]
for each in cmds.ls(sl=True):
    cmds.setAttr(each + '.jo', 0, 0, 0)
for each in cmds.ls(sl=True):
    pos = trans_dict[each][0]
    rot = trans_dict[each][1]
    cmds.xform(each, t=pos, ws=True)
    cmds.xform(each, ro=rot, ws=True)


def chain_in_geo(geo, up_object, flip_tangent=False, size=.4, iter_size=1):
    '''
    Creates a chain following the inside of a geometry
    Args:
        geo: geo to reference
        up_object: object to reference up vector of chain
        flip_tangent: flips the direction that the function is placing the joints in the geo
        size: size of vectors that are cast to find the middle of the geometry

    :return:
    '''

    # If flip tangent is true then flip the axis of the face matrix that is used to iterate through mesh
    if flip_tangent:
        tan_axis = 'z'
        mid_axis = 'x'
    else:
        tan_axis = 'x'
        mid_axis = 'z'

    # Find center point of geo
    verts = cmds.ls(geo + '.vtx[*]', fl=True)
    center = comp.average_position(verts)

    # List for positions of inside of geo
    positions = []
    pos_positions = []
    neg_positions = []

    pos = center
    mtx = cmds.xform(up_object, matrix=True, q=True)

    inside_check = 0
    num_iter = 0
    while inside_check == 0:
        # If gone through first iteration, add vector to original point to get next point inside the mesh
        if num_iter > 0:
            mtx_vecs = maths.matrix_to_vectors(mtx)
            mtx_dict = {'+x': mtx_vecs[0],
                        '-x': mtx_vecs[0] * -1,
                        '+y': mtx_vecs[1],
                        '-y': mtx_vecs[1] * -1,
                        '+z': mtx_vecs[2],
                        '-z': mtx_vecs[2] * -1}
            iter_vec = mtx_dict['+{}'.format(tan_axis)]
            added_pos = pos + (iter_vec * iter_size)
            print pos
            print added_pos

            # From point use matrix to get middle of mesh
            vecs = []
            axes = ['+y', '-y', '+{}'.format(mid_axis), '-{}'.format(mid_axis)]
            for axis in axes:
                vec = mtx_dict[axis]
                vec = vec * size
                new_vec = vec + added_pos
                new_pos = comp.closest_point_on_mesh(geo, new_vec)[0]

                vecs.append(new_pos)

            # Find average position of these vectors to place yourself in the middle of the mesh
            pos = comp.average_position(vecs, dags=False)

            # Check to see if point is inside mesh
            point_on_mesh, normal, face = comp.closest_point_on_mesh(geo, added_pos, add_normal=True)
            vec_between = point_on_mesh - added_pos
            dot = vec_between * normal
            print dot
            if dot < 0:
                inside_check += 1

            # Add point to positions if it is inside the mesh
            pos_positions.append(pos)

        try:
            loc = cmds.spaceLocator()[0]
            cmds.xform(loc, t=added_pos, ws=True)
        except:
            pass

        num_iter += 1

        if num_iter > 15:
            break


    # loc1 = cmds.spaceLocator(name='mid')
    # loc2 = cmds.spaceLocator(name='tan')
    # loc3 = cmds.spaceLocator(name='test')
    #
    # cmds.xform(loc1, t=center)
    # cmds.xform(loc2, t=face_vec)
    # cmds.xform(loc3, t=face_vec + tan_vec)


def face_matrix(geo, point):
    '''
    Creates a matrix of the closest face to a point
    Args:
        geo: geometry to reference
        point: starting point
    Returns:
        Matrix constructed from face
    '''
    # Find closest face and find normal of said face
    obj = om.MSelectionList()
    obj.add(geo)
    mesh = om.MFnMesh(obj.getComponent(0)[0])

    pos = om.MPoint(point[0], point[1], point[2])
    point, normal, face_id = mesh.getClosestPointAndNormal(pos, space=4)
    face = '{}.f[{}]'.format(geo, face_id)
    face_verts = comp.face_to_vertex([face])

    # Get positions of vertices and then construct vectors from their relation to one another
    vtx_vecs = []
    for each in face_verts:
        vec = cmds.xform(each, t=True, ws=True, q=True)
        vec = om.MVector(vec[0], vec[1], vec[2])
        vec.normalize()
        vtx_vecs.append(vec)

    # Depending on flip flag decides which direction to make the tangent vector
    normal.normalize()
    x_vec = vtx_vecs[3] - vtx_vecs[0]
    x_vec.normalize()
    z_vec = normal ^ x_vec
    z_vec.normalize()

    mtx = maths.create_matrix(x_vec=x_vec, y_vec=normal, z_vec=z_vec * -1, pos=point)

    return mtx


def skin_closest_geo(base, geos, separate=True, ordered_geos=[], base_skin_jnts=['Ct_Root_0_JNT'], level=3, vec_size=0.3, remove=None, rename=True, inside=False, virtual_nrb=True):
    '''
    Skins joint chains to geometries based on which is the closest to that joint chain
    Args:
        base (str): 0 joint of each strand with an asterisk Ex: Ct_Hair*_0_JNT
        geos list[(str)]: list of geometries to skin, if separate is True then provide a list with one geo
        separate
        ordered_geos list[(str)]: whether the given geo list is ordered according to the position of the joint chains, use in conjunction with separate flag
        base_skin_jnts list[(str)]: base joint to flood rest of geo that is not skinned
        level int: indicates number of vectors to reference when finding closest geos, highest is 3
        vec_size float: size of vectors coming from objects
        remove (str): removes a string from the base list
        inside (bool): indicates whether the function will make sure joints skinned are within the given geometry
    '''
    cmds.currentUnit(linear='cm')
    print 'Changed current Scene Units to cm'
    unskinned_jnts = []
    new_geo = None
    renamed_geos = dict()
    base_jnts = cmds.ls(base)

    if remove:
        base_jnts.remove(remove)

    # Separate geometries
    if separate:
        base_geo = geos[0]
        if ordered_geos:
            geos = ordered_geos
        else:
            new_geo = cmds.duplicate(base_geo)[0]
            geos = cmds.polySeparate(new_geo)
            geos.pop(-1)

        # Map vertices of base geo to the separated geos
        geo_mapping = dict()
        win = gen.ProgressWindow(num_steps=len(geos), title='Mapping Vertices')
        for geo in geos:
            vtx_list = cmds.ls('{}.vtx[*]'.format(geo), fl=True)
            print 'Mapping {} to {}'.format(geo, base_geo)
            for vtx in vtx_list:
                pos = cmds.xform(vtx, t=True, ws=True, q=True)
                base_vtx = comp.closest_vtx_on_mesh(base_geo, pos)
                geo_mapping[base_vtx] = vtx
            win.step()
        win.end()

    # Skin geos

    if not ordered_geos:
        win = gen.ProgressWindow(num_steps=len(base_jnts), title='Skinning Joints')
        unskinned_jnts = []
        for jnt in base_jnts:
            skin_check = 0
            jnt_base = jnt.split('_0_')[0]
            query_name = jnt.split('_0_')[0] + '_*_JNT'
            strand_jnts = cmds.ls(query_name)
            geo_list = copy.deepcopy(geos)
            while len(geo_list) > 0:
                closest_geo = closest_average_geo(strand_jnts, geo_list, level=level, vec_size=vec_size)

                # Check to see if joints are inside mesh
                inside_check = 0
                for strand_jnt in strand_jnts:
                    pos = cmds.xform(strand_jnt, t=True, ws=True, q=True)
                    pos = om.MVector(pos[0], pos[1], pos[2])
                    point_on_mesh, normal, face = comp.closest_point_on_mesh(closest_geo, pos, add_normal=True)
                    vec_between = point_on_mesh - pos
                    dot = vec_between * normal
                    if dot > 0:
                        inside_check += 1

                if inside_check == len(strand_jnts):
                    cmds.skinCluster([closest_geo] + strand_jnts)
                    geos.remove(closest_geo)
                    if rename:
                        renamed_geos[closest_geo] = jnt_base + '_GEO'

                    skin_check = 1
                    break
                else:
                    geo_list.remove(closest_geo)

            if skin_check == 0:
                if inside:
                    print '{} chain was not skinned'.format(jnt)
                else:
                    print '{} chain may have inaccurate skinning'.format(jnt)
                unskinned_jnts.append(jnt)

            win.step()
        win.end()

    else:
        for i, jnt in enumerate(base_jnts):
            jnt_base = jnt.split('_0_')[0]
            print "Skinning {} chain".format(jnt_base)
            query_name = jnt.split('_0_')[0] + '_*_JNT'
            strand_jnts = cmds.ls(query_name)

            cmds.skinCluster([geos[i]] + strand_jnts)

            if virtual_nrb:
                cmds.select(geos[i] + '.vtx[*]')
                skinFromVirtualNrb.skin_selected(strand_jnts, iters=200)
                cmds.select(clear=True)

    # Skin remaining geos that didn't have joints perfectly inside them
    if not inside:
        for jnt in unskinned_jnts:
            jnt_base = jnt.split('_0_')[0]
            query_name = jnt.split('_0_')[0] + '_*_JNT'
            strand_jnts = cmds.ls(query_name)

            closest_geo = closest_average_geo(strand_jnts, geos, level=level, vec_size=vec_size)
            cmds.skinCluster([closest_geo] + strand_jnts)
            geos.remove(closest_geo)
            if rename:
                renamed_geos[closest_geo] = jnt_base + '_GEO'

    # If base geo was separated
    if separate:
        # Get all joints
        all_jnts = []
        if base_skin_jnts:
            all_jnts += base_skin_jnts
        for jnt in base_jnts:
            query_name = jnt.split('_0_')[0] + '_*_JNT'
            strand_jnts = cmds.ls(query_name)
            all_jnts += strand_jnts

        # Skin base geo and then use mapping to transfer weights
        old_skc = skin.find_skin(base_geo)
        cmds.delete(old_skc)
        base_skc = base_geo.split('_')[0] + '_SKC'
        cmds.skinCluster([base_geo] + all_jnts, name=base_skc)

        # Flood all vtx weights with the base skin joint
        if base_skin_jnts:
            vtx_list = cmds.ls(base_geo + '.vtx[*]', fl=True)
            win = gen.ProgressWindow(num_steps=len(vtx_list), title='Flooding Base Geo')
            for vtx in vtx_list:
                cmds.select(vtx)
                cmds.skinPercent(base_skc, transformValue=(base_skin_jnts[0], 1))
                win.step()
            win.end()

        win = gen.ProgressWindow(num_steps=len(geo_mapping), title='Skinning Vertices')
        for base_vtx, sep_vtx in geo_mapping.items():
            # Get joint values for vertex
            sep_geo = sep_vtx.split('.')[0]
            skc = skin.find_skin(sep_geo)
            if skc:
                values = cmds.skinPercent(skc, sep_vtx, query=True, value=True)
                jnts = cmds.skinPercent(skc, sep_vtx, transform=None, query=True)

                # Create list for applying values
                value_list = list()
                for i, jnt in enumerate(jnts):
                    value_list.append((jnt, values[i]))

                cmds.select(base_vtx)
                cmds.skinPercent(base_skc, transformValue=value_list)

            win.step()
        win.end()

        # Delete separated geos
        if new_geo:
            cmds.delete(new_geo)

    # Rename geos with their named equivalents
    elif rename:
        for geo, new_name in renamed_geos.items():
            cmds.rename(geo, new_name)


def closest_average_geo(dags, geos, level=3, vec_size=0.3):
    # Create default dictionary of geos to see how many times a geo was indicated to be the closest
    closest_geos = dict()
    for geo in geos:
        closest_geos[geo] = 0

    # Find how many joints each geo was closest to
    mapping = joints_to_geos(dags, geos, level=level, vec_size=vec_size)
    for geo, joints in mapping.items():
        closest_geos[geo] = len(joints)

    # Find which geo in the dictionary has the highest amount of instances
    closest_geo = [None, 0]
    for geo, i in closest_geos.items():
        if i > closest_geo[1]:
            closest_geo = [geo, i]

    return closest_geo[0]


def joints_to_geos(joints, geos, level=0, vec_size=0.3):
    # Create dictionary of geos to assign influnces to
    inf_mapping = dict()

    # Create geo dictionary to indicate how often each geo is the closest to a given point
    closest_geos_default = dict()
    for geo in geos:
        closest_geos_default[geo] = 0
    for geo in geos:
        inf_mapping[geo] = list()

    # Find closest mesh to each joint
    for jnt in joints:
        # print 'Finding closest mesh to {}'.format(jnt)
        pos = cmds.xform(jnt, t=True, ws=True, q=True)
        pos = om.MVector(pos[0], pos[1], pos[2])
        closest_geo = None
        if level == 0:
            closest_geo = [comp.find_closest_mesh(pos, geos)]
        else:
            closest_geos = copy.deepcopy(closest_geos_default)
            mtx = cmds.xform(jnt, matrix=True, ws=True, q=True)
            x, y, z, pos = maths.matrix_to_vectors(mtx)

            # Multiply each vec by the influence
            x = x * vec_size
            y = y * vec_size
            z = z * vec_size

            x_pos_vec = x + pos
            x_neg_vec = (x * -1) + pos
            y_pos_vec = y + pos
            y_neg_vec = (y * -1) + pos
            z_pos_vec = z + pos
            z_neg_vec = (z * -1) + pos

            geo1 = comp.find_closest_mesh(pos, geos)
            closest_geos[geo1] += 1
            if level > 0:
                geo2 = comp.find_closest_mesh(x_pos_vec, geos)
                geo3 = comp.find_closest_mesh(x_neg_vec, geos)
                closest_geos[geo2] += 1
                closest_geos[geo3] += 1
            if level > 1:
                geo4 = comp.find_closest_mesh(y_pos_vec, geos)
                geo5 = comp.find_closest_mesh(y_neg_vec, geos)
                closest_geos[geo4] += 1
                closest_geos[geo5] += 1
            if level > 2:
                geo6 = comp.find_closest_mesh(z_pos_vec, geos)
                geo7 = comp.find_closest_mesh(z_neg_vec, geos)
                closest_geos[geo6] += 1
                closest_geos[geo7] += 1

            # Find which geo in the dictionary has the highest amount of instances
            closest_geo = [None, 0]
            for geo, i in closest_geos.items():
                if i > closest_geo[1]:
                    closest_geo = [geo, i]

        # Add joint to the geometry's list it's closest to in the dictionary
        inf_mapping[closest_geo[0]].append(jnt)

    return inf_mapping
#
#
# def joints_to_geos(joints, geos, return_conflicts=False):
#     # Create dictionary of geos to assign influnces to
#     inf_mapping = dict()
#     problem_jnts = list()
#     problem_geos = list()
#     for geo in geos:
#         inf_mapping[geo] = list()
#
#     # Find closest mesh to each joint
#     for jnt in joints:
#         print 'Finding closest mesh to {}'.format(jnt)
#         closest_geos = list()
#         pos = cmds.xform(jnt, t=True, ws=True, q=True)
#         pos = om.MVector(pos[0], pos[1], pos[2])
#         for geo in geos:
#             point_on_mesh, normal, face = comp.closest_point_on_mesh(geo, pos, add_normal=True)
#             vec_between = point_on_mesh - pos
#             distance = vec_between.length()
#             dot = vec_between * normal
#             if dot > 0:
#                 closest_geos.append((geo, dot, distance))
#         print closest_geos
#
#         # Find closest eligible geos
#
#         # If a joint is inside more than one geo add to problem list
#         if len(closest_geos) > 1:
#             problem_jnts.append(jnt)
#             for geo in closest_geos:
#                 if geo[0] not in problem_geos:
#                     problem_geos.append(geo[0])
#         closest_geo = None
#         for info in closest_geos:
#             if not closest_geo:
#                 closest_geo = info
#             else:
#                 if closest_geo[2] > info[2]:
#                     closest_geo = info
#         print 'Closest mesh is {} with dot of {}'.format(closest_geo[0], closest_geo[1])
#         print ''
#         # Add joint to the geometry's list it's closest to in the dictionary
#         inf_mapping[closest_geo[0]].append(jnt)
#
#     if return_conflicts:
#         return problem_geos, problem_jnts, inf_mapping
#     else:
#         return inf_mapping

def bob_postbuild():
    # import facial rig
    cmds.file('C:/Users/feder/Desktop/Axis/scripts/proxy_facial.mb', i=True, ignoreVersion=True)

    # import blendshapes
    cmds.file('C:/Users/feder/Desktop/Axis/projects/chuck/bobCratchit/models/blendshape/bob_blink_BS.ma', i=True, ignoreVersion=True)

    # apply blendshapes
    cmds.blendShape(['head_blink', 'Ct_Head_0_GEO'], name='head_blink_BS')
    cmds.blendShape(['topEyelashes_blink', 'topEyelashesSubD_res1_PLY'], name='topEb_blink_BS')
    cmds.blendShape(['botEyelashes_blink', 'botEyelashesSubD_res1_PLY'], name='botEb_blink_BS')

    for each in ['head_blink', 'topEyelashes_blink', 'botEyelashes_blink']:
        cmds.delete(each)

    # connect blendshapes to rig
    for attr in ['head_blink_BS.head_blink', 'topEb_blink_BS.topEyelashes_blink', 'botEb_blink_BS.botEyelashes_blink']:
        cmds.connectAttr('Ct_brow_CTL.blink', attr)

    # move deformers
    cmds.setAttr('Lf_mouthCorner_loc_guide.t', 3.031, 166.542, 6.774)
    cmds.setAttr('Ct_brow_loc_guide.t', 0, 180, 6.466)
    cmds.setAttr('Ct_jaw_loc_guide.t', 0, 168, 1.288)

    # set guide attributes
    cmds.setAttr('Ct_brow_loc_guide.fallOffDistance', 7.44)
    cmds.setAttr('Ct_brow_CTL.blink', 0)

    # Connect deformers and match them with their given geos
    deformers = {'Ct_jaw_cluster': ['Ct_Head_0_GEO', 'mouthSubD_res1_PLYShape'],
                 'Lf_mouthCorner_softMod': ['Ct_Head_0_GEO'],
                 'Rt_mouthCorner_softMod': ['Ct_Head_0_GEO'],
                 'Ct_brow_softMod': ['Ct_Head_0_GEO', 'eyebrowsProxy_res1_PLY']}

    # Unparent geos from head so they aren't all automatically affected by all deformers
    cmds.select(clear=True)
    geos = cmds.listRelatives('Ct_Head_0_GEO', children=True)
    children = []
    for each in geos:
        typ = cmds.nodeType(each)
        if typ == 'transform':
            children.append(each)

    for each in children:
        cmds.parent(each, world=True)

    # Pair deformers with their geo
    for deformer, geos in deformers.items():
        for geo in geos:
            defSet = cmds.listConnections(deformer, type='objectSet')[0]
            cmds.sets(geo, addElement=defSet)

    # Reparent geos that should be affected by deformers
    cmds.select(clear=True)
    for each in children:
        cmds.parent(each, 'Ct_Head_0_GEO')

    # Set weights
    path = 'C:/Users/feder/Desktop/Axis/projects/chuck/bobCratchit/face/weights/'
    cls_dic = {'Ct_Head_0_GEOShape': ['head_jawCLS_weights_01.json'],
               'mouthSubD_res1_PLYShape': ['mouth_jawCLS_weights_01.json']}
    ebSoftMod_dic = {'Ct_Head_0_GEOShape': ['head_ebSoftMod_weights_01.json']}

    # Set cluster weights
    for shape, files in cls_dic.items():
        for file in files:
            wgts, shape_data = wgt.load_dictionary(path + file)
            wgt.set_deformer_weights('Ct_jaw_cluster', wgts, shape=shape)

    # Set eyebrow softmod weights
    for shape, files in ebSoftMod_dic.items():
        for file in files:
            wgts, shape_data = wgt.load_dictionary(path + file)
            wgt.set_deformer_weights('Ct_brow_softMod', wgts, shape=shape)


def coaster_skin():
    r = 50
    for i in range(0, r):
        bone = 'C_coaster_r_rail_{}_BONE'.format(str(i))
        geo = 'R_rail_{}_GEO'.format(str(i))
        cmds.skinCluster(bone, geo)
    for i in range(0, r):
        bone = 'C_coaster_l_rail_{}_BONE'.format(str(i))
        geo = 'L_rail_{}_GEO'.format(str(i))
        cmds.skinCluster(bone, geo)
    for i in range(0, r):
        bone = 'C_coaster_c_rail_{}_BONE'.format(str(i))
        geo = 'B_tube_{}_GEO'.format(str(i))
        cmds.skinCluster(bone, geo)
    for i in range(0, 16):
        geo_grp = 'ppTrackRingModel_{}_GRP'.format(str(i))
        geos = cmds.listRelatives(geo_grp, c=True)
        bone_num = i * 3
        bone = 'C_coaster_c_rail_{}_BONE'.format(str(bone_num))
        for geo in geos:
            cmds.skinCluster(bone, geo)


def coaster_proxy_skin(create=False):
    if create:
        for i, each in enumerate(cmds.ls(sl=True)):
            nodes = cmds.listRelatives(each, ad=True)
            nodes.append(each)
            for node in nodes:
                name_list = node.split('_')
                name_list.insert(-1, str(i))
                new_name = ''
                for x, string in enumerate(name_list):
                    if string == name_list[-1]:
                        new_name += string
                    else:
                        new_name += string + '_'
                cmds.rename(node, new_name)

        sel = cmds.ls(sl=True)[0]
        for i in range(1, 50):
            geo = cmds.duplicate(sel, rc=True)[0]
            cmds.xform(geo, t=(0, 0, i * 10))
            nodes = cmds.listRelatives(geo, ad=True)
            nodes.append(geo)
            for node in nodes:
                name_list = node.split('_')

                name_list.insert(-1, str(i))
                new_name = ''
                for x, string in enumerate(name_list):
                    if string == name_list[-1]:
                        new_name += string
                    else:
                        new_name += string + '_'
                cmds.rename(node, new_name)

    r = 51
    for i in range(1, r):
        bone_num = i - 1
        if i < 10:
            num = '0' + str(i)
        else:
            num = str(i)

        bone = 'C_coaster_r_rail_{}_BONE'.format(bone_num)
        geo = 'R_rail_{}_proxy_GEO'.format(num)
        cmds.skinCluster(bone, geo)

        bone = 'C_coaster_l_rail_{}_BONE'.format(bone_num)
        geo = 'L_rail_{}_proxy_GEO'.format(num)
        cmds.skinCluster(bone, geo)

        bone = 'C_coaster_c_rail_{}_BONE'.format(bone_num)
        geo = 'C_rail_{}_proxy_GEO'.format(num)
        cmds.skinCluster(bone, geo)

        # bone = 'C_coaster_main0_{}_BONE'.format(bone_num)
        # geo = 'C_strut_{}_proxy_GEO'.format(num)
        # cmds.skinCluster(bone, geo)


def fern_displacement_setup(ref_geos, geos, controls):
    if len(ref_geos) != len(geos):
        print len(ref_geos)
        print len(geos)
        cmds.error("Number of dags doesn't match up")

    elif len(geos) != len(controls):
        print len(geos)
        print len(controls)
        cmds.error("Number of dags doesn't match up")

    for i, ref in enumerate(ref_geos):
        init_scale = cmds.getAttr(ref + '.sx')
        geo = geos[i]
        geo_shape = cmds.listRelatives(geo, s=True)[1]
        ctrl = controls[i]

        attribute.add_generic_blend(geo_shape, 'mtoa_constant_leafScale', max_value=100)
        attribute.add_generic_blend(geo_shape, 'mtoa_constant_initialScale', default=init_scale, max_value=100)

        pma = cmds.createNode('plusMinusAverage', name=geo_shape + '_PMA')
        div = cmds.createNode('math_Divide', name=geo_shape + '_DIV')
        mult = cmds.createNode('math_Multiply', name=geo_shape + '_MULT')

        cmds.connectAttr(ctrl + '.localScaleX', pma + '.input1D[0]')
        cmds.connectAttr(ctrl + '.localScaleY', pma + '.input1D[1]')
        cmds.connectAttr(ctrl + '.localScaleZ', pma + '.input1D[2]')

        cmds.connectAttr(pma + '.output1D', div + '.input1')
        cmds.setAttr(div + '.input2', 3)

        cmds.connectAttr(div + '.output', mult + '.input1')
        cmds.connectAttr(geo_shape + '.mtoa_constant_initialScale', mult + '.input2')

        cmds.connectAttr(mult + '.output', geo_shape + '.mtoa_constant_leafScale')


def new_fern_displacement_setup(geos, controls):

    for i, ref in enumerate(geos):
        # init_scale = cmds.getAttr(ref + '.mtoa_constant_initialScale')
        geo = geos[i]
        geo_shape = cmds.listRelatives(geo, s=True)[1]
        ctrl = controls[i]

        # attribute.add_generic_blend(geo_shape, 'mtoa_constant_leafScale', max_value=100)
        # attribute.add_generic_blend(geo_shape, 'mtoa_constant_initialScale', default=init_scale, max_value=100)

        pma = cmds.createNode('plusMinusAverage', name=geo_shape + '_PMA')
        div = cmds.createNode('math_Divide', name=geo_shape + '_DIV')
        mult = cmds.createNode('math_Multiply', name=geo_shape + '_MULT')
        dm = cmds.createNode('decomposeMatrix', name=ctrl + '_DM')

        cmds.connectAttr(ctrl + '.worldMatrix[0]', dm + '.inputMatrix')

        cmds.connectAttr(dm + '.outputScaleX', pma + '.input1D[0]')
        cmds.connectAttr(dm + '.outputScaleY', pma + '.input1D[1]')
        cmds.connectAttr(dm + '.outputScaleZ', pma + '.input1D[2]')

        cmds.connectAttr(pma + '.output1D', div + '.input1')
        cmds.setAttr(div + '.input2', 3)

        cmds.connectAttr(div + '.output', mult + '.input1')
        cmds.connectAttr(geo_shape + '.mtoa_constant_initialScale', mult + '.input2')

        cmds.connectAttr(mult + '.output', geo_shape + '.mtoa_constant_leafScale')


def integer_conditions(attribute, num, name='vis'):
    dag = attribute.split('.')[0]
    conditions = []
    for i in range(num):
        condition = cmds.createNode('condition', name='{}_{}_{}_COND'.format(dag, name, i))
        conditions.append(condition)

        cmds.connectAttr(attribute, condition + '.firstTerm')
        cmds.setAttr(condition + '.secondTerm', i)
        cmds.setAttr(condition + '.colorIfTrueR', 1)
        cmds.setAttr(condition + '.colorIfFalseR', 0)
        cmds.setAttr(condition + '.operation', 3)

    return conditions


def coaster_visibility_unfold(attribute, dags):
    conds = integer_conditions(attribute, len(dags))
    for i, dag in enumerate(dags):
        cmds.connectAttr(conds[i] + '.outColorR', dag + '.v')


def remove_from_list(num_splits, list1):
    for each in list1:
        num = len(each.split('_'))
        if num > num_splits:
            list1.remove(each)
    return list1


def add_enum(node, name, *args):
    '''Wrapper for making an enum attribute

    Args:
        node (str): node to add attribute to
        name (str): name of attribute
        *args: arguments that will be the enum names of the attribute
    Return:
         (str): attribute name
    '''

    enum_names = ''
    for i, arg in enumerate(args):
        if arg != args[-1]:
            enum_names += arg + ':'
        else:
            enum_names += arg

    cmds.addAttr(node,
                 shortName=name,
                 attributeType='enum',
                 enumName=enum_names,
                 keyable=True)

    return '{}.{}'.format(node, name)


def linspace_curve(curve, num):
    ''' Provide matrices that are an equal distribution along a curve

    Args:
        curve (str): name of curve to get information from
        num (int): number of matrices to be returned
    Return:
         list[om.MMatrix()]: list of matrices
    '''
    sel = om.MSelectionList()
    sel.add(curve)

    crv = om.MFnNurbsCurve()
    crv.setObject(sel.getDagPath(0))

    curve_length = crv.length()
    lengths = np.linspace(0, curve_length, num)

    matrices = []
    mscs = []

    for length in lengths:
        poci = cmds.createNode('pointOnCurveInfo')
        cm = cmds.createNode('composeMatrix')
        msc = cmds.createNode('millSimpleConstraint')
        mscs.append(msc)

        param = crv.findParamFromLength(length)
        if param == 0:
            param = .001
        if length == lengths[-1]:
            param -= .001

        cmds.connectAttr(curve + '.worldSpace[0]', poci + '.inputCurve')
        cmds.setAttr(poci + '.parameter', param)
        cmds.connectAttr(poci + '.position', cm + '.inputTranslate')
        cmds.connectAttr(cm + '.outputMatrix', msc + '.inMatrix')

        aim_vec = crv.tangent(param)
        side_vec = crv.normal(param)
        up_vec = aim_vec ^ side_vec
        pos = crv.getPointAtParam(param)

        mtx = maths.vectors_to_matrix(row1=aim_vec.normal(),
                                      row2=up_vec.normal(),
                                      row3=side_vec.normal(),
                                      row4=pos)
        matrices.append(mtx)

    return matrices, mscs


def gather_controls():
    namespace_ctrls = cmds.ls('*:*CTRL')
    regular_ctrls = cmds.ls('*CTRL')
    all_controls = namespace_ctrls + regular_ctrls
    num_ctrls = None
    ribbon_ctrls = []
    ctrl_order = {}
    for ctrl in all_controls:
        if cmds.ls(ctrl + '.ribbonCtrl'):
            i = cmds.getAttr(ctrl + '.ribbonCtrl')
            ctrl_order[i] = ctrl
        elif cmds.ls(ctrl + '.ribbonRoot'):
            num_ctrls = cmds.getAttr(ctrl + '.numControls')
    for i in range(len(ctrl_order)):
        ribbon_ctrls.append(ctrl_order[i])
    return ribbon_ctrls, num_ctrls


def reset_controls():
    ctrls, num_dags = gather_controls()

    for i, dag in enumerate(ctrls):
        ofs = dag + '_OFS'
        if cmds.ls(ofs + '.curveMSC'):
            msc = cmds.getAttr(ofs + '.curveMSC', asString=True)
            cmds.deleteAttr(ofs + '.curveMSC')
            cmds.delete(msc)
        cmds.setAttr(ofs + '.t', 0, 0, 0)
        cmds.setAttr(ofs + '.r', 0, 0, 0)
        cmds.setAttr(dag + '.r', 0, 0, 0)


def move_to_curve(curve):
    ctrls, num_dags = gather_controls()
    dags = []
    for i in range(num_dags):
        dags.append(ctrls[i])
    matrices, mscs = linspace_curve(curve, num_dags)

    if cmds.ls(ctrls[0] + '_OFS.curveMSC'):
        reset_controls()

    for i, dag in enumerate(dags):
        ofs = dag + '_OFS'
        cmds.connectAttr(ofs + '.parentInverseMatrix[0]', mscs[i] + '.parentInverseMatrix')
        cmds.connectAttr(mscs[i] + '.outTranslate', ofs + '.translate')

        cmds.xform(dag, matrix=matrices[i], ws=True)

        add_enum(ofs, 'curveMSC', mscs[i])


def attach_to_rig(bones, dag):
    ''' Attaches a dag to the nearest point of skinned geometry

    Args:
        name (str): base name of dags created in method
        geo (str): geometry to attach to
        dag (str): dag node to attach to the geometry
    Returns:
        str: name of follicle plane geo
    '''
    if cmds.ls(dag + '_parentConstraint1'):
        cmds.delete(dag + '_parentConstraint1')

    # find closest bones to the given dag node
    constrain_bones = []
    dag_pos = cmds.xform(dag, matrix=True, ws=True, q=True)
    distances = {}
    for bone in bones:
        bone_pos = cmds.xform(bone, matrix=True, ws=True, q=True)
        distance = maths.matrix_distance(dag_pos, bone_pos)
        distances[bone] = distance

    closest_bone = None
    for x in range(2):
        for bone, distance in distances.items():
            if not closest_bone:
                closest_bone = bone
            else:
                if distance < distances[closest_bone]:
                    closest_bone = bone
        constrain_bones.append(closest_bone)

    # constrain dag node to closest bones
    con = cmds.parentConstraint(constrain_bones[0], constrain_bones[1], dag, mo=True)
    print con


def closest_vtx_on_mesh(geo, point):
    obj = om.MSelectionList()
    obj.add(geo)
    mesh = om.MFnMesh(obj.getComponent(0)[0])

    pos = om.MPoint(point[0], point[1], point[2])
    point, normal, face = mesh.getClosestPointAndNormal(pos, space=4)
    face = '{}.f[{}]'.format(geo, face)
    vertices = cmds.polyListComponentConversion(face, tv=True)
    vertices = cmds.ls(vertices, fl=True)
    start_pos = om.MVector(point[0], point[1], point[2])
    # find which vertex has the same position as the given point
    shortest_distance = None
    closest_vtx = None
    for i, vtx in enumerate(vertices):
        pos = cmds.xform(vtx, t=True, ws=True, q=True)
        end_pos = om.MVector(pos[0], pos[1], pos[2])
        distance = (start_pos - end_pos).length()
        if not closest_vtx:
            shortest_distance = distance
            closest_vtx = vtx
        elif distance < shortest_distance:
            shortest_distance = distance
            closest_vtx = vtx
        if distance == 0:
            break
    return closest_vtx


def populate_among_plane(surface, dag, num):
    sel = om.MSelectionList()
    sel.add(surface)

    plane = om.MFnNurbsSurface()
    plane.setObject(sel.getDagPath(0))

    params = np.linspace(0, 1, num)

    new_dags = []
    for par in params:
        new_dag = cmds.duplicate(dag)[0]
        pos = plane.getPointAtParam(0.5, par)
        cmds.xform(new_dag, t=(pos[0], pos[1], pos[2]), ws=True)
        new_dags.append(new_dag)

    for dag in new_dags:
        nrb.attach_to_surface(surface, dag, snap=True)


def create_locs():
    for each in cmds.ls(sl=True):
        if 'SHARED' not in each:
            name = each + '_LOC'
            loc = cmds.spaceLocator(name=name)[0]
            pos = cmds.xform(each, t=True, ws=True, q=True)
            rot = cmds.xform(each, ro=True, ws=True, q=True)

            cmds.xform(loc, t=pos, ws=True)
            cmds.xform(loc, ro=rot, ws=True)


def move_to_locs():
    for each in cmds.ls(sl=True):
        handle = each.split('_LOC')[0]
        pos = cmds.xform(each, t=True, ws=True, q=True)
        rot = cmds.xform(each, ro=True, ws=True, q=True)

        cmds.xform(handle, t=pos, ws=True)
        cmds.xform(handle, ro=rot, ws=True)


def dag_layouts():
    obj_layouts = {}
    obj_parents = {}
    for each in cmds.ls(sl=True):
        if 'Right' in each:
            side = 'R'
        elif 'Left' in each:
            side = 'L'
        else:
            side = 'C'
        obj = lyt.BaseLayoutObject(side, each)
        obj.bind_objects([each])
        obj_layouts[each] = obj
        par = cmds.listRelatives(each, parent=True)[0]
        obj_parents[each] = par
    for dag, obj in obj_layouts.items():
        # get parent of dag
        par_dag = obj_parents[dag]
        try:
            # get layout of the parent dag
            par_obj = obj_layouts[par_dag]
            par_obj.set_main_child(obj)
            par_obj.reset_rotation_object()
        except:
            pass

    for dag, obj in obj_layouts.items():
        # get parent of dag
        print obj.rot_nodes

    for i in range(7):
        for dag, obj in obj_layouts.items():
            pos = cmds.xform(dag, t=True, ws=True, q=True)
            obj.set_position(pos)


def make_bind_objects(dags):
    layouts = {}
    for dag in dags:
        pos = cmds.xform(dag, t=True, ws=True, q=True)
        side_decide = round(pos[0], 2)
        if side_decide < 0:
            side = 'R'
        elif side_decide > 0:
            side = 'L'
        else:
            side = 'C'
        obj = layouts[dag] = lyt.BaseLayoutObject(name=dag, side=side)
        obj.set_position(position=pos)


        obj.bind_objects([dag])

    for dag in dags:
        parent = cmds.listRelatives(dag, p=True)[0]
        if parent:
            if parent in layouts:
                parent_obj = layouts[parent]
                parent_obj.set_main_child(layouts[dag])


def move_bound_objects(iterations=3):
    layout_dags = cmds.ls(sl=True)
    cmds.undoInfo(openChunk=True)
    for i in range(iterations):
        for dag in layout_dags:
            attr_exist = cmds.ls(dag + '.boundObjects')

            if attr_exist:
                objs = cmds.attributeQuery('boundObjects', node=dag, listEnum=True)[0].split(':')
                rot_object = dag.split('_LAYOUT')[0] + '_ROT_LAYOUT'
                pos = cmds.xform(rot_object, t=True, ws=True, q=True)
                rot = cmds.xform(rot_object, ro=True, ws=True, q=True)
                for obj in objs:
                    cmds.xform(obj, t=(pos[0], pos[1], pos[2]), ws=True)
                    cmds.xform(obj, ro=(rot[0], rot[1], rot[2]), ws=True)
    cmds.undoInfo(closeChunk=True)


def abg_export():
    bones = {u'LeftSecondMP': (-0.00021857943771103568, -2.4848083263801083e-17, 0.0006680775734065948), u'LeftSecondDP': (-0.0010928304677501632, -0.0, -0.0023122456409180206), u'RightSecondMP': (3.0332131450951323e-21, -0.0, 0.0003580671036615968), u'LeftFifthPP': (-1.3340212490220438e-08, -0.0, -0.16147570312023163), u'RightThirdMP': (-4.9693130070751804e-17, -0.0, 0.000406397768529132), u'LeftThirdDP': (-2.732076319960692e-05, 5.4641516873856216e-05, 0.0026910810988773283), u'RightFourthPP': (4.969616416923865e-17, -0.0, 0.5599430799484253), u'RightFifthDP': (-0.00024590033136746305, -6.136282412996038e-10, -0.00016393054974585486), u'LeftSecondMC': (-2.4848082084619324e-17, -0.0, 89.703125), u'RightThirdPP': (2.4848082084619324e-17, -0.0, -0.0001912461593747139), u'RightFourthMC': (0.0, -0.0, -90.16017150878906), u'RightFourthMP': (-2.7320755179971457e-05, -0.0, 0.00038968297303654253), u'LeftSecondPP': (-2.7307412892696448e-05, 6.830188794992864e-06, 0.3973163068294525), u'RightShoulder': (0.107912577688694, 0.08004981279373169, -0.36536046862602234), u'LeftFifthMP': (3.108269447693601e-05, -0.0, -0.006874809507280588), u'RightFifthTip': (-5.464151035994291e-05, -0.0, 1.073395013809204), u'LeftFourthDP': (-5.5325814530902945e-18, 0.0016870571205929157, -0.0001466192866534421), u'RightFirstDP': (-1.3340211602042018e-08, -0.0, 0.0004051123687531799), u'RightFifthMP': (-2.730741471168585e-05, -0.0, 0.00038914906326681376), u'LeftThirdPP': (-5.464151035994291e-05, -0.0003824905725196004, 0.0014662075554952025), u'RightElbow': (0.01890065334737301, -0.0020490565802901983, -0.19874624907970428), u'RightSecondDP': (2.4851114528744944e-17, -0.0, -0.0003551864647306502), u'LeftFifthTip': (0.20796573162078857, 0.0015026414766907692, -1.0495914220809937), u'LeftSecondTip': (-0.000136617138026133, 1.242404098393627e-17, -0.003978811898598002), u'RightFifthMC': (-2.4848082084619324e-17, -0.0, -90.26252746582031), u'RightFirstTip': (-2.4848082084619324e-17, -0.0, 1.8525177836181683e-08), u'LeftFourthTip': (-2.7321908419253305e-05, 0.42013856768608093, 1.3752065896987915), u'LeftElbow': (-0.019175542518496513, -0.0021105282939970493, -0.10131122916936874), u'LeftFirstMC': (0.0, -0.0, 89.95431518554688), u'LeftFifthDP': (-2.732076178037611e-05, -0.00013660427410148096, 0.010481149265117898), u'LeftFirstTip': (-2.7320761693757173e-05, -8.209434050276895e-13, -179.99868774421884), u'RightThirdTip': (-1.6250440317097458e-18, 1.3821622178052123e-11, 2.732005571830712e-05), u'LeftFifthMC': (-1.3340212490220438e-08, -0.0, 90.36238098144531), u'RightFirstMC': (1.3340212490220438e-08, -0.0, -90.25364685058594), u'RightSecondMC': (1.3340212490220438e-08, -0.0, -90.40314483642578), u'RightSecondPP': (-1.3340212490220438e-08, -0.0, 0.7029100656509399), u'RightThirdDP': (-0.00016392454563174397, -0.0, -0.00013660290278494358), u'RightCarpus': (134.4861602783203, 90.0, -134.9950714111328), u'RightSecondTip': (-1.3340212490220438e-08, -0.0, -1.2376173863515305e-08), u'RightFirstPP': (-7.454424625385797e-17, -0.0, -0.04671502485871315), u'LeftThirdMC': (3.256887814995224e-12, 7.51320767449215e-05, 90.00020599365234), u'LeftCarpus': (-134.98947143554688, 90.0, 135.00244140625), u'LeftFirstDP': (-0.00030051497742533684, -0.0, -0.00012658274499699473), u'LeftFourthPP': (-0.0001366171200061217, -2.7320755179971457e-05, -0.35976970195770264), u'LeftFourthMC': (-1.3340208937506759e-08, -0.0, 90.25996398925781), u'RightFifthPP': (-2.6680421427727197e-08, -0.0, -0.13770151138305664), u'LeftShoulder': (-0.09532833844423294, 0.27307096123695374, 0.17273110151290894), u'RightFourthDP': (-0.00024588700180528284, 6.419253878197232e-10, -0.00013658349297088215), u'LeftThirdMP': (8.869910017517852e-05, 0.0005122642061389898, -0.004279352936285302), u'LeftFirstPP': (-4.969616416923865e-17, -0.0, 0.045444440096616745), u'RightFourthTip': (-5.4628173529636115e-05, -0.0, -1.439312219619751), u'LeftFourthMP': (0.00013664384266642138, -2.3141581051921227e-26, 7.195306105528285e-05), u'LeftThirdTip': (2.3919005193122068e-05, -0.0002458868259317066, 0.0025581403978811685), u'RightThirdMC': (2.4848082084619324e-17, -0.0, -90.00006866455078)}

    for bone, rot in bones.items():
        cmds.setAttr(bone + '.r', rot[0], rot[1], rot[2])


def add_blendshapes(bs_node, geo, new_bs=None, bs_connect=False, exclude=[]):
    '''Adds the blendshape geos in the scene to the given geo with connections dependent on the given blendshape node
    Args:
        bs_node (str): blendshape to copy
        geo (str): geo to put connections onto
        geo_bs_node (str): if given will put new connections onto this blendshape
        bs_connect (bool): if true will make connections to old bs node, if False
                           will connect to old connections
        exclude list[(str)]: list of targets to exclude
    '''
    conns = {}

    # Get all blendshape targets
    targets = cmds.ls(bs_node + '.w[*]')

    # Get all connections for the targets
    for target in targets:
        weight = target.split('.')[1]

        conn = cmds.listConnections(target, plugs=True)
        if conn:
            conns[weight] = conn[0]
        else:
            conns[weight] = None
    # Create blendshape node if not given
    if not new_bs:
        new_bs = cmds.blendShape(geo, name=geo + '_BS')[0]

    geo_shape = cmds.listRelatives(geo, s=True)[0]

    # Duplicate geos
    with com.Progress(max_value=len(targets)) as prog_bar:
        for i, target in enumerate(targets):
            base_name = target.split('.')[1]
            if base_name not in exclude:
                # If attribute is connected then disconnect so you can toggle it
                if conns[base_name]:
                    cmds.disconnectAttr(conns[base_name], target)
                cmds.setAttr(target, 1)
                bs_geo = base_name
                bs_geo_shape = cmds.listRelatives(bs_geo, s=True)[0]
                mel.eval('blendShape -e -tc on -t {} {} {} 1 -w {} 0  {};'.format(geo_shape, i, bs_geo_shape, i, new_bs))
                if bs_connect:
                    cmds.connectAttr(target, new_bs + '.' + bs_geo_shape)
                elif conns[base_name]:
                    cmds.connectAttr(conns[base_name], new_bs + '.' + bs_geo_shape)

                # rename attribute to it's base name
                mel.eval('blendShapeRenameTargetAlias {} {} {};'.format(new_bs, i, base_name))

                # Reconnect connected attribute
                if conns[base_name]:
                    cmds.connectAttr(conns[base_name], target)
                else:
                    # Set attribute back to 0
                    cmds.setAttr(target, 0)

                prog_bar.step()


def rebuild_blendshapes(bs_node, geo, new_bs=None, bs_connect=False, exclude=[]):
    '''Rebuilds all the blendshape connections onto a new blendshape node on the
    given object
    Args:
        bs_node (str): blendshape to copy
        geo (str): geo to put connections onto
        new_bs (str): if given will put new connections onto this blendshape
        bs_connect (bool): if true will make connections to old bs node, if False
                           will connect to old connections
        exclude list[(str)]: list of targets to exclude
    '''
    conns = {}

    # Get all blendshape targets
    targets = cmds.ls(bs_node + '.w[*]')

    # Get all connections for the targets
    for target in targets:
        weight = target.split('.')[1]

        conn = cmds.listConnections(target, plugs=True)
        if conn:
            conns[weight] = conn[0]
        else:
            conns[weight] = None

    # Create blendshape node if not given
    if not new_bs:
        new_bs = cmds.blendShape(geo, name=geo + '_BS')[0]

    geo_shape = cmds.listRelatives(geo, s=True)[0]

    # Duplicate geos
    with com.Progress(max_value=len(targets)) as prog_bar:
        for i, target in enumerate(targets):
            base_name = target.split('.')[1]
            if base_name not in exclude:
                # If attribute is connected then disconnect so you can toggle it
                if conns[base_name]:
                    cmds.disconnectAttr(conns[base_name], target)
                cmds.setAttr(target, 1)
                bs_geo = cmds.duplicate(geo, name='{}_{}_BS'.format(geo, base_name))[0]
                bs_geo_shape = cmds.listRelatives(bs_geo, s=True)[0]
                mel.eval('blendShape -e -tc on -t {} {} {} 1 -w {} 0  {};'.format(geo_shape, i, bs_geo_shape, i, new_bs))
                if bs_connect:
                    cmds.connectAttr(target, new_bs + '.' + bs_geo_shape)
                elif conns[base_name]:
                    cmds.connectAttr(conns[base_name], new_bs + '.' + bs_geo_shape)

                # rename attribute to it's base name
                mel.eval('blendShapeRenameTargetAlias {} {} {};'.format(new_bs, i, base_name))

                # Reconnect connected attribute
                if conns[base_name]:
                    cmds.connectAttr(conns[base_name], target)
                else:
                    # Set attribute back to 0
                    cmds.setAttr(target, 0)

                prog_bar.step()


def curl_bones(bones, dists, reverse=False):
    cmds.select(clear=True)
    for i, bone in enumerate(bones):
        dist = dists[i]
        if reverse:
            mult = cmds.createNode('math_Multiply')
            cmds.connectAttr('C_petal_CTRL.curl', mult + '.input1')
            cmds.setAttr(mult + '.input2', -1)
            cmds.connectAttr(mult + '.output', bone + '.rx')
        else:
            cmds.connectAttr('C_petal_CTRL.curl', bone + '.rx')
        new_name = bone.split('0_BONE')[0]
        for x in range(1, 4):
            new_bone = new_name + str(x) + '_BONE'
            new_bone = cmds.joint(name=new_bone)
            cmds.parent(new_bone, bone)
            cmds.setAttr(new_bone + '.r', 0, 0, 0)
            cmds.setAttr(new_bone + '.t', 0, dist, 0)
            cmds.setAttr(new_bone + '.jo', 0, 0, 0)
            bone = new_bone

            if reverse:
                mult = cmds.createNode('math_Multiply')
                cmds.connectAttr('C_petal_CTRL.curl', mult + '.input1')
                cmds.setAttr(mult + '.input2', -1)
                cmds.connectAttr(mult + '.output', new_bone + '.rx')
            else:
                cmds.connectAttr('C_petal_CTRL.curl', new_bone + '.rx')

            cmds.select(clear=True)

# tb.curl_bones(cmds.ls(sl=True), [0.05, 0.08, 0.16, 0.23, 0.27, 0.24, 0.07, 0.05])

"""
--------------------------------------------------------------------------------------------------------------------
correctiveBlendshapeCreator.py - Python Script
--------------------------------------------------------------------------------------------------------------------
Copyright 2012 Carlos Chacon L. All rights reserved.
DESCRIPTION:
Eases the creation of corrective blendshapes and their connection to the original mesh.
USAGE:
*Run the script
*Second, Select, in the following order, the master mesh, the first blendshape,the second blendshape. Click the "Create Corrective Blendshape" button.
*Remodel the created blendshape.
*Second, click the "Connect blendshape to master" button.
*Done.
AUTHOR:
Carlos Chacon L. (caedo.00 at gmail dot com)
--------------------------------------------------------------------------------------------------------------------
"""

from maya.cmds import blendShape, setAttr, ls, duplicate, rename, move, expression, select, getAttr, listRelatives, listConnections, window, columnLayout, button,text, separator, showWindow, deleteUI, delete, attributeQuery, pointPosition


#UI Elements
bsWin = "correctiveBSWin"
bsLayout = "correctiveBSLayout"
btnCreateCorrectiveBS = "btnCreateCorrectiveBS"
btnConnectCorrectiveBS = "btnConnectCorrectiveBS"
btnRestart = "btnRestart"
lblMsgBS ="lblMsgBS"
#End UI Elements

#BS variables
master =""
blendshape1 =""
blendshape2 =""
corrective_blendshape ="" #name of the corrective blendshape
dummy_blendshape ="" #name of the dummy blendshape who have the combination of the two original blendshapes and the corrective blendshape
#End BS variables


def showSuccessMsg(msg):
	global lblMsgBS
	text(lblMsgBS , e=True, l=msg, bgc=(0,0.54,0))


def showFailMsg(msg):
	global lblMsgBS
	text(lblMsgBS, e=True, l=msg, bgc=(0.92,0.08,0.19))


def showInfoMsg(msg):
	global lblMsgBS
	text(lblMsgBS, e=True, l=msg, bgc=(0.8,0.8,0.6))


def correctiveBlendshapeExists(obj, blendshape1, blendshape2):
	"""
	Checks to see if a corrective blendshape for the given bs1 and bs2 exists on the master mesh
	"""
	blendshape_node = getBlendshapesFromMaster(obj)[0]
	#Now we need to check if the two possible name combinations exists in the blendshape node.
	corrective_blendshape_name1 = "%s_%s_dummy" % (blendshape1, blendshape2)
	corrective_blendshape_name2 = "%s_%s_dummy" % (blendshape2, blendshape1)
	return (attributeQuery(corrective_blendshape_name1, node=blendshape_node, ex=True) or attributeQuery(corrective_blendshape_name2, node=blendshape_node, ex=True))


def clearHistory(obj):
	"""
	Clears the construction history of specified obj.
	"""
	delete(obj, ch=True)


def getSkinClusterNode(obj):
	"""
	Gets the skin cluster nodes of an object. If there isn't any
	return None.
	"""
	shape_node = listRelatives(obj, shapes=True)
	skincluster_node = listConnections(shape_node[0],d=False,s=True, type="skinCluster")
	return skincluster_node


def hasSkinCluster(obj):
	"""
	Checks if the object is binded to a skin cluster.
	"""
	skincluster_node = getSkinClusterNode(obj)
	if(skincluster_node is not None):
		return True
	else:
		return False


def getShapeNode(obj):
	"""
	Returns the shape node of an object.
	"""
	return listRelatives(obj, shapes=True)


def getBlendshapesFromMaster(obj):
	"""
	Returns the blendshape nodes of target obj.
	"""
	#Blendshape nodes are connected to the shape node, not the transform!
	if(hasSkinCluster(obj)):
		skin_cluster = getSkinClusterNode(obj)
		blendshape_nodes = getBlendshapeNodes(skin_cluster[0])
	else:
		shape_node = listRelatives(obj, shapes=True)
		blendshape_nodes = getBlendshapeNodes(shape_node[0])
	return blendshape_nodes


def getBlendshapeNodes(node):
	return listConnections(node,d=False,s=True, type="blendShape")


def getBlendshapeWeightCount(obj):
	"""
	Returns the total number of weights in a blendshape node.
	"""
	return blendShape(obj, q=True, wc=True)


def setBlendshapeWeight(obj,weight ,value):
	"""
	Turn on blenshape of a object
	"""
	blendshape_node = getBlendshapesFromMaster(obj)[0]
	setAttr(blendshape_node + "." + weight, value )


def getBlendshapeWeight(obj, weight):
	"""
	Get the weight value of blendshape
	"""
	blendshape_node = getBlendshapesFromMaster(obj)[0]
	return getAttr(blendshape_node + "." + weight)


def getVertices(obj):
	vertices = list()
	for v in getAttr(obj+".vrts", multiIndices=True):
		vertices.append(pointPosition(obj+".vtx["+str(v)+"]",w=True))
	return vertices


def getObjWidth(obj):
	vertices = getVertices(obj)
	return max([v[0] for v in vertices]) - min([v[0] for v in vertices])


def getBlendshapePosFromMaster(master):
	"""
	Returns the blendshape position in X based on the master's X position.
	"""
	master_pos = getAttr(master + ".tx")
	return master_pos + getObjWidth(master)


def createCorrectiveBlendshape(master, blendshape1, blendshape2):
	"""
	Creates the corrective blendshape to be reshaped,  using the master as template.
	"""
	corrective_blendshape_pos = getBlendshapePosFromMaster(master)
	corrective_blendshape = "%s_%s_corrective" % (blendshape1, blendshape2)
	duplicate(master, name=corrective_blendshape)
	if (getBlendshapeWeight(master, blendshape1) == 0) and  (getBlendshapeWeight(master, blendshape2) == 0):
		blendShape(blendshape1, blendshape2,corrective_blendshape)
		setBlendshapeWeight(corrective_blendshape, blendshape1, 1)
		setBlendshapeWeight(corrective_blendshape, blendshape2, 1)
		clearHistory(corrective_blendshape)
	move(corrective_blendshape_pos,0,0, corrective_blendshape)
	return corrective_blendshape


def applyBlendshapesToDummy(master, blendshape1, blendshape2, corrective_blendshape):
	"""
	Applies the bs1, bs2 and corrective_bs to a copy of the master,
	in order to create the final substraction of the bs1 and bs2 against
	corrective bs.
	"""
	dummy_blendshape = corrective_blendshape.replace("corrective", "dummy")
	dummy_blendshape_pos = getBlendshapePosFromMaster(corrective_blendshape)
	blendshape_node = getBlendshapesFromMaster(master)[0]
	setAttr(blendshape_node + ".envelope", 0)
	duplicate(master, name=dummy_blendshape)
	setAttr(blendshape_node + ".envelope", 1)
	move(dummy_blendshape_pos,0,0, dummy_blendshape)
	blendshape_node = blendShape(blendshape1, blendshape2, corrective_blendshape, dummy_blendshape)[0]
	setAttr(blendshape_node + "." + blendshape1, -1)
	setAttr(blendshape_node + "." + blendshape2, -1)
	setAttr(blendshape_node + "." + corrective_blendshape, 1)
	return dummy_blendshape


def addDummyToMasterBlendshape(master,dummy):
	"""
	Adds the template blendshape with the corrective substraction
	to the master blendshape.
	"""
	blendshape_node = getBlendshapesFromMaster(master)[0]
	weight_count = getBlendshapeWeightCount(master)
	blendShape(master, e=True, t=[master, weight_count+1,dummy,1])


def addCorrectiveExpressionToMaster(master, blendshape_dummy, blendshape1, blendshape2):
	"""
	Add expression to turn on corrective expression when
	bs1 and bs2 are active.
	"""
	master_blendshape = getBlendshapesFromMaster(master)[0]
	expression_name = "exp_" + blendshape_dummy
	expression_string = master_blendshape+"."+blendshape_dummy + "=" + master_blendshape +"."+blendshape1 + " * " + master_blendshape+"."+blendshape2 + ";"
	expression(s=expression_string, n=expression_name)


def createCopy(*args):
	global master, blendshape1, blendshape2, corrective_blendshape
	blendshapes = ls(sl=True)
	if(len(blendshapes) == 3):
		if(corrective_blendshape is ""):
			master = blendshapes[0]
			if(getBlendshapesFromMaster(master) is not None):
				blendshape1 = blendshapes[1]
				blendshape2 = blendshapes[2]
				if(not correctiveBlendshapeExists(master, blendshape1, blendshape2)):
					corrective_blendshape = createCorrectiveBlendshape(master, blendshape1, blendshape2)
					showSuccessMsg("Corrective blendshape created successfully. Remodel it!")
				else:
					showFailMsg("Corrective blendshape already exists.")
			else:
				showFailMsg("The master mesh doesn't has any blendshape node.")
		else:
			showFailMsg("A corrective blendshape is already on creation process. Hit the restart button if you want to start all over again.")
	else:
		showFailMsg("Incorrect number of blendshapes objs selected! Please select the master, blenshape1 and blendshape2.")


def connectoToMaster(*args):
	global master, blendshape1, blendshape2, corrective_blendshape
	if((master is not "") and (blendshape1 is not "") and (blendshape2 is not "") and (corrective_blendshape is not "")):
		dummy_blendshape = applyBlendshapesToDummy(master, blendshape1, blendshape2, corrective_blendshape)
		addDummyToMasterBlendshape(master, dummy_blendshape)
		addCorrectiveExpressionToMaster(master, dummy_blendshape, blendshape1, blendshape2)
		master = blendshape1 = blendshape2 = corrective_blendshape = dummyblendshape = ""
		showSuccessMsg("Corrective blendshape has been connected successfully.")
	else:
		showFailMsg("The corrective blendshape hasn't been created.")


def restart(*args):
	"""
	Restart the whole corrective blendshape creation process.
	"""
	global master, blendshape1, blendshape2, corrective_blendshape, dummy_blendshape
	master = blendshape1 = blendshape2 = corrective_blendshape = dummy_blendshape = ""
	showInfoMsg("Creation process restarted.")


def showCorrectiveBlendshapeWindow():
	"""
	Shows the GUI for the corrective blendshape creator.
	"""
	global bsWin, bsLayout,btnCreateCorrectiveBS, btnConnectCorrectiveBS,btnRestart,lblMsgBS
	if(window(bsWin, q=True, exists=True)):
		deleteUI(bsWin, window=True)
	window(bsWin,width = 270, title="Corrective BS Creator")
	columnLayout(bsLayout, adjustableColumn = True,p=bsWin,co=("both",10), rs=10)
	separator(p=bsLayout, vis=True)
	button(btnCreateCorrectiveBS,p=bsLayout, l="1. Create Corrective Blendshape",h=50,command=createCopy)
	separator(p=bsLayout)
	button(btnConnectCorrectiveBS,p=bsLayout, l="2. Connect Blendshape to Master", h=50,command=connectoToMaster)
	separator(p=bsLayout)
	separator(p=bsLayout, vis=False)
	button(btnRestart, p=bsLayout, l="Restart", h=50, command=restart)
	separator(p=bsLayout)
	text(lblMsgBS,p=bsLayout,l="Select the master, blendshape1 and blendshape2 to create the corrective BS.",ww=True )
	separator(p=bsLayout, vis=True)
	showWindow()


# showCorrectiveBlendshapeWindow()



# import rigpackage.rigmodules as mods
# from ...rigAssetFLAMESetup import FLAMEPropPreset
#
# from rigpackage import rigmodules
# from rigpackage.rigmodules import rigmodule
# from ...rigAssetFLAMESetup import FLAMEBipedPreset
# from rigpackage import riglibrary as lib
#
# import sys
# p = 'M:/home/RyanF/git/fed-rigging'
# if p not in sys.path:
#     sys.path.append(p)
#
# import create.nurbs as nrb
# import custom_utils.component as comp
# import custom_utils.attribute as attr
# import create.nodes as node
#
# reload(nrb)

# import maya.cmds as cmds
# import maya.api.OpenMaya as om
#
# class TaleneWingsBuild(FLAMEPropPreset):
#
#     # CACHE_RIGTYPE = 'animation'
#
#     def __init__(self, environment):
#         """
#         Initialize the asset and add the modules.
#         """
#         super(TaleneWingsBuild, self).__init__(environment)
#
#         self.ORIENTATION = lib.Orientation.XY
#
#         self.WingBuild('0', ['Primary'], dict(Primary=12))
#         # self.WingBuild('1', ['Primary'], dict(Primary=12))
#
#         mGrps = ChainPostBuild()
#         self.add_module(mGrps)
#
#     def WingBuild(self, name, featherTypes, featherNum):
#         '''
#         Args:
#             featherTypes [list(str)]: primary, secondary, tertiary, etc.
#             featherNum {dict}: key is the feather type, value is the number of feathers
#         @return:
#         '''
#         for side in ['Lf', 'Rt']:
#             orient = lib.Orientation(-self.ORIENTATION.side_vector, -self.ORIENTATION.up_vector) \
#                 if side == 'Lf' else lib.Orientation(self.ORIENTATION.side_vector, self.ORIENTATION.up_vector)
#
#             temp_par = rigmodules.FK('{}_Wing{}Par'.format(side, name), system='PUPPET')
#             temp_par.attach_point << self.Ct_Root.joints[0]
#             self.add_module(temp_par)
#
#             wing = mods.Limb('{}_Wing{}'.format(side, name), system='PUPPET')
#             wing.attach_point << temp_par.joints[0]
#             wing.orientation.value = orient
#             # wing.color.value = side_color
#             wing.attach_scale.value = True
#             wing.gimbal.value = True
#             wing.has_stretch.value = 'ANIMATION' in self.systems
#             wing.has_twist.value = 'ANIMATION' in self.systems
#             wing.has_ik_pin.value = 'ANIMATION' in self.systems
#             wing.twist_joints_number.value = 2
#             wing.has_twist_controls.value = True
#             self.add_module(wing)
#
#             hand = mods.FK('{}_Wing{}Hand'.format(side, name), system='PUPPET')
#             hand.attach_point << wing.joints[-1]
#             self.add_module(hand)
#
#             wing_in = rigmodules.FK('{}_Wing{}In'.format(side, name), system='ANIMATION')
#             wing_in.attach_point << wing.joints[0]
#             self.add_module(wing_in)
#
#             wing_mid_in = rigmodules.FK('{}_Wing{}MidIn'.format(side, name), system='ANIMATION')
#             wing_mid_in.attach_point <<  wing.joints[1]
#             self.add_module(wing_mid_in)
#
#             wing_mid_out = rigmodules.FK('{}_Wing{}MidOut'.format(side, name), system='ANIMATION')
#             wing_mid_out.attach_point <<  wing.joints[2]
#             self.add_module(wing_mid_out)
#
#             wing_out = rigmodules.FK('{}_Wing{}Out'.format(side, name), system='ANIMATION')
#             wing_out.attach_point << hand.joints[0]
#             self.add_module(wing_out)
#
#             # Create dags for rail curves
#             start = rigmodules.FK('{}_Wing{}Start'.format(side, name), system='ANIMATION')
#             self.add_module(start)
#
#             end = rigmodules.FK('{}_Wing{}End'.format(side, name), system='ANIMATION')
#             self.add_module(end)
#
#             # create space switches
#             space_names = ['In', 'MidIn', 'MidOut', 'Out']
#             wings = [wing_in, wing_mid_in, wing_mid_out, wing_out]
#             for i in range(4):
#                 if 0 < i < 3:
#                     wing_joint = rigmodule.RigModuleAttr()
#                     wing_joint.value = '{}_Wing{}{}_middle_SPACE'.format(side, name, space_names[i])
#                 elif i == 0:
#                     wing_joint = wing.joints[0]
#                 else:
#                     wing_joint = hand.joints[0]
#                 space_transform = rigmodule.RigModuleAttr()
#                 space_transform.value = '{}_Wing{}{}_pv_SPACE'.format(side, name, space_names[i])
#                 space = rigmodules.SpaceSwitch('{}_Wing{}{}SpaceSwitch'.format(side, name, space_names[i]), system='ANIMATION', stage=41)
#                 space.control << wings[i].controls[0]
#                 space.switch_type.value = 'parent'
#                 space.targets['root'] << self.Ct_Root.joints[0]
#                 space.targets['placer'] << self.Placer.placer_control
#                 space.targets[space_names[i]] << wing_joint
#                 space.targets['pole'] << space_transform
#                 space.names.value = ['root', 'placer', space_names[i], 'pole']
#                 space.default.value = 2
#                 self.add_module(space)
#
#             post = WingsPost('{}_Wings{}Post'.format(side, name))
#             post.side = side
#             post.wing_name = name
#             post.feth = 'Primary'
#             self.add_module(post)
#
#         for feth in featherTypes:
#             for side in ['Lf', 'Rt']:
#                 num = featherNum[feth]
#                 for i in range(num):
#                     feather = rigmodules.FK('{}_Wing{}_{}Feth{}'.format(side, name, feth, i), system='ANIMATION', stage=20)
#                     self.add_module(feather)
#
# class WingsPost(rigmodules.Module):
#     def __init__(self, name='WingsPost', system="ANIMATION", stage=40):
#         super(WingsPost, self).__init__(name, system=system, stage=stage)
#         self.side = 'Lf'
#         self.wing_name = '0'
#         self.feth = 'Primary'
#
#     def build(self):
#         cmds.currentUnit(linear='cm')
#
#         # Create group to put all components under
#         parts_grp = cmds.createNode('transform', name='{}_Wing{}_parts_GRP'.format(self.side, self.wing_name))
#         # cmds.setAttr(parts_grp + '.v', 0)
#         cmds.parent(parts_grp, 'Placer_world_GRP')
#         parts = []
#
#         wings = dict(In=None, MidIn=None, MidOut=None, Out=None)
#         # Create extra attributes on wings
#         for wing, null in wings.items():
#             wing_ctl = '{}_Wing{}{}_0_CTL'.format(self.side, self.wing_name, wing)
#
#             # Do out twist and in twist for middle, just in for in and just out for out
#             if wing == 'In':
#                 attr.add_float(wing_ctl, 'outTwist', min=-360, max=360, default=0)
#             elif wing == 'Out':
#                 attr.add_float(wing_ctl, 'inTwist', min=-360, max=360, default=0)
#             else:
#                 attr.add_float(wing_ctl, 'outTwist', min=-360, max=360, default=0)
#                 attr.add_float(wing_ctl, 'inTwist', min=-360, max=360, default=0)
#             wings[wing] = cmds.ls('{}_Wing{}{}_*_JNT'.format(self.side, self.wing_name, wing))
#
#         wing_start = cmds.ls('{}_Wing{}Start_*_JNT'.format(self.side, self.wing_name))
#         wing_end = cmds.ls('{}_Wing{}End_*_JNT'.format(self.side, self.wing_name))
#
#         # Attach rail controls to the main wing
#         start_crv = nrb.curve_from_dags('{}_Wing{}Start_CRV'.format(self.side, self.wing_name),
#                                         [wings['In'][0], wings['MidIn'][0], wings['MidOut'][0], wings['Out'][0]],
#                                         degree=1,
#                                         attach_dags=True,
#                                         rebuild=True)
#         end_crv = nrb.curve_from_dags('{}_Wing{}End_CRV'.format(self.side, self.wing_name),
#                                       [wings['In'][-1], wings['MidIn'][-1], wings['MidOut'][-1], wings['Out'][-1]],
#                                       degree=1,
#                                       attach_dags=True,
#                                       rebuild=True)
#         crvs = [start_crv, end_crv]
#         parts += crvs
#
#         # Bind curves to main wing setup and then attach rail controls to curves
#         for i, jnts in enumerate([wing_start, wing_end]):
#             ref_i = i * -1
#             # cmds.skinCluster(wing0[ref_i], wing1[ref_i], wing2[ref_i], crvs[i], name=crvs[i] + '_SKC')
#             for jnt in jnts:
#                 zro = jnt.split('JNT')[0] + 'ZRO'
#                 crv = nrb.dag_to_curve(zro,
#                                        crvs[i],
#                                        up_vec_refs=[wings['In'][ref_i], wings['MidIn'][ref_i], wings['MidOut'][ref_i], wings['Out'][ref_i]],
#                                        snap=False)
#                 parts.append(crv)
#
#
#         # Create Nurbs planes
#         # plane, plane_parts = nrb.multi_birail_nurbs_plane('{}_Wing{}_SURFACE'.format(self.side, self.wing_name),
#         #                                                   [wings['In'], wings['MidIn'], wings['MidOut'], wings['Out']],
#         #                                                   [wing_start, wing_end])
#         # parts += plane_parts
#         profiles = ['In', 'MidIn', 'MidOut', 'Out']
#         surface_names = ['In', 'Mid', 'Out']
#         planes = []
#         for i in range(3):
#             prof1 = wings[profiles[i]]
#             prof2 = wings[profiles[i + 1]]
#             range_add = 3 * i
#             start = wing_start[0 + range_add:4 + range_add]
#             end = wing_end[0 + range_add:4 + range_add]
#             plane, plane_parts = nrb.birail_nurbs_plane(prof1,
#                                                         prof2,
#                                                         '{}_Wing{}{}_SURFACE'.format(self.side, self.wing_name, surface_names[i]),
#                                                         rails=[start, end])
#             parts += plane_parts
#             parts.append(plane)
#             planes.append(plane)
#
#         # Find amount of feathers
#         feth_bases = cmds.ls('{}_Wing{}_{}Feth*_0_CTL'.format(self.side, self.wing_name, self.feth))
#         for i, feather in enumerate(feth_bases):
#             # Create twist attribute
#             attr.add_float(feather, 'twist', default=0, min=-360, max=360)
#             zros = cmds.ls('{}_Wing{}_{}Feth{}_*_ZRO'.format(self.side, self.wing_name, self.feth, i))
#             mgrps = cmds.ls('{}_Wing{}_{}Feth{}_*_JNT_mGrp1'.format(self.side, self.wing_name, self.feth, i))
#             jnts = cmds.ls('{}_Wing{}_{}Feth{}_*_JNT'.format(self.side, self.wing_name, self.feth, i))
#             pxys = []
#             # Create proxy transforms for zros, rotation setup
#             for x, zro in enumerate(zros):
#                 pxy = cmds.createNode('transform', name=zro + '_PXY')
#                 pos = cmds.xform(zro, t=True, ws=True, q=True)
#                 rot = cmds.xform(zro, ro=True, ws=True, q=True)
#                 cmds.xform(pxy, t=pos, ws=True)
#                 cmds.xform(pxy, ro=rot, ws=True)
#
#                 if x > 0:
#                     cmds.parent(pxy, pxys[-1])
#                 else:
#                     parts.append(pxy)
#                 pxys.append(pxy)
#
#                 if x > 0:
#                     cmds.connectAttr(pxy + '.t', zro + '.t')
#                     cmds.connectAttr(pxy + '.r', zro + '.r')
#                 else:
#                     cmds.parentConstraint(pxy, zro)
#
#             # Find closest plane to each zro and attach zro
#             plane_dict = dict(In=['In', 'MidIn'],
#                               Mid=['MidIn', 'MidOut'],
#                               Out=['MidOut', 'Out'])
#             for x, zro in enumerate(zros):
#                 surface = comp.find_closest_surface(zro, planes)
#                 foll = nrb.attach_to_surface(surface, pxys[x], snap=False, scale=False)
#                 parts.append(foll)
#
#                 # Attach Twist
#                 base = surface.split(self.wing_name)[1].split('_')[0]
#                 if x == 0:
#                     blend = node.Node(foll + '_twist', 'blendColors')
#                     add = node.Node(foll + '_twist', 'addDoubleLinear')
#                     in_ctl = '{}_Wing{}{}_0_CTL'.format(self.side, self.wing_name, plane_dict[base][0])
#                     out_ctl = '{}_Wing{}{}_0_CTL'.format(self.side, self.wing_name, plane_dict[base][1])
#                     blend.connect(color1R=out_ctl + '.inTwist',
#                                   color2R=in_ctl + '.outTwist',
#                                   blender=foll + '.parU',
#                                   )
#                     add.connect(input1=blend.node + '.outputR',
#                                 input2=feather + '.twist',
#                                 output=jnts[x] + '.rx')
#                 else:
#                     add.connect(output=jnts[x] + '.rx')
#
#                     # Aim at joint at previous joint
#                     if self.side == 'Rt':
#                         aimVec = (1, 0, 0)
#                         upVec = (0, -1, 0)
#                     else:
#                         aimVec = (-1, 0, 0)
#                         upVec = (0, 1, 0)
#                     cmds.aimConstraint(jnts[x-1], mgrps[x],
#                                        worldUpType='objectrotation',
#                                        worldUpObject=jnts[x-1],
#                                        aimVector=aimVec,
#                                        upVector=upVec,
#                                        mo=True,
#                                        skip=['x', 'z'])
#
#         # Create transforms for spaceswitches
#         space_names = ['In', 'MidIn', 'MidOut', 'Out']
#         for i in range(4):
#             space = cmds.createNode('transform', name='{}_Wing{}{}_pv_SPACE'.format(self.side, self.wing_name, space_names[i]))
#             jnt = '{}_Wing{}_{}_JNT'.format(self.side, self.wing_name, i)
#             if i == 3:
#                 jnt = '{}_Wing{}Hand_0_JNT'.format(self.side, self.wing_name)
#             cmds.pointConstraint(jnt, space, mo=False)
#             if self.side == 'Rt':
#                 aimVec = (-1, 0, 0)
#                 upVec = (0, -1, 0)
#             else:
#                 aimVec = (1, 0, 0)
#                 upVec = (0, 1, 0)
#             cmds.aimConstraint('{}_Wing{}_ik_pole_vector_CTL'.format(self.side, self.wing_name), space,
#                                worldUpType='objectrotation',
#                                worldUpObject=jnt,
#                                aimVector=aimVec,
#                                upVector=upVec,
#                                mo=False)
#
#             parts.append(space)
#
#         # Space for middle rotation between wrist and shoulder
#         for i in range(1, 3):
#             zro = cmds.createNode('transform', name='{}_Wing{}{}_middle_SPACE_ZRO'.format(self.side, self.wing_name, space_names[i]))
#             space = cmds.createNode('transform', name='{}_Wing{}{}_middle_SPACE'.format(self.side, self.wing_name, space_names[i]))
#             cmds.parent(space, zro)
#             cmds.parentConstraint('{}_Wing{}_{}_JNT'.format(self.side, self.wing_name, i), zro, mo=False)
#             mult = node.Node('{}_Wing{}{}_middle_SPACE'.format(self.side, self.wing_name, space_names[i]), 'multiplyDivide')
#             mult.connect(input1X='{}_Wing{}_{}_JNT.ry'.format(self.side, self.wing_name, i),
#                          input2X=-0.5,
#                          outputX=space + '.ry')
#             parts.append(zro)
#
#         # Parent all parts under parts group
#         for each in parts:
#             cmds.parent(each, parts_grp)
#
#         cmds.currentUnit(linear='meter')
#
#
# class ChainPostBuild(rigmodules.Module):
#     def __init__(self, name='ChainPostBuild', system="ANIMATION", stage=35):
#         super(ChainPostBuild, self).__init__(name, system=system, stage=stage)
#
#     def build(self):
#         feathers = cmds.ls('*Feth*JNT')
#         for feth in feathers:
#             # for i in range(2):
#             self.mGrp(feth)
#
#     def m_align(self, obj="", goal_obj=""):
#         cmds.delete(cmds.parentConstraint(goal_obj, obj))
#         return obj
#
#     def mGrp(self, obj=""):
#         m_grp = cmds.createNode("transform", n=obj + "_mGrp#")
#         self.m_align(m_grp, obj)
#
#         par = cmds.listRelatives(obj, p=True)
#         cmds.parent(obj, m_grp)
#         if par:
#             cmds.parent(m_grp, par[0])
#         return m_grp