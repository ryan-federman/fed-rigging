import copy as copy

import maya.cmds as cmds
import maya.api.OpenMaya as om
import sys

import custom_utils.component as comp
import custom_utils.skin as skin
import custom_utils.generic as gen

path = 'M:/home/harryh/git/rigginglab/rigginglab/skinning_utilities'
if path not in sys.path:
   sys.path.append(path)

import skinFromVirtualNrb


# def skin_closest_geo(base_jnts, geos, virtual_nrb=True, suffix='JNT'):
#     '''
#     Skins closest mesh based on the first joint
#     '''
#
#     jnt_vecs = dict()
#     chain_mapping = dict()
#
#     # Record all positions of verts
#     for jnt in base_jnts:
#         pos = cmds.xform(jnt, t=True, ws=True, q=True)
#         jnt_vecs[jnt] = om.MVector(pos[0], pos[1], pos[2])
#
#     for jnt, pos in jnt_vecs.items():
#         # Find closest mesh to joint
#         closest_mesh = None
#         shortest_distance = None
#
#         for geo in geos:
#             closest_point = comp.closest_point_on_mesh(geo, pos)[0]
#             distance = (pos - closest_point).length()
#             if not closest_mesh:
#                 closest_mesh = geo
#                 shortest_distance = distance
#             else:
#                 if distance < shortest_distance:
#                     closest_mesh = geo
#                     shortest_distance = distance
#
#         chain_mapping[jnt] = closest_mesh
#         print closest_mesh
#         geos.remove(closest_mesh)
#
#     # Skin chains to meshes
#     for jnt, geo in chain_mapping.items():
#         jnts = jnt.split('_0_')[0] + '_*_{}'.format(suffix)
#         if virtual_nrb:
#             cmds.select(geo + '.vtx[*]')
#             skinFromVirtualNrb.skin_selected(jnts, iters=200)
#             cmds.select(clear=True)
#         else:
#             cmds.skinCluster(jnts + [geo])


def skin_closest_geo(base_jnts, geos, virtual_nrb=True, suffix='JNT'):
    '''
    Skins closest mesh based on the first joint
    '''

    jnt_vecs = dict()
    chain_mapping = dict()

    # Record all positions of verts
    for jnt in base_jnts:
        pos = cmds.xform(jnt, t=True, ws=True, q=True)
        jnt_vecs[jnt] = om.MVector(pos[0], pos[1], pos[2])

    for geo in geos:
        # Find closest joint to mesh
        closest_jnt = None
        shortest_distance = None

        for jnt, pos in jnt_vecs.items():
            closest_point = comp.closest_point_on_mesh(geo, pos)[0]
            distance = (pos - closest_point).length()
            if not closest_jnt:
                closest_jnt = jnt
                shortest_distance = distance
            else:
                if distance < shortest_distance:
                    closest_jnt = jnt
                    shortest_distance = distance

        chain_mapping[geo] = closest_jnt
        del jnt_vecs[closest_jnt]

    # Skin chains to meshes
    for geo, jnt in chain_mapping.items():
        jnts = cmds.ls(jnt.split('_0_')[0] + '_*_{}'.format(suffix))
        if virtual_nrb:
            cmds.select(geo + '.vtx[*]')
            skinFromVirtualNrb.skin_selected(jnts, iters=200)
            cmds.select(clear=True)
        else:
            cmds.skinCluster(jnts + [geo])

        base_jnts.remove(jnt)
        geos.remove(geo)

    # Print out remaining joint chains to see which were not skinned
    for jnt in base_jnts:
        print '{} was not skinned to a mesh'.format(jnt)


def skin_closest_geo_to_chain(base, geos, separate=True, ordered_geos=[], base_skin_jnts=['Ct_Root_0_JNT'], level=3, vec_size=0.3, remove=None, rename=True, inside=False, virtual_nrb=True):
    '''
    Skins joint chains to geometries based on which is the closest to that joint chain
    Args:
        base (str): 0 joint of each strand with an asterisk Ex: Ct_Hair*_0_JNT
        geos list[(str)]: list of geometries to skin, if separate is True then provide a list with one geo
        separate
        ordered_geos list[(str)]: whether the given geo list is ordered according to the position of the joint chains, use in conjunction with separate flag
        base_skin_jnts list[(str)]: base joint to flood rest of geo that is not skinned
        level int: indicates number of vectors to reference when finding closest geos, highest is 3
        vec_size float: size of vectors coming from objects
        remove (str): removes a string from the base list
        inside (bool): indicates whether the function will make sure joints skinned are within the given geometry
    '''
    cmds.currentUnit(linear='cm')
    print 'Changed current Scene Units to cm'
    unskinned_jnts = []
    new_geo = None
    renamed_geos = dict()
    base_jnts = cmds.ls(base)

    if remove:
        base_jnts.remove(remove)

    # Separate geometries
    if separate:
        base_geo = geos[0]
        if ordered_geos:
            geos = ordered_geos
        else:
            new_geo = cmds.duplicate(base_geo)[0]
            geos = cmds.polySeparate(new_geo)
            geos.pop(-1)

        # Map vertices of base geo to the separated geos
        geo_mapping = dict()
        win = gen.ProgressWindow(num_steps=len(geos), title='Mapping Vertices')
        for geo in geos:
            vtx_list = cmds.ls('{}.vtx[*]'.format(geo), fl=True)
            print 'Mapping {} to {}'.format(geo, base_geo)
            for vtx in vtx_list:
                pos = cmds.xform(vtx, t=True, ws=True, q=True)
                base_vtx = comp.closest_vtx_on_mesh(base_geo, pos)
                geo_mapping[base_vtx] = vtx
            win.step()
        win.end()

    # Skin geos

    if not ordered_geos:
        win = gen.ProgressWindow(num_steps=len(base_jnts), title='Skinning Joints')
        unskinned_jnts = []
        for jnt in base_jnts:
            skin_check = 0
            jnt_base = jnt.split('_0_')[0]
            query_name = jnt.split('_0_')[0] + '_*_JNT'
            strand_jnts = cmds.ls(query_name)
            geo_list = copy.deepcopy(geos)
            while len(geo_list) > 0:
                closest_geo = comp.closest_average_geo(strand_jnts, geo_list, level=level, vec_size=vec_size)

                # Check to see if joints are inside mesh
                inside_check = 0
                for strand_jnt in strand_jnts:
                    pos = cmds.xform(strand_jnt, t=True, ws=True, q=True)
                    pos = om.MVector(pos[0], pos[1], pos[2])
                    point_on_mesh, normal, face = comp.closest_point_on_mesh(closest_geo, pos, add_normal=True)
                    vec_between = point_on_mesh - pos
                    dot = vec_between * normal
                    if dot > 0:
                        inside_check += 1

                if inside_check == len(strand_jnts):
                    cmds.skinCluster([closest_geo] + strand_jnts)
                    geos.remove(closest_geo)
                    if rename:
                        renamed_geos[closest_geo] = jnt_base + '_GEO'

                    skin_check = 1
                    break
                else:
                    geo_list.remove(closest_geo)

            if skin_check == 0:
                if inside:
                    print '{} chain was not skinned'.format(jnt)
                else:
                    print '{} chain may have inaccurate skinning'.format(jnt)
                unskinned_jnts.append(jnt)

            win.step()
        win.end()

    else:
        for i, jnt in enumerate(base_jnts):
            jnt_base = jnt.split('_0_')[0]
            print "Skinning {} chain".format(jnt_base)
            query_name = jnt.split('_0_')[0] + '_*_JNT'
            strand_jnts = cmds.ls(query_name)

            cmds.skinCluster([geos[i]] + strand_jnts)

            if virtual_nrb:
                cmds.select(geos[i] + '.vtx[*]')
                skinFromVirtualNrb.skin_selected(strand_jnts, iters=200)
                cmds.select(clear=True)

    # Skin remaining geos that didn't have joints perfectly inside them
    if not inside:
        for jnt in unskinned_jnts:
            jnt_base = jnt.split('_0_')[0]
            query_name = jnt.split('_0_')[0] + '_*_JNT'
            strand_jnts = cmds.ls(query_name)

            closest_geo = comp.closest_average_geo(strand_jnts, geos, level=level, vec_size=vec_size)
            cmds.skinCluster([closest_geo] + strand_jnts)
            geos.remove(closest_geo)
            if rename:
                renamed_geos[closest_geo] = jnt_base + '_GEO'

    # If base geo was separated
    if separate:
        # Get all joints
        all_jnts = []
        if base_skin_jnts:
            all_jnts += base_skin_jnts
        for jnt in base_jnts:
            query_name = jnt.split('_0_')[0] + '_*_JNT'
            strand_jnts = cmds.ls(query_name)
            all_jnts += strand_jnts

        # Skin base geo and then use mapping to transfer weights
        old_skc = skin.find_skin(base_geo)
        cmds.delete(old_skc)
        base_skc = base_geo.split('_')[0] + '_SKC'
        cmds.skinCluster([base_geo] + all_jnts, name=base_skc)

        # Flood all vtx weights with the base skin joint
        if base_skin_jnts:
            vtx_list = cmds.ls(base_geo + '.vtx[*]', fl=True)
            win = gen.ProgressWindow(num_steps=len(vtx_list), title='Flooding Base Geo')
            for vtx in vtx_list:
                cmds.select(vtx)
                cmds.skinPercent(base_skc, transformValue=(base_skin_jnts[0], 1))
                win.step()
            win.end()

        win = gen.ProgressWindow(num_steps=len(geo_mapping), title='Skinning Vertices')
        for base_vtx, sep_vtx in geo_mapping.items():
            # Get joint values for vertex
            sep_geo = sep_vtx.split('.')[0]
            skc = skin.find_skin(sep_geo)
            if skc:
                values = cmds.skinPercent(skc, sep_vtx, query=True, value=True)
                jnts = cmds.skinPercent(skc, sep_vtx, transform=None, query=True)

                # Create list for applying values
                value_list = list()
                for i, jnt in enumerate(jnts):
                    value_list.append((jnt, values[i]))

                cmds.select(base_vtx)
                cmds.skinPercent(base_skc, transformValue=value_list)

            win.step()
        win.end()

        # Delete separated geos
        if new_geo:
            cmds.delete(new_geo)

    # Rename geos with their named equivalents
    elif rename:
        for geo, new_name in renamed_geos.items():
            cmds.rename(geo, new_name)