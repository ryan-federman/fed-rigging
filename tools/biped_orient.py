import maya.cmds as cmds
import maya.api.OpenMaya as om


def biped_orient():
    '''
    Orients joints of biped according to their needs
    '''

    # Define joints to orient
    l_leg = ['Lf_Leg_{}_JNT'.format(i) for i in range(3)]
    r_leg = ['Rt_Leg_{}_JNT'.format(i) for i in range(3)]
    l_arm = ['Lf_Arm_{}_JNT'.format(i) for i in range(3)]
    r_arm = ['Rt_Arm_{}_JNT'.format(i) for i in range(3)]
    spine = ['Ct_Spine_{}_JNT'.format(i) for i in range(3)]
    neck = ['Ct_Neck_0_JNT', 'Ct_Neck_1_JNT']
    fingers = ['Thumb', 'Index', 'Middle', 'Ring', 'Pinky']

    # Left leg
    rot_to_plane(l_leg, up='x', aim='y', y_reverse=True, z_reverse=True)

    # Right leg
    rot_to_plane(r_leg, up='x', aim='y')

    # Left arm
    rot_to_plane(l_arm, y_reverse=True)

    # Right arm
    rot_to_plane(r_arm, x_reverse=True, y_reverse=True, z_reverse=True)

    # Left fingers
    for finger in fingers:
        if finger == 'Thumb':
            aim_at_next(['Lf_Thumb_{0}_JNT'.format(i) for i in range(4)], y_reverse=True, z_reverse=True)
        else:
            aim_at_next(['Lf_{0}_{1}_JNT'.format(finger, i) for i in range(5)], y_reverse=True, z_reverse=True)

    # Right fingers
    for finger in fingers:
        if finger == 'Thumb':
            aim_at_next(['Rt_Thumb_{0}_JNT'.format(i) for i in range(4)], x_reverse=True, y_reverse=True)
        else:
            aim_at_next(['Rt_{0}_{1}_JNT'.format(finger, i) for i in range(5)], x_reverse=True, y_reverse=True)

    # Spine
    # aim_at_next(spine, aim='y', up='z', up_vector=(0, 0, 1), x_reverse=True, z_reverse=True)
    # aim_at_next(spine_broken, aim='y', up='z', up_vector=(0, 0, 1), x_reverse=True, z_reverse=True)

    # Neck
    aim_at_next(neck, aim='y', up='z', up_vector=(0, 0, 1), x_reverse=True, z_reverse=True)


def rot_to_plane(dags, aim='x', up='y', last_rot=False, x_reverse=False, y_reverse=False, z_reverse=False):
    '''
    Sets rotation of each dag to the plane
    Args:
        dags list[(str)]: dags to change rotation of
        last_rot (bool): if False leaves last dag alone
        x_reverse (bool): reverses final x vector
        y_reverse (bool): reverses final y vector
        z_reverse (bool): reverses final z vector
    '''

    # info regarding whether a vector will be flipped
    x_rev = 1
    y_rev = 1
    z_rev = 1

    if x_reverse:
        x_rev = -1
    if y_reverse:
        y_rev = -1
    if z_reverse:
        z_rev = -1

    # get vectors for plane calculations
    root = cmds.xform(dags[0], t=True, ws=True, q=True)
    mid = cmds.xform(dags[1], t=True, ws=True, q=True)
    end = cmds.xform(dags[2], t=True, ws=True, q=True)

    root = om.MVector(root[0], root[1], root[2])
    mid = om.MVector(mid[0], mid[1], mid[2])
    end = om.MVector(end[0], end[1], end[2])

    dag_vecs = [root, mid, end]

    norm = get_normal_of_plane(root, mid, end)

    mtxs = []
    # rotate dags to plane
    for i, dag in enumerate(dags):
        if i < 2:
            next_vec = dag_vecs[i + 1]
            vec = dag_vecs[i]
        else:
            next_vec = dag_vecs[i]
            vec = dag_vecs[i - 1]

        if i < 2 or (i == 2 and last_rot):
            # get aim vector
            aim_vec = (next_vec - vec).normal()

            # get side vector
            side_vec = (norm ^ aim_vec).normal()

            # Assign the vectors to their appropriate axis
            vecs = {}
            for vec in ['x', 'y', 'z']:
                if aim == vec:
                    vecs[vec] = aim_vec
                elif up == vec:
                    vecs[vec] = norm
                else:
                    vecs[vec] = side_vec

            # create matrix
            dag_mtx = create_matrix(x_vec=(vecs['x'] * x_rev).normal(), y_vec=(vecs['y'] * y_rev).normal(), z_vec=(vecs['z'] * z_rev).normal(), pos=dag_vecs[i])
            mtxs.append(dag_mtx)
        else:
            mtx = cmds.xform(dag, matrix=True, ws=True, q=True)
            rot = cmds.xform(dag, ro=True, ws=True, q=True)
            mtxs.append(mtx)

    for i, mtx in enumerate(mtxs):
        cmds.setAttr(dags[i] + '.jo', 0, 0, 0)
        # apply matrix to dag
        cmds.xform(dags[i], matrix=mtx, ws=True)

    if not last_rot:
        cmds.xform(dags[2], ro=rot, ws=True)


def aim_at_next(dags, up='y', aim='x', up_vector='y', x_reverse=False, y_reverse=False, z_reverse=False):
    '''
    Aims dag at the next dag in the list
    Args:
        dags list[(str)]: dags to change rotation of
        up (str): up vector of final matrix can be x, y, z
        aim (str): aim vector of final matrix, can be x, y or z
        up_vector (str) or (tuple): where to take initial up vector from for orientating matrix, can be x, y, z, world or a tuple containing the vector
        x_reverse (bool): reverses final x vector
        y_reverse (bool): reverses final y vector
        z_reverse (bool): reverses final z vector
    '''

    # info regarding whether a vector will be flipped
    x_rev = 1
    y_rev = 1
    z_rev = 1

    if x_reverse:
        x_rev = -1
    if y_reverse:
        y_rev = -1
    if z_reverse:
        z_rev = -1

    # Store transformation info of dags
    trans_info = {}
    for dag in dags:
        trans_info[dag] = cmds.xform(dag, matrix=True, ws=True, q=True)

    mtxs = []
    # rotate dags to plane
    for i, dag in enumerate(dags):
        if dag != dags[-1]:
            next_dag = dags[i + 1]
            x_vec, y_vec, z_vec, pos = matrix_to_vectors(trans_info[dag])
            next_pos = matrix_to_vectors(trans_info[next_dag])[3]

            # Determine where the up vector is being referenced from
            if up_vector == 'x':
                old_up = x_vec
            elif up_vector == 'y':
                old_up = y_vec
            elif up_vector == 'z':
                old_up = z_vec
            elif up_vector == 'world':
                old_up = om.MVector(0, 1, 0)
            elif isinstance(up_vector, tuple):
                old_up = om.MVector(up_vector[0], up_vector[1], up_vector[2])

            # get aim vector
            aim_vec = (next_pos - pos).normal()

            # get side vector
            side_vec = (old_up ^ aim_vec).normal()

            # get up vector
            up_vec = (side_vec ^ aim_vec).normal()

            # Assign the vectors to their appropriate axis
            vecs = {}
            for vec in ['x', 'y', 'z']:
                if aim == vec:
                    vecs[vec] = aim_vec
                elif up == vec:
                    vecs[vec] = up_vec
                else:
                    vecs[vec] = side_vec

            # create matrix
            dag_mtx = create_matrix(x_vec=(vecs['x'] * x_rev).normal(), y_vec=(vecs['y'] * y_rev).normal(), z_vec=(vecs['z'] * z_rev).normal(), pos=pos)
            mtxs.append(dag_mtx)
        else:
            mtx = cmds.xform(dag, matrix=True, ws=True, q=True)
            rot = cmds.xform(dag, ro=True, ws=True, q=True)
            mtxs.append(mtx)

    for i, mtx in enumerate(mtxs):
        if cmds.nodeType(dags[i]) == 'joint':
            cmds.setAttr(dags[i] + '.jo', 0, 0, 0)
        # apply matrix to dag
        cmds.xform(dags[i], matrix=mtx, ws=True)


def get_normal_of_plane(root, mid, end):
    '''Creates normalized normal vector of a plane
    Args:
        root om.MVector(): Root Vector
        mid om.MVector(): Mid Vector
        end om.MVector(): End Vector
    Return:
        om.MVector(): normal of plane
    '''
    # get vectors of plane
    upper_vec = (mid - root).normal()
    line_vec = (end - mid).normal()

    normal_vector = (upper_vec ^ line_vec).normal()

    return normal_vector


def create_matrix(x_vec=(1, 0, 0), y_vec=(0, 1, 0), z_vec=(0, 0, 1), pos=(0, 0, 0)):
    '''
    Takes 4 vectors as inputs to create an MMatrix
    Returns:
        MMatrix: matrix from given vectors
    '''
    matrix = om.MMatrix((x_vec[0], x_vec[1], x_vec[2], 0,
                        y_vec[0], y_vec[1], y_vec[2], 0,
                        z_vec[0], z_vec[1], z_vec[2], 0,
                        pos[0], pos[1], pos[2], 1))
    return matrix


def matrix_to_vectors(matrix):
    '''Deconstructs a given matrix into 4 vectors
    Args:
        matrix: matrix to be deconstructed
    Return:
        four vectors of the transformation matrix
    '''
    vec1 = om.MVector(matrix[0], matrix[1], matrix[2])
    vec2 = om.MVector(matrix[4], matrix[5], matrix[6])
    vec3 = om.MVector(matrix[8], matrix[9], matrix[10])
    vec4 = om.MVector(matrix[12], matrix[13], matrix[14])

    return [vec1, vec2, vec3, vec4]
