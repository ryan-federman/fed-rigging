import maya.cmds as cmds
import maya.api.OpenMaya as om
import copy as copy

import custom_utils.maths as maths
import custom_utils.component as comp
import custom_utils.generic as gen
import custom_utils.control_shapes as cs
import custom_utils.shape as shape
import custom_utils.attribute as attr
import create.nodes as nodes
import create.nurbs as nrb


def chain_on_mesh(vertices, base_name, start_dag, joint_number=15, aim='x', up='y', suffix='CHAIN', align_to_joint=None):
    '''
    Aligns a chain along a set of vertices on a mesh
    Args:
        vertices list[str]: list of vertices to lie chain on
        base_name (str): base name of chain
        start_dag (str): name of dag to take initial start position of
        aim (str): aim vector of chain
        up (str): up vector to align to normal
        align_to_joint (float): set a float from 0 to 1 to determine how far down the chain it will take the up vector
                                to align the rest of the chain, if None will align all joints along the closest normal
                                on mesh
    '''

    vert_vecs = dict()

    # Get start position of chain
    start_pos = cmds.xform(start_dag, t=True, ws=True, q=True)
    start_pos = om.MVector(start_pos[0], start_pos[1], start_pos[2])

    # Record all positions of verts
    for vtx in vertices:
        pos = cmds.xform(vtx, t=True, ws=True, q=True)
        vert_vecs[vtx] = om.MVector(pos[0], pos[1], pos[2])

    # Find closest vertex to starting locator
    closest_vtx = ''
    closest_distance = None
    for vtx, pos in vert_vecs.items():
        distance = (start_pos - pos).length()
        if not closest_distance:
            closest_distance = distance
            closest_vtx = vtx
        else:
            if distance < closest_distance:
                closest_distance = distance
                closest_vtx = vtx

    cmds.select(clear=True)

    # Create first joint at vtx
    current_jnt = cmds.joint(name='{}_0_{}'.format(base_name, suffix))
    cmds.xform(current_jnt, t=vert_vecs[closest_vtx], ws=True)

    current_pos = vert_vecs[closest_vtx]
    current_vtx = closest_vtx
    del vert_vecs[closest_vtx]
    closest_vtx = None
    closest_distance = None
    closest_pos = None
    # Go through remainder of verts and place the next joint based on the next closest vtx to the current one
    for i in range(len(vert_vecs)):
        for vtx, pos in vert_vecs.items():
            distance = (current_pos - pos).length()
            if not closest_vtx:
                closest_vtx = vtx
                closest_distance = distance
                closest_pos = pos
            else:
                if distance < closest_distance:
                    closest_vtx = vtx
                    closest_distance = distance
                    closest_pos = pos

        # Create joint and parent it to previous joint
        next_jnt = cmds.joint(name='{}_{}_{}'.format(base_name, i + 1, suffix))
        cmds.xform(next_jnt, t=closest_pos, ws=True)

        # Reset attributes for next loop
        del vert_vecs[closest_vtx]
        current_jnt = next_jnt
        current_vtx = closest_vtx
        current_pos = closest_pos
        closest_vtx = None
        closest_distance = None
        closest_pos = None

    # Create chain with equidistant joints based on specified joint number
    chain = cmds.ls(base_name + '*_{}'.format(suffix))
    chain = reposition_dags_to_curve(chain, num_new_dags=joint_number)

    # Align all joints based on the up vector of the geometry
    dag = DagRelationships()
    mesh = vertices[0].split('.vtx')[0]
    dag.list_dags(chain)
    dag.unparent()

    for i, each in enumerate(chain):
        # Project joint down to mesh
        dags_to_mesh([each], mesh, rotate=True, position=False, aim='z', up='y', up_vector=(0, 1, 0))

        # Get norm vec and compare it to the previous aim vec
        if i == 0:
            mtx = cmds.xform(each, matrix=True, q=True)
            vec = maths.axis_in_matrix(mtx, axis='+z', add_pos=False)
            old_up = vec
        else:
            mtx = cmds.xform(each, matrix=True, q=True)
            vec = maths.axis_in_matrix(mtx, axis='+z', add_pos=False)
            dot = vec * old_up
            if dot < 0:
                dags_to_mesh([each], mesh, rotate=True, position=False, aim='-z', up='y',
                                   up_vector=(0, 1, 0))
                vec *= -1

            old_up = vec
    aim_at_next(chain, up=up, aim=aim, up_vector='z', rotate_last=True)

    # If flag is called then align all joints to the vector of the given joint down the chain
    if align_to_joint:
        jnt = chain[int(len(chain) * align_to_joint)]
        mtx = cmds.xform(jnt, matrix=True, ws=True, q=True)
        if '-' not in up:
            up_axis = '+' + up
        else:
            up_axis = up
        up_vector = maths.axis_in_matrix(mtx, up_axis, add_pos=False)
        aim_at_next(chain, up=up, aim=aim, up_vector=up_vector)
    dag.parent()




class DagWorld:
    def __init__(self):
        self.dags = None
        self.children = dict()
        self.parents = dict()

    def unparent(self):
        sel = cmds.ls(sl=True)
        if sel:
            self.dags = sel
            cmds.select(clear=True)
            for each in self.dags:
                self.children[each] = cmds.listRelatives(each, children=True)
                self.parents[each] = cmds.listRelatives(each, parent=True)
                if self.parents[each]:
                    self.parents[each] = self.parents[each][0]

        for each in self.dags:
            try:
                cmds.parent(each, world=True)
            except RuntimeError:
                pass

            if self.children[each]:
                for child in self.children[each]:
                    cmds.parent(child, world=True)

    def reparent(self):
        for dag in self.dags:
            children = self.children[dag]
            parent = self.parents[dag]
            print children
            print parent

            if children:
                for each in children:
                    print each
                    print dag
                    try:
                        cmds.parent(each, dag)
                    except RuntimeError:
                        pass
            if parent:
                try:
                    cmds.parent(dag, parent)
                except RuntimeError:
                    pass


def give_up_locs(joints):
    for each in joints:
        loc1 = cmds.spaceLocator(name='{}_0_UPLOC'.format(each))[0]
        loc2 = cmds.spaceLocator(name='{}_1_UPLOC'.format(each))[0]
        cmds.parent(loc2, loc1)
        cmds.parent(loc1, each)

        cmds.setAttr(loc1 + '.t', 0, 0, 0)
        cmds.setAttr(loc1 + '.r', 0, 0, 0)
        cmds.setAttr(loc2 + '.t', 0, 3, 0)

        cmds.parent(loc1, world=True)



def orient_chains(joints, up='y', aim='x', up_vector='locs'):
    '''
    Orients chains based on the given first joints
    Args:
        joints list[(str)]: list of first joints of chains
        up (str): axis to align to up vector
        aim (str): axis to align to aim vector
        up_vector (str): what to use for determining up vector, 'locs' will use the up locator in the scene
    '''
    for each in joints:
        base = each.split('_0_JNT')[0]
        joints = cmds.ls(base + '_*_JNT')
        if up_vector == 'locs':
            up_vec = ['{}_0_UPLOC'.format(each), '{}_1_UPLOC'.format(each)]
        else:
            up_vec = up_vector
        aim_at_next(joints, up=up, aim=aim, up_vector=up_vec, rotate_last=True)


def switch_axis(dag, sample_axis='x', sample_up_axis='y', target_axis='z', target_up_axis='y'):
    '''
    Re-orients a single dag
    Args:
        dag(str): dag to re orient
        sample_axis(str): axis to take info from, can be x, -x, y, -y, z, -z
        sample_up_axis(str): up axis to take info from, can be x, -x, y, -y, z, -z
        target_axis(str): axis to change to sample axis, can be x, y, z
        target_up_axis(str): axis to change to sample up axis, can be x, y, z
    '''
    # Check to make sure that the target axiss don't have a - in them
    for axis_check in [target_axis, target_up_axis]:
        if '-' in axis_check:
            RuntimeError('Targets can not have a -')
    # Get matrix and deconstruct it
    mtx = cmds.xform(dag, matrix=True, q=True)
    x, y, z, pos = maths.matrix_to_vectors(mtx)
    trans_dict = {'x': x,
                  '-x': x * -1,
                  'y': y,
                  '-y': y * -1,
                  'z': z,
                  '-z': z * -1
                  }
    new_mtx = maths.fixed_matrix(pos, trans_dict[target_axis], trans_dict[target_up_axis], aim=sample_axis, up=sample_up_axis)
    cmds.xform(dag, matrix=new_mtx)


class DagRelationships:
    '''
    Class for parenting dags underneath one another
    '''

    def __init__(self, dags=list(), starting_index=0):
        self.basename = None
        self.dags = dags
        self.index = starting_index

        self.dag_parents = dict()

    def list_dags(self, dags):
        '''
        Overrites Dag list and makes current index the length of the list
        '''

        self.dags = dags
        self.index = len(dags) - 1

        # Clear old parents list
        self.dag_parents = dict()

    def dags_from_curves(self, dag_amount=4, dag_type='joint', up_vector='curve'):
        crvs = cmds.ls('*_placementCRV')

        for crv in crvs:
            basename = crv.split('_placementCRV')[0]
            dags = []
            cmds.select(clear=True)
            # Check to see if dag exists or not
            if not cmds.ls('{}_0_JNT'.format(basename)):
                for i in range(dag_amount):
                    if dag_type == 'locator':
                        dag = cmds.spaceLocator()[0]
                    elif dag_type == 'joint':
                        dag = cmds.joint(name='{}_{}_JNT'.format(basename, i))
                    elif dag_type == 'transform':
                        dag = cmds.createNode('transform')

                    dags.append(dag)

                    # Get up vector
                    if up_vector == 'curve':
                        vec1 = comp.get_MVector('{}_upvec_CTL'.format(basename))
                        vec2 = comp.get_MVector('{}_0_crvLOC'.format(basename))
                        up_vec = vec1 - vec2
                    elif up_vector == 'world':
                        up_vec = om.MVector(0, 1, 0)
                    else:
                        up_vec = up_vector

                dags_to_curve(dags, crv, up_vector=up_vec)

    def create_locator(self, objs, name, distance=1.0, scale=1.0):
        '''
        Creates special locator to reference positions on a mesh for creating a curve
        Args:
            objs list[(str)]: list of objects to create a locator in the middle of
            name (str): basename of locators, indexing happens automatically so do not use special characters like *
            distance (float): starting distance of reference points for locator
            scale (float): size of created locators and controls
        '''
        loc_grp = cmds.ls('loc_placer_grp')
        if not loc_grp:
            cmds.createNode('transform', name='loc_placer_grp')
            attr.add_enum('loc_placer_grp', 'basenames', [name])
        else:
            enums = cmds.attributeQuery('basenames', n='loc_placer_grp', listEnum=True)[0].split(':')
            if name not in enums:
                cmds.deleteAttr('loc_placer_grp.basenames')
                enums = enums + [name]
                attr.add_enum('loc_placer_grp', 'basenames', enums)

        loc_name = '{}_{}'.format(name, self.index)
        loc = dag_to_mid(objs, dag_type='locator', name='{}_posLOC'.format(loc_name))

        # Check to see type of object
        obj_type = cmds.nodeType(objs[0])
        if obj_type == 'transform':
            child = cmds.listRelatives(objs[0])
            child_type = cmds.nodeType(child)
            if child_type == 'mesh':
                obj_type = 'mesh'

        # If selection is part of a mesh orient the locator to that mesh
        if obj_type == 'mesh':
            mesh = objs[0].split('.')[0]
            dags_to_mesh([loc], mesh, rotate=True, position=False, aim='y', up='x')

            # Make sure up vector is pointing in the same way as the last loc created
            if self.dags:
                last_dag = cmds.xform(self.dags[-1], matrix=True, ws=True, q=True)
                current_dag = cmds.xform(loc, matrix=True, ws=True, q=True)
                last_up = maths.matrix_to_vectors(last_dag)[1]
                current_up = maths.matrix_to_vectors(current_dag)[1]
                dot = last_up * current_up
                if dot < 0:
                    dags_to_mesh([loc], mesh, rotate=True, position=False, aim='-y', up='x')

                # If you changed attributes on the previous dag from the default then update this dag as well
                last_dist = cmds.getAttr(self.dags[-1] + '.upNeg')
                if last_dist != distance:
                    distance = last_dist

        attr.add_float(loc, 'upPos', max=500, default=0)
        attr.add_float(loc, 'upNeg', max=500, min=0.001, default=distance)
        attr.lock_attributes(loc, ['sx', 'sy', 'sz'])
        attr.hide_attributes(loc, ['sx', 'sy', 'sz'])

        # Create indicators for where the curve will be created
        up_zero = cmds.createNode('transform', name='{}_upPos_ZERO'.format(loc_name))
        down_zero = cmds.createNode('transform', name='{}_upNeg_ZERO'.format(loc_name))
        up = cs.create_nurbscurve('{}_upPos'.format(loc_name), 'pyramid')
        down = cs.create_nurbscurve('{}_upNeg'.format(loc_name), 'pyramid')
        upLine = cs.create_nurbscurve('{}_upPosLine'.format(loc_name), 'line')
        downLine = cs.create_nurbscurve('{}_upNegLine'.format(loc_name), 'line')
        up_dcpm = nodes.Node(up, 'decomposeMatrix')
        down_dcpm = nodes.Node(down, 'decomposeMatrix')
        down_mult = nodes.Node(down, 'multiplyDivide')

        # Manipulate control shapes
        shape.change_color(up, 'yellow')
        shape.change_color(down, 'brown')
        shape.change_color(upLine, 'green')
        shape.change_color(downLine, 'green')
        shape.modify_shape(up, scale=(1, -1, 1))
        shape.modify_shape(up, translate=(0, 1.5, 0))
        shape.modify_shape(down, translate=(0, -1.5, 0))
        shape.modify_shape(up, scale=(scale, scale, scale))
        shape.modify_shape(down, scale=(scale, scale, scale))
        cmds.setAttr(loc + '.localScale', scale, scale, scale)

        cmds.setAttr(upLine + '.inheritsTransform', 0)
        cmds.setAttr(downLine + '.inheritsTransform', 0)
        cmds.parent(up, up_zero)
        cmds.parent(down, down_zero)
        cmds.parent(down_zero, loc)
        cmds.parent(up_zero, loc)
        cmds.parent(upLine, loc)
        cmds.parent(downLine, loc)

        cmds.setAttr(up_zero + '.t', 0, 0, 0)
        cmds.setAttr(up_zero + '.r', 0, 0, 0)
        cmds.setAttr(down_zero + '.t', 0, 0, 0)
        cmds.setAttr(down_zero + '.r', 0, 0, 0)
        cmds.setAttr(upLine + '.t', 0, 0, 0)
        cmds.setAttr(upLine + '.r', 0, 0, 0)
        cmds.setAttr(downLine + '.t', 0, 0, 0)
        cmds.setAttr(downLine + '.r', 0, 0, 0)

        cmds.connectAttr(loc + '.upPos', up_zero + '.ty')
        down_mult.connect(input1X=loc + '.upNeg',
                          input2X=-1,
                          outputX=down_zero + '.ty')

        cmds.connectAttr(loc + '.worldPosition[0]', upLine + '.controlPoints[0]')
        cmds.connectAttr(loc + '.worldPosition[0]', downLine + '.controlPoints[0]')

        up_dcpm.connect(inputMatrix=up + '.worldMatrix[0]',
                        outputTranslate=upLine + '.controlPoints[1]')
        down_dcpm.connect(inputMatrix=down + '.worldMatrix[0]',
                          outputTranslate=downLine + '.controlPoints[1]')

        cmds.parent(loc, 'loc_placer_grp')

        return loc

    def create_curves(self, mesh=None, created='all', size=0.5):
        '''
        Creates curves based on two points on a mesh
        Args:
            mesh (str): if given then will reference points on mesh to put curve between, if not it will go between the
                        two given points by the locator
            created (str): if 'all' will create for all locators in the scene, can also give this flag a list of the
                           specific basenames you want to create
            size (float): size of the controls and locators created
        '''

        if created == 'all':
            basenames = cmds.attributeQuery('basenames', n='loc_placer_grp', listEnum=True)[0].split(':')
        else:
            basenames = created

        for base in basenames:
            # If curve exists then skip
            curve_check = cmds.ls(base + '_placementCRV')
            if not curve_check:
                # Create curve group if it doesn't exist
                crv_grp = cmds.ls('curve_placer_grp')
                if not crv_grp:
                    cmds.createNode('transform', name='curve_placer_grp')
                    attr.add_enum('curve_placer_grp', 'basenames', [base])
                else:
                    enums = cmds.attributeQuery('basenames', n='curve_placer_grp', listEnum=True)[0].split(':')
                    if base not in enums:
                        cmds.deleteAttr('curve_placer_grp.basenames')
                        enums = enums + [base]
                        attr.add_enum('curve_placer_grp', 'basenames', enums)

                new_locs = []
                locs = cmds.ls('{}_*_posLOC'.format(base))
                for i, loc in enumerate(locs):
                    up_ctl = '{}_{}_upPos'.format(base, i)
                    down_ctl = '{}_{}_upNeg'.format(base, i)
                    up_pos = comp.get_MVector(up_ctl)
                    down_pos = comp.get_MVector(down_ctl)

                    if mesh:
                        up_pos = comp.closest_point_on_mesh(mesh, up_pos)[0]
                        down_pos = comp.closest_point_on_mesh(mesh, down_pos)[0]

                    vec_between = up_pos - down_pos
                    half_vec = down_pos + (vec_between/2)

                    new_loc = cmds.spaceLocator(name='{}_{}_crvLOC'.format(base, i))[0]
                    cmds.setAttr(new_loc + '.localScale', size, size, size)
                    cmds.xform(new_loc, t=(half_vec[0], half_vec[1], half_vec[2]), ws=True)
                    new_locs.append(new_loc)
                    cmds.select(clear=True)

                crv = nrb.curve_from_dags(base + '_placementCRV', new_locs)
                for each in new_locs:
                    cmds.parent(each, crv)
                cmds.parent(crv, 'curve_placer_grp')

                # Create up vector for curve
                up_zero = cmds.createNode('transform', name='{}_upVec_ZERO'.format(base))
                up = cs.create_nurbscurve('{}_upvec_CTL'.format(base), 'pyramid')
                upLine = cs.create_nurbscurve('{}_upvecLine'.format(crv), 'line')
                up_dcpm = nodes.Node(up, 'decomposeMatrix')

                # Manipulate control shapes
                shape.change_color(up, 'yellow')
                shape.change_color(upLine, 'green')
                shape.modify_shape(up, scale=(1, -1, 1))
                shape.modify_shape(up, translate=(0, 1.5, 0))
                shape.modify_shape(up, scale=(size, size, size))

                cmds.setAttr(upLine + '.inheritsTransform', 0)
                cmds.parent(up, up_zero)
                cmds.parent(up_zero, new_locs[0])
                cmds.parent(upLine, up_zero)

                cmds.setAttr(up_zero + '.t', 0, 0, 0)
                cmds.setAttr(up_zero + '.r', 0, 0, 0)
                cmds.setAttr(upLine + '.t', 0, 0, 0)
                cmds.setAttr(upLine + '.r', 0, 0, 0)

                cmds.connectAttr(new_locs[0] + '.worldPosition[0]', upLine + '.controlPoints[0]')

                up_dcpm.connect(inputMatrix=up + '.worldMatrix[0]',
                                outputTranslate=upLine + '.controlPoints[1]')

                # Get base orientation of loc
                old_up_ctl = '{}_0_upPos'.format(base)
                up_pos = comp.get_MVector(old_up_ctl)
                base_pos = comp.get_MVector(new_locs[0])

                normal = up_pos - base_pos

                ty = normal.length()

                mtx = comp.matrix_on_curve(normal, crv)
                cmds.xform(up_zero, matrix=mtx, ws=True)
                cmds.setAttr(up + '.ty', ty)

    def add_dag(self, objs, dag_type, curve=False, curve_num=None, name=None, prefix=None, suffix=None, number=True, distance=1, scale=1):
        '''
        Args:
            objs: objects being used to position dags, if curve give only the curve as a string
            dag_type: type of dag to be created
            curve (str): curve to build dags on
            curve_num (int): amount of dags to build on curve
            name (str): basename of dag, use a * where you want your indexing to occur in the name
            prefix (str):  prefix of dags
            suffix (str): suffix of dags
            number (bool): adds a number to every dag
        '''
        if curve:
            cmds.select(clear=True)
            if self.basename:
                if name not in self.basename:
                    self.clear()
                    self.basename = name
            else:
                self.basename = name

            # Check to see if name exists, if so error
            check = cmds.ls(name)
            if check:
                raise ValueError('Name already exists within scene')

            for i in range(curve_num):
                if '*' in name:
                    index = name.find('*')
                    count = name.count('*')
                    new = name[:index] + gen.create_int_string(self.index, leading=count-1) + name[index:]
                    new_name = new.replace('*', '')
                else:
                    new_name = '{}_{}'.format(name, self.index)
                if prefix:
                    new_name = '{}_{}'.format(prefix, name)
                if suffix:
                    new_name = '{}_{}'.format(name, suffix)

                if dag_type == 'locator':
                    dag = cmds.spaceLocator()[0]
                elif dag_type == 'joint':
                    dag = cmds.joint()
                elif dag_type == 'transform':
                    dag = cmds.createNode('transform')
                cmds.rename(dag, new_name)
                self.dags.append(new_name)
                self.index_up()

            dags_to_curve(self.dags, objs, rotation=False)

        elif dag_type == 'locator':
            if name:
                # Check to see if name is different than any other name in the dag list, if so clear list and start new one
                if self.basename:
                    if name not in self.basename:
                        self.clear()
                        self.basename = name
                else:
                    self.basename = name

                # Check to see if name exists, if so error
                check = cmds.ls(name)
                if check:
                    raise ValueError('Name already exists within scene')

            dag = self.create_locator(objs, name=name, distance=distance, scale=scale)

            if self.index > 0:
                cmds.parent(dag, self.dags[-1])

            self.dags.append(dag)
            self.index_up()

        else:
            if name:
                # Check to see if name is different than any other name in the dag list, if so clear list and start new one
                if self.basename:
                    if name not in self.basename:
                        self.clear()
                        self.basename = name
                else:
                    self.basename = name
                if number:
                    if '*' in name:
                        index = name.find('*')
                        count = name.count('*')
                        new = name[:index] + gen.create_int_string(self.index, leading=count-1) + name[index:]
                        name = new.replace('*', '')
                    else:
                        name = '{}_{}'.format(name, self.index)
                if prefix:
                    name = '{}_{}'.format(prefix, name)
                if suffix:
                    name = '{}_{}'.format(name, suffix)

                # Check to see if name exists, if so error
                check = cmds.ls(name)
                if check:
                    raise ValueError('Name already exists within scene')

            cmds.select(clear=True)

            # Create dag
            dag = dag_to_mid(objs, dag_type=dag_type, name=name)

            if self.index > 0:
                cmds.parent(dag, self.dags[-1])

            self.dags.append(dag)
            self.index_up()

    def current_index(self):
        print self.index

    def index_up(self):
        self.index += 1

    def index_down(self):
        self.index -= 1

    def clear(self):
        self.dags = []
        self.index = 0

    def rotate_dags(self, up='y', aim='x', up_vector='y', rotate_last=True):
        aim_at_next(self.dags, up=up, aim=aim, up_vector=up_vector, rotate_last=rotate_last)

    def unparent(self):
        '''
        Unparents all dags
        '''

        for dag in self.dags:
            parent = cmds.listRelatives(dag, parent=True)
            if parent:
                self.dag_parents[dag] = parent[0]
                cmds.parent(dag, world=True)
            else:
                self.dag_parents[dag] = None

    def parent(self):
        '''
        Reparents all dags to their parents
        '''

        for dag, parent in self.dag_parents.items():
            if parent:
                cmds.parent(dag, parent)

    def delete_last(self):
        '''
        Deletes last dag in the list
        '''

        dag = self.dags[-1]
        cmds.delete(dag)
        self.dags.remove(dag)
        if self.dag_parents:
            self.dag_parents.pop(dag)

        self.index_down()


def dag_to_mid(objs, dag_type='locator', name=None):
    '''
    Move a placement locator to the middle of the given objects
    Args:
        objs list[(str)]: list of objects to put locator in middle of
    '''

    vecs = []
    for obj in objs:
        pos = cmds.xform(obj, t=True, ws=True, q=True)
        vec = om.MVector(pos[0], pos[1], pos[2])
        vecs.append(vec)

    final_pos = maths.average_vectors(vecs)

    if dag_type == 'locator':
        dag = cmds.spaceLocator()[0]
    elif dag_type == 'joint':
        dag = cmds.joint()
    elif dag_type == 'transform':
        dag = cmds.createNode('transform')
    cmds.xform(dag, t=final_pos, ws=True)

    if name:
        cmds.rename(dag, name)
        dag = name

    return dag


def dags_to_mesh(dags, mesh, rotate=False, position=True, aim='x', up='y', up_vector=(0, 1, 0)):
    '''
    Moves dags to the closest point on a mesh
    Args:
        dags list[str]: dags to move to mesh
        mesh (str): mesh to move dags to
        rotate (bool): if True rotate dag to nearest face
    '''

    for i, dag in enumerate(dags):
        pos = cmds.xform(dag, t=True, ws=True, q=True)
        pos = om.MVector(pos[0], pos[1], pos[2])

        point, face = comp.closest_point_on_mesh(mesh, pos)
        if rotate:
            verts = comp.face_to_vertex([face])

            # get vectors for plane calculations
            root = cmds.xform(verts[0], t=True, ws=True, q=True)
            mid = cmds.xform(verts[1], t=True, ws=True, q=True)
            end = cmds.xform(verts[2], t=True, ws=True, q=True)

            root = om.MVector(root[0], root[1], root[2])
            mid = om.MVector(mid[0], mid[1], mid[2])
            end = om.MVector(end[0], end[1], end[2])

            # Get aim and up vectors
            norm = maths.get_normal_of_plane(root, mid, end)
            up_vector = om.MVector(up_vector[0], up_vector[1], up_vector[2])

            # Create matrix
            dag_mtx = maths.fixed_matrix(pos, norm, up_vector, aim=aim, up=up)

            cmds.xform(dag, matrix=dag_mtx)

        if position:
            cmds.xform(dag, t=point, ws=True)


def rot_to_plane(dags, aim='x', up='y', last_rot=False):
    '''
    Sets rotation of each dag to the plane
    Args:
        dags list[(str)]: dags to change rotation of
        last_rot (bool): if False leaves last dag alone
    '''

    # get vectors for plane calculations
    root = cmds.xform(dags[0], t=True, ws=True, q=True)
    mid = cmds.xform(dags[1], t=True, ws=True, q=True)
    end = cmds.xform(dags[2], t=True, ws=True, q=True)

    root = om.MVector(root[0], root[1], root[2])
    mid = om.MVector(mid[0], mid[1], mid[2])
    end = om.MVector(end[0], end[1], end[2])

    dag_vecs = [root, mid, end]

    norm = maths.get_normal_of_plane(root, mid, end)

    mtxs = []

    # rotate dags to plane
    for i, dag in enumerate(dags):
        if i < 2:
            next_vec = dag_vecs[i + 1]
            vec = dag_vecs[i]
        else:
            next_vec = dag_vecs[i]
            vec = dag_vecs[i - 1]

        if i < 2 or (i == 2 and last_rot):
            # get aim vector
            aim_vec = (next_vec - vec).normal()

            # create matrix
            dag_mtx = maths.fixed_matrix(dag_vecs[i], aim_vec, norm, aim=aim, up=up)
            mtxs.append(dag_mtx)
        else:
            mtx = cmds.xform(dag, matrix=True, ws=True, q=True)
            rot = cmds.xform(dag, ro=True, ws=True, q=True)
            mtxs.append(mtx)

    for i, mtx in enumerate(mtxs):
        cmds.setAttr(dags[i] + '.jo', 0, 0, 0)

        # apply matrix to dag
        cmds.xform(dags[i], matrix=mtx, ws=True)

    if not last_rot:
        cmds.xform(dags[2], ro=rot, ws=True)


def aim_at_next(dags, up='y', aim='x', up_vector='y', rotate_last=True):
    '''
    Aims dag at the next dag in the list
    Args:
        dags list[(str)]: dags to change rotation of
        up (str): up vector of final matrix can be x, y, z
        aim (str): aim vector of final matrix, can be x, y or z
        up_vector (str) or (tuple): where to take initial up vector from for orientating matrix, can be x, y, z, world,
                                    a tuple containing the vector, any object in the scene, or a list of two objects to get a vector from
        rotate_last (bool): if True rotates the last dag to aim backwards at the dag before it
    '''
    cmds.select(clear=True)

    # Store transformation info of dags
    trans_info = {}
    for dag in dags:
        trans_info[dag] = cmds.xform(dag, matrix=True, ws=True, q=True)

    mtxs = []
    # rotate dags
    for i, dag in enumerate(dags):
        if dag != dags[-1]:
            next_dag = dags[i + 1]
            x_vec, y_vec, z_vec, pos = maths.matrix_to_vectors(trans_info[dag])
            next_pos = maths.matrix_to_vectors(trans_info[next_dag])[3]

            # Determine where the up vector is being referenced from
            if isinstance(up_vector, om.MVector):
                old_up = up_vector
            elif up_vector == 'x':
                old_up = x_vec
            elif up_vector == 'y':
                old_up = y_vec
            elif up_vector == 'z':
                old_up = z_vec
            elif up_vector == 'world':
                old_up = om.MVector(0, 1, 0)
            elif isinstance(up_vector, tuple):
                old_up = om.MVector(up_vector[0], up_vector[1], up_vector[2])
            elif isinstance(up_vector, basestring):
                obj = cmds.ls(up_vector)
                if obj:
                    obj_pos = cmds.xform(obj, t=True, ws=True, q=True)
                    obj_vec = om.MVector(obj_pos[0], obj_pos[1], obj_pos[2])
                    old_up = (pos - obj_vec) * -1
                else:
                    cmds.error('Put in a valid object')
            elif isinstance(up_vector, list):
                pos1 = cmds.xform(up_vector[0], t=True, ws=True, q=True)
                pos2 = cmds.xform(up_vector[1], t=True, ws=True, q=True)
                vec1 = om.MVector(pos1[0], pos1[1], pos1[2])
                vec2 = om.MVector(pos2[0], pos2[1], pos2[2])
                old_up = (vec2 - vec1).normalize()

            else:
                cmds.error('Put in a valid up vector')

            # get aim vector
            aim_vec = (next_pos - pos).normal()

            # create matrix
            dag_mtx = maths.fixed_matrix(pos, aim_vec, old_up, aim=aim, up=up)
            mtxs.append(dag_mtx)
        else:
            mtx = cmds.xform(dag, matrix=True, ws=True, q=True)
            mtxs.append(mtx)

    for i, mtx in enumerate(mtxs):
        if cmds.nodeType(dags[i]) == 'joint':
            cmds.setAttr(dags[i] + '.jo', 0, 0, 0)

        # apply matrix to dag
        cmds.xform(dags[i], matrix=mtx, ws=True)

    if rotate_last:
        cmds.setAttr(dags[-1] + '.r', 0, 0, 0)
        cmds.setAttr(dags[-1] + '.jo', 0, 0, 0)


def reset_joint_orient(dags):
    '''
    Resets joint orient of given joints
    Args:
        dags list[(str)]: joints in which to reset the orient of
    '''

    parent_dict = dict()
    # save parent and unparent
    for dag in dags:
        par = cmds.listRelatives(dag, parent=True)
        if par:
            parent_dict[dag] = par
            cmds.parent(dag, world=True)

    # If child exists to last dag, unparent it
    child = cmds.listRelatives(dags[-1], children=True)
    if child:
        parent_dict[child[0]] = dags[-1]
        cmds.parent(child, world=True)

    # If dag is a joint reset the joint orient
    for i, dag in enumerate(dags):
        obj_type = cmds.nodeType(dag)
        if obj_type == 'joint':
            rot = cmds.xform(dag, ro=True, ws=True, q=True)
            cmds.setAttr(dag + '.jo', 0, 0, 0)

            # Rotate joints
            cmds.xform(dag, ro=rot, ws=True)

    # Reparent to parents
    for child, parent in parent_dict.items():
        cmds.parent(child, parent)


def orient_dags(dags, rotations=[(0, 0, 0)]):
    '''
    Orients dags while keeping position down a hierarchy
    Args:
        dags list[(str)]: list of dags to orient
        rotations list[(tuple)]: list of orientations for dags, if one rotation is given then it will apply to all dags
    '''
    parent_dict = dict()
    # save parent and unparent
    for dag in dags:
        par = cmds.listRelatives(dag, parent=True)
        if par:
            parent_dict[dag] = par
            cmds.parent(dag, world=True)

    # If child exists to last dag, unparent it
    child = cmds.listRelatives(dags[-1], children=True)
    if child:
        parent_dict[child[0]] = dags[-1]
        cmds.parent(child, world=True)

    # Create rotation list for dags
    dag_rotations = list()
    if len(rotations) == 1:
        for i in range(len(dags)):
            dag_rotations.append(rotations[0])
    else:
        if len(rotations) != len(dags):
            raise ValueError('Length of rotations list does not equal length of dag list')
        dag_rotations = rotations

    # If dag is a joint reset the joint orient
    for i, dag in enumerate(dags):
        obj_type = cmds.nodeType(dag)
        if obj_type == 'joint':
            cmds.setAttr(dag + '.jo', 0, 0, 0)

        # Rotate joints
        cmds.xform(dag, ro=dag_rotations[i], ws=True)

    # Reparent to parents
    for child, parent in parent_dict.items():
        cmds.parent(child, parent)


def reposition_dags_to_curve(dags, num_new_dags, degree=2, rotation=True, rotation_type='next', up='y', aim='x', up_vector='y', suffix='CHAIN'):
    '''
    Creates a curve to reposition and change the amount of dags in a chain
    Args:
        dags list[(str)]: list of dags to reposition/use for curve
        num_new_dags (int): number of new dags to create
        degree (int): degree of curve to reposition dags
        rotation (bool): indicates whether the function will orient the dags
        rotation_type (str): indicates whether the function will use the next dag for it's aim vector or the curve, 'next' or 'curve'
        up (str): axis to use as up vector
        aim (str): axis to use as aim vector
        up_vector (str): where to take initial up vector from for orientating matrix, can be x, y, z, world or a tuple containing the vector
    Return:
        jnts list[(str)]: list of new joints
    '''

    basename = gen.remove_suffix(dags[0])
    basename = gen.remove_suffix(basename)

    # Create curve for repositioning
    crv = nrb.curve_from_dags(dags[0] + '_TEMPCRV', dags, attach_dags=False, rebuild=True, degree=degree)
    cmds.select(clear=True)

    # Create new dags
    cmds.delete(dags)
    jnts = []
    for i in range(num_new_dags):
        jnt = cmds.joint(name='{}_{}_{}'.format(basename, i, suffix))
        jnts.append(jnt)

    dags_to_curve(jnts, crv, rotation=rotation, rotation_type=rotation_type, up=up, aim=aim, up_vector=up_vector)

    cmds.delete(crv)

    return jnts


def dags_to_curve(dags, curve, rotation=True, rotation_type='next', up='y', aim='x', up_vector='y'):
    '''
    Positions dags evenly along a curve
    Args:
        dags list[str]: list of dags to position
        curve (str): curve to move dags to
        rotation (bool): indicates whether the function will orient the dags
        rotation_type (str): indicates whether the function will use the next dag for it's aim vector or the curve, 'next' or 'curve'
        up (str): axis to use as up vector
        aim (str): axis to use as aim vector
        up_vector (str): where to take initial up vector from for orientating matrix, can be x, y, z, world or a tuple containing the vector
    '''
    cmds.undoInfo(openChunk=True)

    num_dags = len(dags)

    # Get main curve info
    sel = om.MSelectionList()
    sel.add(curve)

    crv = om.MFnNurbsCurve()
    crv.setObject(sel.getDagPath(0))

    curve_length = crv.length()

    length_increment = curve_length/float(num_dags-1)
    length = 0.001

    parent_dict = {}

    # position dags to get accurate aim vectors
    for x in range(num_dags):
        # save parent and unparent
        par = cmds.listRelatives(dags[x], parent=True)
        if par:
            parent_dict[dags[x]] = par
            cmds.parent(dags[x], world=True)

        if x != 0:
            length += length_increment
        par = crv.findParamFromLength(length)
        cmds.select(clear=True)

        # get position that dag will be put to
        pos = crv.getPointAtParam(par)

        # apply matrix to dag
        dag = dags[x]
        cmds.xform(dag, t=(pos[0], pos[1], pos[2]), ws=True)

    # Rotate based on given parameters
    if rotation:
        if rotation_type == 'next':
            aim_at_next(dags, up=up, aim=aim, up_vector=up_vector)
        elif rotation_type == 'curve':
            cmds.select(clear=True)

            # Store transformation info of dags
            trans_info = {}
            for dag in dags:
                trans_info[dag] = cmds.xform(dag, matrix=True, ws=True, q=True)

            mtxs = []
            # rotate dags
            for i, dag in enumerate(dags):
                if dag != dags[-1]:
                    x_vec, y_vec, z_vec, pos = maths.matrix_to_vectors(trans_info[dag])

                    # Get vector at nearest position on curve
                    point = om.MPoint(pos[0], pos[1], pos[2])
                    crv_point = crv.closestPoint(point)[0]
                    crv_point = om.MPoint(crv_point[0], crv_point[1], crv_point[2])
                    param = crv.getParamAtPoint(crv_point)

                    # Determine where the up vector is being referenced from
                    if up_vector == 'x':
                        old_up = x_vec
                    elif up_vector == 'y':
                        old_up = y_vec
                    elif up_vector == 'z':
                        old_up = z_vec
                    elif up_vector == 'world':
                        old_up = om.MVector(0, 1, 0)
                    elif isinstance(up_vector, tuple):
                        old_up = om.MVector(up_vector[0], up_vector[1], up_vector[2])

                    # get aim vector
                    aim_vec = crv.tangent(param)

                    # create matrix
                    dag_mtx = maths.fixed_matrix(pos, aim_vec, old_up, aim=aim, up=up)
                    mtxs.append(dag_mtx)
                else:
                    mtx = cmds.xform(dag, matrix=True, ws=True, q=True)
                    mtxs.append(mtx)

            for i, mtx in enumerate(mtxs):
                if cmds.nodeType(dags[i]) == 'joint':
                    cmds.setAttr(dags[i] + '.jo', 0, 0, 0)

                # apply matrix to dag
                cmds.xform(dags[i], matrix=mtx, ws=True)

    # reparent to parents
    for child, parent in parent_dict.items():
        cmds.parent(child, parent)

    cmds.setAttr(dags[-1] + '.jo', 0, 0, 0)

    cmds.select(dags)

    cmds.undoInfo(closeChunk=True)


def dags_on_vector(dags, objects, up_vec_type='first', aim='x', up='y'):
    # position vectors of target objects
    pos1 = cmds.xform(objects[0], t=True, ws=True, q=True)
    pos2 = cmds.xform(objects[1], t=True, ws=True, q=True)

    pos1 = om.MVector(pos1[0], pos1[1], pos1[2])
    pos2 = om.MVector(pos2[0], pos2[1], pos2[2])

    # calculate vector between the two positions
    main_vec = pos2 - pos1
    aim_vec = main_vec.normal()

    # up vectors of objects
    obj1_mtx = cmds.xform(objects[0], matrix=True, ws=True, q=True)
    obj2_mtx = cmds.xform(objects[1], matrix=True, ws=True, q=True)
    obj1_up = maths.matrix_to_vectors(obj1_mtx)[2]
    obj2_up = maths.matrix_to_vectors(obj2_mtx)[2]
    up_blend = obj2_up - obj1_up

    for x, dag in enumerate(dags):
        if dag != 'null':
            mult = float(x)/float(len(dags) - 1)
            add_vec = main_vec * mult
            pos_vec = pos1 + add_vec

            # get base up vec
            if up_vec_type == 'first':
                up_vec = obj1_up
            elif up_vec_type == 'second':
                up_vec = obj2_up
            else:
                up_vec = (obj1_up + (up_blend * mult)).normal()

            # get side vec
            side_vec = up_vec ^ aim_vec

            # final up vec
            up_vec = aim_vec ^ side_vec

            # create matrix
            dag_mtx = maths.fixed_matrix(pos_vec, aim_vec, up_vec, aim=aim, up=up)

            # apply matrix to dag
            dag = dags[x]
            cmds.xform(dag, matrix=dag_mtx, ws=True)


def snap_to_dag(source, target):
    '''Moves one dag object to the position and rotation of another

    Args:
         source (str): object to reference for transforms
         target (str): object to move to source object
    '''

    pos = cmds.xform(source, t=True, ws=True, q=True)
    rot = cmds.xform(source, ro=True, ws=True, q=True)

    cmds.xform(target, t=pos, ws=True)
    cmds.xform(target, ro=rot, ws=True)
