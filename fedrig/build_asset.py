class BuildAsset():
    def __init__(self):
        self.steps = {10: list(),
                      20: list(),
                      30: list(),
                      40: list(),
                      50: list()}

        self.modules = list()

    def build(self):
        for step, mods in self.steps.items():
            for mod in mods:
                mod.build()
