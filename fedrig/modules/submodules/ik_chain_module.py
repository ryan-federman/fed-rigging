import maya.cmds as cmds

import fedrig.layout.layout_objects as lyt
import fedrig.layout.layout_list as lyt_list
import fedrig.base_module as bm
import fedrig.module_attributes as mod_attr
import create.nodes as node

import custom_utils.control_shapes as cs
import custom_utils.maths as maths
import custom_utils.shape as shape
import custom_utils.constraint as con

reload(bm)


class IkChainModule(bm.BaseModule):
    module_type = 'IkChain'

    def __init__(self, name='Ct_IkChain', rig_type='proxy', step=20):
        # Re initialize scene layouts
        self.scene_layouts = lyt_list.LayoutList()

        super(IkChainModule, self).__init__(name=name, rig_type=rig_type, step=step)

    def build(self):
        # create ik joints for chain
        root_jnt = node.Node('{}_root_IK_JNT'.format(self.name), 'joint', list_add=self.module_nodes, suffix=False)
        mid_jnt = node.Node('{}_mid_IK_JNT'.format(self.name), 'joint', list_add=self.module_nodes, suffix=False)
        end_jnt = node.Node('{}_end_IK_JNT'.format(self.name), 'joint', list_add=self.module_nodes, suffix=False)
        self.parts += [root_jnt.node]

        # create bones for ik chain
        root_bone = node.Node('{}_root'.format(self.name), 'joint', list_add=self.module_nodes)
        mid_bone = node.Node('{}_mid'.format(self.name), 'joint', list_add=self.module_nodes)
        end_bone = node.Node('{}_end'.format(self.name), 'joint', list_add=self.module_nodes)

        root_ofs = node.Node(root_bone.node + '_OFS', 'transform', suffix=False, list_add=self.module_nodes)
        mid_ofs = node.Node(mid_bone.node + '_OFS', 'transform', suffix=False, list_add=self.module_nodes)
        end_ofs = node.Node(end_bone.node + '_OFS', 'transform', suffix=False, list_add=self.module_nodes)

        root_zero = node.Node(root_bone.node + '_ZRO', 'transform', suffix=False, list_add=self.module_nodes)
        mid_zero = node.Node(mid_bone.node + '_ZRO', 'transform', suffix=False, list_add=self.module_nodes)
        end_zero = node.Node(end_bone.node + '_ZRO', 'transform', suffix=False, list_add=self.module_nodes)
        self.bones += [root_zero.node, mid_zero.node, end_zero.node]

        cmds.select(clear=True)

        # parent bones correctly under offsets and zeros
        cmds.parent(root_bone.node, root_ofs.node)
        cmds.parent(root_ofs.node, root_zero.node)

        cmds.parent(mid_bone.node, mid_ofs.node)
        cmds.parent(mid_ofs.node, mid_zero.node)
        cmds.parent(mid_zero.node, root_bone.node)

        cmds.parent(end_bone.node, end_ofs.node)
        cmds.parent(end_ofs.node, end_zero.node)
        cmds.parent(end_zero.node, mid_bone.node)

        # put bones into correct positions
        root_mtx = self.get_matrix(0)
        mid_mtx = self.get_matrix(1)
        end_mtx = self.get_matrix(2)

        cmds.xform(root_jnt.node, matrix=root_mtx, ws=True)
        cmds.xform(mid_jnt.node, matrix=mid_mtx, ws=True)
        cmds.xform(end_jnt.node, matrix=end_mtx, ws=True)

        # Constrain bones to joints
        root_con = con.matrix_constraint(root_jnt.node, root_zero.node, maintain_offset=False)
        mid_con = con.matrix_constraint(mid_jnt.node, mid_zero.node, maintain_offset=False)
        end_con = con.matrix_constraint(end_jnt.node, end_zero.node, maintain_offset=False)
        self.custom_nodes += [root_con, mid_con, end_con]

        # Create ikhandle
        mid_jnt.connect(preferredAngleY=-90)
        ikh, effector = cmds.ikHandle(name='{}_{}_IKH'.format(self.side, self.name),
                                      startJoint=root_jnt.node,
                                      endEffector=end_jnt.node)
        self.module_nodes += [ikh, effector]
        self.parts += [ikh]

        # Get Pole Vector position
        root_pos = maths.matrix_to_vectors(root_mtx)[3]
        mid_pos = maths.matrix_to_vectors(mid_mtx)[3]
        end_pos = maths.matrix_to_vectors(end_mtx)[3]

        pv = maths.get_pole_vector(root_pos, mid_pos, end_pos, length=5)

        # Create controls
        root_ctrl = cs.create_nurbscurve('{}_upper_IK_CTL'.format(self.name), 'cube')
        end_ctrl = cs.create_nurbscurve('{}_lower_IK_CTL'.format(self.name), 'long_hexagon')
        pv_ctrl = cs.create_nurbscurve('{}_pv_CTL'.format(self.name), 'diamond')
        self.module_nodes += [root_ctrl, end_ctrl, pv_ctrl]

        shape.modify_shape(end_ctrl, translate=(0, 1, 0), rotate=(0, 90, 0))

        root_ctrl_zero = node.Node(root_ctrl + '_ZRO', 'transform', suffix=False, list_add=self.module_nodes)
        end_ctrl_zero = node.Node(end_ctrl + '_ZRO', 'transform', suffix=False, list_add=self.module_nodes)
        pv_ctrl_zero = node.Node(pv_ctrl + '_ZRO', 'transform', suffix=False, list_add=self.module_nodes)
        self.controls += [root_ctrl_zero.node, end_ctrl_zero.node, pv_ctrl_zero.node]

        cmds.parent(root_ctrl, root_ctrl_zero.node)
        cmds.parent(end_ctrl, end_ctrl_zero.node)
        cmds.parent(pv_ctrl, pv_ctrl_zero.node)

        # Position controls
        cmds.xform(pv_ctrl_zero.node, t=(pv[0], pv[1], pv[2]), ws=True)
        cmds.xform(root_ctrl_zero.node, matrix=root_mtx)
        cmds.xform(end_ctrl_zero.node, matrix=end_mtx)

        # Create connections for controls
        pv_con = cmds.poleVectorConstraint(pv_ctrl, ikh)[0]
        end_pos_con = con.matrix_constraint(end_ctrl, ikh, connect='t')
        end_rot_con = con.matrix_constraint(end_ctrl, end_jnt.node, connect='r')
        root_con = con.matrix_constraint(root_ctrl, root_jnt.node)
        self.custom_nodes += [end_pos_con, end_rot_con, root_con]
        self.module_nodes += [pv_con]

        # Declare in and out connectors for build
        self.in_conns_build = [root_ctrl_zero.node, end_ctrl_zero.node, pv_ctrl_zero.node]
        self.out_conn_build = end_ctrl
