import maya.cmds as cmds

import fedrig.layout.layout_objects as lyt
import fedrig.layout.layout_list as lyt_list
import fedrig.base_module as bm
import fedrig.module_attributes as mod_attr
import create.nodes as node

import custom_utils.control_shapes as cs
import custom_utils.maths as maths
import custom_utils.shape as shape
import custom_utils.constraint as con

reload(bm)
reload(mod_attr)


class BasicFKModule(bm.BaseModule):
    module_type = 'BasicFK'

    def __init__(self, name='default', side='C', initialize=False):
        # Re initialize scene layouts
        self.scene_layouts = lyt_list.LayoutList()

        super(BasicFKModule, self).__init__(name=name, side=side, initialize=initialize)

    def module_layout_objects(self):
        # Create layout objects and base positions
        self.layouts['01'] = root_lyt = lyt.BaseLayoutObject(side=self.side, name=self.name)

        # Declare in and out connectors for module
        self.in_conn_module = root_lyt
        self.out_conn_module = root_lyt

        # Set default attributes for layout objects
        root_lyt.dag.connect(up_in=4,
                             aimType_in=0,
                             upType_in=0)

        root_lyt.set_position((1, 0, 0))

        # If the side is right apply the right transformations and attributes
        if self.side == 'R':
            root_lyt.mirror_self()
            root_lyt.set_position((-1, 0, 0))
        elif self.side == 'C':
            root_lyt.set_position((0, 1, 0))

    def module_attributes(self):
        '''
        Attributes specific to a module that influence a build

        Write dictionary of functions for how to return a value from
        '''
        self.attributes['joint'] = mod_attr.Attribute('Create Joint')
        self.attributes['joint'].check_box()

    def module_build(self):
        # get layout objects
        root = self.layouts['01']
        root_mtx = root.get_output_matrix()

        # Create controls
        ctrl = cs.create_nurbscurve('{}_{}_CTRL'.format(self.side, self.name), 'circle')
        self.module_nodes += [ctrl]

        ctrl_zero = node.Node(ctrl + '_ZERO', 'transform', suffix=False, list_add=self.module_nodes)

        self.controls += [ctrl_zero.node]

        cmds.parent(ctrl, ctrl_zero.node)

        # Position controls
        cmds.xform(ctrl_zero.node, matrix=root_mtx)

        # create bone
        if self.attributes['joint'].value():
            bone = node.Node('{}_{}'.format(self.side, self.name), 'joint', list_add=self.module_nodes)
            print bone
            print bone.node
            bone_ofs = node.Node(bone.node + '_OFS', 'transform', suffix=False, list_add=self.module_nodes)
            bone_zero = node.Node(bone.node + '_ZERO', 'transform', suffix=False, list_add=self.module_nodes)
            self.bones += [bone_zero.node]
            cmds.select(clear=True)

            # parent bones correctly under offsets and zeros
            cmds.parent(bone.node, bone_ofs.node)
            cmds.parent(bone_ofs.node, bone_zero.node)

            # put bones into correct positions
            bone_con = con.matrix_constraint(ctrl, bone_zero.node, maintain_offset=False)
            cmds.setAttr(bone.node + '.t', 0, 0, 0)
            self.custom_nodes += [bone_con]

        # Declare in and out connectors for build
        self.in_conns_build = [ctrl_zero.node]
        self.out_conn_build = ctrl
