import os
import sys


def create_structure(top_name, folder_dict, path):
    main_dir = os.path.join(path, top_name)
    if not os.path.exists(main_dir):
        os.mkdir(main_dir)

    for folder, next in folder_dict.items():
        dir = os.path.join(main_dir, folder)
        if not os.path.exists(dir):
            os.mkdir(dir)
        if next:
            for next_folder in next:
                next_dir = os.path.join(dir, next_folder)
                if not os.path.exists(next_dir):
                    os.mkdir(next_dir)

    return main_dir


def project_structure(name, path):
    '''
    Args:
        name (str): name of project
        path (str): path to put project in
    Return:
        (str): path to top of project structure

    '''
    folders = dict(assets=['character', 'prop'],
                   scripts=['character', 'prop'])

    folder = create_structure(name, folders, path)

    # Create a empty text file for identifying the directory as a project
    file_path = os.path.join(path, name, 'fedrig.txt')
    with open(file_path, "w") as file:
        file.write("Valid fedrig project")

    return folder


def type_structure(name, project_path):
    '''
    Creates new type of asset within project structure
    Args:
        name (str): name of type
        project_path (str): path of project
    '''

    asset_dir = os.path.join(project_path, 'assets', name)
    script_dir = os.path.join(project_path, 'scripts', name)

    os.mkdir(asset_dir)
    os.mkdir(script_dir)

def asset_structure(name, project_path, asset_type):
    '''
    Creates new asset within project structure
    Args:
        name (str): name of asset
        project_path (str): path of project
        asset_type (str): character or prop
    '''
    asset_folders = dict(geometry=None,
                         data=['weights', 'chain', 'controls', 'models', 'build'],
                         rig=['wip', 'build'])
    asset_dir = os.path.join(project_path, 'assets', asset_type)
    script_dir = os.path.join(project_path, 'scripts', asset_type, name)

    character_dir = create_structure(name, asset_folders, asset_dir)

    # Create build file
    file_path = os.path.join(character_dir, 'data', 'build', '{}.py'.format(name))
    file = open(file_path, "w")

    # Write beginning of asset file
    lines = ['import fedrig.base_module as bm',
             '\n',
             'class {}Build(bm.BaseModule):'.format(name),
             '    def __init__(self):',
             '        super({}Build, self).__init__()'.format(name)]
    for line in lines:
        file.write(line)
        file.write('\n')
    file.close()

    os.mkdir(script_dir)
