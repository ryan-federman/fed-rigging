# from Qt import QtCompat, QtWidgets, QtCore, QtGui
from PySide2 import QtWidgets, QtCore, QtGui
from shiboken2 import wrapInstance

from maya import OpenMayaUI as omui
import maya.cmds as cmds
import maya.api.OpenMaya as om

import os

import fedrig.project_structure as struct
import fedrig.chain_info as chain
import fedrig.base_module as bm

try:
    from PySide2.QtCore import *
    from PySide2.QtGui import *
    from PySide2.QtWidgets import *
    from PySide2 import __version__
    from shiboken2 import wrapInstance
except ImportError:
    from PySide.QtCore import *
    from PySide.QtGui import *
    from PySide import __version__
    from shiboken import wrapInstance

import fedrig.module_list as mod_list
import fedrig.layout.layout_list as lyt_list
import fedrig.import_modules as mods
import custom_utils.control_shapes as cs


class FedRig(QtWidgets.QWidget):
    modules = []
    # module_list = mod_list.ModuleList()

    def __init__(self):
        self.attribute_widgets = []

        self.directory_path = ''
        self.project_path = ''

        self.projects = []
        self.types = ['character', 'prop']
        self.assets = []

        self.current_project = ''
        self.current_type = ''
        self.current_asset = ''

        # if the ui exists then delete it
        old_window = omui.MQtUtil_findWindow('FedRig')
        if old_window:
            cmds.deleteUI('FedRig')

        # create a new dialog and give it the main maya window as its parent
        # store it as the parent for our current UI to be put inside
        parent = QtWidgets.QDialog(parent=getMayaMainWindow())

        # set its name so that we can find and delete it later
        parent.setObjectName('FedRig')
        parent.setWindowTitle('Fed Rig')

        super(FedRig, self).__init__(parent=parent)

        QtWidgets.QVBoxLayout(parent)

        self.buildUI()

        self.parent().layout().addWidget(self)

        parent.show()

    def buildUI(self):

        # create main layouts
        main_layout = QtWidgets.QVBoxLayout(self)
        project_button = QtWidgets.QPushButton('PROJECT')
        project_button.setStyleSheet("background-color: black")
        project_button.setFixedHeight(20)

        rigging_button = QtWidgets.QPushButton('RIGGING')
        rigging_button.setStyleSheet("background-color: black")
        rigging_button.setFixedHeight(20)

        create_grp = QtWidgets.QGroupBox()
        top_grp = QtWidgets.QGroupBox()
        self.build_grp = QtWidgets.QGroupBox()

        self.project_layout = create_layout = QtWidgets.QGridLayout(create_grp)
        self.rigging_layout = top_layout = QtWidgets.QGridLayout(top_grp)
        self.build_layout = QtWidgets.QGridLayout(self.build_grp)

        module_buttons_layout = QtWidgets.QGridLayout()

        main_layout.addWidget(project_button)
        main_layout.addWidget(create_grp)
        main_layout.addWidget(rigging_button)
        main_layout.addWidget(top_grp)
        main_layout.addWidget(self.build_grp)
        main_layout.addLayout(module_buttons_layout)

        # Tab for creating/loading of current asset/project
        self.project_tab_widget = QTabWidget()
        self.project_layout.addWidget(self.project_tab_widget)
        self.make_load_tab()
        self.make_create_tab()

        # Tab for most other rigging functions
        self.rigging_tab_widget = QTabWidget()
        self.rigging_layout.addWidget(self.rigging_tab_widget)
        self.make_chain_tab()

        # Tab for build
        self.build_grp.setLayout(self.build_layout)
        self.make_build_tab()

        # top_grp.setFixedHeight(top_grp.sizeHint().height())

    def make_build_tab(self):
        build_button = QtWidgets.QPushButton('Build Rig')
        build_button.clicked.connect(self.build_window)

        self.build_layout.addWidget(build_button, 0, 0, 1, 1)

    def make_chain_tab(self):
        # create the layout for the tab
        self.chain_tab_widget = QWidget()
        self.rigging_tab_widget.addTab(self.chain_tab_widget, "Chain")
        self.chain_tabLayout = QtWidgets.QGridLayout()
        self.chain_tab_widget.setLayout(self.chain_tabLayout)

        # For exporting chain
        comment_text = QtWidgets.QLabel()
        comment_text.setText('Comment:')
        self.chain_comment_line = QtWidgets.QLineEdit()
        export_button = QtWidgets.QPushButton('Export')
        export_button.clicked.connect(self.export_chain)
        export_button.clicked.connect(self.refresh_comments)

        # For importing chain
        version_text = QtWidgets.QLabel()
        version_text.setText('Version:')
        self.versions_cb = QtWidgets.QComboBox()
        self.versions_sb = QtWidgets.QSpinBox()
        self.versions_sb.setRange(-1000, 0)
        import_button = QtWidgets.QPushButton('Import')
        import_button.clicked.connect(self.import_chain)
        self.versions_cb.currentIndexChanged.connect(self.set_chain_index)
        self.versions_sb.valueChanged.connect(self.set_chain_comment)
        # for project in self.projects:
        #     self.versions_cb.addItem(type)
        # self.versions_cb.currentIndexChanged.connect(self.set_project)


        # # Add widgets to layouts
        self.chain_tabLayout.addWidget(comment_text, 0, 0, 1, 1)
        self.chain_tabLayout.addWidget(self.chain_comment_line, 0, 1, 1, 3)
        self.chain_tabLayout.addWidget(export_button, 0, 4, 1, 1)

        self.chain_tabLayout.addWidget(version_text, 1, 0, 1, 1)
        self.chain_tabLayout.addWidget(self.versions_sb, 1, 1, 1, 1)
        self.chain_tabLayout.addWidget(self.versions_cb, 1, 2, 1, 2)
        self.chain_tabLayout.addWidget(import_button, 1, 4, 1, 1)

        #
        # self.load_tabLayout.addWidget(type_text, 2, 0, 1, 1)
        # self.load_tabLayout.addWidget(self.type_cb, 2, 1, 1, 2)
        #
        # self.load_tabLayout.addWidget(asset_text, 3, 0, 1, 1)
        # self.load_tabLayout.addWidget(self.asset_cb, 3, 1, 1, 2)



    def make_controls_tab(self):
        pass

    # Functions for creation/loading of current asset/project
    def make_create_tab(self):
        # create the layout for the tab
        self.create_tab_widget = QWidget()
        self.project_tab_widget.addTab(self.create_tab_widget, "Create")
        self.create_tabLayout = QtWidgets.QGridLayout()
        self.create_tab_widget.setLayout(self.create_tabLayout)

        self.current_project_text = QtWidgets.QLabel()
        self.current_project_text.setText('Current Project: No Project Loaded')

        self.current_type_text = QtWidgets.QLabel()
        self.current_type_text.setText('Current Type: No Type Selected')

        self.create_project_line = QtWidgets.QLineEdit()
        create_project_button = QtWidgets.QPushButton('Create Project')
        create_project_button.clicked.connect(self.create_project)

        self.create_type_line = QtWidgets.QLineEdit()
        create_type_button = QtWidgets.QPushButton('Create Asset Type')
        create_type_button.clicked.connect(self.create_type)

        self.create_asset_line = QtWidgets.QLineEdit()
        create_asset_button = QtWidgets.QPushButton('Create Asset')
        create_asset_button.clicked.connect(self.create_asset)

        self.create_tabLayout.addWidget(self.current_project_text, 0, 0, 1, 1)

        self.create_tabLayout.addWidget(self.current_type_text, 1, 0, 1, 1)

        self.create_tabLayout.addWidget(self.create_project_line, 2, 0, 1, 2)
        self.create_tabLayout.addWidget(create_project_button, 2, 2, 1, 1)

        self.create_tabLayout.addWidget(self.create_type_line, 3, 0, 1, 2)
        self.create_tabLayout.addWidget(create_type_button, 3, 2, 1, 1)

        self.create_tabLayout.addWidget(self.create_asset_line, 4, 0, 1, 2)
        self.create_tabLayout.addWidget(create_asset_button, 4, 2, 1, 1)

    def make_load_tab(self):
        # create the layout for the tab
        self.load_tab_widget = QWidget()
        self.project_tab_widget.addTab(self.load_tab_widget, "Load")
        self.load_tabLayout = QtWidgets.QGridLayout()
        self.load_tab_widget.setLayout(self.load_tabLayout)

        set_text = QtWidgets.QLabel()
        set_text.setText('Project Path:')
        set_button = QtWidgets.QPushButton('Directory Path')
        self.directory_path_line = QtWidgets.QLineEdit()
        set_button.clicked.connect(self.set_path)

        # Types of assets within project
        project_text = QtWidgets.QLabel()
        project_text.setText('Project:')
        self.project_cb = QtWidgets.QComboBox()
        for project in self.projects:
            self.project_cb.addItem(project)
        self.project_cb.currentIndexChanged.connect(self.set_project)

        # Types of assets within project
        type_text = QtWidgets.QLabel()
        type_text.setText('Asset Type:')
        self.type_cb = QtWidgets.QComboBox()
        for type in self.types:
            self.type_cb.addItem(type)
        self.type_cb.currentIndexChanged.connect(self.set_type)

        # Assets within given asset type
        asset_text = QtWidgets.QLabel()
        asset_text.setText('Assets:')
        self.asset_cb = QtWidgets.QComboBox()
        for asset in self.assets:
            self.asset_cb.addItem(asset)
        self.asset_cb.currentIndexChanged.connect(self.set_asset)
        self.asset_cb.currentIndexChanged.connect(self.refresh_comments)


        # Add widgets to layouts
        self.load_tabLayout.addWidget(set_text, 0, 0, 1, 1)
        self.load_tabLayout.addWidget(self.directory_path_line, 0, 1, 1, 2)
        self.load_tabLayout.addWidget(set_button, 0, 3, 1, 1)

        self.load_tabLayout.addWidget(project_text, 1, 0, 1, 1)
        self.load_tabLayout.addWidget(self.project_cb, 1, 1, 1, 2)

        self.load_tabLayout.addWidget(type_text, 2, 0, 1, 1)
        self.load_tabLayout.addWidget(self.type_cb, 2, 1, 1, 2)

        self.load_tabLayout.addWidget(asset_text, 3, 0, 1, 1)
        self.load_tabLayout.addWidget(self.asset_cb, 3, 1, 1, 2)

    def export_chain(self):
        '''
        Writes chain data to the current asset
        '''

        path = os.path.join(self.directory_path, self.current_project, 'assets', self.current_type, self.current_asset, 'data/chain')
        comment = self.chain_comment_line.text()
        chain.write_chain(path, comment)

    def import_chain(self):
        '''
        Imports chain data from the current given index of the current asset
        '''

        path = os.path.join(self.directory_path, self.current_project, 'assets', self.current_type, self.current_asset, 'data/chain')
        chain.import_chain(path, self.versions_sb.value())

    def set_path(self):
        '''
        Sets line with current path and loads all projects within the directory into the combo box
        '''

        path = cmds.fileDialog2(cap='Select Path for Project', fm=3, ds=1)
        self.directory_path_line.setText('')

        # Do nothing if no path is selected
        if not path:
            return

        self.directory_path = path[0]
        self.directory_path_line.setText(self.directory_path)

        # After path is set search for projects in given path
        if self.directory_path:
            self.projects = []
            directories = [d for d in os.listdir(self.directory_path) if os.path.isdir(os.path.join(self.directory_path, d))]
            for d in directories:
                children = os.listdir(os.path.join(self.directory_path, d))
                if 'fedrig.txt' in children:
                    self.projects.append(d)

        self.refresh_projects()

    def set_chain_comment(self):
        '''
        Sets chain comment to current index
        '''

        self.versions_cb.setCurrentIndex(self.versions_sb.value() * -1)

    def set_chain_index(self):
        '''
        Sets version spinbox to index associated with comment
        '''

        self.versions_sb.setValue(self.versions_cb.currentIndex() * -1)

    def refresh_projects(self):
        '''
        Refreshes combo box holding the projects
        '''
        self.project_cb.clear()
        for proj in self.projects:
            self.project_cb.addItem(proj)

    def refresh_types(self):
        '''
        Refreshes combo box holding the asset types
        '''
        self.type_cb.clear()
        for proj in self.types:
            self.type_cb.addItem(proj)

    def refresh_assets(self):
        '''
        Refreshes combo box holding the assets
        '''
        self.asset_cb.clear()
        for proj in self.assets:
            self.asset_cb.addItem(proj)

    def refresh_comments(self):
        '''
        Refreshes comments for chain versions
        '''
        try:
            # Import chain_comments variable
            if self.current_asset:
                path = os.path.join(self.directory_path, self.current_project, 'assets', self.current_type, self.current_asset, 'data/chain')
                for file in os.listdir(path):
                    if 'chain_comments' in file:
                        comment_path = os.path.join(path, 'chain_comments.py')
                        exec (open(comment_path).read())

                        # Reload combo box with updated comments for asset
                        self.versions_cb.clear()
                        chain_comments.reverse()
                        for comment in chain_comments:
                            print comment
                            self.versions_cb.addItem(comment)
        except WindowsError:
            pass

    def create_project(self):
        '''
        Creates project in current directory
        '''
        if self.directory_path:
            new_project = self.create_project_line.text()
            if new_project in self.projects:
                raise ValueError('Project Already Exists')
            else:
                struct.project_structure(new_project, self.directory_path)
                self.projects.append(new_project)
                self.refresh_projects()
                print 'PROJECT: {} has been created'.format(new_project)

    def create_type(self):
        '''
        Creates type in current project
        '''
        if self.directory_path:
            new_type = self.create_type_line.text()
            if new_type in self.types:
                raise ValueError('Type Already Exists')
            else:
                project_path = os.path.join(self.directory_path, self.current_project)
                struct.type_structure(new_type, project_path)
                self.types.append(new_type)
                self.refresh_types()
                print ' TYPE: {} within PROJECT: {} has been created'.format(new_type, self.current_project)

    def create_asset(self):
        '''
        Creates asset under current type
        '''
        if self.directory_path:
            new_asset = self.create_asset_line.text()
            if new_asset in self.assets:
                raise ValueError('Asset Already Exists')
            else:
                project_path = os.path.join(self.directory_path, self.current_project)
                struct.asset_structure(new_asset, project_path, self.current_type)
                self.assets.append(new_asset)
                self.refresh_assets()
                print 'ASSET: {} within TYPE: {} has been created'.format(new_asset, self.current_type)

    def set_project(self):
        '''
        Sets current project to current combobox item
        '''
        self.current_project = self.project_cb.currentText()
        self.current_project_text.setText('Current Project: {}'.format(self.current_project))

        # Find given types in project
        type_path = os.path.join(self.directory_path, self.current_project, 'assets')
        self.types = [d for d in os.listdir(type_path) if os.path.isdir(os.path.join(type_path, d))]
        self.refresh_types()

    def set_type(self):
        '''
        Sets current type to current combobox item
        '''
        self.current_type = self.type_cb.currentText()
        self.current_type_text.setText('Current Type: {}'.format(self.current_type))

        # Find given assets in current type
        asset_path = os.path.join(self.directory_path, self.current_project, 'assets', self.current_type)
        self.assets = [d for d in os.listdir(asset_path) if os.path.isdir(os.path.join(asset_path, d))]

        self.refresh_assets()

    def set_asset(self):
        '''
        Sets current asset to current combobox item and sets the asset path of the base module class
        '''
        self.current_asset = self.asset_cb.currentText()
        bm.BaseModule.asset_path = os.path.join(self.directory_path, self.current_project, 'assets', self.current_type, self.current_asset)

    def build_window(self):
        # if the ui exists then delete it
        old_window = omui.MQtUtil_findWindow('FedRigBuildWindow')
        if old_window:
            cmds.deleteUI('FedRigBuildWindow')

        # create a new dialog and give it the main maya window as its parent
        # store it as the parent for our current UI to be put inside
        parent = QtWidgets.QDialog(parent=getMayaMainWindow())

        # set its name so that we can find and delete it later
        parent.setObjectName('FedRigBuildWindow')
        parent.setWindowTitle('Build')

        main_layout = QtWidgets.QGridLayout(parent)

        warning_text = QtWidgets.QLabel()
        warning_text.setText('Refresh scene and build new rig?')

        yes_button = QtWidgets.QPushButton('Yes')
        yes_button.clicked.connect(self.build)
        yes_button.clicked.connect(self.close_build_window)

        no_button = QtWidgets.QPushButton('No')
        no_button.clicked.connect(self.close_build_window)

        main_layout.addWidget(warning_text, 0, 0, 1, 2)

        main_layout.addWidget(yes_button, 1, 0, 1, 1)
        main_layout.addWidget(no_button, 1, 1, 1, 1)

        parent.show()

    def build(self):
        '''
        Builds rig of current asset
        '''

        # Create new scene
        cmds.file(f=True, new=True)

        # comment_path = os.path.join(path, 'chain_comments.py')
        # exec (open(comment_path).read())

    def close_build_window(self):
        '''
        Closes build window
        '''
        old_window = omui.MQtUtil_findWindow('FedRigBuildWindow')
        if old_window:
            cmds.deleteUI('FedRigBuildWindow')

    ######################################### OLD CODE###########################################
    # def build(self):
    #     # Get Module
    #     mod_name = self.module_cb.currentText()
    #     module = self.module_list.get_module(mod_name)
    #
    #     module.build()
    #     self.reset_attribute_widgets(mod_name)
    #
    # def build_all(self):
    #     for name, mod in self.module_list.module_objs.items():
    #         mod.build()
    #     mod_name = self.module_cb.currentText()
    #     self.reset_attribute_widgets(mod_name)
    #
    #     # mirror all controls that exist over the left side
    #     for each in cmds.ls('L_*CTRL'):
    #         r_ctrl = 'R_' + each.split('L_')[1]
    #         if cmds.ls(r_ctrl):
    #             cs.mirror_curve([each])
    #
    # def deconstruct(self):
    #     # Get Module
    #     mod_name = self.module_cb.currentText()
    #     self.module_text.setText(mod_name)
    #     module = self.module_list.get_module(mod_name)
    #
    #     module.deconstruct()
    #     self.reset_attribute_widgets(mod_name)
    #
    # def deconstruct_all(self):
    #     for name, mod in self.module_list.module_objs.items():
    #         mod.deconstruct()
    #     mod_name = self.module_cb.currentText()
    #     self.reset_attribute_widgets(mod_name)
    #
    # def mirror(self):
    #     mod_name = self.module_cb.currentText()
    #     module = self.module_list.get_module(mod_name)
    #     module.mirror_module()
    #
    # def change_layout_space(self):
    #     '''
    #     Change the selected layouts space
    #     '''
    #
    #     selection = cmds.ls(sl=True)
    #     cmds.select(clear=True)
    #     if selection:
    #         for each in selection:
    #             lyt = each
    #             if 'LAYOUT' in lyt:
    #                 layouts = lyt_list.LayoutList()
    #                 layout = layouts.get_layout(lyt)
    #
    #                 space = self.lyt_space_cb.currentText()
    #                 layout.change_space(space)
    #
    # def change_module_space(self):
    #     '''
    #     Change the selected modules space
    #     '''
    #     space = self.module_space_cb.currentText()
    #     mod_name = self.module_cb.currentText()
    #     module = self.module_list.get_module(mod_name)
    #     module.change_space(space)
    #
    # def refresh(self):
    #     '''
    #     Refreshes the GUI with an updated module list
    #     '''
    #     # Clear
    #     self.module_cb.clear()
    #     self.set_parent_cb.clear()
    #     self.module_text.setText('None')
    #     self.parent_module_text.setText('None')
    #
    #     self.load_modules()
    #
    #     self.module_cb.addItem('None')
    #     for mod in self.modules:
    #         self.module_cb.addItem(mod)
    #
    #     self.set_parent_cb.addItem('None')
    #     for mod in self.modules:
    #         self.set_parent_cb.addItem(mod)
    #
    # def update(self):
    #     '''
    #     Refreshes scene with new module code
    #     '''
    #     reload(mods)
    #     self.module_list.__class__.instances = None
    #     self.load_modules()
    #
    # def delete(self):
    #     '''
    #     Deletes the current module
    #     '''
    #     mod_name = self.module_text.text()
    #     module = self.module_list.get_module(mod_name)
    #     module.delete()
    #     self.refresh()
    #
    # def box_set_parent(self):
    #     '''
    #     Sets the parent of the current module when combo box is clicked
    #     '''
    #     mod_name = self.module_cb.currentText()
    #     parent_name = self.set_parent_cb.currentText()
    #
    #     if mod_name:
    #         if mod_name == parent_name:
    #             if mod_name != 'None':
    #                 raise RuntimeError('Cannot parent module under itself')
    #
    #         mod = self.module_list.get_module(mod_name)
    #         parent = self.module_list.get_module(parent_name)
    #
    #         if mod:
    #             if parent:
    #                 mod.parent(parent)
    #                 self.parent_module_text.setText(parent_name)
    #             else:
    #                 if mod.parent_module:
    #                     mod.unparent()
    #                 self.parent_module_text.setText('None')
    #
    # def set_parent(self):
    #     selection = cmds.ls(sl=True)
    #     mod_name = self.module_cb.currentText()
    #     mod = self.module_list.get_module(mod_name)
    #     if selection:
    #         parent_name = cmds.attributeQuery('module', node=selection[0], listEnum=True)[0]
    #         if mod_name == parent_name:
    #             raise RuntimeError('Cannot parent module under itself')
    #
    #         parent = self.module_list.get_module(parent_name)
    #         if mod:
    #             mod.parent(parent)
    #             self.parent_module_text.setText(parent_name)
    #             index = self.set_parent_cb.findText(parent_name)
    #             self.set_parent_cb.setCurrentIndex(index)
    #     else:
    #         mod.unparent()
    #         self.parent_module_text.setText('None')
    #         self.set_parent_cb.setCurrentIndex(0)
    #
    # def box_set_module(self):
    #     '''
    #     Sets current module when combo box is clicked
    #     '''
    #
    #     # Delete old widgets
    #     for widget in self.attribute_widgets:
    #         self.attribute_layout.removeWidget(widget)
    #         self.delete_widget(widget)
    #
    #     self.attribute_widgets = []
    #
    #     mod_name = self.module_cb.currentText()
    #     self.module_text.setText(mod_name)
    #
    #     # Add new widgets to module
    #     module = self.module_list.get_module(mod_name)
    #     row = 0
    #     if module:
    #         module.initialize_attributes()
    #
    #         for key, attr in module.attributes.items():
    #             for column, widget in enumerate(attr.widgets):
    #                 self.attribute_layout.addWidget(widget, row, int(column), 1, 1)
    #                 self.attribute_widgets.append(widget)
    #             row += 1
    #
    #         # If there is a parent change the widgets related to the parent
    #         if module.parent_module:
    #             parent = module.parent_module.side + '_' + module.parent_module.name
    #             self.parent_module_text.setText(parent)
    #             index = self.set_parent_cb.findText(parent)
    #             self.set_parent_cb.setCurrentIndex(index)
    #         else:
    #             self.parent_module_text.setText('None')
    #             self.set_parent_cb.setCurrentIndex(0)
    #     else:
    #         self.module_text.setText('None')
    #         self.parent_module_text.setText('None')
    #         self.module_cb.setCurrentIndex(0)
    #         self.set_parent_cb.setCurrentIndex(0)
    #
    # def set_module(self):
    #     '''
    #     Sets current module based on selection
    #     '''
    #     # Delete old widgets
    #     for widget in self.attribute_widgets:
    #         self.attribute_layout.removeWidget(widget)
    #         self.delete_widget(widget)
    #
    #     self.attribute_widgets = []
    #
    #     selection = cmds.ls(sl=True)
    #     mod_name = None
    #     if selection:
    #         try:
    #             mod_name = cmds.attributeQuery('module', node=selection[0], listEnum=True)[0]
    #         except RuntimeError:
    #             pass
    #     if mod_name:
    #         module = self.module_list.get_module(mod_name)
    #         row = 0
    #         if module:
    #             # Set module text and module combo box
    #             self.module_text.setText(mod_name)
    #             index = self.module_cb.findText(mod_name)
    #             self.module_cb.setCurrentIndex(index)
    #
    #             module.initialize_attributes()
    #             for key, attr in module.attributes.items():
    #                 for column, widget in enumerate(attr.widgets):
    #                     self.attribute_layout.addWidget(widget, row, int(column), 1, 1)
    #                     self.attribute_widgets.append(widget)
    #                 row += 1
    #
    #             # If there is a parent change the widgets related to the parent
    #             if module.parent_module:
    #                 parent = module.parent_module.side + '_' + module.parent_module.name
    #                 self.parent_module_text.setText(parent)
    #                 index = self.set_parent_cb.findText(parent)
    #                 self.set_parent_cb.setCurrentIndex(index)
    #             else:
    #                 self.parent_module_text.setText('None')
    #     else:
    #         self.module_text.setText('None')
    #         self.parent_module_text.setText('None')
    #         self.module_cb.setCurrentIndex(0)
    #         self.set_parent_cb.setCurrentIndex(0)
    #
    # def load_modules(self):
    #     '''
    #     Loads all modules in the current scene
    #     '''
    #     self.module_list = mod_list.ModuleList()
    #     self.modules = []
    #     for name, mod in self.module_list.module_objs.items():
    #         self.modules.append(name)
    #
    # def set_module_type(self):
    #     '''
    #     Sets default name and side for the given module type before creating
    #     '''
    #     mod = self.create_module_cb.currentText()
    #     side, name = mods.module_names[mod]
    #
    #     # Set default side and name
    #     index = self.side_cb.findText(side)
    #     self.side_cb.setCurrentIndex(index)
    #     self.name_line.setText(name)
    #
    # def create_module(self):
    #     '''
    #     Creates a module from the given selection
    #     '''
    #     mod_type = self.create_module_cb.currentText()
    #     side = self.side_cb.currentText()
    #     name = self.name_line.text()
    #
    #     mods.create_module(mod_type, side=side, name=name, initialize=False)
    #
    #     self.refresh()
    #
    # def toggle_group(self, widget, max_height):
    #     height = widget.height()
    #     if height < 1:
    #         # widget.setFixedHeight(widget.sizeHint().height())
    #         widget.setFixedHeight(max_height)
    #     else:
    #         widget.setFixedHeight(0)
    #
    # def reset_attribute_widgets(self, mod_name):
    #     '''
    #     Resets the attribute widgets in the current layout
    #     :param mod_name:
    #     :return:
    #     '''
    #     # Delete old widgets
    #     for widget in self.attribute_widgets:
    #         self.attribute_layout.removeWidget(widget)
    #         self.delete_widget(widget)
    #
    #     self.attribute_widgets = []
    #
    #     module = self.module_list.get_module(mod_name)
    #     row = 0
    #     if module:
    #         # Set module text and module combo box
    #         self.module_text.setText(mod_name)
    #         index = self.module_cb.findText(mod_name)
    #         self.module_cb.setCurrentIndex(index)
    #
    #         module.initialize_attributes()
    #         for key, attr in module.attributes.items():
    #             for column, widget in enumerate(attr.widgets):
    #                 self.attribute_layout.addWidget(widget, row, int(column), 1, 1)
    #                 self.attribute_widgets.append(widget)
    #             row += 1
    #
    #         # If there is a parent change the widgets related to the parent
    #         if module.parent_module:
    #             parent = module.parent_module.side + '_' + module.parent_module.name
    #             self.parent_module_text.setText(parent)
    #             index = self.set_parent_cb.findText(parent)
    #             self.set_parent_cb.setCurrentIndex(index)
    #         else:
    #             self.parent_module_text.setText('None')

    def delete_widget(self, widget):
        cmds.undoInfo(openChunk=True)
        widget.setParent(None)
        widget.setVisible(False)
        widget.deleteLater()
        cmds.undoInfo(closeChunk=True)


def getMayaMainWindow():
    """
    Since Maya is Qt, we can parent our UIs to it.
    This means that we don't have to manage our UI and can leave it to Maya.
    Returns:
        QtWidgets.QMainWindow: The Maya MainWindow
    """
    # Get a reference to Maya's MainWindow
    win = omui.MQtUtil_mainWindow()

    # Wrap the window reference into Qt
    ptr = wrapInstance(long(win), QtWidgets.QMainWindow)

    return ptr
