import fedrig.modules.submodules.ik_chain_module as ikc
import fedrig.modules.submodules.basic_fk_module as fk
import fedrig.modules.arm_module as arm
import fedrig.modules.leg_module as leg
import fedrig.modules.hand_module as hand
import fedrig.modules.spine_module as spine
import fedrig.modules.neck_module as neck
import fedrig.modules.biped_preset as bi

reload(ikc)
reload(fk)
reload(arm)
reload(leg)
reload(hand)
reload(spine)
reload(neck)


def create_module(type, side='C', name='default', initialize=True):
    '''
    Creates a module
    Args:
        type (str): type of module created
        side (str): side module is created on
        name (str): name of module
        initialize bool: if false creates a module and it's layout objects, if
                         true creates an empty object
    '''
    modules = {'IkChain': ikc.IkChainModule,
               'BasicFK': fk.BasicFKModule,
               'Arm': arm.ArmModule,
               'Leg': leg.LegModule,
               'Hand': hand.HandModule,
               'Spine': spine.SpineModule,
               'Neck': neck.NeckModule,
               'Biped': bi.create_biped}

    module = modules[type](side=side, name=name, initialize=initialize)

    return module


# Names of modules and the name and side that gets loaded by default
module_names = {'IkChain':  ('C', 'ik'),
                'BasicFK': ('C', 'fk'),
                'Arm':  ('L', 'arm'),
                'Leg': ('L', 'leg'),
                'Hand': ('L', 'hand'),
                'Spine': ('C', 'spine'),
                'Neck': ('C', 'neck'),
                'Biped': ('C', 'biped')}
