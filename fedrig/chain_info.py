import custom_utils.generic as gen

import maya.cmds as cmds
import os

def write_chain(data_path, comment):
    '''
    Gather all chain info
    '''

    # Get all chains in the scene
    chains = cmds.ls('*_MOD')

    # List for dictionaries for keeping chain info
    chain_dicts = []

    # Get unique mod names
    mods = []
    for chain in chains:
        mod = gen.remove_suffix(gen.remove_suffix(chain))
        if mod not in mods:
            mods.append(mod)

    # Get information for modules and each of their child joints
    for mod in mods:
        joints = cmds.ls(mod + '_*_MOD')
        mod_info = dict()

        # Module information
        mod_info['num_jnts'] = len(joints)
        mod_info['positions'] = []
        mod_info['rotations'] = []
        mod_info['matrices'] = []
        mod_info['parent'] = ''
        mod_info['name'] = mod

        for i, jnt in enumerate(joints):
            if i == 0:
                try:
                    parent = cmds.listRelatives(jnt, p=True)[0]
                    if 'MOD' in parent:
                        mod_info['parent'] = parent
                    else:
                        cmds.error('{} not parented to valid module'.format(jnt))
                except TypeError:
                    mod_info['parent'] = 'world'

            pos = cmds.xform(jnt, t=True, ws=True, q=True)
            rot = cmds.xform(jnt, ro=True, ws=True, q=True)
            mtx = cmds.xform(jnt, matrix=True, ws=True, q=True)
            mod_info['positions'].append(pos)
            mod_info['rotations'].append(rot)
            mod_info['matrices'].append(mtx)

        chain_dicts.append(mod_info)

    # Create file
    existing_files = []
    for file in os.listdir(data_path):
        if file.endswith(".py"):
            if 'comment' not in file:
                existing_files.append(file)

    file_path = os.path.join(data_path, 'chain_{}.py'.format(gen.create_int_string(len(existing_files) + 1, 2)))
    file = open(file_path, "w")

    # Write chain data to file
    file.write('scene_modules = dict(')
    for module in chain_dicts:
        file.write('{} = '.format(module['name']))
        file.write(str(module))
        file.write(',')
        file.write('\n')
        print '{} Module Added'.format(module['name'])

    file.write(')')
    file.close()

    # Check if comment file exists, if not create new variable for list of comments
    comment_file_path = os.path.join(data_path, 'chain_comments.py')
    try:
        exec (open(comment_file_path).read())
        if not comment:
            comment = 'No Comment'
        chain_comments.append(comment)
    except IOError:
        if not comment:
            comment = 'No Comment'
        chain_comments = [comment]

    # Write chain comment to file
    comment_file = open(comment_file_path, "w")
    comment_file.write('chain_comments = {}'.format(chain_comments))

    comment_file.close()


def import_chain(data_path, file_index):
    '''
    Imports module chains into scene

    Args:
        data_path(str): path of chain data
        file_index(int): index of chain to be imported, 0 is current, -1 is the last exported chain before the current, etc.

    '''

    files = os.listdir(data_path)
    if not files:
        cmds.error('No existing chains')

    # Open given file and import "scene_modules" dictionary
    file_path = os.path.join(data_path, files[file_index - 1])
    exec (open(file_path).read())

    # Create joints
    for mod, mod_info in scene_modules.items():
        cmds.select(clear=True)
        jnts = []
        for i in range(mod_info['num_jnts']):
            jnt = cmds.joint(name='{}_{}_MOD'.format(mod, i))
            jnts.append(jnt)

            # Rotate joints
            cmds.xform(jnt, ro=mod_info['rotations'][i], ws=True)

    # Position joints
    for mod, mod_info in scene_modules.items():
        for i in range(mod_info['num_jnts']):
            jnt = '{}_{}_MOD'.format(mod, i)
            cmds.xform(jnt, t=mod_info['positions'][i], ws=True)

    # Parent joints
    for mod, mod_info in scene_modules.items():
        if 'world' != mod_info['parent']:
            # parent_end_jnt = cmds.ls('{}_*_MOD'.format(mod_info['parent']))[-1]
            cmds.parent('{}_0_MOD'.format(mod), mod_info['parent'])




