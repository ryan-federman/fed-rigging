import copy
import os

import maya.cmds as cmds


def get_cwd():
    return os.getcwd()


class BaseModule(object):
    steps_base = {10: list(),
                  20: list(),
                  30: list(),
                  40: list(),
                  50: list()}

    steps = dict(proxy=copy.deepcopy(steps_base),
                 anim=copy.deepcopy(steps_base),
                 cache=copy.deepcopy(steps_base))

    modules = dict()
    asset_path = str()

    def __init__(self, name='Ct_default', step=20, rig_type='proxy'):
        # Check to see if module name already exists
        if self.modules[name]:
            RuntimeError('Module {} already exists'.format(name))

        # Add instance of class to assigned step and rig types
        rig_types = ['cache']
        if rig_type == 'anim' or rig_type == 'proxy':
            rig_types.append('anim')
        if rig_type == 'proxy':
            rig_types.append('proxy')

        for each in rig_types:
            self.steps[each][step].append(self)

        self.modules[name] = self

        # Pre-build joint transformation info
        self.module_positions = []
        self.module_rotations = []

        self.rig_type = rig_type
        self.name = name
        self.module_grp = None
        self.module_nodes = []
        self.parts = []
        self.controls = []
        self.bones = []
        self.custom_nodes = []

        self.parent_module = None
        self.child_modules = []

        self.in_conn_module = None
        self.out_conn_module = None

        self.in_conns_build = []
        self.out_conn_build = None

        self.step = step

    def build(self):
        pass

    ### DONT OVERRIDE ###
    def build_end(self):
        '''
        Build function for building all modules existing in the steps in order
        '''

        for step, modules in self.steps[self.rig_type].items():
            print 'STEP: {}'.format(step)
            for mod in modules:
                # Build module
                print 'Building {}'.format(mod.name)
                mod.build()

                # Parent module to it's designated parent module
                if mod.parent_module:
                    for each in self.in_conns_build:
                        cmds.parentConstraint(self.parent_module.out_conn_build, each, mo=True)

    def get_module(self, name):
        '''
        Fetches the module instance of the given basename
        '''

        return self.modules[name]

    def parent(self, parent_module):
        '''
        Method for setting the parent module of this module
        '''
        self.parent_module = parent_module

    def get_transforms(self, index, transform='both'):
        '''
        Gets the transformations of the given pre build joint
        Args:
            index(int): index of which joint you will be receiving the transforms from
            transform(str): position, rotation, or both, will return a tuple if the former two, a dictionary of the tuples if the latter
        '''

        # Get current path for where the chain of the asset lies
        chain_path = os.path.join(self.asset_path, '/data/chain')
        files = os.listdir(chain_path)
        if not files:
            cmds.error('No existing chains')

        # Open given file and import "scene_modules" dictionary
        file_path = os.path.join(chain_path, files[-1])
        exec (open(file_path).read())

        # Get information from file
        if transform == 'position' or transform == 'rotation':
            transforms = scene_modules[self.name][transform][index]
        else:
            transforms = dict()
            transforms['positions'] = scene_modules[self.name]['positions'][index]
            transforms['rotations'] = scene_modules[self.name]['rotations'][index]

        return transforms

    def get_matrix(self, index):
        '''
        Gets the matrix of the given pre build joint
        Args:
            index(int): index of which joint you will be receiving the matrix
        '''

        # Get current path for where the chain of the asset lies
        chain_path = os.path.join(self.asset_path, '/data/chain')
        files = os.listdir(chain_path)
        if not files:
            cmds.error('No existing chains')

        # Open given file and import "scene_modules" dictionary
        file_path = os.path.join(chain_path, files[-1])
        exec (open(file_path).read())

        # Get information from file
        transforms = scene_modules[self.name]['matrices'][index]

        return transforms


