# from Qt import QtCompat, QtWidgets, QtCore, QtGui
from PySide2 import QtWidgets, QtCore, QtGui
from shiboken2 import wrapInstance

from maya import OpenMayaUI as omui
import maya.cmds as cmds
import maya.api.OpenMaya as om

import custom_utils.attribute as attr


class Attribute(object):
    '''
    Class object which creates GUI elements to get information for a module
    '''
    widgets = []
    main_widget = None

    type = None
    default_value = None
    value_func = None
    set_func = None

    module = None

    def __init__(self, name, display_name, module):
        '''
        Args:
            name (str): name of attribute
            display_name (str): text to be shown in GUI
            module (moduleObject): module to add attribute to
        '''

        self.name = name
        self.display_name = display_name
        self.module = module
        self.module_grp = module.module_grp
        self.widgets = []
        self.main_widget = None
        self.type = None
        self.default_value = None
        self.value_func = None
        self.set_func = None

        self.initialize_module_value()

    def slider(self, min=0, max=100, default=0):
        # Create widgets
        slider_text = QtWidgets.QLabel()
        slider_text.setText(self.display_name)
        slider_box = QtWidgets.QSpinBox()
        slider = QtWidgets.QSlider(QtCore.Qt.Horizontal)

        # Connect slider and box together
        slider.valueChanged.connect(slider_box.setValue)
        slider_box.valueChanged.connect(slider.setValue)

        # Set min and max values
        if self.default_value:
            default = self.default_value
        slider.setMinimum(min)
        slider_box.setMinimum(min)
        slider.setMaximum(max)
        slider_box.setMaximum(max)
        slider_box.setValue(default)

        # Add to class attributes, add widgets in order that they will be shown
        self.main_widget = slider_box
        self.widgets = [slider_text, slider, slider_box]
        self.type = 'slider'

        # Add widget functions to current class object
        self.value_func = self.main_widget.value
        self.set_func = self.main_widget.setValue
        slider.valueChanged.connect(self.set_module_value)

    def check_box(self):
        cb_text = QtWidgets.QLabel()
        cb_text.setText(self.display_name)
        cb = QtWidgets.QCheckBox()

        self.main_widget = cb
        self.widgets = [cb_text, cb]
        self.type = 'check_box'
        self.value_func = self.main_widget.isChecked
        self.set_func = self.main_widget.setCheckState

    def radio_button(self):
        pass

    def drop_down(self):
        pass

    def value(self):
        '''
        Return Value of widget
        '''
        return self.value_func()

    def set_module_value(self):
        '''
        Sets value of widget
        '''
        # Set value
        value = self.value()
        self.default_value = value

        # Delete attribute if it already exists
        if cmds.ls(self.module_grp + '.' + self.name):
            cmds.deleteAttr(self.module_grp + '.' + self.name)

        # Add attribute with correct value to module group
        if type(value) == float:
            attr.add_float(self.module_grp, self.name, min=value, max=value, default=value)
        elif type(value) == int:
            attr.add_int(self.module_grp, self.name, min=value, max=value, default=value)
        elif type(value) == bool:
            attr.add_bool(self.module_grp, self.name, default=value)
        elif type(value) == basestring:
            attr.add_enum(self.module_grp, self.name, [value])

    def initialize_module_value(self):
        '''
        Gets last stored value of widget and applies it to class object
        '''
        if self.module_grp:
            if cmds.ls(self.module_grp + '.' + self.name):
                self.default_value = cmds.getAttr(self.module_grp + '.' + self.name)
