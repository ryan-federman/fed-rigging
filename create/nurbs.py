# import numpy as np

import maya.cmds as cmds
import maya.api.OpenMaya as om

import custom_utils.control_shapes as cs
import custom_utils.component as comp
import nodes as node
import custom_utils.attribute as attr
reload(node)


# def nurbs_plane(dag_pos, name='generic', sections=30, width=1):
#     """Creates a nurbs plane based on two given positions
#     Args:
#         dag_pos (list[tuple(float)]): Positions that are used for the beginning
#                                       and end of the vector
#         name (str): Base name for the plane that is created
#         sections (int): Amount of sections on the plane
#     Returns:
#         list[str]: Plane object name and node name
#     """
#     crvs = []
#     pos1 = om.MVector(dag_pos[0])
#     pos2 = om.MVector(dag_pos[1])
#
#     for x in range(2):
#         mtx1 = om.MMatrix()
#         mtx1.setElement(3, 2, width)
#
#         mtx2 = om.MMatrix()
#         mtx2.setElement(3, 2, (width) * -1)
#
#         crv = curves.curve_from_matrices([mtx1, mtx2], degree=1)
#         crvs.append(crv)
#
#     cmds.xform(crvs[0], t=pos1, ws=True)
#     cmds.xform(crvs[1], t=pos2, ws=True)
#
#     plane = cmds.loft(crvs[0], crvs[1],
#                       name=name,
#                       ch=True,
#                       uniform=True,
#                       sectionSpans=sections,
#                       range=False,
#                       polygon=0,
#                       rsn=True)
#     cmds.delete(crvs)
#
#     return plane

def birail_nurbs_plane(prof1_dags, prof2_dags, name, rails=None):
    """
    Args:
        dags[(str)]: dag nodes to control plane
        name (str): base name of plane
        side_vector (om.MVector()): side vector to orient/create plane

    Returns:
        str: nurbs plane
    """
    prof_crv1 = curve_from_dags(name=name + '_prof1_CRV', dags=prof1_dags)
    prof_crv2 = curve_from_dags(name=name + '_prof2_CRV', dags=prof2_dags)

    if not rails:
        rail_crv1 = curve_from_dags(name=name + '_rail1_CRV', dags=[prof1_dags[0], prof2_dags[0]], degree=1)
        rail_crv2 = curve_from_dags(name=name + '_rail2_CRV', dags=[prof1_dags[-1], prof2_dags[-1]], degree=1)
    else:
        rail_crv1 = curve_from_dags(name=name + '_rail1_CRV', dags=rails[0], degree=2)
        rail_crv2 = curve_from_dags(name=name + '_rail2_CRV', dags=rails[1], degree=2)

    cmds.rebuildCurve(prof_crv1, ch=1, rpo=1, kr=0, kcp=1, kt=0, s=30, d=2, tol=0.01)
    cmds.rebuildCurve(prof_crv2, ch=1, rpo=1, kr=0, kcp=1, kt=0, s=30, d=2, tol=0.01)

    plane = cmds.doubleProfileBirailSurface(prof_crv1,
                                            prof_crv2,
                                            rail_crv1,
                                            rail_crv2,
                                            po=0,
                                            name=name)
    return plane[0], [prof_crv1, prof_crv2, rail_crv1, rail_crv2]

def multi_birail_nurbs_plane(name, prof_dags, rail_dags):
    """
    NOTE: Rail dags must intersect at the exact point of each profile curve
    Args:
        prof_dags list[list[(str)]]: lists of dags to make profile curves
        prof_dags list[list[(str)]]: two lists of dags to make rail curves
        name (str): base name of plane
        side_vector (om.MVector()): side vector to orient/create plane

    Returns:
        str: nurbs plane
    """
    prof_crvs = []
    for i, profs in enumerate(prof_dags):
        prof_crv = curve_from_dags(name='{}_prof{}_CRV'.format(name, i), dags=profs, degree=2, rebuild=True)
        prof_crvs.append(prof_crv)

    rail_crvs = []
    for i, rails in enumerate(rail_dags):
        rail_crv = curve_from_dags(name='{}_rail{}_CRV'.format(name, i), dags=rails, degree=1, rebuild=True)
        rail_crvs.append(rail_crv)

    # for crv in prof_crvs:
    #     cmds.rebuildCurve(crv, ch=1, rpo=1, kr=0, kcp=1, kt=0, s=30, d=2, tol=0.01)

    crvs = prof_crvs + rail_crvs

    plane = cmds.multiProfileBirailSurface(crvs,
                                           po=0,
                                           name=name)
    return plane[0], crvs


def attach_to_surface(surface, dag, snap=True, scale=False):
    # nodes to attach dag to surface
    foll = cmds.createNode('transform', name=dag + '_custom_foll')

    posi = node.Node(dag, 'pointOnSurfaceInfo')
    matrix = node.Node(dag, 'fourByFourMatrix')
    mm = node.Node(foll, 'multMatrix')
    dm = node.Node(foll, 'decomposeMatrix')

    # find closest point on surface and it's UV values
    dag_pos = cmds.xform(dag, t=True, ws=True, q=True)
    pos, parU, parV = comp.closest_point_on_surface(surface, dag_pos)

    attr.add_float(foll, 'parU', min=0, max=500, default=parU)
    attr.add_float(foll, 'parV', min=0, max=500, default=parV)

    if snap:
        cmds.xform(dag, t=(pos[0], pos[1], pos[2]), ws=True)

    # attach dag to surface

    # create matrix from POSI node
    posi.connect(normalizedTangentVX=matrix.node + '.in00',
                 normalizedTangentVY=matrix.node + '.in01',
                 normalizedTangentVZ=matrix.node + '.in02',

                 normalizedNormalX=matrix.node + '.in10',
                 normalizedNormalY=matrix.node + '.in11',
                 normalizedNormalZ=matrix.node + '.in12',

                 normalizedTangentUX=matrix.node + '.in20',
                 normalizedTangentUY=matrix.node + '.in21',
                 normalizedTangentUZ=matrix.node + '.in22',

                 positionX=matrix.node + '.in30',
                 positionY=matrix.node + '.in31',
                 positionZ=matrix.node + '.in32',

                 inputSurface=surface + '.local',
                 parameterU=foll + '.parU',
                 parameterV=foll + '.parV'
                 )

    mm.connect(matrixIn_0=matrix.node + '.output',
               matrixIn_1=foll + '.parentInverseMatrix[0]',

               matrixSum=dm.node + '.inputMatrix'
               )

    dm.connect(outputTranslate=foll + '.translate',
               outputRotate=foll + '.rotate')

    if snap == True:
        maintainOffset = False
    else:
        maintainOffset = True
    cmds.parentConstraint(foll, dag, mo=maintainOffset)

    if scale:
        POSI0 = cmds.createNode('pointOnSurfaceInfo', name=dag + '_scale0_POSI')
        POSI1 = cmds.createNode('pointOnSurfaceInfo', name=dag + '_scale1_POSI')
        dist = cmds.createNode('math_DistancePoints', name=dag + '_DIST')
        div = cmds.createNode('math_Divide', name=dag + '_DIV')

        cmds.connectAttr(surface + '.local', POSI0 + '.inputSurface')
        cmds.connectAttr(surface + '.local', POSI1 + '.inputSurface')
        cmds.setAttr(POSI0 + '.parameterU', 0)
        cmds.setAttr(POSI1 + '.parameterU', 1)
        cmds.setAttr(POSI0 + '.parameterV', parV)
        cmds.setAttr(POSI1 + '.parameterV', parV)

        cmds.connectAttr(POSI0 + '.position', dist + '.input1')
        cmds.connectAttr(POSI1 + '.position', dist + '.input2')

        init_distance = cmds.getAttr(dist + '.output')
        cmds.setAttr(div + '.input2', init_distance)
        cmds.connectAttr(dist + '.output', div + '.input1')

        cmds.connectAttr(div + '.output', foll + '.sz')

    return foll


def dag_to_curve(dag, curve, up_vec_refs, snap=True, connect=True):
    ''' Attach a dag to the closest point on a curve

    Args:
        dag (str): name of dag node to attach to curve
        curve (str): name of curve to attach to
        up_vec_refs (list(str)): controls to reference for up vec of curve
        snap (bool): whether to snap dag to curve or not
        connect (bool): whether to connect the foll to the dag or not
    '''
    # nodes to attach dag to surface
    foll = cmds.createNode('transform', name=dag + '_custom_foll')

    poci = node.Node(dag, 'pointOnCurveInfo')
    matrix = node.Node(dag, 'fourByFourMatrix')
    mm = node.Node(foll, 'multMatrix')
    dm = node.Node(foll, 'decomposeMatrix')
    blend = node.Node(foll, 'blendVectors', custom_num_vecs=len(up_vec_refs))
    mult = node.Node(dag, 'multDoubleLinear')

    # Check to see if vector products exist
    vecps = dict()
    for ref in up_vec_refs:
        vecps[ref] = cmds.ls(ref + '_VECP')

    # If vector product doesn't exist, create it
    for ref in up_vec_refs:
        if vecps[ref]:
            vecps[ref] = node.Node(vecps[ref][0], 'vectorProduct', initialize=True)
        else:
            vecp1 = node.Node(ref, 'vectorProduct')
            vecp1.connect(operation=3,
                          input1Y=1,
                          matrix=ref + '.worldMatrix[0]',
                          normalizeOutput=1)
            vecps[ref] = vecp1

    # Create vec for cross product
    cross = node.Node(dag + '_CP', 'vectorProduct')
    cross.connect(operation=2,
                  input1=blend.node + '.output',
                  input2=poci.node + '.normalizedTangent',
                  normalizeOutput=1)

    # find closest point on surface and it's UV values
    dag_pos = cmds.xform(dag, t=True, ws=True, q=True)
    pos, par = comp.closest_point_on_curve(curve, dag_pos)
    attr.add_float(foll, 'parameter', default=par)

    if snap:
        cmds.xform(dag, t=(pos[0], pos[1], pos[2]), ws=True)

    # Create blend for up vector
    for i, ref in enumerate(up_vec_refs):
        cmds.connectAttr(vecps[ref].node + '.output', '{}.input{}'.format(blend.node, i + 1))
    # Multiply parameter by amount of vecs to get correct place in blend vector
    mult.connect(input1=foll + '.parameter',
                 input2=len(up_vec_refs) - 1,
                 output=blend.node + '.blend')

    # create matrix from POCI node
    poci.connect(inputCurve=curve + '.local',
                 parameter=foll + '.parameter'
                 )

    matrix.connect(in00=poci.node + '.normalizedTangentX',
                   in01=poci.node + '.normalizedTangentY',
                   in02=poci.node + '.normalizedTangentZ',

                   in10=blend.node + '.outputX',
                   in11=blend.node + '.outputY',
                   in12=blend.node + '.outputZ',

                   in20=cross.node + '.outputX',
                   in21=cross.node + '.outputY',
                   in22=cross.node + '.outputZ',

                   in30=poci.node + '.positionX',
                   in31=poci.node + '.positionY',
                   in32=poci.node + '.positionZ'
                   )

    mm.connect(matrixIn_0=matrix.node + '.output',
               matrixIn_1=foll + '.parentInverseMatrix[0]',

               matrixSum=dm.node + '.inputMatrix'
               )

    dm.connect(outputTranslate=foll + '.translate',
               outputRotate=foll + '.rotate')

    if connect:
        if snap == True:
            maintainOffset = False
        else:
            maintainOffset = True
        cmds.parentConstraint(foll, dag, mo=maintainOffset)

    return foll

def dag_to_curve_param(dag, curve, param):
    '''
    Attach dag's position to a curve at the given parameter
    '''
    poci = node.Node(dag, 'pointOnCurveInfo')
    comp_mtx = node.Node(dag, 'composeMatrix')
    mm = node.Node(dag, 'multMatrix')
    dm = node.Node(dag, 'decomposeMatrix')

    # Create param attribute if it doesnt exist already on dag
    if not cmds.ls(dag + '.parameter'):
        attr.add_float(dag, 'parameter', default=param, max=500)

    # Connect curve position to dag translate
    poci.connect(inputCurve=curve + '.local',
                 parameter=dag + '.parameter')

    comp_mtx.connect(inputTranslate=poci.node + '.position')

    mm.connect(matrixIn_0=comp_mtx.node + '.outputMatrix',
               matrixIn_1=dag + '.parentInverseMatrix[0]',

               matrixSum=dm.node + '.inputMatrix'
               )

    dm.connect(outputTranslate=dag + '.translate')


# def attach_to_surface(surface, dag, snap=True, scale=False):
#     # nodes to attach dag to surface
#     POSI = cmds.createNode('pointOnSurfaceInfo', name=dag + '_POSI')
#     matrix_node = cmds.createNode('fourByFourMatrix', name=dag + '_4X4')
#     foll = cmds.createNode('transform', name=dag + '_custom_foll')
#     mm = node.Node(foll, 'multMatrix')
#     # foll_MSC = cmds.createNode('millSimpleConstraint', name=foll + '_MSC')
#
#     # find closest point on surface and it's UV values
#     dag_pos = cmds.xform(dag, t=True, ws=True, q=True)
#     pos, parU, parV = comp.closest_point_on_surface(surface, dag_pos)
#
#     cmds.xform(dag, t=(pos[0], pos[1], pos[2]), ws=True)
#
#     # attach dag to surface
#     cmds.connectAttr(surface + '.local', POSI + '.inputSurface')
#     cmds.setAttr(POSI + '.parameterU', parU)
#     cmds.setAttr(POSI + '.parameterV', parV)
#
#     # create matrix from POSI node
#     cmds.connectAttr(POSI + '.normalizedTangentVX', matrix_node + '.in00')
#     cmds.connectAttr(POSI + '.normalizedTangentVY', matrix_node + '.in01')
#     cmds.connectAttr(POSI + '.normalizedTangentVZ', matrix_node + '.in02')
#     cmds.connectAttr(POSI + '.normalizedNormalX', matrix_node + '.in10')
#     cmds.connectAttr(POSI + '.normalizedNormalY', matrix_node + '.in11')
#     cmds.connectAttr(POSI + '.normalizedNormalZ', matrix_node + '.in12')
#     cmds.connectAttr(POSI + '.normalizedTangentUX', matrix_node + '.in20')
#     cmds.connectAttr(POSI + '.normalizedTangentUY', matrix_node + '.in21')
#     cmds.connectAttr(POSI + '.normalizedTangentUZ', matrix_node + '.in22')
#     cmds.connectAttr(POSI + '.positionX', matrix_node + '.in30')
#     cmds.connectAttr(POSI + '.positionY', matrix_node + '.in31')
#     cmds.connectAttr(POSI + '.positionZ', matrix_node + '.in32')
#
#     cmds.connectAttr(matrix_node + '.output', foll_MSC + '.inMatrix')
#     cmds.connectAttr(foll + '.parentInverseMatrix[0]', foll_MSC + '.parentInverseMatrix')
#
#     cmds.connectAttr(foll_MSC + '.outTranslate', foll + '.translate')
#     cmds.connectAttr(foll_MSC + '.outRotate', foll + '.rotate')
#
#     cmds.parentConstraint(foll, dag, mo=snap)
#     # constraint.simple_constraint(foll, dag, snap=snap)
#
#     if scale:
#         POSI0 = cmds.createNode('pointOnSurfaceInfo', name=dag + '_scale0_POSI')
#         POSI1 = cmds.createNode('pointOnSurfaceInfo', name=dag + '_scale1_POSI')
#         dist = cmds.createNode('math_DistancePoints', name=dag + '_DIST')
#         div = cmds.createNode('math_Divide', name=dag + '_DIV')
#
#         cmds.connectAttr(surface + '.local', POSI0 + '.inputSurface')
#         cmds.connectAttr(surface + '.local', POSI1 + '.inputSurface')
#         cmds.setAttr(POSI0 + '.parameterU', 0)
#         cmds.setAttr(POSI1 + '.parameterU', 1)
#         cmds.setAttr(POSI0 + '.parameterV', parV)
#         cmds.setAttr(POSI1 + '.parameterV', parV)
#
#         cmds.connectAttr(POSI0 + '.position', dist + '.input1')
#         cmds.connectAttr(POSI1 + '.position', dist + '.input2')
#
#         init_distance = cmds.getAttr(dist + '.output')
#         cmds.setAttr(div + '.input2', init_distance)
#         cmds.connectAttr(dist + '.output', div + '.input1')
#
#         cmds.connectAttr(div + '.output', foll + '.sz')
#
#     return foll

# def dag_to_curve(dag, curve):
#     ''' Attach a dag to the closest point on a curve
#
#     Args:
#         dag (str): name of dag node to attach to curve
#         curve (str): name of curve to attach to
#     '''
#
#     sel = om.MSelectionList()
#     sel.add(curve)
#
#     crv = om.MFnNurbsCurve()
#     crv.setObject(sel.getDagPath(0))
#
#     curve_length = crv.length()
#     max_param = crv.findParamFromLength(curve_length)
#
#     mscs = []
#     up_vecs = []
#     ctrl_params = []
#
#     # create control to control up vector of curve
#     ctrl_height = curve_length/10.0
#     param = 0.001
#     ctrl = dg.create_dag('C_crv_upvec', type='control', ctrl_type='triangle', offset=True)
#     ctrl_ofs = ctrl + '_OFS'
#     ctrl_zero = ctrl + '_ZERO'
#     line_loc = cmds.spaceLocator(name='C_crv_upvec_LOC')[0]
#     ctrl_poci = cmds.createNode('pointOnCurveInfo', name=ctrl + '_POCI')
#     dm = cmds.createNode('decomposeMatrix', name=ctrl + '_DM')
#     sub_vec = cmds.createNode('math_SubtractVector', name=ctrl + '_SV')
#     normalize = cmds.createNode('math_NormalizeVector', name=ctrl + '_NV')
#     ctrl_params.append(param)
#
#     cmds.connectAttr(curve + '.worldSpace[0]', ctrl_poci + '.inputCurve')
#     cmds.setAttr(ctrl_poci + '.parameter', param)
#
#     cmds.connectAttr(ctrl_poci + '.position', ctrl_zero + '.translate')
#     cmds.connectAttr(ctrl_poci + '.position', line_loc + '.translate')
#
#     cmds.setAttr(ctrl_ofs + '.ty', ctrl_height)
#     cmds.connectAttr(ctrl + '.worldMatrix[0]', dm + '.inputMatrix')
#
#     cmds.connectAttr(dm + '.outputTranslate', sub_vec + '.input1')
#     cmds.connectAttr(line_loc + '.translate', sub_vec + '.input2')
#
#     cmds.connectAttr(sub_vec + '.output', normalize + '.input')
#
#     line_curve = curves.curve_from_objects([ctrl, line_loc], name=ctrl + '_vec_CRV')
#     cmds.setAttr(line_curve + '.overrideEnabled', 1)
#     cmds.setAttr(line_curve + '.overrideDisplayType', 2)
#
#     up_vecs.append(normalize)
#
#     # create instance to control pos and rot of controls being attached
#     dag_poci = cmds.createNode('pointOnCurveInfo')
#     fbf = cmds.createNode('fourByFourMatrix')
#     msc = cmds.createNode('millSimpleConstraint')
#     z_vec = cmds.createNode('vectorProduct')
#     mscs.append(msc)
#
#     # connect vecs to pair blend
#     cmds.setAttr(z_vec + '.operation', 2)
#     cmds.connectAttr(normalize + '.output', z_vec + '.input1')
#     cmds.connectAttr(dag_poci + '.normalizedTangent', z_vec + '.input2')
#
#     cmds.connectAttr(curve + '.worldSpace[0]', dag_poci + '.inputCurve')
#     cmds.setAttr(dag_poci + '.parameter', param)
#     cmds.connectAttr(dag_poci + '.normalizedTangentX', fbf + '.in00')
#     cmds.connectAttr(dag_poci + '.normalizedTangentY', fbf + '.in01')
#     cmds.connectAttr(dag_poci + '.normalizedTangentZ', fbf + '.in02')
#     cmds.connectAttr(normalize + '.outputX', fbf + '.in10')
#     cmds.connectAttr(normalize + '.outputY', fbf + '.in11')
#     cmds.connectAttr(normalize + '.outputZ', fbf + '.in12')
#     cmds.connectAttr(z_vec + '.outputX', fbf + '.in20')
#     cmds.connectAttr(z_vec + '.outputY', fbf + '.in21')
#     cmds.connectAttr(z_vec + '.outputZ', fbf + '.in22')
#     cmds.connectAttr(dag_poci + '.positionX', fbf + '.in30')
#     cmds.connectAttr(dag_poci + '.positionY', fbf + '.in31')
#     cmds.connectAttr(dag_poci + '.positionZ', fbf + '.in32')
#     cmds.connectAttr(fbf + '.output', msc + '.inMatrix')
#
#     # attribute on dag to control the position along the curve
#     par_attr = attribute.add_float(dag, 'curveDistance', max_value=max_param)
#     cmds.connectAttr(par_attr, ctrl_poci + '.parameter')
#     cmds.connectAttr(par_attr, dag_poci + '.parameter')
#
#     # connect dag to curve
#     cmds.connectAttr(dag + '_ZERO.parentInverseMatrix[0]', msc + '.parentInverseMatrix')
#     cmds.connectAttr(msc + '.outTranslate', dag + '_ZERO.translate')
#     cmds.connectAttr(msc + '.outRotate', dag + '_ZERO.rotate')


# def linspace_curve(curve, num):
#     ''' Provide matrices that are an equal distribution along a curve
#
#     Args:
#         curve (str): name of curve to get information from
#         num (int): number of matrices to be returned
#     Return:
#          list[om.MMatrix()]: list of matrices
#     '''
#     sel = om.MSelectionList()
#     sel.add(curve)
#
#     crv = om.MFnNurbsCurve()
#     crv.setObject(sel.getDagPath(0))
#
#     curve_length = crv.length()
#     lengths = np.linspace(0, curve_length, num)
#     matrices = []
#     # create instance to control pos and rot of controls being attached
#     for length in lengths:
#         param = crv.findParamFromLength(length)
#         if param == 0:
#             param = .001
#         if length == lengths[-1]:
#             param -= .001
#
#         aim_vec = crv.tangent(param)
#         side_vec = crv.normal(param)
#         up_vec = aim_vec ^ side_vec
#         pos = crv.getPointAtParam(param)
#
#         mtx = maths.vectors_to_matrix(row1=aim_vec.normal(),
#                                       row2=up_vec.normal(),
#                                       row3=side_vec.normal(),
#                                       row4=pos)
#         matrices.append(mtx)
#
#     return matrices


def controls_on_curve(curve):
    '''
    Create controls to control the cvs of a curve
    Args:
        curve (str): name of curve for controls created
    Return:
        list[(str)]: list of controls created
    '''

    cvs = cmds.ls(curve + '.cv[*]', fl=True)
    ctrls = []
    for i, cv in enumerate(cvs):
        ctrl = cs.create_nurbscurve('{}_cv{}_CTRL'.format(curve, str(i)), 'cube')
        cs.change_color(ctrl, 'yellow')
        ctrls.append(ctrl)
        pos = cmds.xform(cv, t=True, ws=True, q=True)
        cmds.xform(ctrl, t=(pos[0], pos[1], pos[2]), ws=True)

        mm = node.Node('{}_cv{}'.format(curve, str(i)), 'multMatrix')
        dm = node.Node('{}_cv{}'.format(curve, str(i)), 'decomposeMatrix')

        mm.connect(matrixIn_0=ctrl + '.worldMatrix[0]',
                   matrixIn_1=curve + '.worldInverseMatrix[0]')
        dm.connect(inputMatrix=mm.node + '.matrixSum',
                   outputTranslate=cv)

    return ctrls


def curve_from_dags(name, dags, degree=2, attach_dags=True, rebuild=False):
    '''
    Creates a curve controlled by the given dag objects
    Args:
        name (str): name of curve
        dags list[(str)]: dags to control curve
        degree (int): degree of curve created
        attach_dags (bool): whether to attach the curve to the given dags
    Returns:
        (str): created curve
    '''
    cmds.select(clear=True)
    points = []
    num = len(dags)
    if degree > 1:
        if len(dags) < 4:
            num = 5
    for x in range(num):
        point = (x, 0, 0)
        points.append(point)
    crv = cmds.curve(p=points, name=name, degree=degree)

    if rebuild:
        cmds.rebuildCurve(crv,
                          ch=1,
                          replaceOriginal=1,
                          keepRange=0,
                          keepControlPoints=1,
                          keepTangents=0,
                          spans=len(points),
                          d=degree,
                          tol=0.01)

    if attach_dags:
        shape = cmds.listRelatives(crv, shapes=True, children=True)[0]
        for x, each in enumerate(dags):
            if degree != 1 and len(dags) == 3:
                if x == 0:
                    dm = node.Node(name=each, node_type='decomposeMatrix')

                    dm.connect(inputMatrix=each + '.worldMatrix[0]',
                               outputTranslate=shape + '.controlPoints[0]')
                    dm.connect(outputTranslate=shape + '.controlPoints[1]')
                elif x == 2:
                    dm = node.Node(name=each, node_type='decomposeMatrix')

                    dm.connect(inputMatrix=each + '.worldMatrix[0]',
                               outputTranslate=shape + '.controlPoints[3]')
                    dm.connect(outputTranslate=shape + '.controlPoints[4]')
                else:
                    dm = node.Node(name=each, node_type='decomposeMatrix')

                    dm.connect(inputMatrix=each + '.worldMatrix[0]',
                               outputTranslate=shape + '.controlPoints[2]')
            else:
                dm = node.Node(name=each, node_type='decomposeMatrix')

                dm.connect(inputMatrix=each + '.worldMatrix[0]',
                           outputTranslate=shape + '.controlPoints[{}]'.format(str(x)))
    else:
        shape = cmds.listRelatives(crv, shapes=True, children=True)[0]
        for x, each in enumerate(dags):
            pos = cmds.xform(each, t=True, ws=True, q=True)
            cmds.setAttr('{}.controlPoints[{}]'.format(shape, x), pos[0], pos[1], pos[2])

    return crv
