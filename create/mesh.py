import maya.cmds as cmds
import maya.api.OpenMaya as om

import nodes as node

import custom_utils.control_shapes as cs
import custom_utils.attribute as attr
import custom_utils.maths as maths
import nurbs as nrb
reload(cs)
reload(attr)
reload(maths)
reload(nrb)


def create_proxy(dags, side='z', size=3, name=None):
    '''
    Creates a proxy geo based on the matrices of a given chain
    Args:
        dags list[(str)]: list of dags to create geometry from
        side (str): indicator of side vector
        size (float): how wide the plane will be
        name (str): name of plane, if not given bases name off first dag
    '''

    # Create plane
    if not name:
        name = dags[0] + '_PROXY'
    subds = len(dags) - 1
    plane = cmds.polyPlane(name=name, sx=subds, sy=1)[0]

    for i, dag in enumerate(dags):
        mtx = cmds.xform(dag, matrix=True, ws=True, q=True)
        pos = cmds.xform(dag, t=True, ws=True, q=True)
        pos = om.MVector(pos[0], pos[1], pos[2])
        pos1 = (maths.axis_in_matrix(mtx, '+' + side, add_pos=False) * size) + pos
        pos2 = (maths.axis_in_matrix(mtx, '-' + side, add_pos=False) * size) + pos
        cmds.xform(plane + '.vtx[{}]'.format(i), t=pos1, ws=True)
        cmds.xform(plane + '.vtx[{}]'.format(i + len(dags)), t=pos2, ws=True)

    return plane


def project_to_mesh(mesh, ref, dag, offset=True, maintainOffset=True, foll_up_vec=False, up_vec_distance=.01, up_vector=(0, 1, 0)):
    '''
    Uses an object to project another object to the closest point on the mesh
    Args:
        mesh(str): mesh to project onto
        ref(str): object to reference position and up vector
        dag(str): dag to attach
        offset(bool): whether to put the transformations onto an offset node or the object
        maintainOffset(bool): whether to maintain the dag's position or move it onto the mesh
        foll_up_vec(bool): whether to use a second follicle for the rotation of the eyes or not
        up_vec_distance(float): distance in U that the follicle up vec will be placed
        up_vector tuple(float, float, float): up vector to extract from reference object
    '''
    cpom = node.Node('{}_foll'.format(dag), 'closestPointOnMesh')
    fbfm = node.Node('{}_foll'.format(dag), 'fourByFourMatrix')
    dcpm = node.Node('{}_foll'.format(dag), 'decomposeMatrix')
    yvec = node.Node('{}_foll_yvec'.format(dag), 'vectorProduct')
    zvec = node.Node('{}_foll_zvec'.format(dag), 'vectorProduct')
    mm = node.Node('{}_foll'.format(dag), 'multMatrix')

    # Create follicle if indicated by flag
    if foll_up_vec:
        foll = cmds.createNode('follicle', name=dag + '_upVecFollShape')
        mesh_shape = cmds.listRelatives(mesh, s=True)[0]
        sub_par = node.Node(foll + '_parsub', node_type='plusMinusAverage')
        sub_up = node.Node('{}_foll_upvec'.format(dag), node_type='plusMinusAverage')
    ref_dcpm = cmds.ls('{}_{}_DCPM'.format(dag, ref))

    # Creates decomposeMatrix if it doesn't already exist
    if not ref_dcpm:
        ref_dcpm = node.Node('{}_{}'.format(dag, ref), 'decomposeMatrix')
        ref_dcpm.connect(inputMatrix=ref + '.worldMatrix[0]')
    else:
        ref_dcpm = node.Node('{}_{}'.format(dag, ref), 'decomposeMatrix', initialize=True)

    if offset:
        ofs = cmds.createNode('transform', name=dag + '_OFS')
    else:
        ofs = None

    # Set up connections for point on mesh node
    cpom.connect(inMesh=mesh + '.worldMesh[0]',
                 inPosition=ref_dcpm.node + '.outputTranslate')

    # Create y and z vecs
    if foll_up_vec:
        cmds.setAttr(foll + ".visibility", False)
        cmds.connectAttr(mesh_shape + '.outMesh', foll + '.inputMesh')
        cmds.connectAttr(mesh_shape + '.worldMatrix[0]', foll + '.inputWorldMatrix')

        cpom.connect(parameterU=foll + '.parameterU',
                     parameterV=sub_par.node + '.input1D[0]')
        sub_par.connect(input1D_1=up_vec_distance,
                        operation=2,
                        output1D=foll + '.parameterV')
        sub_up.connect(operation=2,
                       input3D_0=foll + '.outTranslate',
                       input3D_1=cpom.node + '.position')
        yvec.connect(input1=sub_up.node + '.output3D',
                     operation=0,
                     normalizeOutput=1)
    else:
        yvec.connect(matrix=ref + '.worldMatrix[0]',
                     input1=up_vector,
                     operation=3,
                     normalizeOutput=1)
    zvec.connect(operation=2,
                 input1=yvec.node + '.output',
                 input2=cpom.node + '.normal',
                 normalizeOutput=1)

    # Connect up to matrix
    fbfm.connect(in00=cpom.node + '.normalX',
                 in01=cpom.node + '.normalY',
                 in02=cpom.node + '.normalZ',
                 in10=yvec.node + '.outputX',
                 in11=yvec.node + '.outputY',
                 in12=yvec.node + '.outputZ',
                 in20=zvec.node + '.outputX',
                 in21=zvec.node + '.outputY',
                 in22=zvec.node + '.outputZ',
                 in30=cpom.node + '.positionX',
                 in31=cpom.node + '.positionY',
                 in32=cpom.node + '.positionZ'
                 )
    if ofs:
        mm.connect(matrixIn_0=fbfm.node + '.output',
                   matrixIn_1=ofs + '.parentInverseMatrix[0]')

        dcpm.connect(inputMatrix=mm.node + '.matrixSum',
                     outputTranslate=ofs + '.translate',
                     outputRotate=ofs + '.rotate')

        parent = cmds.listRelatives(dag, p=True)

        # Reorganize into hierarchy
        if parent:
            parent = parent[0]
            cmds.parent(ofs, parent)
        cmds.parent(dag, ofs)

        if not maintainOffset:
            cmds.setAttr(dag + '.t', 0, 0, 0)
            cmds.setAttr(dag + '.r', 0, 0, 0)
    else:
        mm.connect(matrixIn_0=fbfm.node + '.output',
                   matrixIn_1=dag + '.parentInverseMatrix[0]')

        dcpm.connect(inputMatrix=fbfm.node + '.output',
                     outputTranslate=dag + '.translate',
                     outputRotate=dag + '.rotate')


def attach_to_mesh(mesh, dag):
    '''
    Attaches dag using custom made follicle
    @param mesh:
    @param dag:
    @return:
    '''

    # reference two points, the closest point and then a vertex on that face, make two closestPointOnMesh nodes and use that as an aim vector, use the normal of the face as an up vector
    pass

def attach_to_wrap(mesh, dag, size=1):
    '''
    Attaches dag to a mesh by a polygon plane wrapped to the mesh
    Args:
        mesh (str): mesh to attach to
        dag(str): dag to attach
        size(float): size of plane
    Returns:
        list[(str)]: transform of follicle, follicle shape
    '''

    # Create plane
    name = dag + '_rivet_PROXY'
    plane = cmds.polyPlane(name=name, sx=1, sy=1)[0]

    # Bring plane to dag
    cmds.parent(plane, dag)
    cmds.setAttr(plane + '.t', 0, 0, 0)
    cmds.setAttr(plane + '.r', 0, 0, 0)
    cmds.setAttr(plane + '.s', size, size, size)
    cmds.parent(plane, 'Placer_world_GRP')

    # Wrap plane
    sel = [plane, mesh]
    cmds.select(sel)
    cmds.CreateWrap(sel)
    cmds.select(clear=True)

    # Create follicle and attach follicle to plane
    transform, foll = attach_to_follicle(plane, dag, uv=[0.5, 0.5])

    return [transform, foll]

def attach_to_follicle(mesh, dag, uv=[]):
    '''
    Attaches dag to a mesh by using a follicle
    Args:
        mesh (str): mesh to attach dag to
        dag (str): dag to attach to mesh
        uv list[float, float]: if given will attach at the given uvs, if not will find the closest uv to the dag
    Returns:
        list[(str)]: transform of follicle, follicle shape
    '''

    # Create follicle
    mesh_shape = cmds.listRelatives(mesh, s=True)[0]
    transform = cmds.createNode('transform', name=dag + '_foll')
    foll = cmds.createNode('follicle', name=dag + '_follShape', parent=transform)
    cmds.connectAttr(foll + ".outTranslate", transform + ".t", force=True)
    cmds.connectAttr(foll + ".outRotate", transform + ".r", force=True)
    cmds.setAttr(foll + ".visibility", False)
    cmds.connectAttr(mesh_shape + '.outMesh', foll + '.inputMesh')
    cmds.connectAttr(mesh_shape + '.worldMatrix[0]', foll + '.inputWorldMatrix')

    # Initialize mesh
    obj = om.MSelectionList()
    obj.add(mesh)
    geo = om.MFnMesh(obj.getComponent(0)[0])

    if uv:
        u = uv[0]
        v = uv[1]
    else:
        # Get position of dag then the UVs at that position
        pos = cmds.xform(dag, t=True, ws=True, q=True)
        point = om.MPoint(pos[0], pos[1], pos[2])
        cp = geo.getClosestPoint(point)[0]
        u, v, face = geo.getUVAtPoint(cp)

    cmds.setAttr(foll + '.parameterU', u)
    cmds.setAttr(foll + '.parameterV', v)

    # Connect follicle to dag
    cmds.parentConstraint(transform, dag, mo=True)

    return [transform, foll]

def two_follicle_rivet(mesh, dag, foll_vec=(0, 1, 0), offset=True, maintainOffset=True):
    '''
    Attaches dag to a mesh by using two follicles, one of them being referenced as the up vector for a more controlled rotation
    Args:
        mesh (str): mesh to attach dag to
        dag (str): dag to attach to mesh
        foll_vec tuple(int): vec from the first follicle to position the "up follicle"
        maintainOffset(bool): whether to maintain the dag's position or move it onto the mesh
    Returns:
        list[(str)]: transform of follicle, follicle shape
    '''
    # Initialize mesh
    obj = om.MSelectionList()
    obj.add(mesh)
    geo = om.MFnMesh(obj.getComponent(0)[0])
    mesh_shape = cmds.listRelatives(mesh, s=True)[0]

    # Create first follicle
    transform = cmds.createNode('transform', name=dag + '_mainfoll')
    main_foll = cmds.createNode('follicle', name=dag + '_mainfollShape', parent=transform)
    cmds.connectAttr(main_foll + ".outTranslate", transform + ".t", force=True)
    cmds.connectAttr(main_foll + ".outRotate", transform + ".r", force=True)
    cmds.setAttr(main_foll + ".visibility", False)
    cmds.connectAttr(mesh_shape + '.outMesh', main_foll + '.inputMesh')
    cmds.connectAttr(mesh_shape + '.worldMatrix[0]', main_foll + '.inputWorldMatrix')

    # Get position of dag then the UVs at that position
    pos = cmds.xform(dag, t=True, ws=True, q=True)
    point = om.MPoint(pos[0], pos[1], pos[2])
    cp = geo.getClosestPoint(point)[0]
    u, v, face = geo.getUVAtPoint(cp)

    cmds.setAttr(main_foll + '.parameterU', u)
    cmds.setAttr(main_foll + '.parameterV', v)

    # Create second follicle
    transform = cmds.createNode('transform', name=dag + '_upfoll')
    up_foll = cmds.createNode('follicle', name=dag + '_upfollShape', parent=transform)
    cmds.connectAttr(up_foll + ".outTranslate", transform + ".t", force=True)
    cmds.connectAttr(up_foll + ".outRotate", transform + ".r", force=True)
    cmds.setAttr(up_foll + ".visibility", False)
    cmds.connectAttr(mesh_shape + '.outMesh', up_foll + '.inputMesh')
    cmds.connectAttr(mesh_shape + '.worldMatrix[0]', up_foll + '.inputWorldMatrix')

    # Get position of dag then the UVs at that position
    vec1 = om.MVector(foll_vec[0], foll_vec[1], foll_vec[2])
    vec2 = om.MVector(pos[0], pos[1], pos[2])
    vec3 = vec1 + vec2
    up_point = om.MPoint(vec3[0], vec3[1], vec3[2])
    cp = geo.getClosestPoint(up_point)[0]
    u, v, face = geo.getUVAtPoint(cp)

    cmds.setAttr(up_foll + '.parameterU', u)
    cmds.setAttr(up_foll + '.parameterV', v)

    # Create network for the output 'follicle'
    # cpom = node.Node('{}_foll'.format(dag), 'closestPointOnMesh')
    fbfm = node.Node('{}_foll'.format(dag), 'fourByFourMatrix')
    dcpm = node.Node('{}_foll'.format(dag), 'decomposeMatrix')
    yvec = node.Node('{}_foll_yvec'.format(dag), 'vectorProduct')
    zvec = node.Node('{}_foll_zvec'.format(dag), 'vectorProduct')
    pma = node.Node('{}_foll_yvec'.format(dag), 'plusMinusAverage')
    mm = node.Node('{}_foll'.format(dag), 'multMatrix')

    if offset:
        ofs = cmds.createNode('transform', name=dag + '_OFS')
    else:
        ofs = None

    # Set up connections for point on mesh node
    # Create y and z vecs
    pma.connect(operation=2,
                input3D_0=up_foll + '.outTranslate',
                input3D_1=main_foll + '.outTranslate')
    yvec.connect(input1=pma.node + '.output3D',
                 operation=0,
                 normalizeOutput=1)
    zvec.connect(operation=2,
                 input1=yvec.node + '.output',
                 input2=main_foll + '.outNormal',
                 normalizeOutput=1)

    # Connect up to matrix
    fbfm.connect(in00=main_foll + '.outNormalX',
                 in01=main_foll + '.outNormalY',
                 in02=main_foll + '.outNormalZ',
                 in10=yvec.node + '.outputX',
                 in11=yvec.node + '.outputY',
                 in12=yvec.node + '.outputZ',
                 in20=zvec.node + '.outputX',
                 in21=zvec.node + '.outputY',
                 in22=zvec.node + '.outputZ',
                 in30=main_foll + '.outTranslateX',
                 in31=main_foll + '.outTranslateY',
                 in32=main_foll + '.outTranslateZ',
                 )
    if ofs:
        mm.connect(matrixIn_0=fbfm.node + '.output',
                   matrixIn_1=ofs + '.parentInverseMatrix[0]')

        dcpm.connect(inputMatrix=mm.node + '.matrixSum',
                     outputTranslate=ofs + '.translate',
                     outputRotate=ofs + '.rotate')

        parent = cmds.listRelatives(dag, p=True)

        # Reorganize into hierarchy
        if parent:
            parent = parent[0]
            cmds.parent(ofs, parent)
        cmds.parent(dag, ofs)

        if not maintainOffset:
            cmds.setAttr(dag + '.t', 0, 0, 0)
            cmds.setAttr(dag + '.r', 0, 0, 0)
    else:
        mm.connect(matrixIn_0=fbfm.node + '.output',
                   matrixIn_1=dag + '.parentInverseMatrix[0]')

        dcpm.connect(inputMatrix=fbfm.node + '.output',
                     outputTranslate=dag + '.translate',
                     outputRotate=dag + '.rotate')


    # # Connect follicle to dag
    # cmds.parentConstraint(transform, dag, mo=True)