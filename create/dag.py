import maya.cmds as cmds
import maya.api.OpenMaya as om

import nodes as node

import custom_utils.control_shapes as cs
import custom_utils.attribute as attr
import custom_utils.maths as maths
import custom_utils.constraint as con
import nurbs as nrb
reload(cs)
reload(attr)
reload(maths)
reload(nrb)


# mirror an object over the x axis
def mirror_object(object, mirrored_object):
    mtx = cmds.xform(object, matrix=True, ws=True, q=True)
    new_mtx = maths.mirror_matrix(mtx)
    cmds.xform(mirrored_object, matrix=new_mtx, ws=True)


def create_mirror_objs(objects, side='Lf'):
    for each in objects:
        mirrors = {'L':'R',
                   'R':'L',
                   'Lf':'Rt',
                   'Rt':'Lf'}
        new_name = mirrors[side] + '_' + each.split(side + '_')[1]
        obj = cmds.duplicate(each, name=new_name)[0]
        cmds.parent(obj, world=True)
        mirror_object(each, obj)


def snap_to_dag(source, target):
    '''Moves one dag object to the position and rotation of another

    Args:
         source (str): object to reference for transforms
         target (str): object to move to source object
    '''

    pos = cmds.xform(source, t=True, ws=True, q=True)
    rot = cmds.xform(source, ro=True, ws=True, q=True)

    cmds.xform(target, t=pos, ws=True)
    cmds.xform(target, ro=rot, ws=True)


def dags_on_curve(up_curve, curve, name, dag_type, dags=[], num_dags=10):
    '''
    Creates an even distribution of dag objects along a curve
    Args:
        up_curve (str): curve for up vector of dags
        curve (str): curve to attach bones to
        name (str): base name of dags
        dag_type (str): type of dag created
        dags list[(str)]: list of dags to attach to curve, if not given then function defaults to num_dags
        num_dags int: number of dags created
    Return:
        list[(str)]: list of zero nodes for the dags
    '''
    attr.add_float(curve, 'parameter', default=0, min=-1)
    attr.add_float(curve, 'parameterScale', default=1)

    # Get main curve info
    sel = om.MSelectionList()
    sel.add(curve)
    sel.add(up_curve)

    crv = om.MFnNurbsCurve()
    up_crv = om.MFnNurbsCurve()
    crv.setObject(sel.getDagPath(0))
    up_crv.setObject(sel.getDagPath(1))

    curve_length = crv.length()
    max_param = crv.findParamFromLength(curve_length) - .001

    # create control to control up vector of curve
    if dags:
        num_dags = len(dags)
    length_increment = curve_length/float(num_dags)
    length = 0.001
    dag_zeros = []
    if dags:
        num_dags = len(dags)
    else:
        num_dags = num_dags + 1
    for x in range(num_dags):
        if x != 0:
            length += length_increment
        par = crv.findParamFromLength(length)
        up_par = up_crv.findParamFromLength(length)
        cmds.select(clear=True)

        if dags:
            dag = dags[x]
        else:
            if dag_type == 'bone':
                dag = cmds.joint(name=name + '_{}_JNT'.format(x))
            elif dag_type == 'locator':
                dag = cmds.spaceLocator(name=name + '_{}_LOC'.format(x))[0]
        if not dags:
            zero = cmds.createNode('transform', name=dag + '_ZRO')
        poci = cmds.createNode('pointOnCurveInfo', name=dag + '_POCI')
        up_poci = cmds.createNode('pointOnCurveInfo', name=dag + '_up_POCI')
        sub_vec = cmds.createNode('plusMinusAverage', name=dag + '_upvec_SV')
        up_vec_normal = cmds.createNode('vectorProduct', name=dag + '_upvec_VP')
        cp = cmds.createNode('vectorProduct', name=poci + 'cross_VP')
        mtx = cmds.createNode('fourByFourMatrix', name=poci + '_MTX')
        param_add = cmds.createNode('plusMinusAverage', name=dag + '_param_ADD')
        dm = cmds.createNode('decomposeMatrix', name=dag + '_DM')
        mm = cmds.createNode('multMatrix', name=dag + '_MM')
        param_sub = cmds.createNode('plusMinusAverage', name=dag + '_param_SUB')
        param_cond = cmds.createNode('condition', name=dag + '_param_COND')
        param_scale = cmds.createNode('multDoubleLinear', name=dag + '_paramScale_MDL')
        zero_cond = cmds.createNode('condition', name=dag + '_zeroCheck_COND')

        if par > max_param:
            default = max_param
        elif par < 0.001:
            default = 0.001
        else:
            default = par
        attr.add_float(dag, 'parameter', min=0.001, max=max_param, default=default)

        if not dags:
            cmds.parent(dag, zero)

        cmds.connectAttr(dag + '.parameter', param_add + '.input1D[0]')
        cmds.connectAttr(curve + '.parameter', param_add + '.input1D[1]')

        # condition for if the parameter goes over one
        cmds.connectAttr(param_add + '.output1D', param_sub + '.input1D[0]')
        cmds.setAttr(param_sub + '.input1D[1]', 1)

        cmds.setAttr(param_cond + '.operation', 2)
        cmds.connectAttr(param_add + '.output1D', param_cond + '.firstTerm')
        cmds.setAttr(param_cond + '.secondTerm', 1)
        cmds.connectAttr(param_sub + '.output1D', param_cond + '.colorIfTrueR')
        cmds.connectAttr(param_add + '.output1D', param_cond + '.colorIfFalseR')

        # Connect scale to condition
        cmds.connectAttr(curve + '.parameterScale', param_scale + '.input1')
        cmds.connectAttr(param_cond + '.outColorR', param_scale + '.input2')

        # Connect scale to zero condition
        cmds.setAttr(zero_cond + '.operation', 5)
        cmds.setAttr(zero_cond + '.colorIfTrueR', .001)
        cmds.connectAttr(param_scale + '.output', zero_cond + '.firstTerm')
        cmds.connectAttr(param_scale + '.output', zero_cond + '.colorIfFalseR')


        # connect curves to POCIs
        cmds.connectAttr(curve + '.worldSpace[0]', poci + '.inputCurve')
        cmds.connectAttr(zero_cond + '.outColorR', poci + '.parameter')

        cmds.connectAttr(zero_cond + '.outColorR', up_poci + '.parameter')
        cmds.connectAttr(up_curve + '.worldSpace[0]', up_poci + '.inputCurve')
        # cmds.setAttr(up_poci + '.parameter', up_par)

        # x vector
        cmds.connectAttr(poci + '.normalizedTangentX', mtx + '.in00')
        cmds.connectAttr(poci + '.normalizedTangentY', mtx + '.in01')
        cmds.connectAttr(poci + '.normalizedTangentZ', mtx + '.in02')

        # y vector
        cmds.setAttr(sub_vec + '.operation', 2)
        cmds.connectAttr(up_poci + '.position', sub_vec + '.input3D[0]')
        cmds.connectAttr(poci + '.position', sub_vec + '.input3D[1]')

        cmds.setAttr(up_vec_normal + '.operation', 0)
        cmds.setAttr(up_vec_normal + '.normalizeOutput', 1)
        cmds.connectAttr(sub_vec + '.output3D', up_vec_normal + '.input1')

        cmds.connectAttr(up_vec_normal + '.outputX', mtx + '.in10')
        cmds.connectAttr(up_vec_normal + '.outputY', mtx + '.in11')
        cmds.connectAttr(up_vec_normal + '.outputZ', mtx + '.in12')

        # z vector
        cmds.setAttr(cp + '.operation', 2)
        cmds.setAttr(cp + '.normalizeOutput', 1)
        cmds.connectAttr(up_vec_normal + '.output', cp + '.input1')
        cmds.connectAttr(poci + '.normalizedTangent', cp + '.input2')

        cmds.connectAttr(cp + '.outputX', mtx + '.in20')
        cmds.connectAttr(cp + '.outputY', mtx + '.in21')
        cmds.connectAttr(cp + '.outputZ', mtx + '.in22')

        # position of matrix
        cmds.connectAttr(poci + '.positionX', mtx + '.in30')
        cmds.connectAttr(poci + '.positionY', mtx + '.in31')
        cmds.connectAttr(poci + '.positionZ', mtx + '.in32')

        # constrain loc to matrix
        if dags:
            zero = dag
        cmds.connectAttr(mtx + '.output', mm + '.matrixIn[0]')
        cmds.connectAttr(zero + '.parentInverseMatrix[0]', mm + '.matrixIn[1]')

        cmds.connectAttr(mm + '.matrixSum', dm + '.inputMatrix')

        cmds.connectAttr(dm + '.outputTranslate', zero + '.translate')
        cmds.connectAttr(dm + '.outputRotate', zero + '.rotate')

        dag_zeros += [zero]

    return dag_zeros


def dags_to_mesh(name, curve, mesh, dag_type='bone', ctrl_type='sphere', num_dags=10):
    '''
    Creates an even distribution of dag objects along a curve
    Args:
        name (str): base name of dags
        curve (str): curve to attach bones to
        mesh (str): shape node of mesh to connect to
        dag_type (str): type of dag created
        num_dags int: number of dags created
    Return:
        list[(str)]: list of zero nodes for the dags
    '''
    attr.add_float(curve, 'parameter', default=0)

    # Get main curve info
    sel = om.MSelectionList()
    sel.add(curve)

    crv = om.MFnNurbsCurve()
    crv.setObject(sel.getDagPath(0))

    curve_length = crv.length()
    max_param = crv.findParamFromLength(curve_length) - .001

    # create control to control up vector of curve
    length_increment = curve_length/float(num_dags)
    length = 0.001
    dag_zeros = []
    for x in range(num_dags + 1):
        if x != 0:
            length += length_increment
        par = crv.findParamFromLength(length)
        cmds.select(clear=True)

        if dag_type == 'bone':
            dag = cmds.joint(name=name + '_{}_BONE'.format(x))
        elif dag_type == 'locator':
            dag = cmds.spaceLocator(name=name + '_{}_LOC'.format(x))[0]
        elif dag_type == 'control':
            dag = cs.create_nurbscurve(name + '_{}_CTRL'.format(x), ctrl_type)
        zero = cmds.createNode('transform', name=dag + '_ZERO')
        poci = node.Node(dag, 'pointOnCurveInfo')
        cpom = node.Node(dag, 'closestPointOnMesh')
        fbfm = node.Node(dag, 'fourByFourMatrix')
        dcpm = node.Node(dag, 'decomposeMatrix')
        y_vp = node.Node(dag + '_yvec', 'vectorProduct')
        z_vp = node.Node(dag + '_zvec', 'vectorProduct')

        cmds.parent(dag, zero)

        if par > max_param:
            default = max_param
        elif par < 0.001:
            default = 0.001
        else:
            default = par
        attr.add_float(dag, 'parameter', max=max_param, min=0.001, default=default)

        # Points on mesh and curve
        poci.connect(inputCurve=curve + '.worldSpace[0]',
                     parameter=dag + '.parameter')
        cpom.connect(inMesh=mesh + '.worldMesh[0]',
                     inPosition=poci.node + '.position',
                     inputMatrix=mesh + '.worldMatrix[0]')

        # Create remaining vectors for matrix
        z_vp.connect(input1=poci.node + '.normalizedTangent',
                     input2=cpom.node + '.normal',
                     operation=2,
                     normalizeOutput=1)
        y_vp.connect(input1=poci.node + '.normalizedTangent',
                     input2=z_vp.node + '.output',
                     operation=2,
                     normalizeOutput=1)

        # Connect vectors to matrix
        fbfm.connect(in00=poci.node + '.normalizedTangentX',
                     in01=poci.node + '.normalizedTangentY',
                     in02=poci.node + '.normalizedTangentZ',
                     in10=y_vp.node + '.outputX',
                     in11=y_vp.node + '.outputY',
                     in12=y_vp.node + '.outputZ',
                     in20=z_vp.node + '.outputX',
                     in21=z_vp.node + '.outputY',
                     in22=z_vp.node + '.outputZ',
                     in30=cpom.node + '.positionX',
                     in31=cpom.node + '.positionY',
                     in32=cpom.node + '.positionZ')

        # Connect matrix to dag
        dcpm.connect(inputMatrix=fbfm.node + '.output',
                     outputTranslate=zero + '.translate',
                     outputRotate=zero + '.rotate')

        dag_zeros += [zero]

    return dag_zeros


def ctrls_to_crv(name, curve, ctrl_type='sphere', num_ctrls=10):
    '''
    Creates an even distribution of controls along a curve, no rotation
    Args:
        name (str): base name of dags
        curve (str): curve to attach bones to
        ctrl_type (str): type of ctrl created
        num_ctrls int: number of dags created
    Return:
        list[(str)]: list of zero nodes for the ctrls
    '''
    attr.add_float(curve, 'parameter', default=0)

    # Get main curve info
    sel = om.MSelectionList()
    sel.add(curve)

    crv = om.MFnNurbsCurve()
    crv.setObject(sel.getDagPath(0))

    curve_length = crv.length()
    max_param = crv.findParamFromLength(curve_length) - .001

    # create control to control up vector of curve
    length_increment = curve_length/float(num_ctrls)
    length = 0.001
    dag_zeros = []
    for x in range(num_ctrls + 1):
        if x != 0:
            length += length_increment
        par = crv.findParamFromLength(length)
        cmds.select(clear=True)

        dag = cs.create_nurbscurve(name=name + '_{}_CTRL'.format(x), shape_type=ctrl_type)
        zero = cmds.createNode('transform', name=dag + '_ZERO')
        poci = node.Node(dag, 'pointOnCurveInfo')

        cmds.parent(dag, zero)

        if par > max_param:
            default = max_param
        elif par < 0.001:
            default = 0.001
        else:
            default = par
        attr.add_float(dag, 'parameter', max=max_param, min=0.001, default=default)

        poci.connect(inputCurve=curve + '.worldSpace[0]',
                     parameter=dag + '.parameter',
                     position=zero + '.translate')

        dag_zeros += [zero]

    return dag_zeros


def curve_from_dags(name, dags, num_dags=10):
    '''
    Creates a curve and up curve based on the given controls, attaches dags to
    the created curve with both rotation and translation
    Args:
        name (str): base name of dags and curves created
        dags list[str]: dags to reference
        num_dags int: num dags created
    Return:
        list[str]: list of zeros of dags created
    '''

    # create curves and bones
    crv = nrb.curve_from_dags('{}_CRV'.format(name), dags)
    up_crv = cmds.duplicate(crv, name='{}_up_CRV'.format(name))[0]

    # move cvs of up curve
    cvs = cmds.ls(up_crv + '.cv[*]', fl=True)
    matrices = []
    for each in dags:
        mtx = cmds.xform(each, matrix=True, ws=True, q=True)
        matrices += [mtx]

    crv_jnts = []
    for i, mtx in enumerate(matrices):
        x, y, z, pos = maths.matrix_to_vectors(mtx)
        cv_pos = y + pos

        jnt = node.Node(cvs[i] + '_JNT', 'joint', suffix=False, list_add=crv_jnts)

        cmds.xform(cvs[i], t=cv_pos, ws=True)
        jnt_pos = nrb.closest_point_on_curve(up_crv, cv_pos)
        cmds.xform(jnt.node, t=jnt_pos, ws=True)

        # constrain joint to a given control
        mcon = con.matrix_constraint(dags[i], jnt.node, maintain_offset=True)
        cmds.setAttr(mcon + '.posVector', 0, 1, 0)

    skc = cmds.skinCluster(crv_jnts, up_crv)[0]

    # create bones for main spine
    bone_zeros = dags_on_curve(up_crv, crv, name, 'bone', num_dags=num_dags)

    return bone_zeros


def get_parent(dag, level=None):
    """Queries the parent of a node

    Args:
        dag (str): node to get the parent of
        level (int): amount of parents above node to query, 'top' will get
                     the top node of the hierarchy
    Return:
        (str): parent node
    """

    if not level or level == 0:
        parent = cmds.listRelatives(dag, p=True)[0]
    elif level == 'top':
        parent = cmds.listRelatives(dag, ap=True)[-1]
    else:
        parents = cmds.listRelatives(dag, ap=True)
        if level >= len(parents):
            parent = parents[-1]
        else:
            parent = parents[level]

    return parent

def movable_pivot(name, ctrl, scnd_ctrl=None, ctrl_type='cube'):
    '''Creates a movable pivot under a given control

    Args:
         ctrl (str): control to make a movable pivot under
         ctrl_type (str): type of control that will be created
    '''
    if not scnd_ctrl:
        scnd_ctrl = ctrl.split('CTL')[0] + 'SCND_CTL'
    pivot = name + 'Pivot_0_CTL'

    if not scnd_ctrl:
        cs.create_nurbscurve(scnd_ctrl, ctrl_type)
    cs.create_nurbscurve(pivot, 'diamond')

    if not scnd_ctrl:
        ctrl_zero = cmds.createNode('transform', name=scnd_ctrl + '_ZRO')
    else:
        ctrl_zero = cmds.listRelatives(scnd_ctrl, p=True)[0]
    pivot_zero = cmds.createNode('transform', name=pivot + '_ZRO')

    reverse = cmds.createNode('multiplyDivide', name=pivot + '_MULT')

    # Get position of control
    pos = cmds.xform(ctrl_zero, t=True, ws=True, q=True)
    rot = cmds.xform(ctrl_zero, ro=True, ws=True, q=True)
    cmds.xform(pivot_zero, t=pos, ws=True)
    cmds.xform(pivot_zero, ro=rot, ws=True)

    if not scnd_ctrl:
        cmds.parent(scnd_ctrl, ctrl_zero)
    cmds.parent(pivot, pivot_zero)
    cmds.parent(ctrl_zero, pivot)
    cmds.parent(pivot_zero, ctrl)

    cmds.connectAttr(pivot + '.t', ctrl + '.rotatePivot')

    for axis in ['X', 'Y', 'Z']:
        cmds.setAttr(reverse + '.input2{}'.format(axis), -1)
    cmds.connectAttr(pivot + '.t', reverse + '.input1')

    cmds.connectAttr(reverse + '.output', ctrl_zero + '.t')

# def section_controls(follow, side, name, default_pos=(0.0, 3.0, 0.0), function_attrs=None, **kwargs):
#     '''
#
#     Args:
#         follow (str): control/dag node that the section control follows
#         side (str): C/L/R side control is on, influences color
#         name (str): base name for control
#         default_pos tuple(float, float, float): default position for positioning
#                                                 of control in world space above
#                                                 followed control
#         function_attrs list[(str)]: attributes to create proxy attributes of
#         **kwargs: key is the attribute name, value is a list of the objects
#                   whose visibility is being driven. If value is an attribute
#                   then the section attribute will be proxy to that attribute.
#     Return:
#          str: node name for section control
#     '''
#     # get color of control
#     if side == 'L':
#         color = 18
#     elif side == 'R':
#         color = 13
#     else:
#         color = 17
#
#     zero, ofs, ctrl = cs.gear_shape('{}_{}_section_CTRL'.format(side, name))
#
#     # set color
#     cmds.setAttr(ctrl + '.overrideEnabled', 1)
#     cmds.setAttr(ctrl + '.overrideColor', color)
#
#     # visibility attributes
#     attribute.add_headline(ctrl, 'show')
#     for attr, attr_list in kwargs.items():
#         # make sure item is a list
#         obj_type = type(attr_list)
#         if obj_type != 'list':
#             ValueError('Keyword values must be a list')
#         local_attr.force_visibility_attr(ctrl, attr, items=attr_list)
#
#     # function attributes
#     if function_attrs:
#         attribute.add_headline(ctrl, 'function')
#         for attr in function_attrs:
#             # create proxy attribute to attribute
#             attr_name = attr.split('.')[1]
#             local_attr.add_proxy(ctrl, attr, attr_name)
#
#     # constrain control to follow control
#     pos = cmds.xform(follow, t=True, ws=True, q=True)
#     cmds.xform(zero, t=pos, ws=True)
#     constraint.simple_constraint(follow, zero, snap=False, connect='t')
#
#     # set offset from follow control
#     cmds.setAttr(ofs + '.translate', default_pos[0], default_pos[1], default_pos[2])


# def dag_to_geo(dag, geo):
#     position_attr = attribute.add_generic_blend(dag, 'meshPosition')
#     geo_shape = cmds.listRelatives(geo, s=True)[0]
#
#     sel = om.MSelectionList()
#     sel.add(geo_shape)
#
#     plane = om.MFnMesh()
#     plane.setObject(sel.getDagPath(0))
#
#     foll = cmds.createNode('follicle', name=dag + '_FOLL')
#     foll_grp = cmds.listRelatives(foll, p=True)
#     foll_grp = cmds.rename(foll_grp, foll + '_GRP')
#
#     pos = cmds.xform(dag, t=True, ws=True, q=True)
#     point = om.MPoint(om.MVector(pos))
#
#     parU, parV, face = plane.getUVAtPoint(point, space=4)
#
#     cmds.connectAttr(foll + '.outTranslate', foll_grp + '.translate')
#     cmds.connectAttr(foll + '.outRotate', foll_grp + '.rotate')
#     cmds.connectAttr(geo_shape + '.outMesh', foll + '.inputMesh')
#     cmds.connectAttr(geo_shape + '.worldMatrix', foll + '.inputWorldMatrix')
#     cmds.setAttr(foll + '.parameterU', parU)
#     cmds.setAttr(position_attr, parV)
#     cmds.connectAttr(position_attr, foll + '.parameterV')
#
#     constraint.simple_constraint(foll_grp, dag, snap=False)
