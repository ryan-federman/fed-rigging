import maya.cmds as cmds
import custom_nodes as cn
reload(cn)


class Node:
    '''
    Allows for easy creation of nodes and creates an object in which you can
    easily set and connect attributes.
    On creation custom flags for custom nodes must be used with "custom_" before
    the flag name.
    For attributes like transform attributes that have both an input and output
    you must write "in" or "out" followed by the attribute in camelcase to signify
    whether you want to connect to this attribute or connect this attribute to
    another attribute. This also applies to all user created attributes on a node
    In order to connect attributes upon creation use a flag with the attribute
    name and have it equal the attribute you are connecting it to.
    For nodes that have custom attributes like blend vectors add "custom_" before the given attribute

    node (str): string name of node object
    inputs list[(str)]: list of inputs for a given node so the class knows to connect
                        to said attribute
    outputs list[(str)]: list of outputs for a given node so the class knows to connect
                         said attribute to another attribute
    suffix (str): preset suffix of a node
    '''
    node = None
    inputs = None
    outputs = None
    suffix = None

    def __init__(self, name, node_type, list_add=None, **kwargs):
        '''
        Args:
            name: base name of node
            node_type: type of node being created
            list_add: if a list is given add this object into a list
            kwargs: flags to either connect attributes of this object to another
                    object or custom creation flags for custom nodes
        '''
        if 'initialize' in kwargs:
            self.initialize_object(name, node_type)
        else:
            if 'suffix' not in kwargs or kwargs['suffix']:
                try:
                    del kwargs['suffix']
                except KeyError:
                    pass
                self.create(name, node_type, **kwargs)
                self.rename(new_name='{}_{}'.format(name, self.suffix))
            else:
                del kwargs['suffix']
                self.create(name, node_type, **kwargs)

        # if called add to a list
        if type(list_add) == list:
            self.add_to_list(list_add)

    def create(self, name, node_type, **kwargs):
        custom_kwargs = {}
        del_keys = []
        for key in kwargs:
            if 'custom' in key:
                new_key = key.split('custom_')[1]
                value = kwargs[key]
                custom_kwargs[new_key] = value
                del_keys.append(key)
        if custom_kwargs:
            self.node, self.inputs, self.outputs, self.suffix = nodes[node_type](name, **custom_kwargs)
            for key in del_keys:
                del kwargs[key]
        else:
            self.node, self.inputs, self.outputs, self.suffix = nodes[node_type](name)

        self.connect(**kwargs)

    def initialize_object(self, name, node_type):
        '''
        Used for not creating a node but for initializing it with this class's attributes
        Args:
            name (str): name of node
            node_type (str): type of node created
        '''
        self.node = name
        node, self.inputs, self.outputs, self.suffix = nodes[node_type](name + '_TEMP')
        self.update_new_attributes()
        cmds.delete(node)

    def connect(self, **kwargs):
        self.update_new_attributes()
        for attribute, value in kwargs.items():

            if attribute in self.inputs:
                value_type = type(value)
                attribute = self.convert_indexed_attribute(attribute)
                self.attribute_exists(attribute)
                if value_type is tuple:
                    cmds.setAttr('{}.{}'.format(self.node, attribute), value[0], value[1], value[2])
                elif value_type is float or value_type is int:
                    cmds.setAttr('{}.{}'.format(self.node, attribute), value)
                elif value_type is str or value_type is unicode:
                    cmds.connectAttr(value, '{}.{}'.format(self.node, attribute))

            elif attribute in self.outputs:
                attribute = self.convert_indexed_attribute(attribute)
                cmds.connectAttr('{}.{}'.format(self.node, attribute), value)

            else:
                raise ValueError('Attribute {} does not exist in object'.format(attribute))

    def convert_indexed_attribute(self, attribute):
        '''
        Take attributes that can't be written out as keys and convert them to
        their actual attribute names
        '''
        if '_' in attribute:
            if '_in' in attribute:
                new_attr = attribute.split('_in')[0]
            elif '_out' in attribute:
                new_attr = attribute.split('_out')[0]
            else:
                attr_list = attribute.split('_')
                new_attr = ''
                for i, attr in enumerate(attr_list):
                    try:
                        new_attr += '[{}]'.format(int(attr))
                    except ValueError:
                        if i == 0:
                            new_attr = attr
                        else:
                            new_attr += '.' + attr
            return new_attr
        else:
            return attribute

    def attribute_exists(self, attribute):
        '''
        Check to see if the given attribute as a key actually exists
        '''
        if not cmds.ls('{}.{}'.format(self.node, attribute)):
            raise ValueError('Attribute {} does not exist'.format(attribute))

    def update_new_attributes(self):
        created_attrs = cmds.listAttr(self.node, ud=True)
        if created_attrs:
            for attr in created_attrs:
                if attr + '_in' not in self.inputs:
                    self.inputs.append(attr + '_in')
                    self.outputs.append(attr + '_out')

    def rename(self, new_name=None, suffix=False):
        if new_name:
            if suffix:
                if suffix not in self.node:
                    new_name += '_' + self.suffix
                    new_name = cmds.rename(self.node, new_name)
            else:
                new_name = cmds.rename(self.node, new_name)
        else:
            new_name = self.node + '_' + self.suffix
            new_name = cmds.rename(self.node, new_name)

        self.node = new_name

    def add_to_list(self, node_list):
        '''
        If called add to a given list of nodes
        '''

        node_list.append(self.node)

# base attributes for nodes that have a transform
transform_inputs = ['translate_in', 'translateX_in', 'translateY_in', 'translateZ_in',
                    'rotate_in', 'rotateX_in', 'rotateY_in', 'rotateZ_in',
                    'scale_in', 'scaleX_in', 'scaleY_in', 'scaleZ_in',
                    'visibility']
transform_outputs = ['inverseMatrix', 'matrix', 'parentInverseMatrix_0', 'parentMatrix_0',
                     'worldInverseMatrix_0', 'worldMatrix_0', 'xformMatrix']
for each in transform_inputs:
    if '_in' in each:
        attr = each.split('in')[0] + 'out'
        transform_outputs.append(attr)


# functions to create individual nodes and return values for the class
def plusMinusAverage(name):
    node = cmds.createNode('plusMinusAverage', name=name)
    inputs = ['operation']
    outputs = ['output1D', 'output2D', 'output2Dx', 'output2Dy', 'output3D', 'output3Dx','output3Dy', 'output3Dz']
    suffix = 'PMA'

    for i in range(0, 100):
        inputs.append('input1D_{}'.format(str(i)))
        inputs.append('input2D_{}'.format(str(i)))
        inputs.append('input2D_{}_input2Dx'.format(str(i)))
        inputs.append('input2D_{}_input2Dy'.format(str(i)))
        inputs.append('input3D_{}'.format(str(i)))
        inputs.append('input3D_{}_input3Dx'.format(str(i)))
        inputs.append('input3D_{}_input3Dy'.format(str(i)))
        inputs.append('input3D_{}_input3Dz'.format(str(i)))

    return node, inputs, outputs, suffix


def joint(name):
    node = cmds.joint(name=name)
    inputs = ['drawLabel', 'type', 'radius',
              'preferredAngle', 'preferredAngleX', 'preferredAngleY', 'preferredAngleZ'] + transform_inputs
    outputs = transform_outputs
    suffix = 'BONE'

    return node, inputs, outputs, suffix


def transform(name):
    node = cmds.createNode('transform', name=name)
    inputs = transform_inputs
    outputs = transform_outputs
    suffix = 'GRP'

    return node, inputs, outputs, suffix


def fourByFourMatrix(name):
    node = cmds.createNode('fourByFourMatrix', name=name)
    inputs = ['in00', 'in01', 'in02', 'in03',
              'in10', 'in11', 'in12', 'in13',
              'in20', 'in21', 'in22', 'in23',
              'in30', 'in31', 'in32', 'in33']
    outputs = ['output']
    suffix = 'FBFM'

    return node, inputs, outputs, suffix


def multMatrix(name):
    node = cmds.createNode('multMatrix', name=name)
    inputs = []
    outputs = ['matrixSum']
    suffix = 'MLTM'
    for i in range(0, 100):
        inputs.append('matrixIn_{}'.format(str(i)))

    return node, inputs, outputs, suffix

def composeMatrix(name):
    node = cmds.createNode('composeMatrix', name=name)
    inputs = ['inputTranslate', 'inputTranslateX', 'inputTranslateY', 'inputTranslateZ',
              'inputRotate', 'inputRotateX', 'inputRotateY', 'inputRotateZ',
              'inputScale', 'inputScaleX', 'inputScaleY', 'inputScaleZ']
    outputs = ['outputMatrix']
    suffix = 'CPM'

    return node, inputs, outputs, suffix

def decomposeMatrix(name):
    node = cmds.createNode('decomposeMatrix', name=name)
    inputs = ['inputMatrix']
    outputs = ['outputQuat', 'outputQuatX', 'outputQuatY', 'outputQuatZ',
               'outputRotate', 'outputRotateX', 'outputRotateY', 'outputRotateZ',
               'outputScale', 'outputScaleX', 'outputScaleY', 'outputScaleZ',
               'outputShear', 'outputShearX', 'outputShearY', 'outputShearZ',
               'outputTranslate', 'outputTranslateX', 'outputTranslateY', 'outputTranslateZ']
    suffix = 'DCPM'

    return node, inputs, outputs, suffix

def choice(name):
    node = cmds.createNode('choice', name=name)
    inputs = ['selector']
    outputs = ['output']
    suffix = 'CHOICE'
    for i in range(0, 100):
        inputs.append('input_{}'.format(str(i)))

    return node, inputs, outputs, suffix


def vectorProduct(name):
    node = cmds.createNode('vectorProduct', name=name)
    inputs = ['operation', 'normalizeOutput', 'matrix',
              'input1', 'input1X', 'input1Y', 'input1Z',
              'input2', 'input2X', 'input2Y', 'input2Z']
    outputs = ['output', 'outputX', 'outputY', 'outputZ']
    suffix = 'VECP'

    return node, inputs, outputs, suffix


def multiplyDivide(name):
    node = cmds.createNode('multiplyDivide', name=name)
    inputs = ['operation',
              'input1', 'input1X', 'input1Y', 'input1Z',
              'input2', 'input2X', 'input2Y', 'input2Z']
    outputs = ['output', 'outputX', 'outputY', 'outputZ']
    suffix = 'MULT'

    return node, inputs, outputs, suffix

def multDoubleLinear(name):
    node = cmds.createNode('multDoubleLinear', name=name)
    inputs = ['input1',
              'input2']
    outputs = ['output']
    suffix = 'MDL'

    return node, inputs, outputs, suffix

def addDoubleLinear(name):
    node = cmds.createNode('addDoubleLinear', name=name)
    inputs = ['input1',
              'input2']
    outputs = ['output']
    suffix = 'ADL'

    return node, inputs, outputs, suffix


def condition(name):
    node = cmds.createNode('condition', name=name)
    inputs = ['operation', 'firstTerm', 'secondTerm',
              'colorIfTrue', 'colorIfTrueR', 'colorIfTrueG', 'colorIfTrueB',
              'colorIfFalse', 'colorIfFalseR', 'colorIfFalseG', 'colorIfFalseB']
    outputs = ['outColor', 'outColorR', 'outColorG', 'outColorB']
    suffix = 'COND'

    return node, inputs, outputs, suffix


def pairBlend(name):
    node = cmds.createNode('pairBlend', name=name)
    inputs = ['weight', 'inTranslate1', 'inTranslate2', 'inRotate1', 'inRotate2',
              'inTranslateX1', 'inTranslateY1', 'inTranslateZ1',
              'inTranslateX2', 'inTranslateY2', 'inTranslateZ2',
              'inRotateX1', 'inRotateY1', 'inRotateZ1',
              'inRotateX2', 'inRotateY2', 'inRotateZ2']
    outputs = ['outRotate', 'outRotateX', 'outRotateY', 'outRotateZ',
               'outTranslate', 'outTranslateX', 'outTranslateY', 'outTranslateZ']
    suffix = 'PBLND'

    return node, inputs, outputs, suffix


def reverse(name):
    node = cmds.createNode('reverse', name=name)
    inputs = ['input', 'inputX', 'inputY', 'inputZ']
    outputs = ['output', 'outputX', 'outputY', 'outputZ']
    suffix = 'REV'

    return node, inputs, outputs, suffix


def distanceBetween(name):
    node = cmds.createNode('distanceBetween', name=name)
    inputs = ['inMatrix1', 'inMatrix2', 'point1', 'point2',
              'point1X', 'point1Y', 'point1Z',
              'point2X', 'point2Y', 'point2Z']
    outputs = ['distance']
    suffix = 'DIST'

    return node, inputs, outputs, suffix


def blendColors(name):
    node = cmds.createNode('blendColors', name=name)
    inputs = ['blender', 'color1', 'color2',
              'color1R', 'color1G', 'color1B',
              'color2R', 'color2G', 'color2B']
    outputs = ['output', 'outputR', 'outputG', 'outputB']
    suffix = 'BC'

    return node, inputs, outputs, suffix


def pointOnCurveInfo(name):
    node = cmds.createNode('pointOnCurveInfo', name=name)
    inputs = ['inputCurve', 'parameter']
    outputs = ['position', 'positionX', 'positionY', 'positionZ',
               'normal', 'normalX', 'normalY', 'normalZ',
               'normalizedNormal', 'normalizedNormalX', 'normalizedNormalY', 'normalizedNormalZ',
               'tangent', 'tangentX', 'tangentY', 'tangentZ',
               'normalizedTangent', 'normalizedTangentX', 'normalizedTangentY', 'normalizedTangentZ',
               'curvatureCenter', 'curvatureCenterX', 'curvatureCenterY', 'curvatureCenterZ',
               'curvatureRadius']
    suffix = 'POCI'

    return node, inputs, outputs, suffix

def curveInfo(name):
    node = cmds.createNode('curveInfo', name=name)
    inputs = ['inputCurve']
    outputs = ['arcLength']
    suffix = 'CI'

    for i in range(0, 100):
        outputs.append('controlPoints{}'.format(str(i)))

    return node, inputs, outputs, suffix


def pointOnSurfaceInfo(name):
    node = cmds.createNode('pointOnSurfaceInfo', name=name)
    inputs = ['inputSurface', 'parameterU', 'parameterV']
    outputs = ['position', 'positionX', 'positionY', 'positionZ',
               'normal', 'normalX', 'normalY', 'normalZ',
               'normalizedNormal', 'normalizedNormalX', 'normalizedNormalY', 'normalizedNormalZ',
               'tangentU', 'tangentUx', 'tangentUy', 'tangentUz',
               'normalizedTangentU', 'normalizedTangentUX', 'normalizedTangentUY', 'normalizedTangentUZ',
               'tangentV', 'tangentVx', 'tangentVy', 'tangentVz',
               'normalizedTangentV', 'normalizedTangentVX', 'normalizedTangentVY', 'normalizedTangentVZ'
               ]
    suffix = 'POSI'

    return node, inputs, outputs, suffix


def closestPointOnMesh(name):
    node = cmds.createNode('closestPointOnMesh', name=name)
    inputs = ['inPosition', 'inPositionX', 'inPositionY', 'inPositionZ',
              'inMesh', 'inputMatrix']
    outputs = ['position', 'positionX', 'positionY', 'positionZ',
               'normal', 'normalX', 'normalY', 'normalZ',
               'closestFaceIndex', 'closestVertexIndex', 'parameterU', 'parameterV']
    suffix = 'CPOM'

    return node, inputs, outputs, suffix


def matrixConstraint(name):
    node = cn.matrix_constraint(name=name)
    inputs = ['sourceMatrix', 'parentInverseMatrix',
              'xVector', 'xVectorX', 'xVectorY', 'xVectorZ',
              'yVector', 'yVectorX', 'yVectorY', 'yVectorZ',
              'zVector', 'zVectorX', 'zVectorY', 'zVectorZ',
              'posVector', 'posVectorX', 'posVectorY', 'posVectorZ']
    outputs = ['outTranslate', 'outRotate', 'outScale', 'outMatrix']
    suffix = 'MCON'

    return node, inputs, outputs, suffix


def blendVectors(name, num_vecs=2):
    node = cn.blend_vectors(name=name, num_vecs=num_vecs)
    inputs = ['blend']
    outputs = ['output', 'outputR', 'outputG', 'outputB']

    for i in range(num_vecs + 1):
        new_inputs = ['input' + str(i),
                      'input' + str(i) + 'R',
                      'input' + str(i) + 'G',
                      'input' + str(i) + 'B']
        inputs += new_inputs

    suffix = 'BVEC'

    return node, inputs, outputs, suffix


def matrixBlend(name, num_matrices=2):
    node = cn.blend_matrices(name, num_matrices=num_matrices)
    inputs = ['blend']
    outputs = ['output']

    for i in range(num_matrices + 1):
        new_inputs = ['input' + str(i)]
        inputs += new_inputs

    suffix = 'BMTX'

    return node, inputs, outputs, suffix


nodes = {'plusMinusAverage': plusMinusAverage,
         'joint': joint,
         'transform': transform,
         'fourByFourMatrix': fourByFourMatrix,
         'multMatrix': multMatrix,
         'decomposeMatrix': decomposeMatrix,
         'composeMatrix': composeMatrix,
         'choice': choice,
         'vectorProduct': vectorProduct,
         'multiplyDivide': multiplyDivide,
         'multDoubleLinear': multDoubleLinear,
         'addDoubleLinear': addDoubleLinear,
         'condition': condition,
         'pairBlend': pairBlend,
         'reverse': reverse,
         'distanceBetween': distanceBetween,
         'blendColors': blendColors,
         'pointOnCurveInfo': pointOnCurveInfo,
         'curveInfo': curveInfo,
         'pointOnSurfaceInfo': pointOnSurfaceInfo,
         'closestPointOnMesh': closestPointOnMesh,
         'matrixConstraint': matrixConstraint,
         'blendVectors': blendVectors,
         'matrixBlend': matrixBlend}
