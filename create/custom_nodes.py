import maya.cmds as cmds

import custom_utils.attribute as attr


def matrix_constraint(name='default_MCON'):
    """
    Args:
        name (str) = name of asset node
        maintain_offset (bool): maintains the offset between the objects
    Returns:
        str: name of new asset node
    """
    asset = cmds.container(name=name)
    cmds.addAttr(asset, at='matrix', sn='sourceMatrix', keyable=True)
    cmds.addAttr(asset, at='matrix', sn='parentInverseMatrix', keyable=True)
    cmds.container(asset, edit=True, publishName='outTranslate')
    cmds.container(asset, edit=True, publishName='outRotate')
    cmds.container(asset, edit=True, publishName='outScale')
    cmds.container(asset, edit=True, publishName='outMatrix')

    mlt_mtx = cmds.createNode('multMatrix', name=asset + '_MLTM')
    dm = cmds.createNode('decomposeMatrix', name=asset + '_DM')

    # create attributes for inputting vectors for offset
    cmds.container(asset, edit=True, publishName='xVector')
    cmds.container(asset, edit=True, publishName='yVector')
    cmds.container(asset, edit=True, publishName='zVector')
    cmds.container(asset, edit=True, publishName='posVector')

    # create the vectors of the target matrix
    x_vp = cmds.createNode('vectorProduct', name=asset + '_xvec_VP')
    y_vp = cmds.createNode('vectorProduct', name=asset + '_yvec_VP')
    z_vp = cmds.createNode('vectorProduct', name=asset + '_zvec_VP')
    pos_vp = cmds.createNode('vectorProduct', name=asset + '_posvec_VP')

    x_pma = cmds.createNode('plusMinusAverage', name=asset + '_xvec_PMA')
    y_pma = cmds.createNode('plusMinusAverage', name=asset + '_yvec_PMA')
    z_pma = cmds.createNode('plusMinusAverage', name=asset + '_zvec_PMA')
    mtx = cmds.createNode('fourByFourMatrix', name=asset + '_MTX')

    nodes = [x_vp, y_vp, z_vp, pos_vp, x_pma, y_pma, z_pma, mtx, mlt_mtx, dm]
    cmds.container(asset, edit=True, addNode=nodes)

    cmds.container(asset, edit=True, bindAttr=[x_vp + '.input1', 'xVector'])
    cmds.connectAttr(asset + '.sourceMatrix', x_vp + '.matrix')
    cmds.setAttr(x_vp + '.operation', 4)

    cmds.container(asset, edit=True, bindAttr=[y_vp + '.input1', 'yVector'])
    cmds.connectAttr(asset + '.sourceMatrix', y_vp + '.matrix')
    cmds.setAttr(y_vp + '.operation', 4)

    cmds.container(asset, edit=True, bindAttr=[z_vp + '.input1', 'zVector'])
    cmds.connectAttr(asset + '.sourceMatrix', z_vp + '.matrix')
    cmds.setAttr(z_vp + '.operation', 4)

    cmds.container(asset, edit=True, bindAttr=[pos_vp + '.input1', 'posVector'])
    cmds.connectAttr(asset + '.sourceMatrix', pos_vp + '.matrix')
    cmds.setAttr(pos_vp + '.operation', 4)

    cmds.setAttr(x_pma + '.operation', 2)
    cmds.connectAttr(x_vp + '.output', x_pma + '.input3D[0]')
    cmds.connectAttr(pos_vp + '.output', x_pma + '.input3D[1]')

    cmds.setAttr(y_pma + '.operation', 2)
    cmds.connectAttr(y_vp + '.output', y_pma + '.input3D[0]')
    cmds.connectAttr(pos_vp + '.output', y_pma + '.input3D[1]')

    cmds.setAttr(z_pma + '.operation', 2)
    cmds.connectAttr(z_vp + '.output', z_pma + '.input3D[0]')
    cmds.connectAttr(pos_vp + '.output', z_pma + '.input3D[1]')

    cmds.connectAttr(x_pma + '.output3Dx', mtx + '.in00')
    cmds.connectAttr(x_pma + '.output3Dy', mtx + '.in01')
    cmds.connectAttr(x_pma + '.output3Dz', mtx + '.in02')
    cmds.connectAttr(y_pma + '.output3Dx', mtx + '.in10')
    cmds.connectAttr(y_pma + '.output3Dy', mtx + '.in11')
    cmds.connectAttr(y_pma + '.output3Dz', mtx + '.in12')
    cmds.connectAttr(z_pma + '.output3Dx', mtx + '.in20')
    cmds.connectAttr(z_pma + '.output3Dy', mtx + '.in21')
    cmds.connectAttr(z_pma + '.output3Dz', mtx + '.in22')
    cmds.connectAttr(pos_vp + '.outputX', mtx + '.in30')
    cmds.connectAttr(pos_vp + '.outputY', mtx + '.in31')
    cmds.connectAttr(pos_vp + '.outputZ', mtx + '.in32')

    # connect matrix to target
    cmds.connectAttr(mtx + '.output', mlt_mtx + '.matrixIn[0]')
    cmds.connectAttr(asset + '.parentInverseMatrix', mlt_mtx + '.matrixIn[1]')

    cmds.setAttr(asset + '.xVector', 1, 0, 0)
    cmds.setAttr(asset + '.yVector', 0, 1, 0)
    cmds.setAttr(asset + '.zVector', 0, 0, 1)

    cmds.connectAttr(mlt_mtx + '.matrixSum', dm + '.inputMatrix')
    cmds.container(asset, edit=True, bindAttr=[dm + '.outputTranslate', 'outTranslate'])
    cmds.container(asset, edit=True, bindAttr=[dm + '.outputRotate', 'outRotate'])
    cmds.container(asset, edit=True, bindAttr=[dm + '.outputScale', 'outScale'])
    cmds.container(asset, edit=True, bindAttr=[mlt_mtx + '.matrixSum', 'outMatrix'])

    return asset


def blend_vectors(name='blendVectors', num_vecs=2):
    node = cmds.createNode('network', name=name)
    attr.add_float(node, 'blend', max=num_vecs - 1, default=0.0)
    attr.add_vector(node, 'input1')
    attr.add_vector(node, 'input2')
    attr.add_vector(node, 'output', visible=False, keyable=False)

    for i in range(num_vecs - 1):
        blend = cmds.createNode('blendColors', name='{}_{}_BVEC_BLEND'.format(name, str(i)))
        remap = cmds.createNode('remapValue', name='{}_{}_BVEC_RMP'.format(name, str(i)))

        cmds.connectAttr(node + '.blend', remap + '.inputValue')
        cmds.setAttr(remap + '.inputMin', 0 + i)
        cmds.setAttr(remap + '.inputMax', 1 + i)
        cmds.setAttr(remap + '.outputMin', 1)
        cmds.setAttr(remap + '.outputMax', 0)

        cmds.connectAttr(remap + '.outValue', blend + '.blender')
        if i == 0:
            cmds.connectAttr(node + '.input1', blend + '.color1')
            cmds.connectAttr(node + '.input2', blend + '.color2')
        else:
            attr.add_vector(node, 'input' + str(i + 2))
            cmds.connectAttr(prev_blend + '.output', blend + '.color1')
            cmds.connectAttr(node + '.input' + str(i + 2), blend + '.color2')
        prev_blend = blend

    cmds.connectAttr(prev_blend + '.output', node + '.output')

    return node


def blend_matrices(name='blendMatrices', num_matrices=2):
    asset = cmds.container(name=name)
    attr.add_float(asset, 'blend', max=num_matrices - 1, default=0.0)
    cmds.container(asset, edit=True, publishName='output')

    blend_x = blend_vectors(name + '_xVec_BMTX_BVEC', num_vecs=num_matrices)
    blend_y = blend_vectors(name + '_yVec_BMTX_BVEC', num_vecs=num_matrices)
    blend_z = blend_vectors(name + '_zVec_BMTX_BVEC', num_vecs=num_matrices)
    blend_pos = blend_vectors(name + '_posVec_BMTX_BVEC', num_vecs=num_matrices)
    fbfm = cmds.createNode('fourByFourMatrix', name=name + '_BMTX_FBFM')
    cmds.container(asset, edit=True, addNode=[blend_x, blend_y, blend_z, blend_pos, fbfm])

    cmds.connectAttr(asset + '.blend', blend_x + '.blend')
    cmds.connectAttr(asset + '.blend', blend_y + '.blend')
    cmds.connectAttr(asset + '.blend', blend_z + '.blend')
    cmds.connectAttr(asset + '.blend', blend_pos + '.blend')

    cmds.connectAttr(blend_x + '.outputR', fbfm + '.in00')
    cmds.connectAttr(blend_x + '.outputG', fbfm + '.in01')
    cmds.connectAttr(blend_x + '.outputB', fbfm + '.in02')
    cmds.connectAttr(blend_y + '.outputR', fbfm + '.in10')
    cmds.connectAttr(blend_y + '.outputG', fbfm + '.in11')
    cmds.connectAttr(blend_y + '.outputB', fbfm + '.in12')
    cmds.connectAttr(blend_z + '.outputR', fbfm + '.in20')
    cmds.connectAttr(blend_z + '.outputG', fbfm + '.in21')
    cmds.connectAttr(blend_z + '.outputB', fbfm + '.in22')
    cmds.connectAttr(blend_pos + '.outputR', fbfm + '.in30')
    cmds.connectAttr(blend_pos + '.outputG', fbfm + '.in31')
    cmds.connectAttr(blend_pos + '.outputB', fbfm + '.in32')

    cmds.container(asset, edit=True, bindAttr=[fbfm + '.output', 'output'])

    for i in range(num_matrices):
        cmds.addAttr(asset, shortName='input' + str(i + 1), attributeType='matrix')
        x_vec = cmds.createNode('vectorProduct', name='{}_{}_xVec_BMTX_VECP'.format(name, str(i+1)))
        y_vec = cmds.createNode('vectorProduct', name='{}_{}_yVec_BMTX_VECP'.format(name, str(i+1)))
        z_vec = cmds.createNode('vectorProduct', name='{}_{}_zVec_BMTX_VECP'.format(name, str(i+1)))
        x_pma = cmds.createNode('plusMinusAverage', name='{}_{}_xVec_BMTX_PMA'.format(name, str(i+1)))
        y_pma = cmds.createNode('plusMinusAverage', name='{}_{}_yVec_BMTX_PMA'.format(name, str(i+1)))
        z_pma = cmds.createNode('plusMinusAverage', name='{}_{}_zVec_BMTX_PMA'.format(name, str(i+1)))
        pos_vec = cmds.createNode('pointMatrixMult', name='{}_{}_posVec_BMTX_PMM'.format(name, str(i+1)))
        cmds.container(asset, edit=True, addNode=[x_vec, y_vec, z_vec, pos_vec])

        cmds.connectAttr(asset + '.input' + str(i + 1), x_vec + '.matrix')
        cmds.connectAttr(asset + '.input' + str(i + 1), y_vec + '.matrix')
        cmds.connectAttr(asset + '.input' + str(i + 1), z_vec + '.matrix')
        cmds.connectAttr(asset + '.input' + str(i + 1), pos_vec + '.inMatrix')

        for vec in [x_vec, y_vec, z_vec]:
            cmds.setAttr(vec + '.operation', 4)
            cmds.setAttr(vec + '.normalizeOutput', 1)

        for pma in [x_pma, y_pma, z_pma]:
            cmds.setAttr(pma + '.operation', 2)

        cmds.setAttr(x_vec + '.input1', 1, 0, 0)
        cmds.setAttr(y_vec + '.input1', 0, 1, 0)
        cmds.setAttr(z_vec + '.input1', 0, 0, 1)
        cmds.setAttr(pos_vec + '.inPoint', 0, 0, 0)

        cmds.connectAttr(x_vec + '.output', x_pma + '.input3D[0]')
        cmds.connectAttr(pos_vec + '.output', x_pma + '.input3D[1]')
        cmds.connectAttr(y_vec + '.output', y_pma + '.input3D[0]')
        cmds.connectAttr(pos_vec + '.output', y_pma + '.input3D[1]')
        cmds.connectAttr(z_vec + '.output', z_pma + '.input3D[0]')
        cmds.connectAttr(pos_vec + '.output', z_pma + '.input3D[1]')

        cmds.connectAttr(x_pma + '.output3D', blend_x + '.input' + str(i + 1))
        cmds.connectAttr(y_pma + '.output3D', blend_y + '.input' + str(i + 1))
        cmds.connectAttr(z_pma + '.output3D', blend_z + '.input' + str(i + 1))
        cmds.connectAttr(pos_vec + '.output', blend_pos + '.input' + str(i + 1))

    return asset


def curve_matrix():
    '''
    Matrix that returns the position and rotation along a curve
    '''
    pass
